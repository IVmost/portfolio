package simulation;

import java.util.Random;

public class GridSpace {
	public GridSpaceType spaceType;
	public int spaceValue;
	public int pheromoneConcentration;
	public int nestPheromone;
	public int crumbTrail;
	public boolean crumbsAdded;
	public int crumbDecayCounter;
	public int x;
	public int y;
	private GridSpace up;
	private GridSpace down;
	private GridSpace left;
	private GridSpace right;

	public GridSpace(int x, int y) {
		this.spaceType = GridSpaceType.OpenSpace;
		this.spaceValue = 0;
		this.pheromoneConcentration = 0;
		this.crumbTrail = 0;
		this.x = x;
		this.y = y;
		this.nestPheromone=0;
		this.crumbsAdded=true;
		this.crumbDecayCounter=0;
	}

	public void setAdjacent(GridSpace up, GridSpace down, GridSpace left,
			GridSpace right) {
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
	}

	public int StartGrow(int gridStart, int obstacleId, int currentDepth,
			int desiredDepth) {
		int gridLeft = gridStart;
		if (currentDepth == desiredDepth) {
			gridLeft = growObstacle(gridLeft, obstacleId);
		} else {
			if (up != null && gridLeft > 0)
				gridLeft = up.StartGrow(gridLeft, obstacleId, currentDepth + 1,
						desiredDepth);
			if (down != null && gridLeft > 0)
				gridLeft = down.StartGrow(gridLeft, obstacleId,
						currentDepth + 1, desiredDepth);
			if (left != null && gridLeft > 0)
				gridLeft = left.StartGrow(gridLeft, obstacleId,
						currentDepth + 1, desiredDepth);
			if (right != null && gridLeft > 0)
				gridLeft = right.StartGrow(gridLeft, obstacleId,
						currentDepth + 1, desiredDepth);
		}

		if (currentDepth == 1 && gridLeft > 0) {
			while (gridLeft > 0) {
				desiredDepth++;
				if (up != null && gridLeft > 0)
					gridLeft = up.StartGrow(gridLeft, obstacleId,
							currentDepth + 1, desiredDepth);
				if (down != null && gridLeft > 0)
					gridLeft = down.StartGrow(gridLeft, obstacleId,
							currentDepth + 1, desiredDepth);
				if (left != null && gridLeft > 0)
					gridLeft = left.StartGrow(gridLeft, obstacleId,
							currentDepth + 1, desiredDepth);
				if (right != null && gridLeft > 0)
					gridLeft = right.StartGrow(gridLeft, obstacleId,
							currentDepth + 1, desiredDepth);
			}

		}
		return gridLeft = 0;
	}

	public boolean checkValid(int obstacleId) {
		if (spaceType == GridSpaceType.Obstacle)
			return false;
		if (up != null)
			if (!up.checkChild(obstacleId))
				return false;
		if (down != null)
			if (!down.checkChild(obstacleId))
				return false;
		if (left != null)
			if (!left.checkChild(obstacleId))
				return false;
		if (right != null)
			if (!right.checkChild(obstacleId))
				return false;
		return true;
	}

	public int growObstacle(int gridLeft, int obstacleId) {
		if (spaceType == GridSpaceType.OpenSpace) {
			spaceType = GridSpaceType.Obstacle;
			spaceValue = obstacleId;
			gridLeft--;
		}
		if (up != null && gridLeft > 0)
			if (up.spaceType == GridSpaceType.OpenSpace
					&& up.checkChild(obstacleId)) {
				up.spaceType = GridSpaceType.Obstacle;
				up.spaceValue = obstacleId;
				gridLeft--;
			}
		if (down != null && gridLeft > 0)
			if (down.spaceType == GridSpaceType.OpenSpace
					&& down.checkChild(obstacleId)) {
				down.spaceType = GridSpaceType.Obstacle;
				down.spaceValue = obstacleId;
				gridLeft--;
			}
		if (left != null && gridLeft > 0)
			if (left.spaceType == GridSpaceType.OpenSpace
					&& left.checkChild(obstacleId)) {
				left.spaceType = GridSpaceType.Obstacle;
				left.spaceValue = obstacleId;
				gridLeft--;
			}
		if (right != null && gridLeft > 0)
			if (right.spaceType == GridSpaceType.OpenSpace
					&& right.checkChild(obstacleId)) {
				right.spaceType = GridSpaceType.Obstacle;
				right.spaceValue = obstacleId;
				gridLeft--;
			}
		return gridLeft;
	}

	public boolean checkChild(int obstacleId) {
		if (up != null)
			if (up.spaceType != GridSpaceType.OpenSpace
					&& up.spaceValue != obstacleId)
				return false;
		if (down != null)
			if (down.spaceType != GridSpaceType.OpenSpace
					&& down.spaceValue != obstacleId)
				return false;
		if (left != null)
			if (left.spaceType != GridSpaceType.OpenSpace
					&& left.spaceValue != obstacleId)
				return false;
		if (right != null)
			if (right.spaceType != GridSpaceType.OpenSpace
					&& right.spaceValue != obstacleId)
				return false;

		return true;

	}

	public GridSpace getLowestPheromoneAdjacent(GridSpace previousLocation) {
		int highestConcentration=-1;
		GridSpace tempSpace = null;
		if (crumbTrail > 0) {
			tempSpace = FollowCrumbTrail(previousLocation);
		} else {
			if (up != null) {
				if ((up.spaceType == GridSpaceType.FoodNode&& up!= previousLocation) || up.crumbTrail > 0)
					return up;
				highestConcentration = up.pheromoneConcentration;
				tempSpace = up;

			}
			if (down != null) {
				if (down.spaceType == GridSpaceType.FoodNode
						|| down.crumbTrail > 0)
					return down;
				if ((down.pheromoneConcentration > highestConcentration&& down != previousLocation)
						|| highestConcentration == -1) {
					highestConcentration = down.pheromoneConcentration;
					tempSpace = down;
				}
			}
			if (left != null) {
				if (left.spaceType == GridSpaceType.FoodNode
						|| left.crumbTrail > 0)
					return left;
				if ((left.pheromoneConcentration > highestConcentration&& left!= previousLocation)
						|| highestConcentration == -1) {
					highestConcentration = left.pheromoneConcentration;
					tempSpace = left;
				}
			}
			if (right != null) {
				if (right.spaceType == GridSpaceType.FoodNode
						|| right.crumbTrail > 0)
					return right;
				if ((right.pheromoneConcentration > highestConcentration && right != previousLocation)
						|| highestConcentration == -1) {
					highestConcentration = right.pheromoneConcentration;
					tempSpace = right;
				}
			}
			
		}
		return tempSpace;
	}

	private GridSpace FollowCrumbTrail(GridSpace previousLocation) {
		int highestCrumbTrail = -1;
		GridSpace tempSpace = this;
		
		if (up != null) {
			if (up.spaceType== GridSpaceType.Nest && up!=previousLocation && previousLocation.crumbTrail>0)
				return previousLocation;
			if (up.spaceType == GridSpaceType.FoodNode)
				return up;
			if (highestCrumbTrail <= up.crumbTrail && up!=previousLocation) {
				highestCrumbTrail = up.crumbTrail;
				tempSpace = up;
			}
		}
		if (down != null) {
			if (down.spaceType== GridSpaceType.Nest&& down!=previousLocation&& previousLocation.crumbTrail>0)
				return previousLocation;
			if (down.spaceType == GridSpaceType.FoodNode)
				return down;
			if (highestCrumbTrail <= down.crumbTrail && down!=previousLocation) {
				highestCrumbTrail = down.crumbTrail;
				tempSpace = down;
			}
		}
		if (left != null) {
			if (left.spaceType== GridSpaceType.Nest&& left!=previousLocation&& previousLocation.crumbTrail>0)
				return previousLocation;
			if (left.spaceType == GridSpaceType.FoodNode)
				return left;
			if (highestCrumbTrail <= left.crumbTrail && left != previousLocation) {
				highestCrumbTrail = left.crumbTrail;
				tempSpace = left;
			}

		}
		if (right != null) {
			if (right.spaceType== GridSpaceType.Nest&& right!=previousLocation&& previousLocation.crumbTrail>0)
				return previousLocation;
			if (right.spaceType == GridSpaceType.FoodNode )
				return right;
			if (highestCrumbTrail <= right.crumbTrail&& right != previousLocation) {
				highestCrumbTrail = right.crumbTrail;
				tempSpace = right;
			}
		}
		if (highestCrumbTrail == -1 && previousLocation !=this){
			tempSpace=FollowCrumbTrail(this);
			highestCrumbTrail=tempSpace.crumbTrail;
		}
		if (highestCrumbTrail == 0) {
				spaceType = GridSpaceType.OpenSpace;
				spaceValue = 0;
				crumbTrail = 0;
				tempSpace = this;
			
		}

		return tempSpace;

	}

	public void takeFood() {
		int foodTaken = 0;
		
		spaceValue--;
		
		if (spaceValue == 0)
			spaceType = GridSpaceType.OpenSpace;
	
	}

	public GridSpace getHighestPheromoneAdjacent(GridSpace previousLocation) {
		int highestConcentration = -1;
		GridSpace tempSpace = null;
		if (up != null) {
			if (up.spaceType == GridSpaceType.Nest)
				return up;
			if (highestConcentration <= up.nestPheromone
					&& up != previousLocation && up.spaceType != GridSpaceType.Obstacle) {
				highestConcentration = up.nestPheromone;
				tempSpace = up;
			}

		}
		if (down != null) {
			if (down.spaceType == GridSpaceType.Nest)
				return down;
			if (highestConcentration <= down.nestPheromone
					&& down != previousLocation && down.spaceType != GridSpaceType.Obstacle) {
				highestConcentration = down.nestPheromone;
				tempSpace = down;
			}
		}
		if (left != null) {
			if (left.spaceType == GridSpaceType.Nest)
				return left;
			if (highestConcentration <= left.nestPheromone
					&& left != previousLocation && left.spaceType!= GridSpaceType.Obstacle) {
				highestConcentration = left.nestPheromone;
				tempSpace = left;
			}

		}
		if (right != null) {
			if (right.spaceType == GridSpaceType.Nest)
				return left;
			if (highestConcentration <= right.nestPheromone
					&& right != previousLocation && right.spaceType !=GridSpaceType.Obstacle) {
				highestConcentration = right.nestPheromone;
				tempSpace = right;
			}
		}

		return tempSpace;
	}

	public GridSpace getRandomAdjacent(GridSpace previousLocation) {
		GridSpace tempSpace = null;
		Random rand = new Random();
		if (up !=null)
			if (up.spaceType==GridSpaceType.FoodNode)
				return up;
		if (down !=null)
			if (down.spaceType==GridSpaceType.FoodNode)
				return down;
		if (left !=null)
			if (left.spaceType==GridSpaceType.FoodNode)
				return left;
		if (right !=null)
			if (right.spaceType==GridSpaceType.FoodNode)
				return right;
		if (up !=null)
			if (up.crumbTrail >0)
				return up;
		if (down !=null)
			if (down.crumbTrail >0)
				return down;
		if (left !=null)
			if (left.crumbTrail >0)
				return left;
		if (right !=null)
			if (right.crumbTrail >0)
				return right;
		
		if (rand.nextBoolean() && previousLocation!=null){
			if (up !=null && down !=null){
				if (up==previousLocation && down.spaceType!=GridSpaceType.Obstacle)
					return down;
				if (down==previousLocation && up.spaceType!=GridSpaceType.Obstacle)
					return up;
			}
			if (left !=null && right != null){
				if (left==previousLocation && right.spaceType!=GridSpaceType.Obstacle)
					return right;
				if (right==previousLocation && left.spaceType!=GridSpaceType.Obstacle)
					return left;
			}
		}
		int tempSelect;
		while (tempSpace == null) {
			tempSelect = rand.nextInt(4);
			switch (tempSelect) {
			case 0:
				tempSpace = up;
				break;
			case 1:
				tempSpace = down;
				break;
			case 2:
				tempSpace = left;
				break;
			case 3:
				tempSpace = right;
				break;
			}
			if (tempSpace != null)
				if (tempSpace.spaceType == GridSpaceType.Obstacle)
					tempSpace = null;

		}
		return tempSpace;
	}

	public void emitPheromones(int pheromoneStrength) {
		if (spaceType !=GridSpaceType.Obstacle)
			pheromoneConcentration = pheromoneStrength;
		else 
			pheromoneConcentration=0;
		
		if (pheromoneConcentration > 255)
			pheromoneConcentration = 255;

	}

	public void update(int pheromoneSpread, int pheromoneDecay) {
		int tempPheromoneSpread= (int) (pheromoneConcentration*(pheromoneSpread/100.0f));
		if (tempPheromoneSpread > 0) {
			
			pheromoneConcentration*=1.0f-pheromoneDecay/100.0f;
			if (up != null)
				if (up.pheromoneConcentration < tempPheromoneSpread)
					up.emitPheromones(tempPheromoneSpread);
			if (down != null)
				if (down.pheromoneConcentration <  tempPheromoneSpread)
					down.emitPheromones( tempPheromoneSpread);
			if (left != null)
				if (left.pheromoneConcentration <  tempPheromoneSpread)
					left.emitPheromones( tempPheromoneSpread);
			if (right != null)
				if (right.pheromoneConcentration <  tempPheromoneSpread)
					right.emitPheromones(tempPheromoneSpread);
			
		} 
		if (pheromoneConcentration < 2)
			pheromoneConcentration = 0;
		if (nestPheromone >0){
			if (up != null)
				if (up.nestPheromone < nestPheromone)
					up.emitNestPheromones((int)(nestPheromone*0.90f));
			if (down != null)
				if (down.nestPheromone < nestPheromone)
					down.emitNestPheromones((int)(nestPheromone*0.90f));
			if (left != null)
				if (left.nestPheromone < nestPheromone)
					left.emitNestPheromones((int)(nestPheromone*0.90f));
			if (right != null)
				if (right.nestPheromone < nestPheromone)
					right.emitNestPheromones((int)(nestPheromone*0.90f));

		}
			
		if (spaceType == GridSpaceType.Nest)
			nestPheromone += 100;
		if (crumbsAdded){
			crumbDecayCounter=0;
			crumbsAdded=false;
		}else{
			crumbDecayCounter++;
			if (crumbDecayCounter>100){
				crumbDecayCounter=0;
				crumbTrail--;
			}
				
				
		}
	}

	private void emitNestPheromones(int i) {
		if (spaceType!=GridSpaceType.Obstacle)
			nestPheromone=i;
		else
			nestPheromone=0;
		
	}

	public void dropCrumb() {
		crumbTrail++;
		crumbsAdded=true;

	}

	public void takeCrumb() {
		crumbTrail--;
		
	}

	public void removeCrumbs() {
		crumbTrail=0;
		
	}

}
