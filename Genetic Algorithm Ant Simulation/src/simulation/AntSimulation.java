package simulation;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.util.TimerTask;

public class AntSimulation{
	private int numOfAnts;
	private int antSpeed;
	

	private int pheromoneStrength;
	private int pheromoneSpread;
	private int pheromoneDecay;

	private int numberOfFoodNodes;
	private int foodPerNode;

	private int numberOfObstacles;
	private int sizeOfObstacles;

	private GridSpace[][] world;
	private GridSpace nest;
	private ArrayList<Ant> ants;
	
	
	
	public AntSimulation(int numOfAnts, int antSpeed,
			int pheromoneStrength, int pheromoneSpread, int pheromoneDecay,
			int numberOfFoodNodes, int foodPerNode, int numberOfObstacles,
			int sizeOfObstacles) {
		this.numOfAnts=numOfAnts;
		this.antSpeed=antSpeed;
		this.pheromoneStrength=pheromoneStrength;
		this.pheromoneSpread=pheromoneSpread;
		this.pheromoneDecay=pheromoneDecay;
		this.numberOfFoodNodes=numberOfFoodNodes;
		this.foodPerNode=foodPerNode;
		this.numberOfObstacles=numberOfObstacles;
		this.sizeOfObstacles=sizeOfObstacles;
		this.nest=null;
		this.world=new GridSpace[100][100];
		ants=new ArrayList<Ant>();
		initializeWorld();
	
		
	}
	private void initializeWorld() {
		for (int x=0; x < world.length; x++){
			for (int y=0; y < world[x].length;y++){
				world[x][y]=new GridSpace(x,y);
			}
		}
		
		for (int x=0; x< world.length;x++){
			for (int y=0;y<world[x].length;y++){
				GridSpace up=null;
				GridSpace down=null;
				GridSpace left=null;
				GridSpace right=null;
				if (y-1 >=0)
					up=world[x][y-1];
				if (y+1 != world[x].length)
					down=world[x][y+1];
				if (x-1 >=0)
					left=world[x-1][y];
				if (x+1 < world.length)
					right=world[x+1][y];
				world[x][y].setAdjacent(up, down, left, right);
			}
		}
		
		Random rand = new Random();
		if (numberOfObstacles > 0){
			for (int i=0; i< numberOfObstacles; i++){
				int randX= rand.nextInt(world.length);
				int randY= rand.nextInt(world[randX].length);
				while(!world[randX][randY].checkValid(i)){
					randX= rand.nextInt(world.length);
					randY= rand.nextInt(world[randX].length);
				}
				world[randX][randY].StartGrow(sizeOfObstacles, i, 1, 1);
			}
		}
		for (int i=0; i < numberOfFoodNodes;i++){
			int randX= rand.nextInt(world.length);
			int randY= rand.nextInt(world[randX].length);
			while (world[randX][randY].spaceType != GridSpaceType.OpenSpace){
				randX= rand.nextInt(world.length);
				randY= rand.nextInt(world[randX].length);
			}
			world[randX][randY].spaceType=GridSpaceType.FoodNode;
			world[randX][randY].spaceValue=foodPerNode;
		}
		while (nest==null){
			int randX= rand.nextInt(world.length);
			int randY= rand.nextInt(world[randX].length);
			if (world[randX][randY].spaceType==GridSpaceType.OpenSpace && world[randX][randY].checkValid(-1)){
				nest=world[randX][randY];
				nest.spaceType=GridSpaceType.Nest;
				nest.spaceValue=pheromoneStrength;
			}
		}
	}
	
	public void update(){
		if (ants.size() < numOfAnts){
			ants.add(new Ant(nest.getRandomAdjacent(null),pheromoneStrength));
		}
		for (Ant ant : ants){
			ant.AntUpdate();
		}
		
		for (int x=0; x< world.length;x++)
			for (int y=0; y<world[x].length;y++){
				world[x][y].update(pheromoneSpread,pheromoneDecay);
			}
	}
	public void updateGraphic(Canvas simulation){
		simulation.setBackground(null);
		
		simulation.repaint();
		//simulation.paint(g);
		
		
	}
	public GridSpace[][] getWorld() {
		
		return world;
	}
	public ArrayList<Ant> getAnts() {
		
		return ants;
	}
	public int getNestPheromone() {
		
		return nest.nestPheromone;
	}
	
	

}
