package simulation;

import java.util.HashMap;

public class Ant {
	private GridSpace currentLocation;
	private GridSpace previousLocation;
	private AntState currentState;
	private boolean carryingFood;
	private boolean destroyTrail;
	private int pheromoneStrength;
	private int stuckCounter;
	private HashMap<GridSpace,Integer> stuckTrap;
	Ant(GridSpace currentLocation,int pheromoneStrength){
		this.currentLocation=currentLocation;
		this.currentState=AntState.Exploration;
		this.carryingFood=false;
		this.pheromoneStrength=pheromoneStrength;
		this.previousLocation=currentLocation;
		this.stuckCounter=0;
		this.stuckTrap=new HashMap<GridSpace, Integer>();
		
	}
	
	public void AntUpdate(){
		GridSpace moveCandidate=currentLocation;
		if (currentLocation.spaceType==GridSpaceType.FoodNode && currentState!=AntState.Return)
		{
			currentState=AntState.Return;
			carryingFood=true;
			currentLocation.takeFood();
			previousLocation=currentLocation;
			if (currentLocation.spaceType==GridSpaceType.OpenSpace)
				destroyTrail=true;
			else
				destroyTrail=false;
		}
		
		
		if (currentState==AntState.Return){
			if (currentLocation.spaceType == GridSpaceType.Nest)
			{
				currentState=AntState.Exploration;
				carryingFood=false;
				destroyTrail=false;
			} else {
				moveCandidate=currentLocation.getHighestPheromoneAdjacent(previousLocation);
			}
		}
		if (currentState==AntState.Exploration){
			moveCandidate=currentLocation.getLowestPheromoneAdjacent(previousLocation);
			if (moveCandidate == null || moveCandidate.pheromoneConcentration==0 || moveCandidate.spaceType==GridSpaceType.Obstacle)
			{
				currentState=AntState.Random;
			}
		}
		if (currentState==AntState.Random){
			moveCandidate=currentLocation.getRandomAdjacent(previousLocation);
			destroyTrail=false;
			if (moveCandidate.crumbTrail >  0 || moveCandidate.spaceType==GridSpaceType.FoodNode || moveCandidate.pheromoneConcentration>0)
				currentState=AntState.Exploration;
		}
		
		if (carryingFood==true && currentLocation.spaceType==GridSpaceType.OpenSpace){
			if (destroyTrail)
				currentLocation.removeCrumbs();
			else
				currentLocation.dropCrumb();
		
		}
		if (carryingFood==true && !destroyTrail)
			currentLocation.emitPheromones(pheromoneStrength);
		previousLocation=currentLocation;
		if (!stuckTrap.containsKey(currentLocation))
		{
			stuckTrap.put(currentLocation, 1);
			if (stuckTrap.size()>5){
				stuckTrap.clear();
				stuckCounter=0;
			}
		} else {
			stuckCounter++;
			if (stuckCounter>10){
				currentState=AntState.Random;
				if (currentLocation.crumbTrail>0)
					currentLocation.removeCrumbs();
			}
				
		}
//		if (moveCandidate != currentLocation ){
//			previousLocation=currentLocation;
//			if (stuckTrap.size()>2){
//				stuckCounter=0;
//				stuckTrap.clear();
//			}
//		}
//		else{
//			stuckCounter++;
//			if (stuckCounter>10){
//				moveCandidate=previousLocation;
//				stuckCounter=0;
//			}
//				
//		}
			
		currentLocation=moveCandidate;
	}

	public GridSpace getLocation() {
		
		return currentLocation;
	}

	public boolean isCarrying() {
	
		return carryingFood;
	}
}
