package Interface;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import simulation.Ant;
import simulation.AntSimulation;
import simulation.GridSpace;
import simulation.GridSpaceType;

public class SimulationDrawing extends Canvas {

	private AntSimulation antSimulation;

	public SimulationDrawing() {
		antSimulation = null;
	}

	public void paint(Graphics g) {

		int width = getSize().width;
		int height = getSize().height;
		if (antSimulation != null) {
			Image backbuffer=createImage(width,height);
			Graphics bg=backbuffer.getGraphics();
			
			int nestPheromoneValue=antSimulation.getNestPheromone();
			GridSpace[][] world = antSimulation.getWorld();
			int widthStep = width / world.length;
			int heightStep = height / world[0].length;

			for (int x = 0; x < world.length; x++) {
				for (int y = 0; y < world[x].length; y++) {
					if (world[x][y].spaceType == GridSpaceType.Obstacle) {
						bg.setColor(Color.black);
					}
					if (world[x][y].spaceType == GridSpaceType.Nest) {
						bg.setColor(Color.blue);
					}
					if (world[x][y].spaceType == GridSpaceType.FoodNode) {
						bg.setColor(Color.green);
					}

					if (world[x][y].spaceType == GridSpaceType.OpenSpace) {
						if (world[x][y].pheromoneConcentration>0){
							if (world[x][y].pheromoneConcentration >= 255) {
								bg.setColor(new Color(255, 0, 0));
							} else {
								bg.setColor(new Color(255,
										255 - world[x][y].pheromoneConcentration,
										255 - world[x][y].pheromoneConcentration));
							}
						} else {
							float nestAdjust= (float) Math.min(1.0, ((float)world[x][y].nestPheromone)/2500.0f);
							bg.setColor(new Color(255,255- (int)(255* nestAdjust),255));
						
						}
						bg.fillRect(x * widthStep, y * heightStep, widthStep,
								heightStep);
					} else
						bg.fillRect(x * widthStep, y * heightStep, widthStep,
								heightStep);
					if (world[x][y].crumbTrail > 0) {
						bg.setColor(Color.cyan);
						int xHalf = x * widthStep + widthStep / 4;
						int yHalf = y * heightStep + heightStep / 4;
						bg.fillRect(xHalf, yHalf, widthStep / 2, heightStep / 2);
					}
				}
			}
			ArrayList<Ant> ants = antSimulation.getAnts();

			for (Ant ant : ants) {
				GridSpace currentLoc = ant.getLocation();
				if (ant.isCarrying())
					bg.setColor(Color.green);
				else
					bg.setColor(Color.gray);
				bg.fillOval(currentLoc.x * widthStep, currentLoc.y * heightStep,
						widthStep, heightStep);
			}
			g.drawImage(backbuffer,0,0,this);
		} else {
			g.setColor(Color.black);
			g.fillRect(0, 0, width, height);
		}
	}

	public void updateSimlation(AntSimulation antSimulation) {
		this.antSimulation = antSimulation;
	}

}
