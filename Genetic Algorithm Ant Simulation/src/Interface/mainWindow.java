package Interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.Canvas;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import simulation.AntSimulation;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

public class mainWindow extends JFrame {

	private JPanel contentPane;
	private JSlider antSlider;
	private JSlider pheromoneStrengthSlider;
	private JLabel lblAntSpeed;
	private JSlider antSpeedSlider;
	private JLabel lblDecayRate;
	private JSlider spreadSlider;
	private JLabel lblPheromoneStrength;
	private JLabel lblAnts;
	private JLabel lblNumberOfNodes;
	private JSlider foodNodeSlider;
	private JSlider decaySlider;
	private JLabel lblSpreadSpeed;
	private JLabel lblFoodPerNode;
	private JSlider foodPerNodeSlider;
	private JLabel lblNumberOfObstacles;
	private JSlider numObstacleSlider;
	private JLabel lblSize;
	private JSlider sizeSlider;
	private JButton btnReset;
	private JButton btnStop;
	private JButton btnStart;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;

	private int numOfAnts;
	private int antSpeed;


	private int pheromoneStrength;
	private int pheromoneSpread;
	private int pheromoneDecay;

	private int numberOfFoodNodes;
	private int foodPerNode;

	private int numberOfObstacles;
	private int sizeOfObstacles;

	private AntSimulation antSimulation;
	private boolean runProgram;
	private int testCounter;

	private Timer simulationTimer;	
	private ActionListener timerListener;
	private SimulationDrawing simulationSpace;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainWindow frame = new mainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public mainWindow() {
		timerListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				runSimulation();
				
			}
		};
		antSimulation = null;
		runProgram = false;
		simulationTimer=null;
		simulationTimer= new javax.swing.Timer(100, timerListener);
		simulationTimer.setDelay(100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 951, 803);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 270, 0, 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Simulation",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 7;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		simulationSpace = new SimulationDrawing();
		simulationSpace.setBackground(Color.BLACK);
		GridBagConstraints gbc_simulationSpace = new GridBagConstraints();
		gbc_simulationSpace.fill = GridBagConstraints.BOTH;
		gbc_simulationSpace.gridheight = 10;
		gbc_simulationSpace.gridwidth = 26;
		gbc_simulationSpace.insets = new Insets(0, 0, 5, 5);
		gbc_simulationSpace.gridx = 0;
		gbc_simulationSpace.gridy = 0;
		panel.add(simulationSpace, gbc_simulationSpace);

		panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Ants",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.insets = new Insets(0, 0, 5, 5);
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 1;
		contentPane.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 0, 0 };
		gbl_panel_4.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		lblAnts = new JLabel("Ants: 100");
		GridBagConstraints gbc_lblAnts = new GridBagConstraints();
		gbc_lblAnts.anchor = GridBagConstraints.WEST;
		gbc_lblAnts.insets = new Insets(0, 0, 5, 0);
		gbc_lblAnts.gridx = 0;
		gbc_lblAnts.gridy = 0;
		panel_4.add(lblAnts, gbc_lblAnts);

		antSlider = new JSlider();
		antSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				numOfAnts = antSlider.getValue();
				lblAnts.setText("Num: " + numOfAnts);
			}
		});

		GridBagConstraints gbc_antSlider = new GridBagConstraints();
		gbc_antSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_antSlider.insets = new Insets(0, 0, 5, 0);
		gbc_antSlider.gridx = 0;
		gbc_antSlider.gridy = 1;
		panel_4.add(antSlider, gbc_antSlider);
		antSlider.setValue(100);
		antSlider.setMajorTickSpacing(100);
		antSlider.setMinorTickSpacing(10);
		antSlider.setPaintTicks(true);
		antSlider.setMinimum(100);
		antSlider.setMaximum(1000);

		lblAntSpeed = new JLabel("Ant Moves Per Second: 1");
		GridBagConstraints gbc_lblAntSpeed = new GridBagConstraints();
		gbc_lblAntSpeed.anchor = GridBagConstraints.WEST;
		gbc_lblAntSpeed.insets = new Insets(0, 0, 5, 0);
		gbc_lblAntSpeed.gridx = 0;
		gbc_lblAntSpeed.gridy = 2;
		panel_4.add(lblAntSpeed, gbc_lblAntSpeed);

		antSpeedSlider = new JSlider();
		antSpeedSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				antSpeed = antSpeedSlider.getValue();
				lblAntSpeed.setText("Ant Moves Per Second: " + antSpeed);
				boolean timerRunning=simulationTimer.isRunning();
				if (timerRunning)
					simulationTimer.stop();
				
				simulationTimer.setDelay(1000/antSpeed);
				if (timerRunning)
					simulationTimer.start();
		}});
		antSpeedSlider.setSnapToTicks(true);
		antSpeedSlider.setPaintTicks(true);
		GridBagConstraints gbc_antSpeedSlider = new GridBagConstraints();
		gbc_antSpeedSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_antSpeedSlider.insets = new Insets(0, 0, 5, 0);
		gbc_antSpeedSlider.gridx = 0;
		gbc_antSpeedSlider.gridy = 3;
		panel_4.add(antSpeedSlider, gbc_antSpeedSlider);
		antSpeedSlider.setValue(1);
		antSpeedSlider.setMinimum(1);
		antSpeedSlider.setMaximum(30);
		antSpeedSlider.setMajorTickSpacing(1);

		panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Pheromone",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 1;
		contentPane.add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		lblPheromoneStrength = new JLabel("Strength: ");
		GridBagConstraints gbc_lblPheromoneStrength = new GridBagConstraints();
		gbc_lblPheromoneStrength.anchor = GridBagConstraints.WEST;
		gbc_lblPheromoneStrength.insets = new Insets(0, 0, 5, 0);
		gbc_lblPheromoneStrength.gridx = 0;
		gbc_lblPheromoneStrength.gridy = 0;
		panel_3.add(lblPheromoneStrength, gbc_lblPheromoneStrength);

		pheromoneStrengthSlider = new JSlider();
		pheromoneStrengthSlider.setValue(255);
		pheromoneStrengthSlider.setMaximum(255);
		pheromoneStrengthSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				pheromoneStrength = pheromoneStrengthSlider.getValue();
				lblPheromoneStrength.setText("Strength: " + pheromoneStrength);
			}
		});
		GridBagConstraints gbc_pheromoneStrengthSlider = new GridBagConstraints();
		gbc_pheromoneStrengthSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_pheromoneStrengthSlider.insets = new Insets(0, 0, 5, 0);
		gbc_pheromoneStrengthSlider.gridx = 0;
		gbc_pheromoneStrengthSlider.gridy = 1;
		panel_3.add(pheromoneStrengthSlider, gbc_pheromoneStrengthSlider);
		pheromoneStrengthSlider.setSnapToTicks(true);
		pheromoneStrengthSlider.setPaintTicks(true);
		pheromoneStrengthSlider.setMinorTickSpacing(1);
		pheromoneStrengthSlider.setMinimum(1);
		pheromoneStrengthSlider.setMajorTickSpacing(10);

		lblSpreadSpeed = new JLabel("Spread Speed:");
		GridBagConstraints gbc_lblSpreadSpeed = new GridBagConstraints();
		gbc_lblSpreadSpeed.anchor = GridBagConstraints.WEST;
		gbc_lblSpreadSpeed.insets = new Insets(0, 0, 5, 0);
		gbc_lblSpreadSpeed.gridx = 0;
		gbc_lblSpreadSpeed.gridy = 2;
		panel_3.add(lblSpreadSpeed, gbc_lblSpreadSpeed);

		spreadSlider = new JSlider();
		spreadSlider.setMinorTickSpacing(1);
		spreadSlider.setValue(75);
		spreadSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				pheromoneSpread = spreadSlider.getValue();
				lblSpreadSpeed.setText("Adjacent Square Amount: " + pheromoneSpread+"%");
			}
		});
		GridBagConstraints gbc_spreadSlider = new GridBagConstraints();
		gbc_spreadSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_spreadSlider.insets = new Insets(0, 0, 5, 0);
		gbc_spreadSlider.gridx = 0;
		gbc_spreadSlider.gridy = 3;
		panel_3.add(spreadSlider, gbc_spreadSlider);
		spreadSlider.setSnapToTicks(true);
		spreadSlider.setPaintTicks(true);
		spreadSlider.setMinimum(1);
		spreadSlider.setMajorTickSpacing(10);

		lblDecayRate = new JLabel("Decay Rate:");
		GridBagConstraints gbc_lblDecayRate = new GridBagConstraints();
		gbc_lblDecayRate.anchor = GridBagConstraints.WEST;
		gbc_lblDecayRate.insets = new Insets(0, 0, 5, 0);
		gbc_lblDecayRate.gridx = 0;
		gbc_lblDecayRate.gridy = 4;
		panel_3.add(lblDecayRate, gbc_lblDecayRate);

		decaySlider = new JSlider();
		decaySlider.setMinorTickSpacing(1);
		decaySlider.setValue(10);
		decaySlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				pheromoneDecay = decaySlider.getValue();
				lblDecayRate.setText("Decay Rate: " + pheromoneDecay+"%");
			}
		});
		GridBagConstraints gbc_decaySlider = new GridBagConstraints();
		gbc_decaySlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_decaySlider.gridx = 0;
		gbc_decaySlider.gridy = 5;
		panel_3.add(decaySlider, gbc_decaySlider);
		decaySlider.setSnapToTicks(true);
		decaySlider.setPaintTicks(true);
		decaySlider.setMinimum(1);
		decaySlider.setMajorTickSpacing(10);

		panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Food",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.gridx = 2;
		gbc_panel_2.gridy = 1;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		lblNumberOfNodes = new JLabel("Number of Nodes:");
		GridBagConstraints gbc_lblNumberOfNodes = new GridBagConstraints();
		gbc_lblNumberOfNodes.anchor = GridBagConstraints.WEST;
		gbc_lblNumberOfNodes.insets = new Insets(0, 0, 5, 0);
		gbc_lblNumberOfNodes.gridx = 0;
		gbc_lblNumberOfNodes.gridy = 0;
		panel_2.add(lblNumberOfNodes, gbc_lblNumberOfNodes);

		foodNodeSlider = new JSlider();
		foodNodeSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				numberOfFoodNodes = foodNodeSlider.getValue();
				lblNumberOfNodes.setText("Number of Nodes: "
						+ numberOfFoodNodes);
			}
		});
		GridBagConstraints gbc_foodNodeSlider = new GridBagConstraints();
		gbc_foodNodeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_foodNodeSlider.insets = new Insets(0, 0, 5, 0);
		gbc_foodNodeSlider.gridx = 0;
		gbc_foodNodeSlider.gridy = 1;
		panel_2.add(foodNodeSlider, gbc_foodNodeSlider);
		foodNodeSlider.setValue(10);
		foodNodeSlider.setSnapToTicks(true);
		foodNodeSlider.setPaintTicks(true);
		foodNodeSlider.setMinimum(1);
		foodNodeSlider.setMaximum(30);
		foodNodeSlider.setMajorTickSpacing(1);

		lblFoodPerNode = new JLabel("Food per Node:");
		GridBagConstraints gbc_lblFoodPerNode = new GridBagConstraints();
		gbc_lblFoodPerNode.anchor = GridBagConstraints.WEST;
		gbc_lblFoodPerNode.insets = new Insets(0, 0, 5, 0);
		gbc_lblFoodPerNode.gridx = 0;
		gbc_lblFoodPerNode.gridy = 2;
		panel_2.add(lblFoodPerNode, gbc_lblFoodPerNode);

		foodPerNodeSlider = new JSlider();
		foodPerNodeSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				foodPerNode = foodPerNodeSlider.getValue();
				lblFoodPerNode.setText("Food per Node: " + foodPerNode);
			}
		});
		GridBagConstraints gbc_foodPerNodeSlider = new GridBagConstraints();
		gbc_foodPerNodeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_foodPerNodeSlider.gridx = 0;
		gbc_foodPerNodeSlider.gridy = 3;
		panel_2.add(foodPerNodeSlider, gbc_foodPerNodeSlider);
		foodPerNodeSlider.setSnapToTicks(true);
		foodPerNodeSlider.setPaintTicks(true);
		foodPerNodeSlider.setMinimum(50);
		foodPerNodeSlider.setMajorTickSpacing(50);
		foodPerNodeSlider.setMaximum(1000);

		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Obstacles",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.gridx = 3;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		lblNumberOfObstacles = new JLabel("Number Of Obstacles:");
		GridBagConstraints gbc_lblNumberOfObstacles = new GridBagConstraints();
		gbc_lblNumberOfObstacles.anchor = GridBagConstraints.WEST;
		gbc_lblNumberOfObstacles.gridwidth = 4;
		gbc_lblNumberOfObstacles.insets = new Insets(0, 0, 5, 0);
		gbc_lblNumberOfObstacles.gridx = 0;
		gbc_lblNumberOfObstacles.gridy = 0;
		panel_1.add(lblNumberOfObstacles, gbc_lblNumberOfObstacles);

		numObstacleSlider = new JSlider();
		numObstacleSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				numberOfObstacles = numObstacleSlider.getValue();
				lblNumberOfObstacles.setText("Number: " + numberOfObstacles);
			}
		});
		GridBagConstraints gbc_numObstacleSlider = new GridBagConstraints();
		gbc_numObstacleSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_numObstacleSlider.gridwidth = 4;
		gbc_numObstacleSlider.insets = new Insets(0, 0, 5, 0);
		gbc_numObstacleSlider.gridx = 0;
		gbc_numObstacleSlider.gridy = 1;
		panel_1.add(numObstacleSlider, gbc_numObstacleSlider);
		numObstacleSlider.setSnapToTicks(true);
		numObstacleSlider.setPaintTicks(true);
		numObstacleSlider.setValue(15);
		numObstacleSlider.setMaximum(30);
		numObstacleSlider.setMajorTickSpacing(1);

		lblSize = new JLabel("Size:");
		GridBagConstraints gbc_lblSize = new GridBagConstraints();
		gbc_lblSize.anchor = GridBagConstraints.WEST;
		gbc_lblSize.gridwidth = 4;
		gbc_lblSize.insets = new Insets(0, 0, 5, 0);
		gbc_lblSize.gridx = 0;
		gbc_lblSize.gridy = 2;
		panel_1.add(lblSize, gbc_lblSize);

		sizeSlider = new JSlider();
		sizeSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				sizeOfObstacles = sizeSlider.getValue();
				lblSize.setText("Size: " + sizeOfObstacles);
			}
		});
		GridBagConstraints gbc_sizeSlider = new GridBagConstraints();
		gbc_sizeSlider.fill = GridBagConstraints.HORIZONTAL;
		gbc_sizeSlider.gridwidth = 4;
		gbc_sizeSlider.insets = new Insets(0, 0, 0, 5);
		gbc_sizeSlider.gridx = 0;
		gbc_sizeSlider.gridy = 3;
		panel_1.add(sizeSlider, gbc_sizeSlider);
		sizeSlider.setSnapToTicks(true);
		sizeSlider.setPaintTicks(true);
		sizeSlider.setValue(30);
		sizeSlider.setMinimum(1);
		sizeSlider.setMajorTickSpacing(1);
		sizeSlider.setMaximum(60);

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStart.setEnabled(false);
				btnStop.setEnabled(true);
				btnReset.setEnabled(true);
				if (antSimulation==null) {
					testCounter = 0;
					antSimulation = new AntSimulation(numOfAnts, antSpeed,
							 pheromoneStrength, pheromoneSpread,
							pheromoneDecay, numberOfFoodNodes, foodPerNode,
							numberOfObstacles, sizeOfObstacles);
					simulationSpace.updateSimlation(antSimulation);
				}
				
				simulationTimer.start();
				 
				
				
			}
		});
		
		GridBagConstraints gbc_btnStart = new GridBagConstraints();
		gbc_btnStart.anchor = GridBagConstraints.EAST;
		gbc_btnStart.insets = new Insets(0, 0, 0, 5);
		gbc_btnStart.gridx = 4;
		gbc_btnStart.gridy = 2;
		contentPane.add(btnStart, gbc_btnStart);

		btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart.setEnabled(true);
				btnStop.setEnabled(false);
				simulationTimer.stop();

				
			}
		});
		GridBagConstraints gbc_btnStop = new GridBagConstraints();
		gbc_btnStop.insets = new Insets(0, 0, 0, 5);
		gbc_btnStop.gridx = 5;
		gbc_btnStop.gridy = 2;
		contentPane.add(btnStop, gbc_btnStop);

		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulationTimer.stop();
				simulationSpace.repaint();
				
				btnStart.setEnabled(true);
				btnStop.setEnabled(false);
				btnReset.setEnabled(false);
				runProgram = false;
				antSimulation = null;
				simulationSpace.updateSimlation(null);
				simulationSpace.setBackground(Color.black);
				simulationSpace.repaint();
			}
		});
		btnReset.setEnabled(false);
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.gridx = 6;
		gbc_btnReset.gridy = 2;
		contentPane.add(btnReset, gbc_btnReset);
	}

	protected void runSimulation() {
		
		antSimulation.update();
		simulationSpace.repaint();
	}

}
