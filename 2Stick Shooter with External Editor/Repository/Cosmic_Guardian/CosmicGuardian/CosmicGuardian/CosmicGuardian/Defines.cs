﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    //Just one big class to store ship variables or other important information. Used to ensure that you don't have to go hunting for
    //a bunch of different variables in a large amount of locations
    static class Defines
    {
        //Base stats for the player's ship

        public static float PLAYER_GRAVITY_MULTIPLIER = 1; //How much gravity affects this object
        public static float PLAYER_MASS = 250; //The mass of the ship. Affects how much it is slowed down and damaged after colliding with something

        //Default shields

        public static float DEFAULT_SHIELDS_HEALTH = 300;
        public static float DEFAULT_SHIELDS_COOLDOWN = 2200;
        public static float DEFAULT_SHIELDS_REGEN = 35;

        //Default engine

        public static float DEFAULT_ENGINE_MAX_ACCELERAION = 1000;
        public static float DEFAULT_ENGINE_MAX_BOOST_ACCELERAION = 1500;
        public static float DEFAULT_ENGINE_MAX_VELOCITY = 500;
        public static float DEFAULT_ENGINE_MAX_BOOST_VELOCITY = 750;
        public static float DEFAULT_ENGINE_BOOST_TIME = 3000;
        public static float DEFAULT_ENGINE_BOOST_REGEN = 750;

        //Damaged engine

        public static float DAMAGED_ENGINE_MAX_ACCELERAION = 130;
        public static float DAMAGED_ENGINE_MAX_BOOST_ACCELERAION  = 200;
        public static float DAMAGED_ENGINE_MAX_VELOCITY  = 130;
        public static float DAMAGED_ENGINE_MAX_BOOST_VELOCITY  = 200;
        public static float DAMAGED_ENGINE_BOOST_TIME = 2000;
        public static float DAMAGED_ENGINE_BOOST_REGEN = 2000;

        //Test projectile

        public static float BULLET_HEALTH = 1;
        public static float BULLET_DAMAGE = 5;

        public static float BULLET_MAX_VELOCITY = 800;
        public static float BULLET_MASS = 5;
        public static float BULLET_GRAVITY_MULTIPLIER = 0;

        //Stats for Enemy Skirmisher

        public static float ENEMY_SKIRMISHER_ACCELERATION = 750;
        public static float ENEMY_SKIRMISHER_MAX_VELOCITY = 600;
        public static float ENEMY_SKIRMISHER_SHIELDS = 20;
        public static float ENEMY_SKIRMISHER_GRAVITY_MULTIPLIER = 1;
        public static float ENEMY_SKIRMISHER_MASS = 100;
        public static int ENEMY_SKIRMISHER_SCORE = 10;

        public static float ENEMY_FIGHTER_ACCELERATION = 300;
        public static float ENEMY_FIGHTER_MAX_VELOCITY = 400;
        public static float ENEMY_FIGHTER_SHIELDS = 250;
        public static float ENEMY_FIGHTER_GRAVITY_MULTIPLIER = 1;
        public static float ENEMY_FIGHTER_MASS = 600;
        public static int ENEMY_FIGHTER_SCORE = 50;

        public static float ENEMY_MINI_BOSS_ACCELERATION = 400;
        public static float ENEMY_MINI_BOSS_MAX_VELOCITY = 350;
        public static float ENEMY_MINI_BOSS_SHIELDS = 1200;
        public static float ENEMY_MINI_BOSS_GRAVITY_MULTIPLIER = 1;
        public static float ENEMY_MINI_BOSS_MASS = 3000;
        public static int ENEMY_MINI_BOSS_SCORE = 1000;

        public static float ENEMY_KAMIKAZE_ACCELERATION = 425;
        public static float ENEMY_KAMIKAZE_MAX_VELOCITY = 425;
        public static float ENEMY_KAMIKAZE_SHIELDS = 1;
        public static float ENEMY_KAMIKAZE_GRAVITY_MULTIPLIER = 1;
        public static float ENEMY_KAMIKAZE_MASS = 1;
        public static int ENEMY_KAMIKAZE_SCORE = 50;

        public static float ENEMY_TURRET_ACCELERATION = 0;
        public static float ENEMY_TURRET_MAX_VELOCITY = 0;
        public static float ENEMY_TURRET_SHIELDS = 500;
        public static float ENEMY_TURRET_GRAVITY_MULTIPLIER = 0;
        public static float ENEMY_TURRET_MASS = 10000;
        public static int ENEMY_TURRET_SCORE = 300;

        public static float ENEMY_MOTHERSHIP_ACCELERATION = 300;
        public static float ENEMY_MOTHERSHIP_MAX_VELOCITY = 300;
        public static float ENEMY_MOTHERSHIP_SHIELDS = 3000;
        public static float ENEMY_MOTHERSHIP_GRAVITY_MULTIPLIER = 1;
        public static float ENEMY_MOTHERSHIP_MASS = 28000;
        public static int ENEMY_MOTHERSHIP_SCORE = 5000;

        //Level Results

        public static int IN_PROGRESS = 0;
        public static int WIN = 1;
        public static int LOSS = -1;

        //Counter for how long before bullets begin to rotate
        public static float PROJECTILE_ROTATE_COUNTER = 200;

        //MiniMap values
        public static float MINIMAP_HEIGHT = 225;
        public static float MINIMAP_WIDTH = 225;

        //Options
        public static bool SOUND_EFFECTS = true;

        //AI stuff
        public static float AI_FIRE_ANGLE_SKIRM = 15;
        public static float AI_FIRE_ANGLE_FIGHT = 35;
        public static float AI_FIRE_ANGLE_MINI = 15;
        public static float AI_FIRE_ANGLE_KAMI = 15;
        public static float AI_FIRE_ANGLE_TUR = 15;
        public static float AI_FIRE_ANGLE_MOTH = 15;
        
        //Other
        public static float KAMIKAZE_DAMAGE = 120;
        public static float SCREEN_WIDTH=1280;
        public static float Screen_Width_Scaler = SCREEN_WIDTH / 1280;
        public static float SCREEN_HEIGHT = 800;

        public static float Screen_height_Scaler = SCREEN_HEIGHT / 800;
        public static float HOMING_TIMER = 200;

        public static float BULLET_MAX_DISTANCE = SCREEN_WIDTH;

    }
}
