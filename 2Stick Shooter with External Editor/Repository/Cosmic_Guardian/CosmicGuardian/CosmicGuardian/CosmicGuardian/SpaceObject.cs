﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    abstract class SpaceObject
    {
        protected Vector2 position, velocity;
        //protected float collisionRadius; //Note: I'm using circles for collision detection due to the ease of use. Since most of the time
                                                //an enemy will have shields up a circular hit box makes sense and if shields are down then we
                                                //just need to make sure that units are either small enough or circular enough that the hit box
                                                //will remain accurate.

        protected float gravityMultiplier, mass, maxVelocity;
        //protected Vector2 objectOffset;

        //Get functions

        public virtual Vector2 getPosition()
        {
            return position;
        }

        public virtual Vector2 getVelocity()
        {
            return velocity;
        }

        public virtual float getMaxVelocity()
        {
            return maxVelocity;
        }

        public virtual float getMass()
        {
            return mass;
        }

        public virtual float getGravityMultiplier()
        {
            return gravityMultiplier;
        }

        //Set functions

        public virtual void setPosition(Vector2 p)
        {
            position = p;
        }

        public virtual void setVelocity(Vector2 v)
        {
            velocity = v;
        }

        public virtual bool isDead()
        {
            return false;
        }

        public void fixVelocity(GameTime gameTime)
        {
            float currentVelocity = velocity.Length();

            if (maxVelocity < currentVelocity)
            {
                float scale = maxVelocity / currentVelocity;
                velocity.X *= scale;
                velocity.Y *= scale;
            }
        }

        public virtual float collisionDist()
        {
            return 0;
        }

        public virtual void Update(GameTime gameTime)
        {
            float time = (float)(gameTime.ElapsedGameTime.Milliseconds / 1000f);
            position += velocity * time;
            fixVelocity(gameTime);
        }

        public virtual void decHealth(float dec)
        {

        }

        /*public virtual Vector2 getPositionOffset()
        {
            return position + objectOffset;
        }

        public virtual Vector2 getOffset()
        {
            return objectOffset;
        }*/

        /*public virtual void offset(Vector2 screensize, Vector2 worldSize)
        {
            if (position.X - (screensize.X / 2) < 0)
            {
                objectOffset.X = position.X - (screensize.X / 2);
            }
            else if (position.X + (screensize.X / 2) > worldSize.X)
            {
                objectOffset.X = position.X - (worldSize.X - screensize.X / 2);
            }
            else
            {
                objectOffset.X = 0;
            }
            


            if (position.Y - (screensize.Y / 2) < 0)
            {
                objectOffset.Y = position.Y - (screensize.Y / 2);
            } else if (position.Y + (screensize.Y / 2) > worldSize.Y)
            {
                objectOffset.Y =position.Y-(worldSize.Y-screensize.Y/2);
            } else{
                objectOffset.Y=0;
            }
        }*/
    }
}
