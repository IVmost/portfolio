﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    enum AIState { IDLE,AGRESSIVE,DEFENSIVE,SPECIAL}
   
   interface AIInterfaceBehaviorSelector
    {
        AIState evaluateBehavior(Vector2 position,float health, float maxHealth, AIState currentState, Vector2 playerPosition);
    }
}
