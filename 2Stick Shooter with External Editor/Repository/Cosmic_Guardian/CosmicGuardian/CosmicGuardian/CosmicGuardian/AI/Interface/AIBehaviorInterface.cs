﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    public struct AICommand
    {
        public float rotate;
        public float thrust;
        public bool fire;
        public float timer;



        public AICommand(float rotate,float thrust, bool fire, float timer)
        {
            this.rotate=rotate;
            this.thrust=thrust;
            this.fire=fire;
            this.timer=timer;
        }
    }
    interface AIBehaviorInterface
    {
        AICommand aiBehavior(Vector2 position, Vector2 target);
    }
}
