﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class BasicAIBehavior : AIInterfaceBehaviorSelector
    {
        public AIState evaluateBehavior(Vector2 position, float health, float maxHealth, AIState currentState, Vector2 playerPostion)
        {
            double distance=Math.Abs((playerPostion-position).Length());
           
            if (health < (maxHealth / 2))
                if (distance < 250)
                    return AIState.SPECIAL;
               else
                    return AIState.DEFENSIVE;
            
            if (distance < 500)
                return AIState.AGRESSIVE;
            if (distance >= 500)
                return AIState.IDLE;
           
            return AIState.IDLE;
        }
    }
}
