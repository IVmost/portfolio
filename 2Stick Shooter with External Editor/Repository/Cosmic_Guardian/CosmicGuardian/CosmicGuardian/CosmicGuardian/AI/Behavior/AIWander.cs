﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    //AI for enemy ship wandering

    class AIWander:AIBehaviorInterface
    {
        public AICommand aiBehavior(Microsoft.Xna.Framework.Vector2 position, Microsoft.Xna.Framework.Vector2 target)
        {
            Random rand = new Random();
            float rotation= (float)(rand.Next(361));
            float thrust = (float)(0.05 * rand.Next(20));
            int wanderTime = (rand.Next(2) + 1) * 2;
            return new AICommand(rotation, thrust, false, wanderTime);
        }
    }
}
