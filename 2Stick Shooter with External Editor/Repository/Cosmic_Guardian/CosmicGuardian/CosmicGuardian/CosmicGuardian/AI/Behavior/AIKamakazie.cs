﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{

    class AIKamakazie:AIBehaviorInterface
    {

        public AICommand aiBehavior(Microsoft.Xna.Framework.Vector2 position, Microsoft.Xna.Framework.Vector2 target)
        {
            //compute the vector direction to a target
            Vector2 direction = (target-position);
            //compute the distance from the target
            double distance = Math.Abs(direction.Length());

            //normalize the direction vector
            direction = Vector2.Normalize(direction);

        
            //set the thrust speed of the ship
            float thrust = 1;

            bool fire = false;
            if (distance < 0)
                fire = true;
            

            //calculation for the rotation office
            float rotation = (float)(Math.Atan2(direction.Y, direction.X) * (180 / Math.PI)) + 90.0f;
            if (rotation < 0)
            {
                rotation += 360.0f;
            }

            //checking for if the angle is toward the cardinal points
         /*   if (direction.X == 0)
            {
                if (direction.Y > 0)
                {
                    rotation = 180;
                }
                else
                {
                    rotation = 0;
                }
            }
            else if (direction.Y == 0)
            {
                if (direction.X > 0)
                    rotation = 90;
                else
                    rotation = 270;
            }
            // calculates the angle if they are facing 0 degrees
            else if (direction.Y < 0)
            {
                rotation = (float)((Math.Atan(direction.X / direction.Y))*180/Math.PI);
                if (direction.X < 0)
                    rotation += 360;
            }
            //calculate the angle if they are faction 270 or 90
            else
            {
                rotation = (float)(Math.Atan(direction.Y / direction.X));
                if (direction.X < 0)
                    rotation += 270;
                else
                    rotation += 90;
            }*/
            return new AICommand(rotation, thrust, fire, 0.1f);
        }
    }
}
