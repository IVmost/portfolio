﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class LevelSelect
    {
        SpriteBatch spriteBatch;
        SpriteFont font1, font2;
        SoundEffect navSound;
        Texture2D background, aButton, dpad, blueButton, greyButton;
        int screenWidth, screenHeight;
        bool levelOneActivated, levelTwoActivated, levelThreeActivated, levelFourActivated, levelFiveActivated, exitActivated, navLatch;
        bool exitPressed;
        String levelTwoString, levelThreeString, levelFourString, levelFiveString;
        bool levelTwoLocked, levelThreeLocked, levelFourLocked, levelFiveLocked;

        public LevelSelect(SpriteBatch batch, Texture2D back, Texture2D buttonA, Texture2D blue, Texture2D grey, Texture2D d, int w, int h, SpriteFont f1, SpriteFont f2, SoundEffect e)
        {
            spriteBatch = batch;
            background = back;
            aButton = buttonA;
            dpad = d;
            blueButton = blue;
            greyButton = grey;
            screenHeight = h;
            screenWidth = w;
            font1 = f1;
            font2 = f2;
            levelOneActivated = true;
            levelTwoActivated = false;
            levelThreeActivated = false;
            levelFourActivated = false;
            levelFiveActivated = false;
            exitActivated = false;
            navLatch = false;
            navSound = e;
            exitPressed = false;

            levelTwoString = "Level 2";
            levelThreeString = "Level 3";
            levelFourString = "Level 4";
            levelFiveString = "Level 5";

            levelTwoLocked = true;
            levelThreeLocked = true;
            levelFourLocked = true;
            levelFiveLocked = true;
        }

        public virtual void Update(GameTime gameTime, MainMenu m)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);
                if (levelOneActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelOneActivated = false;
                        levelTwoActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelOneActivated = false;
                        exitActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }
                else if (levelTwoActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelTwoActivated = false;
                        levelThreeActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelTwoActivated = false;
                        levelOneActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }
                else if (levelThreeActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelThreeActivated = false;
                        levelFourActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelThreeActivated = false;
                        levelTwoActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }
                else if (levelFourActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelFourActivated = false;
                        levelFiveActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelThreeActivated = true;
                        levelFourActivated = false;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }
                else if (levelFiveActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelFiveActivated = false;
                        exitActivated = true;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelFourActivated = true;
                        levelFiveActivated = false;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }
                else if (exitActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        levelOneActivated = true;
                        exitActivated = false;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        levelFiveActivated = true;
                        exitActivated = false;
                        navLatch = true;
                        if (m.playSounds)
                            navSound.Play();
                    }
                }

                if (exitActivated)
                {
                    if (i.enterButton)
                    {
                        exitPressed = true;
                    }
                    if (!i.enterButton && exitPressed)
                    {
                        m.displayLevelSelect = false;
                        m.displayMainMenu = true;
                        levelOneActivated = true;
                        exitActivated = false;
                        exitPressed = false;
                    }
                }

                if (levelOneActivated)
                {
                    if (i.enterButton)
                    {
                        m.displayLevelSelect = false;
                        m.levelNumber = 1;
                        m.displayGame = true;
                    }
                }

                if (levelTwoActivated)
                {
                    if (i.enterButton && !levelTwoLocked)
                    {
                        m.displayLevelSelect = false;
                        m.levelNumber = 2;
                        m.displayGame = true;
                    }
                }

                if (levelThreeActivated)
                {
                    if (i.enterButton && !levelThreeLocked)
                    {
                        m.displayLevelSelect = false;
                        m.levelNumber = 3;
                        m.displayGame = true;
                    }
                }

                if (levelFourActivated)
                {
                    if (i.enterButton && !levelFourLocked)
                    {
                        m.displayLevelSelect = false;
                        m.levelNumber = 4;
                        m.displayGame = true;
                    }
                }

                if (levelFiveActivated)
                {
                    if (i.enterButton && !levelFiveLocked)
                    {
                        m.displayLevelSelect = false;
                        m.levelNumber = 5;
                        m.displayGame = true;
                    }
                }

                if ((i.menuNavigate.Down == ButtonState.Released) && (i.menuNavigate.Up == ButtonState.Released))
                {
                    navLatch = false;
                }

                if (levelTwoLocked)
                    levelTwoString = "Level 2: Locked";
                else
                    levelTwoString = "Level 2: Unlocked";

                if (levelThreeLocked)
                    levelThreeString = "Level 3: Locked";
                else
                    levelThreeString = "Level 3: Unlocked";

                if (levelFourLocked)
                    levelFourString = "Level 4: Locked";
                else
                    levelFourString = "Level 4: Unlocked";

                if (levelFiveLocked)
                    levelFiveString = "Level 5: Locked";
                else
                    levelFiveString = "level 5: Unlocked";

                if (m.levelUnlocked == 2)
                {
                    levelTwoLocked = false;
                }
                else if (m.levelUnlocked == 3)
                {
                    levelTwoLocked = false;
                    levelThreeLocked = false;
                }
                else if (m.levelUnlocked == 4)
                {
                    levelTwoLocked = false;
                    levelThreeLocked = false;
                    levelFourLocked = false; 
                }
                else if (m.levelUnlocked == 5)
                {
                    levelTwoLocked = false;
                    levelThreeLocked = false;
                    levelFourLocked = false;
                    levelFiveLocked = false;
                }
        }

        public virtual void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            spriteBatch.DrawString(font1, "Select", new Vector2(80, screenHeight - 57 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, "Navigate", new Vector2(80, screenHeight - 135 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(aButton, new Vector2(5 * Defines.Screen_Width_Scaler, screenHeight - 75 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(dpad, new Vector2(5 * Defines.Screen_Width_Scaler, screenHeight - 150 * Defines.Screen_height_Scaler), new Rectangle(0, 0, dpad.Width, dpad.Height), Color.White, 0.0f, new Vector2(0, 0), 0.1f, SpriteEffects.None, 0);

            spriteBatch.DrawString(font2, "SELECT LEVEL", new Vector2(430 * Defines.Screen_Width_Scaler, 50 * Defines.Screen_height_Scaler), Color.White);

            if (levelOneActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, 130 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, 130 * Defines.Screen_height_Scaler), Color.White);

            if (levelTwoActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height + 160 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height + 160 * Defines.Screen_height_Scaler), Color.White);

            if (levelThreeActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 2 + 190 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 2 + 190 * Defines.Screen_height_Scaler), Color.White);

            if (levelFourActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 3 + 220 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 3 + 220 * Defines.Screen_height_Scaler), Color.White);

            if (levelFiveActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 4 + 250 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 4 + 250 * Defines.Screen_height_Scaler), Color.White);

            if (exitActivated)
                spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 5 + 280 * Defines.Screen_height_Scaler), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 5 + 280 * Defines.Screen_height_Scaler), Color.White);

            spriteBatch.DrawString(font1, "Level 1: Unlocked", new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, 115 * Defines.Screen_height_Scaler + blueButton.Height / 2), Color.White);
            spriteBatch.DrawString(font1, levelTwoString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 1.5f + 145 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, levelThreeString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 2.5f + 175 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, levelFourString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 3.5f + 205 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, levelFiveString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 4.5f + 230 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, "Exit", new Vector2(blueButton.Width + 440*Defines.Screen_Width_Scaler, blueButton.Height * 5.5f + 255 * Defines.Screen_height_Scaler), Color.White);
        }
    }
}
