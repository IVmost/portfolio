﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class EnemyMothership : Enemy
    {
        private static Texture2D sprite;
        private static float imageScale, collisionRadius;

        private List<Weapon> weapons;
        private Weapon currWeapon;
        private static Weapon dummyGun, dummyRocket;

        bool fireRockets = false;
        int rocketsFired = 0;

        float rocketsCooldown = 5000;
        float rocketsCooldownCount = 0;

        Random r = new Random();

        //Current Attitude of the ship
        private AIState currentState;
        //Reaction archytype for choosing the AI
        //later this will be moved into the idividual ship entity class
        private static AIInterfaceBehaviorSelector aiSelector = null;
        private static AIBehaviorInterface aiIdle = null;
        private static AIBehaviorInterface aiAgressive = null;
        private static AIBehaviorInterface aiDefensive = null;
        private static AIBehaviorInterface aiSpecial = null;
        //Constructors for the player's ship
        private double nextAIStep;
        private AICommand currentAIcommand;

        private Vector2 playerPos;

        private float shipRotation;
        private float rotationSpeed = 180;

        private Color testingColor;
        public EnemyMothership()
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            //weapon = new TestWeapon();

            currentAIcommand = new AICommand(0, 0, false, 0);

            testingColor = Color.White;
            //only need to be assigned once note for each creation
            nextAIStep = 0;

            if (aiSelector == null)
                aiSelector = new BasicAIBehavior();
            if (aiIdle == null)
                aiIdle = new AIAttack();
            if (aiAgressive == null)
                aiAgressive = new AIAttack();
            if (aiDefensive == null)
                aiDefensive = new AIAttack();
            if (aiSpecial == null)
                aiSpecial = new AIAttack();

            acceleration = Defines.ENEMY_MOTHERSHIP_ACCELERATION;
            maxVelocity = Defines.ENEMY_MOTHERSHIP_MAX_VELOCITY;
            shields = Defines.ENEMY_MOTHERSHIP_SHIELDS;
            maxShields = Defines.ENEMY_MOTHERSHIP_SHIELDS;
            mass = Defines.ENEMY_MOTHERSHIP_MASS;
            gravityMultiplier = Defines.ENEMY_MOTHERSHIP_GRAVITY_MULTIPLIER;

            weapons = new List<Weapon>();

            if (dummyGun != null)
            {
                weapons.Add(new Weapon(dummyGun, Vector2.Zero));
            }

            if (dummyRocket != null)
            {
                weapons.Add(new Weapon(dummyRocket, new Vector2(0, 20)));
                weapons.Add(new Weapon(dummyRocket, new Vector2(0, -20)));
            }

            //objectOffset = new Vector2(0, 0);


            currentState = AIState.IDLE;
        }

        public void assignTexture(Texture2D t, float s)
        {
            sprite = t;
            imageScale = s;
            collisionRadius = (Math.Max(sprite.Width, sprite.Height) * imageScale) / 2f; //Note: This can be changed later depending on what parts the player ship will have
        }

        public void assignWeapons(Weapon w, Weapon w2)
        {
            dummyGun = w;
            dummyRocket = w2;
            
        }

        public override void AI(GameTime gameTime, Vector2 playerPosition, Vector2 playerVelocity)
        {
            playerVel = playerVelocity;
            playerPos = playerPosition;
            float time = (float)(gameTime.ElapsedGameTime.Milliseconds / 1000.0);
            foreach (Weapon w in weapons)
                w.Update(gameTime);

            //AI code will go here. Function can be modified later based on what other information a ship will need
            //to make its decisions
            AIState newState = aiSelector.evaluateBehavior(position, shields, maxShields, currentState, playerPosition);
            if (newState != currentState)
            {
                nextAIStep = 0;
                currentState = newState;
            }
            nextAIStep -= gameTime.ElapsedGameTime.Milliseconds;
            if (nextAIStep < 0)
            {

                if (currentState == AIState.AGRESSIVE)
                {
                    //testingColor = Color.Red;
                    currentAIcommand = aiAgressive.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.DEFENSIVE)
                {
                    //testingColor = Color.Yellow;
                    currentAIcommand = aiDefensive.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.IDLE)
                {
                    //testingColor = Color.Green;
                    currentAIcommand = aiIdle.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.SPECIAL)
                {
                    //testingColor = Color.Gray;
                    currentAIcommand = aiSpecial.aiBehavior(position, playerPosition);
                }
                nextAIStep = currentAIcommand.timer * 1000;
            }
            adjustShip(currentAIcommand, time);
            fixVelocity(gameTime);
            position += velocity * time;
        }

        private void adjustShip(AICommand newCommand, float time)
        {
            float difference = newCommand.rotate - shipRotation;
            if (Math.Abs(difference) > 2)
            {
                float rotationStep = rotationSpeed * time;
                float scaler;

                if (difference > 0)
                {
                    scaler = 1;
                    if (difference > Math.Abs(difference - 360))
                        scaler = -1;
                }
                else
                {
                    scaler = -1;
                    if (Math.Abs(difference) > (difference + 360))
                    {
                        scaler = 1;
                    }

                }

                if (rotationStep > Math.Abs(difference))
                {
                    shipRotation += difference;
                }
                else
                {
                    shipRotation += (rotationStep * scaler);
                }

                if (shipRotation < 0)
                    shipRotation += 360;
                if (shipRotation >= 360)
                {
                    shipRotation -= 360;
                }

            }
            if (Math.Abs(difference) < 20)
            {
                double radRotation = shipRotation * Math.PI / 180.0;
                Vector2 newVector = new Vector2((float)Math.Sin(radRotation), -(float)Math.Cos(radRotation));
                if (shipRotation < 90 && shipRotation >= 0)
                {
                    radRotation = shipRotation * Math.PI / 180.0;
                    newVector = new Vector2((float)Math.Sin(radRotation), -(float)Math.Cos(radRotation));
                }
                if (shipRotation < 180 && shipRotation >= 90)
                {
                    radRotation = (shipRotation - 90) * Math.PI / 180;
                    newVector = new Vector2((float)Math.Cos(radRotation), (float)Math.Sin(radRotation));
                }
                if (shipRotation < 270 && shipRotation >= 180)
                {
                    radRotation = (shipRotation - 180) * Math.PI / 180;
                    newVector = new Vector2(-(float)Math.Sin(radRotation), (float)Math.Cos(radRotation));
                }
                if (shipRotation < 360 && shipRotation >= 270)
                {
                    radRotation = (shipRotation - 270) * Math.PI / 180;
                    newVector = new Vector2(-(float)Math.Cos(radRotation), -(float)Math.Sin(radRotation));
                }

                if (r.Next(100) <= 10 && rocketsCooldownCount > rocketsCooldown && !fireRockets)
                {
                    fireRockets = true;
                    rocketsCooldownCount = 0;
                }
                else
                {
                    rocketsCooldownCount += time * 1000;
                }

                if (rocketsFired == 0 && !fireRockets)
                {
                    currWeapon = weapons[0];
                }
                else
                {
                    if (rocketsFired == 1)
                    {
                        currWeapon = weapons[2];
                    }
                    else
                    {
                        currWeapon = weapons[1];
                    }

                }

                velocity += acceleration * newVector * time * newCommand.thrust;
                playerVel.Normalize();
                newVector += (playerVel * 0.2f);

                if (Math.Abs(difference) <= Defines.AI_FIRE_ANGLE_MOTH && newCommand.fire)
                {
                    Vector2 dir = playerPos - position;
                    dir.Normalize();
                    firedProjectile = currWeapon.fire(position, velocity, dir, this, Defines.SOUND_EFFECTS);
                    if (firedProjectile != null && (currWeapon == weapons[1] || currWeapon == weapons[2]))
                    {
                        rocketsFired++;
                        if (rocketsFired == 2)
                        {
                            fireRockets = false;
                            rocketsFired = 0;
                        }
                    }
                }
            }
        }

        //Returns the radius of the ship's hit circle
        public override float collisionDist()
        {
            return collisionRadius;
        }

        //See PlayerShip class to see how this works
        public override void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 cameraPosition)

//        public void draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 playerPosition)
        {
            Vector2 origin = new Vector2((sprite.Width / 2), (sprite.Height / 2));

            Vector2 tempVector = position - (Vector2)cameraPosition;
            Vector2 screenOrigin = new Vector2(screenSize.X / 2, screenSize.Y / 2);
            Vector2 drawLocation = tempVector + screenOrigin;

            float radius = collisionDist();

            float radRotation = (float)((shipRotation) * Math.PI / 180.0);
            if (drawLocation.X + radius >= 0 && drawLocation.Y + radius >= 0 && drawLocation.X - radius <= screenSize.X + sprite.Width && drawLocation.Y - radius <= screenSize.Y + sprite.Height)
                spriteBatch.Draw(sprite, drawLocation, null, testingColor, radRotation, origin, imageScale, SpriteEffects.None, 0);

        }

        public override Projectile getProjectile()
        {
            return firedProjectile;
        }

        public override void resetProjectile()
        {
            firedProjectile = null;


        }
    }
}
