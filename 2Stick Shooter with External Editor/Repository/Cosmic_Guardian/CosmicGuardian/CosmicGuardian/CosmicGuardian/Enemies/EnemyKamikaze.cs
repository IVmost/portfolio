﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class EnemyKamikaze : Enemy
    {
        private static Texture2D sprite;
        private static float imageScale, collisionRadius;

        private Weapon weapon;
        private static Weapon dummyWeapon;

        //Current Attitude of the ship
        private AIState currentState;
        //Reaction archytype for choosing the AI
        //later this will be moved into the idividual ship entity class
        private static AIInterfaceBehaviorSelector aiSelector = null;
        private static AIBehaviorInterface aiIdle = null;
        private static AIBehaviorInterface aiAgressive = null;
        private static AIBehaviorInterface aiDefensive = null;
        private static AIBehaviorInterface aiSpecial = null;
        //Constructors for the player's ship
        private double nextAIStep;
        private AICommand currentAIcommand;

        private float shipRotation;
        private float rotationSpeed = 180;

        private Color testingColor;
        public EnemyKamikaze()
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            //weapon = new TestWeapon();

            currentAIcommand = new AICommand(0, 0, false, 0);

            testingColor = Color.White;
            //only need to be assigned once note for each creation
            nextAIStep = 0;

            if (aiSelector == null)
                aiSelector = new BasicAIBehavior();
            if (aiIdle == null)
                aiIdle = new AIKamakazie();
            if (aiAgressive == null)
                aiAgressive = new AIKamakazie();
            if (aiDefensive == null)
                aiDefensive = new AIKamakazie();
            if (aiSpecial == null)
                aiSpecial = new AIKamakazie();

            acceleration = Defines.ENEMY_KAMIKAZE_ACCELERATION;
            maxVelocity = Defines.ENEMY_KAMIKAZE_MAX_VELOCITY;
            shields = Defines.ENEMY_KAMIKAZE_SHIELDS;
            maxShields = Defines.ENEMY_KAMIKAZE_SHIELDS;
            mass = Defines.ENEMY_KAMIKAZE_MASS;
            gravityMultiplier = Defines.ENEMY_KAMIKAZE_GRAVITY_MULTIPLIER;

            if (dummyWeapon != null)
                weapon = new Weapon(dummyWeapon, Vector2.Zero);

            //objectOffset = new Vector2(0, 0);


            currentState = AIState.SPECIAL;
        }

        public void assignTexture(Texture2D t, float s)
        {
            sprite = t;
            imageScale = s;
            collisionRadius = (Math.Max(sprite.Width, sprite.Height) * imageScale) / 2f; //Note: This can be changed later depending on what parts the player ship will have
        }

        public void assignWeapon(Weapon w)
        {
            dummyWeapon = w;
        }

        public override void AI(GameTime gameTime, Vector2 playerPosition, Vector2 playerVelocity)
        {
            float time = (float)(gameTime.ElapsedGameTime.Milliseconds / 1000.0);
            weapon.Update(gameTime);

            //AI code will go here. Function can be modified later based on what other information a ship will need
            //to make its decisions
            AIState newState = aiSelector.evaluateBehavior(position, shields, maxShields, currentState, playerPosition);
            if (newState != currentState)
            {
                nextAIStep = 0;
                currentState = newState;
            }
            nextAIStep -= gameTime.ElapsedGameTime.Milliseconds;
            if (nextAIStep < 0)
            {
                if (currentState == AIState.AGRESSIVE)
                {
                    //testingColor = Color.Red;
                    currentAIcommand = aiAgressive.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.DEFENSIVE)
                {
                    //testingColor = Color.Yellow;
                    currentAIcommand = aiDefensive.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.IDLE)
                {
                    //testingColor = Color.Green;
                    currentAIcommand = aiIdle.aiBehavior(position, playerPosition);
                }
                if (currentState == AIState.SPECIAL)
                {
                    //testingColor = Color.Gray;
                    currentAIcommand = aiSpecial.aiBehavior(position, playerPosition);
                }
                nextAIStep = currentAIcommand.timer * 1000;
            }
            adjustShip(currentAIcommand, time);
            fixVelocity(gameTime);
            position += velocity * time;
        }

        private void adjustShip(AICommand newCommand, float time)
        {
            float difference = newCommand.rotate - shipRotation;
            if (Math.Abs(difference) > 2)
            {
                float rotationStep = rotationSpeed * time;
                float scaler;

                if (difference > 0)
                {
                    scaler = 1;
                    if (difference > Math.Abs(difference - 360))
                        scaler = -1;
                }
                else
                {
                    scaler = -1;
                    if (Math.Abs(difference) > (difference + 360))
                    {
                        scaler = 1;
                    }

                }

                if (rotationStep > Math.Abs(difference))
                {
                    shipRotation += difference;
                }
                else
                {
                    shipRotation += (rotationStep * scaler);
                }

                if (shipRotation < 0)
                    shipRotation += 360;
                if (shipRotation >= 360)
                {
                    shipRotation -= 360;
                }

            }
            if (Math.Abs(difference) < 20)
            {
                double radRotation = shipRotation * Math.PI / 180.0;
                Vector2 newVector = new Vector2((float)Math.Sin(radRotation), -(float)Math.Cos(radRotation));
                if (shipRotation < 90 && shipRotation >= 0)
                {
                    radRotation = shipRotation * Math.PI / 180.0;
                    newVector = new Vector2((float)Math.Sin(radRotation), -(float)Math.Cos(radRotation));
                }
                if (shipRotation < 180 && shipRotation >= 90)
                {
                    radRotation = (shipRotation - 90) * Math.PI / 180;
                    newVector = new Vector2((float)Math.Cos(radRotation), (float)Math.Sin(radRotation));
                }
                if (shipRotation < 270 && shipRotation >= 180)
                {
                    radRotation = (shipRotation - 180) * Math.PI / 180;
                    newVector = new Vector2(-(float)Math.Sin(radRotation), (float)Math.Cos(radRotation));
                }
                if (shipRotation < 360 && shipRotation >= 270)
                {
                    radRotation = (shipRotation - 270) * Math.PI / 180;
                    newVector = new Vector2(-(float)Math.Cos(radRotation), -(float)Math.Sin(radRotation));
                }
                velocity += acceleration * newVector * time * newCommand.thrust;
                if (Math.Abs(difference) <= Defines.AI_FIRE_ANGLE_KAMI && newCommand.fire)
                    firedProjectile = weapon.fire(position, velocity, newVector, this, Defines.SOUND_EFFECTS);
            }
        }

        //Returns the radius of the ship's hit circle
        public override float collisionDist()
        {
            return collisionRadius;
        }

        //See PlayerShip class to see how this works
        public override void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 cameraPosition)

//        public void draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 playerPosition)
        {
            Vector2 origin = new Vector2((sprite.Width / 2), (sprite.Height / 2));

            Vector2 tempVector = position - (Vector2)cameraPosition;
            Vector2 screenOrigin = new Vector2(screenSize.X / 2, screenSize.Y / 2);
            Vector2 drawLocation = tempVector + screenOrigin;

            float radius = collisionDist();

            float radRotation = (float)((shipRotation) * Math.PI / 180.0);
            if (drawLocation.X + radius >= 0 && drawLocation.Y + radius >= 0 && drawLocation.X - radius <= screenSize.X + sprite.Width && drawLocation.Y - radius <= screenSize.Y + sprite.Height)
                spriteBatch.Draw(sprite, drawLocation, null, testingColor, radRotation, origin, imageScale, SpriteEffects.None, 0);

        }

        public override Projectile getProjectile()
        {
            return firedProjectile;
        }

        public override void resetProjectile()
        {
            firedProjectile = null;


        }
    }
}
