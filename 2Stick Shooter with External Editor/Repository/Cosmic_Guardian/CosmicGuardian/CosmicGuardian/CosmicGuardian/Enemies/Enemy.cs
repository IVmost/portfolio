﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    abstract class Enemy : SpaceObject
    {
        protected float acceleration, shields, maxShields;
        protected Projectile firedProjectile;
        protected Vector2 playerVel;

        //Constructors for the player's ship

        public abstract void AI(GameTime gameTime, Vector2 playerPosition, Vector2 playerVelocity);

        public override bool isDead()
        {
            return (shields < 0);
        }

        public override void decHealth(float dec)
        {
            shields -= dec;
        }

        public abstract Projectile getProjectile();

        public abstract void resetProjectile();

        public abstract void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 playerPosition);

        public float getAcceleration()
        {
            return acceleration;
        }

        public float getShields()
        {
            return shields;
        }
    }
}
