﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class Camera
    {
        private Vector2 position;

        public Camera()
        {
            position = new Vector2(0, 0);
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public void setPosition(Vector2 p)
        {
            position = p;
        }
    }
}
