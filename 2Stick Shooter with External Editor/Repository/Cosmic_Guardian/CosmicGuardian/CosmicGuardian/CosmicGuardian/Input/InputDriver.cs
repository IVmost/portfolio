﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;              // needed for Vector2 & Rectangle
using Microsoft.Xna.Framework.Input;



namespace CosmicGuardian
{
    public struct inputStruct
    {
        public Vector2 movement;
        public Vector2 aim;
        public bool afterburner;
        public bool primaryFire;
        public bool secondaryFire;
        public bool previousWeapon;
        public bool nextWeapon;
        public GamePadDPad menuNavigate;
        public bool minimapButton;
        public bool pauseButton;
        public bool warpButton;
        public bool cancelButton;
        public bool enterButton;
        public bool debugButton;


        public inputStruct(int p1, int p2)
        {
            movement = new Vector2(0, 0);
            aim = new Vector2(0, 0);
            afterburner = false;
            primaryFire = false;
            secondaryFire = false;
            previousWeapon = false;
            nextWeapon = false;
            menuNavigate = new GamePadDPad();
            minimapButton = false;
            pauseButton = false;
            warpButton = false;
            cancelButton = false;
            enterButton = false;
            debugButton = false;
        }
    }
   
    static class InputDriver
    {
        //private bool keyBoardEnabled;
        static private inputStruct input;
//        public InputDriver()
//        {
//            keyBoardEnabled = false;
//        }

        static public inputStruct PollGamePadInput(PlayerIndex playerNo)
        {
            
            GamePadState gamePadState = GamePad.GetState(playerNo);
            input.movement.X = gamePadState.ThumbSticks.Left.X;
            input.movement.Y = -gamePadState.ThumbSticks.Left.Y;
            input.aim.X = gamePadState.ThumbSticks.Right.X;
            input.aim.Y = -gamePadState.ThumbSticks.Right.Y;
            input.afterburner = (gamePadState.Buttons.LeftStick)==ButtonState.Pressed;
            input.primaryFire = (gamePadState.Triggers.Left)>0.5f;
            input.secondaryFire = (gamePadState.Triggers.Right)>0.5f;
            input.previousWeapon = (gamePadState.Buttons.LeftShoulder)==ButtonState.Pressed;
            input.nextWeapon = (gamePadState.Buttons.RightShoulder)==ButtonState.Pressed;
            input.menuNavigate = gamePadState.DPad;
            input.minimapButton = (gamePadState.Buttons.Back)==ButtonState.Pressed;
            input.pauseButton = (gamePadState.Buttons.Start)==ButtonState.Pressed;
            input.warpButton = (gamePadState.Buttons.Y)==ButtonState.Pressed;
            input.cancelButton = (gamePadState.Buttons.B)==ButtonState.Pressed;
            input.enterButton = (gamePadState.Buttons.A)==ButtonState.Pressed;
            input.debugButton = (gamePadState.Buttons.BigButton) == ButtonState.Pressed;
            return input;
        }
    }
}
