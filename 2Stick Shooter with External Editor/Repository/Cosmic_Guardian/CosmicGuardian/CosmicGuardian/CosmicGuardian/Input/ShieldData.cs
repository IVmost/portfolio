﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{

    class ShieldData
    {
        public String shieldID;
        public String shieldName;
        public Int32 maxShield;
        public float rechargeRate;
        public float rechargeDelay;
	
        public ShieldData()
        {
        }
    }
}
