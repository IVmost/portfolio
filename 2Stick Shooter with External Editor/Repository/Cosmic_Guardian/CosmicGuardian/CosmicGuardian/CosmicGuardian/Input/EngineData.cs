﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    
    class EngineData
    {
        public String engineID;
        public String engineName;
        public String engineSpriteName;
        public Int32 maxSpeed;
        public Int32 acceleration;
        public float maxBoost;

        public EngineData()
        {
        }
    }
}
