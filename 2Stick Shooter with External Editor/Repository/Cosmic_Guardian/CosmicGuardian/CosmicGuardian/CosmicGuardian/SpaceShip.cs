﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    abstract class SpaceShip : SpaceObject
    {
        protected float acceleration, maxVelocity, health, maxHealth, shields, maxShields, shieldRegen, 
                        shieldDisableTime, shieldDisableCounter;

        //If the velocity of a ship is greater than its maximum then it is reduced by a percentage without affecting the direction of the ship
        public void fixVelocity()
        {
            float currentVelocity = velocity.Length();
            float scale = maxVelocity / currentVelocity;

            if (scale < 1)
            //if(maxVelocity < currentVelocity)
            {
                velocity.X *= scale;
                velocity.Y *= scale;
                //velocity = Vector2.Multiply(velocity, 0.99f);
            }
        }

        //Overrided set function for velocity that also takes into account the max velocity of an object
        public override void setVelocity(Vector2 v)
        {
            velocity = v;
            //fixVelocity();
        }

        //Note: Spaceships have both AI and control functions so that 
        public virtual void controls(GameTime gameTime)
        {

        }

        public virtual void AI(GameTime gameTime, Vector2 playerPosition)
        {

        }


        public abstract void draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2? playerPosition);
    }
}
