﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class MediumAsteroid : GravityObject
    {
        private static Texture2D sprite;
        private static float imageScale;
        private static float collisionRadius;

        public MediumAsteroid()
        {
            gravity = 20;
            closeRadius = 400;
            farRadius = 800;
            health = -1;
            maxVelocity = 450;
            mass = 800;
        }

        public MediumAsteroid(MediumAsteroid g)
        {
            gravity = g.gravity;
            closeRadius = g.closeRadius;
            farRadius = g.farRadius;
            health = g.health;
            maxVelocity = g.maxVelocity;
            mass = g.mass;
        }

        public void assignTexture(Texture2D t, float iS)
        {
            sprite = t;
            imageScale = iS;
            collisionRadius = (Math.Max(t.Width, t.Height) * imageScale) / 2f;
        }

        public override float collisionDist()
        {
            return collisionRadius;
        }

        public override void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 cameraPosition)
        {
            Vector2 origin = new Vector2((sprite.Width / 2), (sprite.Height / 2));

            Vector2 tempVector = position - (Vector2)cameraPosition;
            Vector2 screenOrigin = new Vector2(screenSize.X / 2, screenSize.Y / 2);
            Vector2 drawLocation = tempVector + screenOrigin;

            float radius = collisionDist();

            if (drawLocation.X + radius >= 0 && drawLocation.Y + radius >= 0 && drawLocation.X - radius <= screenSize.X + sprite.Width && drawLocation.Y - radius <= screenSize.Y + sprite.Height)
                spriteBatch.Draw(sprite, drawLocation, null, Color.White, 0, origin, imageScale, SpriteEffects.None, 0);
        }
    }
}
