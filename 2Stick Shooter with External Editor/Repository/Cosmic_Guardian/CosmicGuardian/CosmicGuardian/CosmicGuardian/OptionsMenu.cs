﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    //Options menu class
    class OptionsMenu
    {
        SpriteFont font1, font2;
        SpriteBatch spriteBatch;
        private int screenHeight, screenWidth;
        private Texture2D background, dpad, aButton, bButton, blueButton, greyButton, controlsImage;
        private Boolean howToActivated, exitActivated, musicActivated, soundActivated, navLatch, selectLatch, exitPressed, showControls;
        private SoundEffect navSound;
        public Boolean playMusic, playSoundEffects;
        private String musicString, soundEffectsString;

        //Options menu constructor
        public OptionsMenu(Texture2D back, Texture2D d, Texture2D a, Texture2D b, Texture2D blue, Texture2D grey, Texture2D c, int height, int width, SpriteBatch batch, SpriteFont f1, SpriteFont f2, SoundEffect s)
        {
            background = back;
            dpad = d;
            aButton = a;
            bButton = b;
            screenHeight = height;
            screenWidth = width;
            spriteBatch = batch;
            font1 = f1;
            font2 = f2;
            blueButton = blue;
            greyButton = grey;
            controlsImage = c;
            howToActivated = true;
            exitActivated = false;
            navLatch = false;
            selectLatch = false;
            exitPressed = false;
            showControls = false;
            playMusic = true;
            playSoundEffects = true;
            musicString = "Music: ON";
            soundEffectsString = "Sound FX: ON";
            navSound = s;
        }

        //Update options menu
        public virtual void Update(GameTime gameTime, MainMenu m)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);
            if (!showControls)
            {
                if (howToActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        howToActivated = false;
                        musicActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        howToActivated = false;
                        exitActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }
                }
                else if (musicActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        musicActivated = false;
                        soundActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        musicActivated = false;
                        howToActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }
                }
                else if (soundActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        soundActivated = false;
                        exitActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        soundActivated = false;
                        musicActivated = true;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }
                }
                else if (exitActivated)
                {
                    if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                    {
                        howToActivated = true;
                        exitActivated = false;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }

                    if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                    {
                        soundActivated = true;
                        exitActivated = false;
                        navLatch = true;
                        if (playSoundEffects)
                            navSound.Play();
                    }
                }

                if (exitActivated)
                {
                    if (i.enterButton)
                    {
                        exitPressed = true;
                    }
                    if (!i.enterButton && exitPressed)
                    {
                        m.displayOptionsMenu = false;
                        m.displayMainMenu = true;
                        howToActivated = true;
                        exitActivated = false;
                        exitPressed = false;
                    }
                }

                if (howToActivated)
                {
                    if (i.enterButton)
                    {
                        showControls = true;
                    }
                }

                if (musicActivated)
                {
                    if (i.enterButton && !selectLatch)
                    {
                        playMusic = !playMusic;
                        selectLatch = true;
                    }
                }

                if (soundActivated)
                {
                    if (i.enterButton && !selectLatch)
                    {
                        playSoundEffects = !playSoundEffects;
                        m.playSounds = playSoundEffects;
                        selectLatch = true;
                    }
                }

                if (!i.enterButton)
                    selectLatch = false;

                if ((i.menuNavigate.Down == ButtonState.Released) && (i.menuNavigate.Up == ButtonState.Released))
                {
                    navLatch = false;
                }
            }
            else
            {
                if (i.cancelButton)
                    showControls = false;
            }

            if (playMusic)
                musicString = "Music: ON";
            else
                musicString = "Music: OFF";

            if (playSoundEffects)
                soundEffectsString = "Sound FX: ON";
            else
                soundEffectsString = "Sound FX: OFF";
        }

        //Draw options menu
        public virtual void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            spriteBatch.DrawString(font1, "Select", new Vector2(80, screenHeight - 57 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font1, "Navigate", new Vector2(80, screenHeight - 135 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(aButton, new Vector2(5, screenHeight - 75 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(dpad, new Vector2(5, screenHeight - 150 * Defines.Screen_height_Scaler), new Rectangle(0, 0, dpad.Width, dpad.Height), Color.White, 0.0f, new Vector2(0, 0), 0.1f, SpriteEffects.None, 0);

            if (showControls)
            {
                Vector2 temp = new Vector2(screenWidth / 2 - controlsImage.Width / 2, screenHeight / 2 - controlsImage.Height / 2);
                spriteBatch.Draw(controlsImage, temp, Color.White);
                spriteBatch.Draw(bButton, new Vector2(temp.X, temp.Y - bButton.Height), Color.White);
                spriteBatch.DrawString(font1, "Back", new Vector2(temp.X + bButton.Width, temp.Y - 45), Color.White);
            }
            else
            {
                spriteBatch.DrawString(font2, "OPTIONS", new Vector2(430 * Defines.Screen_Width_Scaler, 100*Defines.Screen_height_Scaler), Color.White);

                if (howToActivated)
                    spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, 180 * Defines.Screen_height_Scaler), Color.White);
                else
                    spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, 180 * Defines.Screen_height_Scaler), Color.White);

                if (musicActivated)
                    spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height + 210 * Defines.Screen_height_Scaler), Color.White);
                else
                    spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height + 210 * Defines.Screen_height_Scaler), Color.White);

                if (soundActivated)
                    spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 2 + 240 * Defines.Screen_height_Scaler), Color.White);
                else
                    spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 2 + 240 * Defines.Screen_height_Scaler), Color.White);

                if (exitActivated)
                    spriteBatch.Draw(blueButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 3 + 270 * Defines.Screen_height_Scaler), Color.White);
                else
                    spriteBatch.Draw(greyButton, new Vector2(430 * Defines.Screen_Width_Scaler, blueButton.Height * 3 + 270 * Defines.Screen_height_Scaler), Color.White);

                spriteBatch.DrawString(font1, "Control", new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, 155 * Defines.Screen_height_Scaler + blueButton.Height / 2), Color.White);
                spriteBatch.DrawString(font1, "Layout", new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, 175 * Defines.Screen_height_Scaler + blueButton.Height / 2), Color.White);
                spriteBatch.DrawString(font1, musicString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 1.5f + 200 * Defines.Screen_height_Scaler), Color.White);
                spriteBatch.DrawString(font1, soundEffectsString, new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 2.5f + 225 * Defines.Screen_height_Scaler), Color.White);
                spriteBatch.DrawString(font1, "Exit", new Vector2(blueButton.Width + 440 * Defines.Screen_Width_Scaler, blueButton.Height * 3.5f + 255 * Defines.Screen_height_Scaler), Color.White);
            }
        }
    }
}
