﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class Projectile : SpaceObject
    {

        private float damage, health, distanceTraveled, maxDistance, rotateCount;
        private double rotateAngle;

        protected Texture2D sprite;
        protected float imageScale;
        protected float collisionRadius;

        float recalcTarget;

        private bool homingMode;
        private Vector2 playerPosition;
        private float lastUpdate;

        public Projectile()
        {
            health = Defines.BULLET_HEALTH;
            damage = Defines.BULLET_DAMAGE;
            maxVelocity = Defines.BULLET_MAX_VELOCITY;
            mass = Defines.BULLET_MASS;
            gravityMultiplier = Defines.BULLET_GRAVITY_MULTIPLIER;
            maxDistance = Defines.BULLET_MAX_DISTANCE;
            lastUpdate = 0;
        }

        public Projectile(Projectile p)
        {
            health = p.health;
            damage = p.damage;
            maxVelocity = p.maxVelocity;
            mass = p.mass;
            gravityMultiplier = p.gravityMultiplier;
            maxDistance = p.maxDistance;
            sprite = p.sprite;
            imageScale = p.imageScale;
            collisionRadius = p.collisionRadius;
            homingMode = p.homingMode;
            recalcTarget = p.recalcTarget;
        }

        public virtual void assignTexture(Texture2D t, float iS)
        {
            sprite = t;
            imageScale = iS;
            collisionRadius = (Math.Max(t.Width, t.Height) * imageScale) / 2f;
        }

        public float getDamage()
        {
            return damage;
        }

        public float getMaxDistance()
        {
            return maxDistance;
        }

        public virtual void decHealth()
        {
            --health;
        }

        public override bool isDead()
        {
            return (health == 0 || distanceTraveled > maxDistance);
        }

        public virtual void setVelocity(Vector2 v, Vector2 dir, int acc)
        {
            double currAngle = Math.Atan2(dir.Y, dir.X);
            Random rand = new Random();
            double offset = rand.Next(-acc, acc);
            offset *= (Math.PI / 180);
            currAngle += offset;
            dir.X = (float)Math.Cos(currAngle);
            dir.Y = (float)Math.Sin(currAngle);
            rotateAngle = currAngle + Math.PI/2;
            if (rotateAngle < 0)
                rotateAngle += 2 * Math.PI;
            
            velocity = v + (dir * maxVelocity);
        }

        public void homing(GameTime gameTime, Vector2 playerPos)
        {
            recalcTarget += gameTime.ElapsedGameTime.Milliseconds;

            if (homingMode && recalcTarget > Defines.HOMING_TIMER)
            {
                Vector2 dir = playerPos - position;
                dir.Normalize();
                velocity = dir * maxVelocity;
                recalcTarget = 0;
            }
        }

        public override void Update(GameTime gameTime)
        {
            float time = (float)(gameTime.ElapsedGameTime.Milliseconds / 1000f);
            if (homingMode && lastUpdate > 200)
            {
                lastUpdate = 0;
                Random r = new Random();
                Vector2 dir = playerPosition - position;
                dir.Normalize();
                velocity = dir * maxVelocity;
            }
            lastUpdate += gameTime.ElapsedGameTime.Milliseconds;
            position += velocity * time;
            distanceTraveled += velocity.Length() * time;

            if (rotateCount > Defines.PROJECTILE_ROTATE_COUNTER)
            {
                double currAngle = Math.Atan2(velocity.Y, velocity.X);
                rotateAngle = currAngle + Math.PI / 2;
                if (rotateAngle < 0)
                    rotateAngle += 2 * Math.PI;
            }
            else
            {
                rotateCount += gameTime.ElapsedGameTime.Milliseconds;
            }

            //fixVelocity(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 cameraPosition)
        {
            Vector2 origin = new Vector2((sprite.Width / 2), (sprite.Height / 2));

            Vector2 tempVector = position - (Vector2)cameraPosition;
            Vector2 screenOrigin = new Vector2(screenSize.X / 2, screenSize.Y / 2);
            Vector2 drawLocation = tempVector + screenOrigin;

            float radius = collisionDist();

            if (drawLocation.X + radius >= 0 && drawLocation.Y + radius >= 0 && drawLocation.X + radius <= screenSize.X + sprite.Width && drawLocation.Y + radius <= screenSize.Y + sprite.Height)
                spriteBatch.Draw(sprite, drawLocation, null, Color.White, (float)rotateAngle, origin, imageScale, SpriteEffects.None, 0);
        }

        public bool inHomingMode()
        {
            return homingMode;
        }

        public void setHomingMode(bool h)
        {
            homingMode = h;
        }

        public void setStats(float h, float m, float v, float gM, float p)
        {
            health = h;
            mass = m;
            maxVelocity = v;
            gravityMultiplier = gM;
            damage = p;
        }
    }
}
