﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class Weapon
    {
        protected Projectile projectile;
        protected float cooldown, currentCooldown, recoil;
        protected int accuracy;
        protected Vector2 offset;
        protected Texture2D sprite;
        protected float imageScale, volume;
        protected SoundEffect effect;

        public Weapon(Weapon w, Vector2 o)
        {
            projectile = w.projectile;
            cooldown = w.cooldown;
            currentCooldown = w.cooldown; 
            recoil = w.recoil;
            accuracy = w.accuracy;
            sprite = w.sprite;
            imageScale = w.imageScale;
            effect = w.effect;
            volume = w.volume;
            offset = o;
        }

        public Weapon(WeaponData d, Texture2D wT, float wTS, Texture2D pT, float pTS, SoundEffect e, float v, Projectile proj)
        {
            offset = Vector2.Zero;
            cooldown = 1000/d.weaponFireRate;
            currentCooldown = 0;
            recoil = d.weaponRecoil;
            accuracy = d.accuracy;
            projectile = proj;
            projectile.assignTexture(pT, pTS);
            assignTexture(wT, wTS);
            effect = e;
            volume = v * 0.4f;
        }

        /*public TestWeapon(Vector2 o)
        {
            offset = o;
            cooldown = 1000 / Defines.WEAPON_FIRERATE;
            currentCooldown = 0;
            recoil = Defines.WEAPON_RECOIL;
            accuracy = Defines.WEAPON_ACCURACY;
        }*/

        public void assignTexture(Texture2D t, float iS)
        {
            sprite = t;
            imageScale = iS;
        }

        public virtual Projectile fire(Vector2 p, Vector2 v, Vector2 dir, SpaceObject s1, bool sound)
        {
            //Creates a new projectile with a velocity of v + the projectile's velocity and returns it
            if (currentCooldown <= 0)
            {
                if (dir != Vector2.Zero)
                    dir.Normalize();
                currentCooldown = cooldown;
                Projectile proj;
                proj = new Projectile(projectile);
                proj.setPosition(p + offset);
                proj.setVelocity(v, dir, accuracy);
                s1.setVelocity(s1.getVelocity() - dir * recoil);
                if(sound)
                    effect.Play(volume, 0.0f, 0.0f);

                return proj;
            }
            return null;
        }

        public void Update(GameTime gameTime)
        {
            if (currentCooldown > 0)
                currentCooldown -= gameTime.ElapsedGameTime.Milliseconds;
        }

        public float projectileDist()
        {
            return projectile.getMaxDistance();
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 drawLocation, float podRotation)
        {
            Vector2 origin = Vector2.Zero;
            origin = new Vector2((sprite.Width / 2f), (sprite.Height / 2f) + (20 * imageScale));
            spriteBatch.Draw(sprite, new Vector2(drawLocation.X, drawLocation.Y), null, Color.White, podRotation, origin, imageScale, SpriteEffects.None, 0);
        }

        public Texture2D getSprite()
        {
            return sprite;
        }

        public void setHomingMode(bool b)
        {
            projectile.setHomingMode(b);
        }
    }
}
