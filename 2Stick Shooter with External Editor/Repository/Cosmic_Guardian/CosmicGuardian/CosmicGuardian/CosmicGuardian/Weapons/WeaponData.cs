﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    enum WeaponType { Player, Enemy }

    class WeaponData
    {
        public String weaponID;
        public String weaponName;
        public Int32 weaponPower;
        public float weaponFireRate;
        public Int32 shotVelocity;
        public Int32 weaponRecoil;
        public Int32 shotMass;
        public float shotGravityMultiplier;
        public Int32 accuracy;
        public WeaponType weaponType;
        public String gunSpriteFilename;
        public String shotSpriteFilename;
        public String weaponSoundEffectFileName;
        public Int32 shotHealth;

        public WeaponData( String weaponID,
         String weaponName,
         int weaponPower,
         float weaponFireRate,
         int shotVelocity,
         int weaponRecoil,
         int shotMass,
         float shotGravityMultiplier,
         int accuracy,
         WeaponType weaponType,
         String gunSpriteFilename,
         String shotSpriteFilename,
         String weaponSoundEffectFileName,
         int shotHealth)
        {
          this.weaponID=weaponID;
          this.weaponName=weaponName;
          this.weaponPower=weaponPower;
            this.weaponFireRate=weaponFireRate;
            this.shotVelocity=shotVelocity;
            this.weaponRecoil=weaponRecoil;
            this.shotMass=shotMass;
            this.shotGravityMultiplier=shotGravityMultiplier;
            this.accuracy=accuracy;
            this.weaponType=weaponType;
            this.gunSpriteFilename=gunSpriteFilename;
            this.shotSpriteFilename=shotSpriteFilename;
            this.weaponSoundEffectFileName=weaponSoundEffectFileName;
            this.shotHealth=shotHealth;

        }
        public WeaponData()
        {
        }
    }
}
