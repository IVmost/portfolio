﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    //Main menu class
    public class MainMenu
    {
        Texture2D background;
        Texture2D gameEmblem;
        Texture2D buttonUp;
        Texture2D buttonDown;
        Texture2D buttonNew;
        Texture2D buttonUpgrade;
        Texture2D buttonOptions;
        Texture2D buttonCredits;
        Texture2D aButtonImage;
        Texture2D dPadButtonImage;
        SpriteFont font;
        SpriteBatch spriteBatch;

        bool buttonNew_Activate;
        bool buttonUpgrade_Activate;
        bool buttonOptions_Activate;
        bool buttonCredits_Activate;

        bool downPressed;
        bool upPressed;
        bool optionsPressed;
        bool levelPressed;
        bool upgradePressed;

        public bool displayGame;
        public bool displayMainMenu;
        public bool displayCreditScreen;
        public bool displayOptionsMenu;
        public bool displayLevelSelect;
        public bool displayUpgradeMenu;

        int screenWidth;
        int screenHeight;

        private SoundEffect navSound;
        public bool playSounds;

        public int levelNumber, levelUnlocked;

        //Main menu constructor
        public MainMenu(Texture2D e, Texture2D u, Texture2D d, Texture2D b, Texture2D aButton, Texture2D dPadButton, int width, int height, SpriteFont f, SpriteBatch batch, SoundEffect sound)
        {
            aButtonImage = aButton;
            dPadButtonImage = dPadButton;
            gameEmblem = e;
            buttonUp = u;
            buttonDown = d;
            background = b;
            screenWidth = width;
            screenHeight = height;
            font = f;
            buttonNew = buttonUp;
            buttonNew_Activate = true;
            buttonUpgrade = buttonDown;
            buttonUpgrade_Activate = false;
            buttonOptions = buttonDown;
            buttonOptions_Activate = false;
            buttonCredits = buttonDown;
            buttonCredits_Activate = false;

            downPressed = false;
            upPressed = false;
            optionsPressed = false;
            levelPressed = false;
            upgradePressed = false;

            spriteBatch = batch;

            displayGame = false;
            displayMainMenu = true;
            displayCreditScreen = false;
            displayOptionsMenu = false;
            displayLevelSelect = false;
            displayUpgradeMenu = false;
            navSound = sound;
            playSounds = true;
            levelNumber = 1;
            levelUnlocked = 1;
        }

        //Updates the main menu
        public virtual void Update(GameTime gameTime)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);


            if (i.menuNavigate.Down == ButtonState.Released)
            {
                downPressed = false;
            }

            if (i.menuNavigate.Up == ButtonState.Released)
            {
                upPressed = false;
            }

            //Allows for navigation of the main menu
            if (buttonNew_Activate)
            {
                buttonNew = buttonUp;
                buttonUpgrade = buttonDown;
                buttonOptions = buttonDown;
                buttonCredits = buttonDown;

                if ((i.menuNavigate.Down == ButtonState.Pressed) && (downPressed == false))
                {
                    buttonNew_Activate = false;
                    buttonUpgrade_Activate = true;
                    downPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
                if ((i.menuNavigate.Up == ButtonState.Pressed) && (upPressed == false))
                {
                    buttonNew_Activate = false;
                    buttonCredits_Activate = true;
                    upPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
            }
            else if (buttonUpgrade_Activate)
            {
                buttonNew = buttonDown;
                buttonUpgrade = buttonUp;
                buttonOptions = buttonDown;
                buttonCredits = buttonDown;
                if ((i.menuNavigate.Down == ButtonState.Pressed) && (downPressed == false))
                {
                    buttonUpgrade_Activate = false;
                    buttonOptions_Activate = true;
                    downPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
                if ((i.menuNavigate.Up == ButtonState.Pressed) && (upPressed == false))
                {
                    buttonUpgrade_Activate = false;
                    buttonNew_Activate = true;
                    upPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
            }
            else if (buttonOptions_Activate)
            {
                buttonNew = buttonDown;
                buttonUpgrade = buttonDown;
                buttonOptions = buttonUp;
                buttonCredits = buttonDown;
                if ((i.menuNavigate.Down == ButtonState.Pressed) && (downPressed == false))
                {
                    buttonOptions_Activate = false;
                    buttonCredits_Activate = true;
                    downPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
                if ((i.menuNavigate.Up == ButtonState.Pressed) && (upPressed == false))
                {
                    buttonOptions_Activate = false;
                    buttonUpgrade_Activate = true;
                    upPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
            }
            else if (buttonCredits_Activate)
            {
                buttonNew = buttonDown;
                buttonUpgrade = buttonDown;
                buttonOptions = buttonDown;
                buttonCredits = buttonUp;
                if ((i.menuNavigate.Down == ButtonState.Pressed) && (downPressed == false))
                {
                    buttonCredits_Activate = false;
                    buttonNew_Activate = true;
                    downPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
                if ((i.menuNavigate.Up == ButtonState.Pressed) && (upPressed == false))
                {
                    buttonCredits_Activate = false;
                    buttonOptions_Activate = true;
                    upPressed = true;
                    if (playSounds)
                        navSound.Play();
                }
            }

            //Switches to the appropriate menu
            if (buttonNew_Activate)
            {
                if (!i.enterButton)
                    displayGame = false;
                else
                    levelPressed = true;

                if (!i.enterButton && levelPressed)
                {
                    displayLevelSelect = true;
                    displayMainMenu = false;
                    levelPressed = false;
                }
            }
            else if (buttonUpgrade_Activate)
            {
                if (!i.enterButton)
                    displayGame = false;
                else
                    upgradePressed = true;

                if (!i.enterButton && upgradePressed)
                {
                    displayUpgradeMenu = true;
                    displayMainMenu = false;
                    upgradePressed = false;
                }
            }
            else if (buttonOptions_Activate)
            {
                if (!i.enterButton)
                    displayGame = false;
                else
                    optionsPressed = true;

                if (!i.enterButton && optionsPressed)
                {
                    displayOptionsMenu = true;
                    displayMainMenu = false;
                    optionsPressed = false;
                }
            }
            else if (buttonCredits_Activate)
            {
                if (!i.enterButton)
                {
                    displayGame = false;
                }
                else
                {
                    displayCreditScreen = true;
                    displayMainMenu = false;
                }
            }
        }

        //Draws the main menu
        public virtual void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            spriteBatch.DrawString(font, "PLAY GAME", new Vector2(330*Defines.Screen_Width_Scaler, 170*Defines.Screen_height_Scaler + buttonNew.Height / 2), Color.White);
            spriteBatch.DrawString(font, "UPGRADES", new Vector2(330 * Defines.Screen_Width_Scaler, buttonNew.Height + 240 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font, "OPTIONS", new Vector2(330 * Defines.Screen_Width_Scaler, buttonNew.Height * 2 + 265 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font, "CREDITS", new Vector2(330 * Defines.Screen_Width_Scaler, buttonNew.Height * 3 + 300 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font, "Select", new Vector2(80, screenHeight - 57 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.DrawString(font, "Navigate", new Vector2(80, screenHeight - 135 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(buttonNew, new Vector2(430 * Defines.Screen_Width_Scaler, 180 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(buttonUpgrade, new Vector2(430 * Defines.Screen_Width_Scaler, buttonNew.Height + 210 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(buttonOptions, new Vector2(430 * Defines.Screen_Width_Scaler, buttonNew.Height * 2 + 240 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(buttonCredits, new Vector2(430 * Defines.Screen_Width_Scaler, buttonNew.Height * 3 + 270 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(gameEmblem, new Vector2((((screenWidth / 2) - (gameEmblem.Width / 2) * Defines.Screen_Width_Scaler) + 130 * Defines.Screen_Width_Scaler), (screenHeight / 2) - (gameEmblem.Height / 2)), new Rectangle(0, 0, (int)(background.Width * Defines.Screen_Width_Scaler), (int)(background.Height * Defines.Screen_height_Scaler)), Color.White, 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
            spriteBatch.Draw(aButtonImage, new Vector2(5 * Defines.Screen_Width_Scaler, screenHeight - 75 * Defines.Screen_height_Scaler), Color.White);
            spriteBatch.Draw(dPadButtonImage, new Vector2(5 * Defines.Screen_Width_Scaler, screenHeight - 150 * Defines.Screen_height_Scaler), new Rectangle(0, 0, (dPadButtonImage.Width), (dPadButtonImage.Height)), Color.White, 0.0f, new Vector2(0, 0), 0.1f, SpriteEffects.None, 0);
        }
    }
}
