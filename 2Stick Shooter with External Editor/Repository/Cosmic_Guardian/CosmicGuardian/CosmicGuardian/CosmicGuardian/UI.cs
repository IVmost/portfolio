﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class UI
    {
        Texture2D background, dot, weaponNode, shieldBar, shieldFrame, whiteTexture;
        Color shieldBarColor;
        int fade;
        Boolean drawMinimap, minimapLatch;
        Rectangle view;
        SpriteFont font;
        int score;

        public void drawRectangle(SpriteBatch aSpriteBatch, Rectangle rect, Color aColor)
        {
            aSpriteBatch.Draw(whiteTexture, new Rectangle(rect.Left, rect.Top, rect.Width, 1), aColor);
            aSpriteBatch.Draw(whiteTexture, new Rectangle(rect.Left, rect.Bottom, rect.Width, 1), aColor);
            aSpriteBatch.Draw(whiteTexture, new Rectangle(rect.Left, rect.Top, 1, rect.Height), aColor);
            aSpriteBatch.Draw(whiteTexture, new Rectangle(rect.Right, rect.Top, 1, rect.Height), aColor);
        }

        public UI(Texture2D b, Texture2D d, Texture2D node, Texture2D bar, Texture2D frame, Texture2D w, SpriteFont f)
        {
            background = b;
            dot = d;
            weaponNode = node;
            shieldBar = bar;
            shieldFrame = frame;
            shieldBarColor = new Color(0, 191, 255, 255);
            fade = 255;
            drawMinimap = true;
            minimapLatch = false;
            whiteTexture = w;
            view = new Rectangle(0, 0, 1, 1);
            font = f;
        }

        public void draw(PlayerShip player, List<Enemy> enemies, Vector2 size, Vector2 screenSize, SpriteBatch spriteBatch)
        {
            Vector2 temp = Vector2.Zero;
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);

            if (i.minimapButton && !minimapLatch)
            {
                drawMinimap = !drawMinimap;
                minimapLatch = true;
            }

            if (!i.minimapButton)
            {
                minimapLatch = false;
            }

            if (drawMinimap)
            {
                temp = new Vector2(Defines.MINIMAP_WIDTH / background.Width, Defines.MINIMAP_HEIGHT / background.Height);
                spriteBatch.Draw(background, new Vector2(screenSize.X - Defines.MINIMAP_WIDTH, screenSize.Y - Defines.MINIMAP_HEIGHT),
                                 null, new Color(255, 255, 255, 200), 0, Vector2.Zero, temp, SpriteEffects.None, 0);

                temp.X = player.getPosition().X / size.X;
                temp.Y = player.getPosition().Y / size.Y;
                temp.X *= Defines.MINIMAP_WIDTH;
                temp.Y *= Defines.MINIMAP_HEIGHT;
                temp.X = screenSize.X - Defines.MINIMAP_WIDTH + temp.X;
                temp.Y = screenSize.Y - Defines.MINIMAP_HEIGHT + temp.Y;

                int boxX, boxY;
                boxX = (int)((screenSize.X/size.X) * Defines.MINIMAP_WIDTH);
                boxY = (int)((screenSize.Y/size.Y) * Defines.MINIMAP_HEIGHT);

                spriteBatch.Draw(dot, temp, null, Color.LimeGreen, 0f, new Vector2(dot.Width / 2, dot.Height / 2), 0.2f, SpriteEffects.None, 0);

                if (((temp.X + boxX / 2) < screenSize.X - 10) && ((temp.Y + boxY / 2) < screenSize.Y - 10))
                    view = new Rectangle((int)Math.Max(screenSize.X - Defines.MINIMAP_WIDTH, (temp.X - boxX / 2)), (int)Math.Max(screenSize.Y - Defines.MINIMAP_HEIGHT, (temp.Y - boxY / 2)), boxX, boxY);

                if (((temp.X + boxX / 2) > screenSize.X - 10) && ((temp.Y + boxY / 2) < screenSize.Y - 10))
                    view = new Rectangle((int)Math.Min(screenSize.X - boxX, (temp.X - boxX / 2)), (int)Math.Max(screenSize.Y - Defines.MINIMAP_HEIGHT, (temp.Y - boxY / 2)), boxX, boxY);

                if (((temp.X + boxX / 2) < screenSize.X - 10) && ((temp.Y + boxY / 2) > screenSize.Y - 10))
                    view = new Rectangle((int)Math.Max(screenSize.X - Defines.MINIMAP_WIDTH, (temp.X - boxX / 2)), (int)Math.Min(screenSize.Y - boxY, (temp.Y - boxY / 2)), boxX, boxY);

                if (((temp.X + boxX / 2) > screenSize.X - 10) && ((temp.Y + boxY / 2) > screenSize.Y))
                    view = new Rectangle((int)Math.Min(screenSize.X - boxX, (temp.X - boxX / 2)), (int)Math.Min(screenSize.Y - boxY, (temp.Y - boxY / 2)), boxX, boxY);
                //view = new Rectangle((int)Math.Max(screenSize.X - Defines.MINIMAP_WIDTH, (temp.X - boxX / 2)), (int)Math.Max(screenSize.Y - Defines.MINIMAP_HEIGHT, (temp.Y - boxY / 2)), (int)(boxX - Math.Max(0, ((screenSize.X - Defines.MINIMAP_WIDTH) - (temp.X - boxX / 2)))), (int)(boxY - Math.Max(0, ((screenSize.Y - Defines.MINIMAP_HEIGHT) - (temp.Y - boxY / 2)))));

                drawRectangle(spriteBatch, view, Color.White);

                foreach (Enemy e in enemies)
                {
                    temp.X = e.getPosition().X / size.X;
                    temp.Y = e.getPosition().Y / size.Y;
                    temp.X *= Defines.MINIMAP_WIDTH;
                    temp.Y *= Defines.MINIMAP_HEIGHT;
                    temp.X = screenSize.X - Defines.MINIMAP_WIDTH + temp.X;
                    temp.Y = screenSize.Y - Defines.MINIMAP_HEIGHT + temp.Y;

                    if (e is EnemyMiniBoss || e is EnemyMothership)
                    {
                        spriteBatch.Draw(dot, temp, null, Color.DarkRed, 0f, new Vector2(dot.Width / 2, dot.Height / 2), 0.3f, SpriteEffects.None, 0);
                    }
                    else
                    {
                        spriteBatch.Draw(dot, temp, null, Color.Red, 0f, new Vector2(dot.Width / 2, dot.Height / 2), 0.2f, SpriteEffects.None, 0);
                    }
                }
            }

            Texture2D selectedWeaponSprite = player.getWeaponImage();

            if (player.getPosition().X < 300)
            {
                if (player.getPosition().Y < 100)
                    fade = 0;
                else
                    fade = 255;
            }
            else
            {
                fade = 255;
            }

            if (player.getShields() > 0.0f)
            {
                if ((player.getShields() / player.getMaxShields()) >= 0.5f)
                {
                    shieldBarColor = new Color(0, 191, 255, fade);
                }
                else if (((player.getShields() / player.getMaxShields()) >= 0.25f) && ((player.getShields() / player.getMaxShields()) < 0.5f))
                {
                    shieldBarColor = new Color(255, 215, 0, fade);
                }
                else if (((player.getShields() / player.getMaxShields()) > 0.0f) && ((player.getShields() / player.getMaxShields()) < 0.25f))
                {
                    shieldBarColor = new Color(255, 0, 0, fade);
                }
            }

            Vector2 shieldFrameLocation = new Vector2(20, 20);
            Vector2 weaponNodeLocation = new Vector2((47 + shieldFrameLocation.X - weaponNode.Width / 2), (shieldFrameLocation.Y + 1 - weaponNode.Height / 2));
            spriteBatch.Draw(shieldBar, new Rectangle(71 + (int)shieldFrameLocation.X, 6 + (int)shieldFrameLocation.Y, (int)(shieldBar.Width * (player.getShields() / player.getMaxShields())), shieldBar.Height), new Rectangle(0, 0, shieldBar.Width, shieldBar.Height), shieldBarColor, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0);
            spriteBatch.Draw(shieldFrame, shieldFrameLocation, new Rectangle(0, 0, shieldFrame.Width, shieldFrame.Height), new Color(255, 255, 255, fade), 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
            spriteBatch.Draw(weaponNode, weaponNodeLocation, new Rectangle(0, 0, weaponNode.Width, weaponNode.Height), new Color(0, 0, 255, fade), 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
            spriteBatch.Draw(selectedWeaponSprite, new Vector2(shieldFrameLocation.X + 46 - selectedWeaponSprite.Width / 2, shieldFrameLocation.Y + 18), new Rectangle(0, 0, selectedWeaponSprite.Width, selectedWeaponSprite.Height), new Color(255, 255, 255, fade), 0.0f, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, "Score: " + score, new Vector2(91 + (int)shieldFrameLocation.X, 26 + (int)shieldFrameLocation.Y), new Color(255, 255, 255, fade));
        }

        public int getScore()
        {
            return score;
        }

        public void setScore(int s)
        {
            score = s;
        }
    }
}
