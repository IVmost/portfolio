﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class PlayerShip : SpaceObject
    {
        private static float acceleration, accelerationBoost, maxVelocityBoost, shields, maxShields, shieldRegen, shieldDisableTime, 
                             shieldDisableCounter, boostTime, boostMaxTime, boostRegen;
        private static Texture2D shipBody, frame, engineStop, engineSlow, engineFast, engineFastest, engineBoost, shield;
        private static Texture2D currentEngine;
        Vector2 aimDirection;

        private static float imageScale, collisionRadius;

        private float engineRotation, podRotation;

        private static List<Weapon> weapons;
        private int currentWeaponIndex;

        private Boolean nextWeaponLatch, previousWeaponLatch, boostLatch;

        private Projectile firedProjectile;

        public Projectile getProjectile()
        {
            return firedProjectile;
        }

        public void resetProjectile()
        {
            firedProjectile = null;
        }
        private Color shieldColor;

        //Constructors for the player's ship

        private static Engine enginePart;
        private static Shields shieldsPart;

        private static SoundEffect shieldLow, shieldCharging;
        private int shieldLowCount;
        private bool shieldChargeLatch;

        public PlayerShip()
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            aimDirection = new Vector2(0, -1);

            setEquipment(new DefaultEngine(), new DefaultShields());

            mass = Defines.PLAYER_MASS;
            gravityMultiplier = Defines.PLAYER_GRAVITY_MULTIPLIER;

            if(weapons == null)
                weapons = new List<Weapon>();
            currentWeaponIndex = 0;

            nextWeaponLatch = true;
            previousWeaponLatch = true;

            engineRotation = 0;
            podRotation = 0;

            shieldColor = new Color(0, 191, 255);

            shieldLowCount = 60;
            shieldChargeLatch = true;
        }

        //Used to assign textures to this ship type. Can be modified later if a ship will be mode from multiple parts. 
        //Used so that code is properly seperated
        public void assignTexture(Texture2D sB, Texture2D f, Texture2D eS, Texture2D eSl, Texture2D eF, Texture2D eFa, Texture2D eB, Texture2D sH, Texture2D sFrame, Texture2D sBar, Texture2D wNode, float s)
        {
            shipBody = sB;
            frame = f;
            engineStop = eS;
            engineSlow = eSl;
            engineFast = eF;
            engineFastest = eFa;
            engineBoost = eB;
            currentEngine = eS;
            imageScale = s;
            shield = sH;
            collisionRadius = (Math.Max(frame.Width, frame.Height) * imageScale) / 2f; //Note: This can be changed later depending on what parts the player ship will have
        }

        public void addWeapon(Weapon w)
        {
            weapons.Add(w);
        }

        public override void Update(GameTime gameTime)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);
            float time = (float)(gameTime.ElapsedGameTime.Milliseconds / 1000f); //Converts milliseconds to seconds
            weapons[currentWeaponIndex].Update(gameTime);
            boostTime = boostTime + 16.6f;
            shieldLowCount++;

            Vector2 movement = i.movement;
            //Stores engineRotation in a float to save it before rotation is called. The reason is because rotation returns a value that is
            //not a number at times. This is to ensure that if this occurs then the engine's last position is saved and returned to
            //engineRotation in order to ensure that the engines are still drawn.
            float tempRotation = engineRotation;
            engineRotation = rotation(movement.X, -movement.Y);
            if (float.IsNaN(engineRotation))
                engineRotation = tempRotation;

            tempRotation = podRotation;
            podRotation = rotation(i.aim.X, -i.aim.Y);
            if (float.IsNaN(podRotation))
            {
                podRotation = tempRotation;
            }
            else
            {
                aimDirection = i.aim;
            }

            if ((i.afterburner) && (boostTime > (boostMaxTime + boostRegen)) && (!boostLatch))
            {
                maxVelocity = maxVelocityBoost;
                acceleration = accelerationBoost;
                boostTime = 0.0f;
                boostLatch = true;
            }

            if ((boostLatch) && (boostTime > boostMaxTime))
            {
                maxVelocity = enginePart.getMaxVelocity();
                acceleration = enginePart.getMaxAcceleration();
            }

            if ((!i.afterburner) && (boostTime > boostMaxTime))
            {
                boostLatch = false;
            }

            velocity += movement * (acceleration * time);
            fixVelocity(gameTime); //Prevents the velocity from going over its maximum while still preserving the correct direction
            position += velocity * time;

            float length = movement.Length();

            if (length == 0)
            {
                currentEngine = engineStop;
            }
            else if (length < 0.5f)
            {
                currentEngine = engineSlow;
            }
            else if (length < 0.8f)
            {
                currentEngine = engineFast;
            }
            else
            {
                currentEngine = engineFastest;
            }

            if (boostTime < boostMaxTime)
            {
                currentEngine = engineBoost;
            }

            if (i.primaryFire)
            {
                firedProjectile = weapons[currentWeaponIndex].fire(position, velocity, aimDirection, this, Defines.SOUND_EFFECTS);
            }

            //Update the shields
            if (shields > 0.0f)
            {
                if ((shields / maxShields) >= 0.5f)
                {
                    shieldColor = new Color(0, 191, 255);
                }
                else if (((shields / maxShields) >= 0.25f) && ((shields / maxShields) < 0.5f))
                {
                    shieldColor = new Color(255, 215, 0);
                }
                else if (((shields / maxShields) > 0.0f) && ((shields / maxShields) < 0.25f))
                {
                    shieldColor = new Color(255, 0, 0);
                    if (shieldLowCount > 45 && Defines.SOUND_EFFECTS)
                    {
                        shieldLow.Play();
                        shieldLowCount = 0;
                    }
                }
            }

            if (shieldDisableCounter > 0)
            {
                shieldDisableCounter -= gameTime.ElapsedGameTime.Milliseconds;
                shieldChargeLatch = false;
            }
            
            if (shields < maxShields && shieldDisableCounter <= 0 && shields >= 0)
            {
                shields += (shieldRegen * time);
                if (!shieldChargeLatch && ((shields / maxShields) < 0.5) && Defines.SOUND_EFFECTS)
                {
                    shieldCharging.Play();
                    shieldChargeLatch = true;
                }
            }

            if (shields >= maxShields)
            {
                shields = maxShields;
            }

            //Updates the selected weapon when next is pressed
            if (i.nextWeapon && nextWeaponLatch && previousWeaponLatch)
            {
                if (currentWeaponIndex < (weapons.Count - 1))
                    currentWeaponIndex++;
                else
                    currentWeaponIndex = 0;
                nextWeaponLatch = false;
            }

            if (!i.nextWeapon)
                nextWeaponLatch = true;

            //Updates the selected weapon when previous is pressed
            if (i.previousWeapon && previousWeaponLatch && nextWeaponLatch)
            {
                if (currentWeaponIndex > 0)
                    currentWeaponIndex--;
                else
                    currentWeaponIndex = (weapons.Count - 1);
                previousWeaponLatch = false;
            }

            if (!i.previousWeapon)
                previousWeaponLatch = true;
        }

        //Function used to calculate the rotation of the body and engines of the ship
        //Takes as Parameters: the x and y values of the corresponding joystick
        public float rotation(float x, float y)
        {
            double amount, radians, degrees;
            Vector2 baseVector = new Vector2(0, 1);

            amount = (x * baseVector.X + y * baseVector.Y) / (Math.Sqrt((x * x) + (y * y)) * Math.Sqrt((baseVector.X * baseVector.X) + (baseVector.Y * baseVector.Y)));
            degrees = Math.Acos(amount);

            if (x < 0.0f)
            {
                radians = Math.PI + (Math.PI - degrees);
                return (float)radians;
            }
            else
            {
                return (float)degrees;
            }
        }

        public override float collisionDist()
        {
            if(shields > 0)
                return collisionRadius;
            return ((Math.Max(shipBody.Width, shipBody.Height) * imageScale) / 2f);
        }

        //Draws the ship at the center of the screen
        public void draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 cameraPosition)
        {
            //Vector2 drawLocation = new Vector2(screenSize.X / 2f, screenSize.Y / 2f);
            //drawLocation += objectOffset;

            Vector2 tempVector = position - (Vector2)cameraPosition;
            Vector2 screenOrigin = new Vector2(screenSize.X / 2, screenSize.Y / 2);
            Vector2 drawLocation = tempVector + screenOrigin;

            float radius = collisionDist();

            if (drawLocation.X + radius >= 0 && drawLocation.Y + radius >= 0 && drawLocation.X + radius <= screenSize.X + frame.Width && drawLocation.Y + radius <= screenSize.Y + frame.Height)
            {
                weapons[currentWeaponIndex].Draw(spriteBatch, screenSize, drawLocation, podRotation);

                Vector2 origin = new Vector2((engineStop.Width / 2f), 14);
                float frameWidth = frame.Width - 4;
                float frameHeight = frame.Height - 4;

                spriteBatch.Draw(currentEngine, drawLocation - new Vector2((frameWidth/ 2f) * imageScale, (frameHeight/ 2f) * imageScale), null, Color.White,
                                 engineRotation, origin, imageScale, SpriteEffects.None, 0);
                spriteBatch.Draw(currentEngine, drawLocation + new Vector2((frameWidth / 2f) * imageScale, (frameHeight / 2f) * imageScale), null, Color.White,
                                 engineRotation, origin, imageScale, SpriteEffects.None, 0);
                spriteBatch.Draw(currentEngine, drawLocation - new Vector2((frameWidth / 2f) * imageScale, 0) + new Vector2(0, (frameHeight / 2f) * imageScale),
                                 null, Color.White, engineRotation, origin, imageScale, SpriteEffects.None, 0);
                spriteBatch.Draw(currentEngine, drawLocation + new Vector2((frameWidth / 2f) * imageScale, 0) - new Vector2(0, (frameHeight / 2f) * imageScale),
                                 null, Color.White, engineRotation, origin, imageScale, SpriteEffects.None, 0);

                origin = new Vector2((frame.Width / 2f), (frame.Height / 2f));
                spriteBatch.Draw(frame, drawLocation, null, Color.White, 0, origin, imageScale, SpriteEffects.None, 0);

                origin = new Vector2((shipBody.Width / 2f), (shipBody.Height / 2f) + (4.5f * imageScale));
                spriteBatch.Draw(shipBody, drawLocation, null, Color.White, podRotation, origin, imageScale, SpriteEffects.None, 0);
            }


            if (!this.isDead())
                spriteBatch.Draw(shield, new Vector2(drawLocation.X - ((shield.Width / 2) * (imageScale * 0.95f)), drawLocation.Y - ((shield.Height / 2) * (imageScale * 0.95f))), 
                                 new Rectangle(0, 0, shield.Width, shield.Height), shieldColor, 0.0f, new Vector2(0, 0), (imageScale * 0.95f), SpriteEffects.None, 0);
        }

        public override bool isDead()
        {
            return (shields < 0);
        }

        /*public void decShields(float f)
        {
            if (shields > 0)
            {
                shields -= f;
                shields = Math.Max(0, shields);
                shieldDisableCounter = shieldDisableTime;
            }
            else
            {
                shields = -1;
            }
        }*/

        public override void decHealth(float dec)
        {
            if (shields > 0)
            {
                shields -= dec;
                shields = Math.Max(0, shields);
                shieldDisableCounter = shieldDisableTime;
            }
            else
            {
                shields = -1;
            }
        }

        public float getShields()
        {
            return shields;
        }

        public float getMaxShields()
        {
            return maxShields;
        }

        public float getAcceleration()
        {
            return acceleration;
        }

        public Texture2D getWeaponImage()
        {
            return weapons[currentWeaponIndex].getSprite();
        }

        public void setShieldLowSound(SoundEffect s)
        {
            shieldLow = s;
        }

        public void setShieldChargeSound(SoundEffect s)
        {
            shieldCharging = s;
        }

        public void deleteWeapons()
        {
            weapons = new List<Weapon>();
        }

        public void setEquipment(Engine e, Shields s)
        {
            enginePart = e;
            shieldsPart = s;

            acceleration = enginePart.getMaxAcceleration();
            accelerationBoost = enginePart.getMaxAccelerationBoost();
            maxVelocity = enginePart.getMaxVelocity();
            maxVelocityBoost = enginePart.getMaxVelocityBoost();
            boostMaxTime = enginePart.getBoostTime();
            boostTime = boostMaxTime;
            boostRegen = enginePart.getBoostRegen();

            maxShields = shieldsPart.getMaxShields();
            shields = shieldsPart.getMaxShields();
            shieldRegen = shieldsPart.getShieldRegen();
            shieldDisableTime = shieldsPart.getShieldDownTime();
            shieldDisableCounter = 0;
        }
    }
}
