﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    abstract class GravityObject : SpaceObject
    {
        protected float gravity, closeRadius, farRadius, health;

        public float getGravity()
        {
            return gravity;
        }

        public float getCloseRadius()
        {
            return closeRadius;
        }

        public float getFarRadius()
        {
            return farRadius;
        }

        public override void decHealth(float dec)
        {
            if(health > 0)
                health = Math.Max(0, health - dec);
        }

        public override bool isDead()
        {
            return (health == 0);
        }

        public void kill()
        {
            health = 0;
        }

        public abstract void Draw(SpriteBatch spriteBatch, Vector2 screenSize, Vector2 playerPosition);
    }
}
