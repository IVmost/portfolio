﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    static class Physics
    {
        //Checks to see if the ship has hit the side of a level. If it has the ship bounces off the edge with no reduction in velocity

        public static void checkIfInBounds(SpaceObject s, Gameworld l)
        {
            Vector2 position = s.getPosition();

            if (!(s is BigAsteroid || s is MediumAsteroid || s is SmallAsteroid))
            {
                Vector2 velocity = s.getVelocity();

                if (position.X < s.collisionDist())
                {
                    position.X = s.collisionDist();
                    s.setPosition(position);
                    velocity.X = -velocity.X;
                    s.setVelocity(velocity);
                }
                if (position.X > l.getSize().X - s.collisionDist())
                {
                    position.X = l.getSize().X - s.collisionDist();
                    s.setPosition(position);
                    velocity.X = -velocity.X;
                    s.setVelocity(velocity);
                }
                if (position.Y < s.collisionDist())
                {
                    position.Y = s.collisionDist();
                    s.setPosition(position);
                    velocity.Y = -velocity.Y;
                    s.setVelocity(velocity);
                }
                if (position.Y > l.getSize().Y - s.collisionDist())
                {
                    position.Y = l.getSize().Y - s.collisionDist();
                    s.setPosition(position);
                    velocity.Y = -velocity.Y;
                    s.setVelocity(velocity);
                }
            }
            else
            {
                if (position.X + s.collisionDist() < 0)
                {
                    ((GravityObject)s).kill();
                }
                if (position.X - s.collisionDist() > l.getSize().X)
                {
                    ((GravityObject)s).kill();
                }
                if (position.Y + s.collisionDist() < 0)
                {
                    ((GravityObject)s).kill();
                }
                if (position.Y - s.collisionDist() > l.getSize().Y)
                {
                    ((GravityObject)s).kill();
                }
            }
        }

        //Checks to see if two ships have collided. All ships have a circle for a hit box due to ease of collision detection and
        //the speed that a collision can be detected. At the moment all that happens is that the ships velocity is inverted but
        //a more logical system is being worked on

        public static void checkCollision(SpaceObject s1, SpaceObject s2)
        {
            
            float currentDist = Vector2.Distance(s1.getPosition(), s2.getPosition());
            float collisionDist = s1.collisionDist() + s2.collisionDist();

            Vector2 s1Velocity = s1.getVelocity();
            Vector2 s2Velocity = s2.getVelocity();

            if (currentDist <= collisionDist)
            {
                Physics.transferEnergy(s1, s2);
            }
        }

        private static void transferEnergy(SpaceObject s1, SpaceObject s2)
        {
            Vector2 s1VelocityOld, s2VelocityOld, s1VelocityNew, s2VelocityNew;
            float s1Mass, s2Mass;

            s1VelocityOld = s1.getVelocity();
            s2VelocityOld = s2.getVelocity();

            s1Mass = s1.getMass();
            s2Mass = s2.getMass();

            s1VelocityNew.X = (((s1Mass - s2Mass) / (s1Mass + s2Mass)) * s1VelocityOld.X) + (((2 * s2Mass)/(s1Mass + s2Mass)) * s2VelocityOld.X);
            s1VelocityNew.Y = (((s1Mass - s2Mass) / (s1Mass + s2Mass)) * s1VelocityOld.Y) + (((2 * s2Mass) / (s1Mass + s2Mass)) * s2VelocityOld.Y);

            s2VelocityNew.X = (((s2Mass - s1Mass) / (s2Mass + s1Mass)) * s2VelocityOld.X) + (((2 * s1Mass) / (s2Mass + s1Mass)) * s1VelocityOld.X);
            s2VelocityNew.Y = (((s2Mass - s1Mass) / (s2Mass + s1Mass)) * s2VelocityOld.Y) + (((2 * s1Mass) / (s2Mass + s1Mass)) * s1VelocityOld.Y);

            if (s1 is Projectile)
            {
                s2.decHealth(((Projectile)s1).getDamage());
                ((Projectile)s1).decHealth();
            }
            else if(s2 is EnemyKamikaze)
            {
                float s2HealthChange = (float)(Math.Pow(s2VelocityOld.X - s2VelocityNew.X, 2) + Math.Pow(s2VelocityOld.Y - s2VelocityNew.Y, 2)) * 0.0001f;
                s2.decHealth(s2HealthChange);
                s1.decHealth(Defines.KAMIKAZE_DAMAGE);
            }
            else if((s1 is EnemyTurret || s1 is EnemyMothership) && s2 is GravityObject)
            {
                ((GravityObject)s2).kill();
            }
            else
            {
                float s1HealthChange = (float)(Math.Pow(s1VelocityOld.X - s1VelocityNew.X, 2) + Math.Pow(s1VelocityOld.Y - s1VelocityNew.Y, 2)) * 0.0001f;
                float s2HealthChange = (float)(Math.Pow(s2VelocityOld.X - s2VelocityNew.X, 2) + Math.Pow(s2VelocityOld.Y - s2VelocityNew.Y, 2)) * 0.0001f;
                s1.decHealth(s1HealthChange);
                s2.decHealth(s2HealthChange);
            }

            if (!(s1 is EnemyTurret))
            {
                s1.setVelocity(s1VelocityNew);
                s2.setVelocity(s2VelocityNew);
            }
        }

        public static void applyGravity(GravityObject g, SpaceObject s, GameTime gameTime)
        {
            Vector2 dir = s.getPosition() - g.getPosition();
            if(dir != Vector2.Zero)
                dir.Normalize();
            Vector2 gravity = (dir * g.getGravity() * s.getGravityMultiplier()) * (gameTime.ElapsedGameTime.Milliseconds / 1000f);
            float percent = 1 - ((s.getPosition() - g.getPosition()).Length() - g.getCloseRadius())/g.getFarRadius();//(s.getPosition().Length() - g.getCloseRadius()) / (g.getFarRadius() - g.getCloseRadius());
            if (percent < 0)
                percent = 0;
            if (percent > 1)
                percent = 1;
            s.setVelocity(s.getVelocity() - (gravity * percent));
        }
    }
}
