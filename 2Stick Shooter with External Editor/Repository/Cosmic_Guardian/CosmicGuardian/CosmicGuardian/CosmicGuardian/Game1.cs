using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using wox.serial;

namespace CosmicGuardian
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    /// 

    

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D shipBody;
        Texture2D frame;
        Texture2D engineStop;
        Texture2D engineSlow;
        Texture2D engineFast;
        Texture2D engineFastest;
        Texture2D engineBoost;

        Texture2D shield;

        Texture2D shieldIndicatorFrame;
        Texture2D shieldBar;
        Texture2D weaponNode;

        Texture2D mainMenuBackground;
        Texture2D buttonA;
        Texture2D buttonBlue;
        Texture2D buttonGrey;
        Texture2D dPad;
        Texture2D emblem;

        Texture2D creditBackground;
        Texture2D bButton;

        Texture2D controls;

        Texture2D asteroid;

        Texture2D minimap;

        SpriteFont mainMenuFont, largeFont;

        MainMenu mainMenu;
        CreditScreen creditScreen;
        OptionsMenu optionsMenu;
        LevelSelect levelSelectMenu;
        UpgradeMenu upgradeMenu;

        //Levels
        Level1 Level1;
        Texture2D Level1Background;
        float Level1Scale = 1.5f;

        Level2 Level2;
        Texture2D Level2Background;
        float Level2Scale = 1.5f;

        Level3 Level3;
        Texture2D Level3Background;
        float Level3Scale = 1f;

        Level4 Level4;
        Texture2D Level4Background;
        float Level4Scale = 1.2f;

        Level5 Level5;
        Texture2D Level5Background;
        float Level5Scale = 1.2f;

        bool levelRunning;

        //Player SHips
        PlayerShip dummyPlayer;

        //Enemies
        EnemySkirmisher dummyEnemySkirmisher;
        Texture2D enemySkirmisherTexture;

        EnemyFighter dummyEnemyFighter;
        Texture2D enemyFighterTexture;

        EnemyMiniBoss dummyEnemyMiniBoss;
        Texture2D enemyMiniBossTexture;

        EnemyKamikaze dummyEnemyKamikaze;
        Texture2D enemyKamikazeTexture;

        EnemyTurret dummyEnemyTurret;
        Texture2D enemyTurretTexture;

        EnemyMothership dummyEnemyMothership;
        Texture2D enemyMothershipTexture;

        //Test stuff

        BigAsteroid dummyBigAstroid;
        MediumAsteroid dummyMidAstroid;
        SmallAsteroid dummySmallAstroid;

        //Weapons
        Weapon[] weapons;

        Boolean pauseButtonLatch, pauseButtonLatch2;

        private MediaLibrary myLibrary;
        private Song menuMusic, gameMusic, epicMusic;
        private Boolean menuMusicLatch;

        private SoundEffect menuButtonSound, shieldLow, shieldCharge;

        public Texture2D whiteTexture;

        private bool resultLatch, levelUnlocked;

        UI ui;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //Sets the resolution of the screen
            //graphics.ToggleFullScreen();
            graphics.PreferredBackBufferHeight = (int)Defines.SCREEN_HEIGHT;
            graphics.PreferredBackBufferWidth = (int)Defines.SCREEN_WIDTH;
            Content.RootDirectory = "Content";
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            dummyPlayer = new PlayerShip();

            //Initializes enemies
            dummyEnemySkirmisher = new EnemySkirmisher(); //Since a non static cannot call static methods this variable is used to assign a texture to the ship type
            dummyEnemyFighter = new EnemyFighter();
            dummyEnemyMiniBoss = new EnemyMiniBoss();
            dummyEnemyKamikaze = new EnemyKamikaze();
            dummyEnemyTurret = new EnemyTurret();
            dummyEnemyMothership = new EnemyMothership();

            //Initializes Asteroids
            dummyBigAstroid = new BigAsteroid();
            dummyMidAstroid = new MediumAsteroid();
            dummySmallAstroid = new SmallAsteroid();

            pauseButtonLatch = false;

            levelRunning = false;
            resultLatch = false;
            levelUnlocked = false;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            String dataDirectory = @"../../../../CosmicGuardianContent/DataFiles/";
            WeaponData[] testWeapon=null;
            
            if (File.Exists(dataDirectory + "Weapon.xml"))
            {
                Console.Out.WriteLine("Exists");
                testWeapon = (WeaponData[])(Easy.load(dataDirectory + "Weapon.xml")); 
            }
            else
            {
                Console.Out.WriteLine("Does Not Exist");
            }
   
            //Textures for the player ship
            shipBody = Content.Load<Texture2D>(@"Player/ship_body");
            frame = Content.Load<Texture2D>(@"Player/frame");
            engineStop = Content.Load<Texture2D>(@"Player/engine_stop");
            engineSlow = Content.Load<Texture2D>(@"Player/engine_slow");
            engineFast = Content.Load<Texture2D>(@"Player/engine_fast");
            engineFastest = Content.Load<Texture2D>(@"Player/engine_fastest");
            engineBoost = Content.Load<Texture2D>(@"Player/engine_boost");

            shield = Content.Load<Texture2D>(@"Player/shieldWhite");

            shieldIndicatorFrame = Content.Load<Texture2D>(@"UI/shieldFrame2");
            shieldBar = Content.Load<Texture2D>(@"UI/shieldBar");
            weaponNode = Content.Load<Texture2D>(@"UI/weaponNode");

            //Assigns textures to the player
            dummyPlayer.assignTexture(shipBody, frame, engineStop, engineSlow, engineFast, engineFastest, engineBoost, shield, shieldIndicatorFrame, shieldBar, weaponNode,  0.6f);

            //Textures for the enemy ships
            enemySkirmisherTexture = Content.Load<Texture2D>(@"Enemies/enemySkirmisher");
            dummyEnemySkirmisher.assignTexture(enemySkirmisherTexture, 1.7f);

            enemyFighterTexture = Content.Load<Texture2D>(@"Enemies/enemyUpgraded");
            dummyEnemyFighter.assignTexture(enemyFighterTexture, 2.0f);

            enemyMiniBossTexture = Content.Load<Texture2D>(@"Enemies/enemyBoss");
            dummyEnemyMiniBoss.assignTexture(enemyMiniBossTexture, 3.0f);

            enemyKamikazeTexture = Content.Load<Texture2D>(@"Enemies/enemyKamikaze");
            dummyEnemyKamikaze.assignTexture(enemyKamikazeTexture, 1.0f);

            enemyTurretTexture = Content.Load<Texture2D>(@"Enemies/enemyTurret");
            dummyEnemyTurret.assignTexture(enemyTurretTexture, 1.0f);

            enemyMothershipTexture = Content.Load<Texture2D>(@"Enemies/enemyMotherShip");
            dummyEnemyMothership.assignTexture(enemyMothershipTexture, 1.3f);

            float effectVolume = 0.3f;
            //Music
            myLibrary = new MediaLibrary();
            menuMusic = Content.Load<Song>(@"Sounds/menuMusic");
            gameMusic = Content.Load<Song>(@"Sounds/gameMusic");
            epicMusic = Content.Load<Song>(@"Sounds/epicMusic");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.3f;
            MediaPlayer.Play(menuMusic);
            menuMusicLatch = false;

            menuButtonSound = Content.Load<SoundEffect>(@"Sounds/menuSound");

            shieldLow = Content.Load<SoundEffect>(@"Sounds/shieldLow");
            shieldCharge = Content.Load<SoundEffect>(@"Sounds/shieldCharge");
            dummyPlayer.setShieldLowSound(shieldLow);
            dummyPlayer.setShieldChargeSound(shieldCharge);

            mainMenuBackground = Content.Load<Texture2D>(@"Menus/mainMenuBackground");
            buttonA = Content.Load<Texture2D>(@"Menus/buttonA");
            buttonBlue = Content.Load<Texture2D>(@"Menus/buttonBlue");
            buttonGrey = Content.Load<Texture2D>(@"Menus/buttonGrey");
            dPad = Content.Load<Texture2D>(@"Menus/d-Pad");
            emblem = Content.Load<Texture2D>(@"Menus/gameEmblem");

            creditBackground = Content.Load<Texture2D>(@"Menus/creditScreenBackground");
            bButton = Content.Load<Texture2D>(@"Menus/buttonB");
            controls = Content.Load<Texture2D>(@"Menus/gameControls");

            mainMenuFont = Content.Load<SpriteFont>(@"SpriteFont1");
            largeFont = Content.Load<SpriteFont>(@"largeFont");
            mainMenu = new MainMenu(emblem, buttonBlue, buttonGrey, mainMenuBackground, buttonA, dPad,
                                    graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, mainMenuFont, spriteBatch, menuButtonSound);

            creditScreen = new CreditScreen(creditBackground, emblem, bButton, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, mainMenuFont, spriteBatch);
            optionsMenu = new OptionsMenu(mainMenuBackground, dPad, buttonA, bButton, buttonBlue, buttonGrey, controls, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth, spriteBatch, mainMenuFont, largeFont, menuButtonSound);
            levelSelectMenu = new LevelSelect(spriteBatch, mainMenuBackground, buttonA, buttonBlue, buttonGrey, dPad, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, mainMenuFont, largeFont, menuButtonSound);
            upgradeMenu = new UpgradeMenu(spriteBatch, graphics.PreferredBackBufferHeight, graphics.PreferredBackBufferWidth, mainMenuFont, largeFont, mainMenuBackground, dPad, buttonA, buttonBlue, buttonGrey, menuButtonSound);

            //Astroids
            asteroid = Content.Load<Texture2D>(@"GravityObjects/asteroidLarge");

            dummyBigAstroid.assignTexture(asteroid, 8f);
            dummyMidAstroid.assignTexture(asteroid, 2f);
            dummySmallAstroid.assignTexture(asteroid, 0.7f);

            //Weapon loading code
            Texture2D weapon, bullet;
            weapons = new Weapon[testWeapon.Length];
            SoundEffect effect;

            for (int i = 0; i < weapons.Length; i++)
            {
                
                weapon = Content.Load<Texture2D>("Weapons/"+testWeapon[i].gunSpriteFilename);
                bullet = Content.Load<Texture2D>("Weapons/" + testWeapon[i].shotSpriteFilename);
                
                String tempString="Sounds/"+testWeapon[i].weaponSoundEffectFileName;
                effect = Content.Load<SoundEffect>(@tempString);
                Projectile proj = new Projectile();
                proj.setStats(testWeapon[i].shotHealth, testWeapon[i].shotMass, testWeapon[i].shotVelocity, testWeapon[i].shotGravityMultiplier, testWeapon[i].weaponPower);
                weapons[i] = new Weapon(testWeapon[i], weapon, 0.7f, bullet, 2.5f, effect, effectVolume, proj);

            }

            dummyEnemySkirmisher.assignWeapon(new Weapon(weapons[4], Vector2.Zero));
            dummyEnemyFighter.assignWeapon(new Weapon(weapons[5], Vector2.Zero));
            dummyEnemyMiniBoss.assignWeapons(new Weapon(weapons[5], Vector2.Zero), new Weapon(weapons[7], Vector2.Zero));
            dummyEnemyKamikaze.assignWeapon(new Weapon(weapons[0], Vector2.Zero));
            dummyEnemyTurret.assignWeapon(new Weapon(weapons[4], Vector2.Zero));
            dummyEnemyMothership.assignWeapons(new Weapon(weapons[6], Vector2.Zero), new Weapon(weapons[7], Vector2.Zero));

            if (weapons != null)
            {
                for (int i = 0; i < weapons.Length; i++)
                {
                    dummyPlayer.addWeapon(new Weapon(weapons[i], Vector2.Zero));
                }

            }

            //Level 1
            minimap = Content.Load<Texture2D>(@"UI/minimapBackground");
            whiteTexture = new Texture2D(this.GraphicsDevice, 1, 1);
            whiteTexture.SetData<Color>(new Color[] { Color.White });
            ui = new UI(minimap, weaponNode, weaponNode, shieldBar, shieldIndicatorFrame, whiteTexture, mainMenuFont);
            Level1 = new Level1(ui);
            Level1Background = Content.Load<Texture2D>(@"Levels/SpaceBackground");
            Level1.assignBackground(Level1Background, Level1Scale);
            Level1.addPlayer(new PlayerShip());

            //Level 2
            //UI ui2 = new UI(minimap, weaponNode, weaponNode, shieldBar, shieldIndicatorFrame, whiteTexture);
            Level2 = new Level2(ui);
            Level2Background = Content.Load<Texture2D>(@"Levels/KamikazeLevel");
            Level2.assignBackground(Level2Background, Level2Scale);
            Level2.addPlayer(new PlayerShip());

            //Level 3
            //UI ui3 = new UI(minimap, weaponNode, weaponNode, shieldBar, shieldIndicatorFrame, whiteTexture);
            Level3 = new Level3(ui);
            Level3Background = Content.Load<Texture2D>(@"Levels/Miniboss");
            Level3.assignBackground(Level3Background, Level3Scale);
            Level3.addPlayer(new PlayerShip());

            //Level 4
            //UI ui4 = new UI(minimap, weaponNode, weaponNode, shieldBar, shieldIndicatorFrame,whiteTexture);
            Level4 = new Level4(ui);
            Level4Background = Content.Load<Texture2D>(@"Levels/Asteroids");
            Level4.assignBackground(Level4Background, Level4Scale);
            Level4.addPlayer(new PlayerShip());

            //Level 5
            Level5 = new Level5(ui);
            Level5Background = Content.Load<Texture2D>(@"Levels/Mothership");
            Level5.assignBackground(Level5Background, Level5Scale);
            Level5.addPlayer(new PlayerShip());
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) == true)
                this.Exit();

            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);

            if (mainMenu.displayMainMenu)
            {
                mainMenu.Update(gameTime);
                if (i.pauseButton)
                    this.Exit();
            }
            if (mainMenu.displayCreditScreen)
            {
                creditScreen.Update(gameTime, mainMenu);
            }
            if (mainMenu.displayOptionsMenu)
            {
                optionsMenu.Update(gameTime, mainMenu);
            }
            if (mainMenu.displayLevelSelect)
            {
                levelSelectMenu.Update(gameTime, mainMenu);
            }
            if (mainMenu.displayUpgradeMenu)
            {
                upgradeMenu.Update(gameTime, mainMenu);
            }
            if (mainMenu.displayGame)
            {
                if (!menuMusicLatch)
                {
                    if (mainMenu.levelNumber == 5)
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Volume = 0.8f;
                        MediaPlayer.Play(epicMusic);
                        menuMusicLatch = true;
                    }
                    else
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Volume = 1.0f;
                        MediaPlayer.Play(gameMusic);
                        menuMusicLatch = true;
                    }
                }

                if (!pauseButtonLatch)
                {
                    if (mainMenu.levelNumber == 1)
                    {
                        if (levelRunning)
                        {
                            int result = Defines.WIN;
                            if (!Level1.getLevelEnd())
                                result = Level1.runSimulation(gameTime);

                            if (result != Defines.IN_PROGRESS)
                            {
                                Level1.setLevelEnd(true);
                                if (result == Defines.WIN && !resultLatch)
                                {
                                    resultLatch = true;
                                    levelUnlocked = true;
                                    Level1.setLevelFailed(false);
                                }
                                else if (result == Defines.LOSS && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level1.setLevelFailed(true);
                                }
                                if (i.cancelButton)
                                {
                                    mainMenu.displayGame = false;
                                    mainMenu.displayMainMenu = true;
                                    MediaPlayer.Stop();
                                    MediaPlayer.Volume = 0.5f;
                                    MediaPlayer.Play(menuMusic);
                                    menuMusicLatch = false;
                                    Level1 = new Level1(ui);
                                    Level1.assignBackground(Level1Background, Level1Scale);
                                    Level1.addPlayer(new PlayerShip());
                                    levelRunning = false;
                                    if ((mainMenu.levelUnlocked < 2) && (levelUnlocked))
                                    {
                                        mainMenu.levelUnlocked = 2;
                                        levelUnlocked = false;
                                    }
                                    Level1.setLevelEnd(false);
                                    Level1.setLevelFailed(false);
                                    resultLatch = false;
                                }
                            }
                        }
                        else
                        {
                            resetEquipment(1);
                            levelRunning = true;
                        }
                    }
                    else if (mainMenu.levelNumber == 2)
                    {
                        if (levelRunning)
                        {
                            int result = Defines.WIN;
                            if (!Level2.getLevelEnd())
                                result = Level2.runSimulation(gameTime);

                            if (result != Defines.IN_PROGRESS)
                            {

                                Level2.setLevelEnd(true);
                                if (result == Defines.WIN && !resultLatch)
                                {
                                    resultLatch = true;
                                    levelUnlocked = true;
                                    Level2.setLevelFailed(false);
                                }
                                else if (result == Defines.LOSS && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level2.setLevelFailed(true);
                                }
                                if (i.cancelButton)
                                {
                                    mainMenu.displayGame = false;
                                    mainMenu.displayMainMenu = true;
                                    MediaPlayer.Stop();
                                    MediaPlayer.Volume = 0.5f;
                                    MediaPlayer.Play(menuMusic);
                                    menuMusicLatch = false;
                                    Level2 = new Level2(ui);
                                    Level2.assignBackground(Level2Background, Level2Scale);
                                    Level2.addPlayer(new PlayerShip());
                                    levelRunning = false;
                                    if ((mainMenu.levelUnlocked < 3) && (levelUnlocked))
                                    {
                                        mainMenu.levelUnlocked = 3;
                                        levelUnlocked = false;
                                    }
                                    Level2.setLevelEnd(false);
                                    Level2.setLevelFailed(false);
                                    resultLatch = false;
                                }


                            }
                        }
                        else
                        {
                            resetEquipment(2);
                            levelRunning = true;
                        }
                    }
                    else if (mainMenu.levelNumber == 3)
                    {
                        if (levelRunning)
                        {
                            int result = Defines.WIN;
                            if (!Level3.getLevelEnd())
                                result = Level3.runSimulation(gameTime);

                            if (result != Defines.IN_PROGRESS)
                            {
                                Level3.setLevelEnd(true);
                                if (result == Defines.WIN && !resultLatch)
                                {
                                    resultLatch = true;
                                    levelUnlocked = true;
                                    Level3.setLevelFailed(false);
                                }
                                else if (result == Defines.LOSS && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level3.setLevelFailed(true);
                                }
                                if (i.cancelButton)
                                {
                                    mainMenu.displayGame = false;
                                    mainMenu.displayMainMenu = true;
                                    MediaPlayer.Stop();
                                    MediaPlayer.Volume = 0.5f;
                                    MediaPlayer.Play(menuMusic);
                                    menuMusicLatch = false;
                                    Level3 = new Level3(ui);
                                    Level3.assignBackground(Level3Background, Level3Scale);
                                    Level3.addPlayer(new PlayerShip());
                                    levelRunning = false;
                                    if ((mainMenu.levelUnlocked < 4) && (levelUnlocked))
                                    {
                                        mainMenu.levelUnlocked = 4;
                                        levelUnlocked = false;
                                    }
                                    Level3.setLevelEnd(false);
                                    Level3.setLevelFailed(false);
                                    resultLatch = false;
                                }
                            }
                        }
                        else
                        {
                            resetEquipment(3);
                            levelRunning = true;
                        }
                    }
                    else if (mainMenu.levelNumber == 4)
                    {
                        if (levelRunning)
                        {
                            int result = Defines.WIN;
                            if (!Level4.getLevelEnd())
                                result = Level4.runSimulation(gameTime);

                            if (result != Defines.IN_PROGRESS)
                            {
                                Level4.setLevelEnd(true);
                                if (result == Defines.WIN && !resultLatch)
                                {
                                    resultLatch = true;
                                    levelUnlocked = true;
                                    Level4.setLevelFailed(false);
                                }
                                else if (result == Defines.LOSS && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level4.setLevelFailed(true);
                                }
                                if (i.cancelButton)
                                {
                                    mainMenu.displayGame = false;
                                    mainMenu.displayMainMenu = true;
                                    MediaPlayer.Stop();
                                    MediaPlayer.Volume = 0.5f;
                                    MediaPlayer.Play(menuMusic);
                                    menuMusicLatch = false;
                                    Level4 = new Level4(ui);
                                    Level4.assignBackground(Level4Background, Level4Scale);
                                    Level4.addPlayer(new PlayerShip());
                                    levelRunning = false;
                                    if ((mainMenu.levelUnlocked < 5) && (levelUnlocked))
                                    {
                                        mainMenu.levelUnlocked = 5;
                                        levelUnlocked = false;
                                    }
                                    Level4.setLevelEnd(false);
                                    Level4.setLevelFailed(false);
                                    resultLatch = false;
                                }

                            }
                        }
                        else
                        {
                            resetEquipment(4);
                            levelRunning = true;
                        }
                    }
                    else if (mainMenu.levelNumber == 5)
                    {
                        if (levelRunning)
                        {
                            int result = Defines.WIN;
                            if (!Level5.getLevelEnd())
                                result = Level5.runSimulation(gameTime);

                            if (result != Defines.IN_PROGRESS)
                            {
                                Level5.setLevelEnd(true);
                                if (result == Defines.WIN && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level5.setLevelFailed(false);
                                }
                                else if (result == Defines.LOSS && !resultLatch)
                                {
                                    resultLatch = true;
                                    Level5.setLevelFailed(true);
                                }
                                if (i.cancelButton)
                                {
                                    mainMenu.displayGame = false;
                                    if (result == Defines.WIN)
                                        mainMenu.displayCreditScreen = true;
                                    else
                                        mainMenu.displayMainMenu = true;
                                    MediaPlayer.Stop();
                                    MediaPlayer.Volume = 0.5f;
                                    MediaPlayer.Play(menuMusic);
                                    menuMusicLatch = false;
                                    Level5 = new Level5(ui);
                                    Level5.assignBackground(Level5Background, Level5Scale);
                                    Level5.addPlayer(new PlayerShip());
                                    levelRunning = false;
                                    Level5.setLevelEnd(false);
                                    Level5.setLevelFailed(false);
                                    resultLatch = false;
                                }
                            }
                        }
                        else
                        {
                            resetEquipment(5);
                            levelRunning = true;
                        }
                    }
                }

                if (i.pauseButton && !pauseButtonLatch2)
                {
                    pauseButtonLatch = !pauseButtonLatch;
                    pauseButtonLatch2 = true;
                }

                if (!i.pauseButton)
                {
                    pauseButtonLatch2 = false;
                }
            }

            if (!optionsMenu.playMusic)
            {
                MediaPlayer.Pause();
            }

            if (optionsMenu.playMusic && (MediaPlayer.State == MediaState.Paused))
            {
                MediaPlayer.Resume();
            }

            if (!optionsMenu.playSoundEffects)
                Defines.SOUND_EFFECTS = false;
            else
                Defines.SOUND_EFFECTS = true;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            Vector2 screenSize = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            if (mainMenu.displayMainMenu)
            {
                mainMenu.Draw(gameTime);
            }

            if (mainMenu.displayCreditScreen)
            {
                creditScreen.Draw(gameTime);
            }

            if (mainMenu.displayUpgradeMenu)
            {
                upgradeMenu.Draw(gameTime);
            }

            if (mainMenu.displayGame)
            {

                if (mainMenu.levelNumber == 1)
                {
                    if (Level1.getLevelEnd())
                        Level1.drawEndLevel(spriteBatch, screenSize, mainMenuFont);
                    else
                        Level1.drawLevel(spriteBatch, screenSize);
                }
                else if (mainMenu.levelNumber == 2)
                {
                    if (Level2.getLevelEnd())
                        Level2.drawEndLevel(spriteBatch, screenSize, mainMenuFont);
                    else
                        Level2.drawLevel(spriteBatch, screenSize);
                }
                else if (mainMenu.levelNumber == 3)
                {
                    if (Level3.getLevelEnd())
                        Level3.drawEndLevel(spriteBatch, screenSize, mainMenuFont);
                    else
                        Level3.drawLevel(spriteBatch, screenSize);
                }
                else if (mainMenu.levelNumber == 4)
                {
                    if (Level4.getLevelEnd())
                        Level4.drawEndLevel(spriteBatch, screenSize, mainMenuFont);
                    else
                        Level4.drawLevel(spriteBatch, screenSize);
                }
                else if (mainMenu.levelNumber == 5)
                {
                    if (Level5.getLevelEnd())
                        Level5.drawEndLevel(spriteBatch, screenSize, mainMenuFont);
                    else
                        Level5.drawLevel(spriteBatch, screenSize);
                }
            }

            if (mainMenu.displayLevelSelect)
            {
                levelSelectMenu.Draw(gameTime);
            }

            if (mainMenu.displayOptionsMenu)
            {
                optionsMenu.Draw(gameTime);
            }

            if (pauseButtonLatch)
                spriteBatch.DrawString(mainMenuFont, "PAUSED", new Vector2(screenSize.X/2 - 32, screenSize.Y/2 - 6), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        void resetEquipment(int level)
        {
            if (level == 1)
            {
                dummyPlayer.deleteWeapons();
                dummyPlayer.addWeapon(weapons[0]);
                dummyPlayer.setEquipment(new DefaultEngine(), new DefaultShields());
            }
            else if (level == 2)
            {
                dummyPlayer.deleteWeapons();
                dummyPlayer.addWeapon(weapons[1]);
                dummyPlayer.setEquipment(new DamagedEngine(), new DefaultShields());
            }
            else if (level == 3)
            {
                dummyPlayer.deleteWeapons();
                dummyPlayer.addWeapon(weapons[2]);
                dummyPlayer.setEquipment(new DefaultEngine(), new DefaultShields());
            }
            else if (level == 4)
            {
                dummyPlayer.deleteWeapons();
                dummyPlayer.addWeapon(weapons[2]);
                dummyPlayer.addWeapon(weapons[3]);
                dummyPlayer.setEquipment(new DefaultEngine(), new DefaultShields());
            }
            else if (level == 5)
            {
                dummyPlayer.deleteWeapons();
                dummyPlayer.addWeapon(weapons[2]);
                dummyPlayer.addWeapon(weapons[3]);
                dummyPlayer.setEquipment(new DefaultEngine(), new DefaultShields());
            }
        }
    }
}
