﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    abstract class Shields
    {
        protected float maxShields, shieldRegen, shieldDownTime;

        public float getMaxShields()
        {
            return maxShields;
        }

        public float getShieldRegen()
        {
            return shieldRegen;
        }

        public float getShieldDownTime()
        {
            return shieldDownTime;
        }
    }
}
