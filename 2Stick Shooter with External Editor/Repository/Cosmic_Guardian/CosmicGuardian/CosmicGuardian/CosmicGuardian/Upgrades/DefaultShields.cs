﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    class DefaultShields : Shields
    {
        public DefaultShields()
        {
            maxShields = Defines.DEFAULT_SHIELDS_HEALTH;
            shieldDownTime = Defines.DEFAULT_SHIELDS_COOLDOWN;
            shieldRegen = Defines.DEFAULT_SHIELDS_REGEN;
        }
    }
}
