﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    class DamagedEngine : Engine
    {
        public DamagedEngine()
        {
            maxAcceleration = Defines.DAMAGED_ENGINE_MAX_ACCELERAION;
            maxBoostAcceleration = Defines.DAMAGED_ENGINE_MAX_BOOST_ACCELERAION;
            maxVelocity = Defines.DAMAGED_ENGINE_MAX_VELOCITY;
            maxBoostVelocity = Defines.DAMAGED_ENGINE_MAX_BOOST_VELOCITY;
            boostTime = Defines.DAMAGED_ENGINE_BOOST_TIME;
            boostRegen = Defines.DAMAGED_ENGINE_BOOST_REGEN;
        }
    }
}
