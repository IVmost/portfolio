﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    abstract class Engine
    {
        protected float maxAcceleration, maxBoostAcceleration, maxVelocity, maxBoostVelocity, boostTime, boostRegen;

        public float getMaxAcceleration()
        {
            return maxAcceleration;
        }

        public float getMaxAccelerationBoost()
        {
            return maxBoostAcceleration;
        }

        public float getMaxVelocity()
        {
            return maxVelocity;
        }

        public float getMaxVelocityBoost()
        {
            return maxBoostVelocity;
        }

        public float getBoostTime()
        {
            return boostTime;
        }

        public float getBoostRegen()
        {
            return boostRegen;
        }
    }
}
