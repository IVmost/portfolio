﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CosmicGuardian
{
    class DefaultEngine : Engine
    {
        public DefaultEngine()
        {
            maxAcceleration = Defines.DEFAULT_ENGINE_MAX_ACCELERAION;
            maxBoostAcceleration = Defines.DEFAULT_ENGINE_MAX_BOOST_ACCELERAION;
            maxVelocity = Defines.DEFAULT_ENGINE_MAX_VELOCITY;
            maxBoostVelocity = Defines.DEFAULT_ENGINE_MAX_BOOST_VELOCITY;
            boostTime = Defines.DEFAULT_ENGINE_BOOST_TIME;
            boostRegen = Defines.DEFAULT_ENGINE_BOOST_REGEN;
        }
    }
}
