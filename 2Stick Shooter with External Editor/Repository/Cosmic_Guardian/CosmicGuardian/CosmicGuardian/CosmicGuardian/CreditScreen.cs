﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    //Credit screen class
    class CreditScreen
    {

        private Texture2D background, gameEmblem, back;
        private int screenWidth, screenHeight, scroll;
        SpriteBatch spriteBatch;
        SpriteFont font;

        //Credit screen constructor
        public CreditScreen(Texture2D b, Texture2D e, Texture2D bButton, int width, int height, SpriteFont f, SpriteBatch batch)
        {
            background = b;
            gameEmblem = e;
            back = bButton;
            screenWidth = width;
            screenHeight = height;
            spriteBatch = batch;
            font = f;
            scroll = screenHeight / 2;
        }

        //Credit screen update
        public virtual void Update(GameTime gameTime, MainMenu m)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);

            if (i.cancelButton)
            {
                m.displayCreditScreen = false;
                m.displayMainMenu = true;
                scroll = screenHeight / 2;
            }

            scroll--;

            if (scroll < ((-1 * screenHeight / 2) - 300))
                scroll = screenHeight / 2;
        }

        //Draw the credit screen
        public virtual void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            spriteBatch.Draw(back, new Vector2(20, 20), Color.White);
            spriteBatch.Draw(gameEmblem, new Vector2(screenWidth / 2 - gameEmblem.Width * 0.5f - 20, screenHeight / 2 + scroll + 20), new Rectangle(0, 0, gameEmblem.Width, gameEmblem.Height), Color.White, 0.0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, "Return To Main Menu", new Vector2(20 + back.Width, 40), Color.White);
            spriteBatch.DrawString(font, "Andrew Bown", new Vector2 (screenWidth/2, screenHeight/2 + scroll), Color.Red, 0.0f, new Vector2(0, 0), 2.0f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, "Anthony Evangelista", new Vector2(screenWidth / 2, screenHeight / 2 + 100 + scroll), Color.Red, 0.0f, new Vector2(0, 0), 2.0f, SpriteEffects.None, 0);
            spriteBatch.DrawString(font, "Tyler Haskell", new Vector2(screenWidth / 2, screenHeight / 2 + 200 + scroll), Color.Red, 0.0f, new Vector2(0, 0), 2.0f, SpriteEffects.None, 0);
        }
    }
}
