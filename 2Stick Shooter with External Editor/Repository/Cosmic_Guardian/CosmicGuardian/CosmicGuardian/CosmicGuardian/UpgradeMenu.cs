﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace CosmicGuardian
{
    class UpgradeMenu
    {
        SpriteFont font1, font2;
        SpriteBatch spriteBatch;
        private int screenHeight, screenWidth;
        private Texture2D background, dpad, aButton, blueButton, greyButton;
        private SoundEffect navSound;
        private bool exitActivated, exitPressed, navLatch;

        public UpgradeMenu(SpriteBatch batch, int height, int width, SpriteFont f1, SpriteFont f2, Texture2D b, Texture2D d, Texture2D buttonA, Texture2D blue, Texture2D grey, SoundEffect effect)
        {
            spriteBatch = batch;
            screenHeight = height;
            screenWidth = width;
            font1 = f1;
            font2 = f2;
            background = b;
            dpad = d;
            aButton = buttonA;
            blueButton = blue;
            greyButton = grey;
            navSound = effect;
            exitActivated = true;
            exitPressed = false;
            navLatch = false;
        }

        public virtual void Update(GameTime gameTime, MainMenu m)
        {
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);
            if (exitActivated)
            {
                if (i.menuNavigate.Down == ButtonState.Pressed && !navLatch)
                {
                    //Do Nothing
                }

                if (i.menuNavigate.Up == ButtonState.Pressed && !navLatch)
                {
                    //Do Nothing
                }
            }

            if (exitActivated)
            {
                if (i.enterButton)
                {
                    exitPressed = true;
                }
                if (!i.enterButton && exitPressed)
                {
                    m.displayUpgradeMenu = false;
                    m.displayMainMenu = true;
                    exitPressed = false;
                }
            }

            if ((i.menuNavigate.Down == ButtonState.Released) && (i.menuNavigate.Up == ButtonState.Released))
            {
                navLatch = false;
            }
        }

        public virtual void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(background, new Rectangle(0, 0, screenWidth, screenHeight), Color.White);
            spriteBatch.DrawString(font1, "Select", new Vector2(80, screenHeight - 57), Color.White);
            spriteBatch.DrawString(font1, "Navigate", new Vector2(80, screenHeight - 135), Color.White);
            spriteBatch.Draw(aButton, new Vector2(5, screenHeight - 75), Color.White);
            spriteBatch.Draw(dpad, new Vector2(5, screenHeight - 150), new Rectangle(0, 0, dpad.Width, dpad.Height), Color.White, 0.0f, new Vector2(0, 0), 0.1f, SpriteEffects.None, 0);

            if (exitActivated)
                spriteBatch.Draw(blueButton, new Vector2(430, blueButton.Height * 3 + 270), Color.White);
            else
                spriteBatch.Draw(greyButton, new Vector2(430, blueButton.Height * 3 + 270), Color.White);

            spriteBatch.DrawString(font2, "UPGADES", new Vector2(430, 100), Color.White);
            spriteBatch.DrawString(font1, "Exit", new Vector2(blueButton.Width + 440, blueButton.Height * 3.5f + 255), Color.White);
        }
    }
}
