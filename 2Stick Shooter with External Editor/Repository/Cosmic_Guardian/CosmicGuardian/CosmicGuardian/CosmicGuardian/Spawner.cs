﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    class Spawner
    {
        private SpaceObject spawnee;
        private float cooldown, cooldownCount;
        private int stock;
        private int side; //The side of the level enemies spawn at. 1 = north, 2 = east, 3 = south, 4 = west

        public Spawner(SpaceObject s, float cooldown, float startCount, int stock, int side)
        {
            spawnee = s;
            cooldownCount = startCount;
            this.cooldown = cooldown;
            this.stock = stock;
            this.side = side;
        }

        public void update(GameTime gameTime)
        {
            if (cooldownCount > 0)
            {
                cooldownCount -= gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        public SpaceObject spawn(Gameworld g)
        {
            if(cooldownCount <= 0 && !outOfStock())
            {
                cooldownCount = cooldown;
                if(stock > 0)
                    --stock;
                if (spawnee is EnemySkirmisher)
                {
                    EnemySkirmisher newSpawnee = new EnemySkirmisher();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is EnemyFighter)
                {
                    EnemyFighter newSpawnee = new EnemyFighter();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is EnemyMiniBoss)
                {
                    EnemyMiniBoss newSpawnee = new EnemyMiniBoss();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is EnemyKamikaze)
                {
                    EnemyKamikaze newSpawnee = new EnemyKamikaze();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is EnemyTurret)
                {
                    EnemyTurret newSpawnee = new EnemyTurret();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is EnemyMothership)
                {
                    EnemyMothership newSpawnee = new EnemyMothership();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if(spawnee is BigAsteroid)
                {
                    BigAsteroid newSpawnee = new BigAsteroid();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is MediumAsteroid)
                {
                    MediumAsteroid newSpawnee = new MediumAsteroid();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
                else if (spawnee is SmallAsteroid)
                {
                    SmallAsteroid newSpawnee = new SmallAsteroid();
                    sideHelper(newSpawnee, g);
                    return newSpawnee;
                }
            }
            return null;
        }

        private void sideHelper(SpaceObject s, Gameworld g)
        {
            Vector2 position, velocity;
            Random r = new Random();
            if (side == 1)
            {
                position = new Vector2(r.Next((int)(s.collisionDist() + 5), (int)(g.getSize().X - s.collisionDist() - 5)), s.collisionDist() + 5);
                velocity = new Vector2(r.Next((int)(-s.getMaxVelocity() * 0.5), (int)(s.getMaxVelocity() * 0.5)), s.getMaxVelocity());
            }
            else if (side == 2)
            {
                position = new Vector2(g.getSize().X - s.collisionDist() - 5, r.Next((int)(s.collisionDist() + 5), (int)(g.getSize().Y - s.collisionDist() - 5)));
                velocity = new Vector2(-s.getMaxVelocity(), r.Next((int)(-s.getMaxVelocity() * 0.5), (int)(s.getMaxVelocity() * 0.5)));
            }
            else if (side == 3)
            {
                position = new Vector2(r.Next((int)(s.collisionDist() + 5), (int)(g.getSize().X - s.collisionDist() - 5)), g.getSize().Y - s.collisionDist() - 5);
                velocity = new Vector2(r.Next((int)(s.getMaxVelocity() * 0.5), (int)(s.getMaxVelocity() * 0.5)), -s.getMaxVelocity());
            }
            else //if (side == 4)
            {
                position = new Vector2(s.collisionDist() - 5, r.Next((int)(s.collisionDist() + 5), (int)(g.getSize().Y - s.collisionDist() - 5)));
                velocity = new Vector2(s.getMaxVelocity(), r.Next((int)(-s.getMaxVelocity() * 0.5), (int)(s.getMaxVelocity() * 0.5)));
            }

            s.setPosition(position);
            s.setVelocity(velocity);
        }

        public bool outOfStock()
        {
            return (stock == 0);
        }

        public SpaceObject getSpawnee()
        {
            return spawnee;
        }
    }
}
