﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Diagnostics;

namespace CosmicGuardian
{
    class Level5 : Gameworld
    {
        public Level5(UI u)
        {
            //The following code is just used to test a level with enemies

            enemyShips = new List<Enemy>();
            playerProjectiles = new List<Projectile>();
            enemyProjectiles = new List<Projectile>();
            gravityObjects = new List<GravityObject>();
            spawners = new List<Spawner>();

            maxEnemies = 15;
            maxGravs = 0;

            EnemySkirmisher ship;

            for (int i = 0; i < 20; i++)
            {
                ship = new EnemySkirmisher();
                ship.setPosition(new Vector2(0, 0));
                enemyShips.Add(ship);
            }

            EnemyFighter ship2;

            for (int i = 0; i < 2; i++)
            {
                ship2 = new EnemyFighter();
                ship2.setPosition(new Vector2(0, 0));
                enemyShips.Add(ship2);
            }

            enemyShips.Add(new EnemyMothership());

            ship = new EnemySkirmisher();
            ship2 = new EnemyFighter();

            spawners.Add(new Spawner(ship, 3000, 200, -1, 2));
            spawners.Add(new Spawner(ship, 3000, 300, -1, 2));

            spawners.Add(new Spawner(ship2, 3000, 100, -1, 2));

            spawners.Add(new Spawner(ship, 3000, 400, -1, 2));

            ui = u;
        }

        public void assignBackground(Texture2D b, float s)
        {
            Random r = new Random();
            background = b;
            imageScale = s;
            size = new Vector2(b.Bounds.Width * imageScale, b.Bounds.Height * imageScale);

            playerStart = new Vector2(50, size.Y / 2);

            foreach (Enemy e in enemyShips)
            {
                int x = (int)size.X;
                int y = (int)size.Y;
                int x2 = r.Next((int)(x / 1.5f), x);
                int y2 = r.Next(y);
                e.setPosition(new Vector2(x2, y2));
            }

            camera = new Camera();
        }

        protected override int result()
        {
            if (isMothershipDead())
            {
                spawners.Clear();
            }

            if (enemyShips.Count == 0 && spawners.Count == 0)
            {
                return Defines.WIN;
            }
            else if (player.isDead())
            {
                return Defines.LOSS;
            }
            return Defines.IN_PROGRESS;
        }

        private bool isMothershipDead()
        {
            foreach (Enemy e in enemyShips)
            {
                if (e is EnemyMothership)
                    return false;
            }
            return true;
        }
    }
}
