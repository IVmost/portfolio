﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Diagnostics;

namespace CosmicGuardian
{
    class Level4 : Gameworld
    {
        public Level4(UI u)
        {
            //The following code is just used to test a level with enemies

            enemyShips = new List<Enemy>();
            playerProjectiles = new List<Projectile>();
            enemyProjectiles = new List<Projectile>();
            gravityObjects = new List<GravityObject>();
            spawners = new List<Spawner>();

            maxEnemies = 20;
            maxGravs = 80;

            EnemyTurret ship;

            for (int i = 0; i < 12; i++)
            {
                ship = new EnemyTurret();
                ship.setPosition(new Vector2(0, 0));
                enemyShips.Add(ship);
            }

            BigAsteroid big = new BigAsteroid();
            MediumAsteroid mid = new MediumAsteroid();
            SmallAsteroid small = new SmallAsteroid();

            spawners.Add(new Spawner(big, 35000, 20000, -1, 1));

            spawners.Add(new Spawner(mid, 1200, 100, -1, 1));
            spawners.Add(new Spawner(mid, 1200, 200, -1, 1));
            spawners.Add(new Spawner(mid, 1200, 200, -1, 1));
            spawners.Add(new Spawner(mid, 1200, 200, -1, 1));
            spawners.Add(new Spawner(mid, 1200, 300, -1, 1));

            spawners.Add(new Spawner(small, 500, 500, -1, 1));
            spawners.Add(new Spawner(small, 500, 600, -1, 1));
            spawners.Add(new Spawner(small, 500, 700, -1, 1));
            spawners.Add(new Spawner(small, 500, 800, -1, 1));
            spawners.Add(new Spawner(small, 500, 900, -1, 1));
            spawners.Add(new Spawner(small, 500, 700, -1, 1));
            spawners.Add(new Spawner(small, 500, 800, -1, 1));
            spawners.Add(new Spawner(small, 500, 900, -1, 1));
            spawners.Add(new Spawner(small, 500, 1000, -1, 1));
            spawners.Add(new Spawner(small, 500, 1100, -1, 1));

            ui = u;
        }

        public void assignBackground(Texture2D b, float s)
        {
            Random r = new Random();
            background = b;
            imageScale = s;
            size = new Vector2(b.Bounds.Width * imageScale, b.Bounds.Height * imageScale);

            foreach (Enemy e in enemyShips)
            {
                int x = (int)size.X;
                int y = (int)size.Y;
                int x2 = r.Next(x/3, x);
                int y2 = r.Next((int)(y * 0.1f), (int)(y * 0.9f));
                e.setPosition(new Vector2(x2, y2));
            }

            playerStart = new Vector2(50, size.Y / 2);

            camera = new Camera();
        }

        protected override int result()
        {
            if (enemyShips.Count == 0)
            {
                return Defines.WIN;
            }
            else if (player.isDead())
            {
                return Defines.LOSS;
            }
            return Defines.IN_PROGRESS;
        }
    }
}
