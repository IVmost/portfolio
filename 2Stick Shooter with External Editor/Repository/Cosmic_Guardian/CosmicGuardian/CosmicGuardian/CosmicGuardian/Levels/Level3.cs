﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Diagnostics;

namespace CosmicGuardian
{
    class Level3 : Gameworld
    {
        bool bossSpawned;

        public Level3(UI u)
        {
            //The following code is just used to test a level with enemies

            enemyShips = new List<Enemy>();
            playerProjectiles = new List<Projectile>();
            enemyProjectiles = new List<Projectile>();
            gravityObjects = new List<GravityObject>();
            spawners = new List<Spawner>();

            maxEnemies = 20;
            maxGravs = 0;
            bossSpawned = false;

            EnemySkirmisher ship;

            for (int i = 0; i < 20; i++)
            {
                ship = new EnemySkirmisher();
                ship.setPosition(new Vector2(0, 0));
                enemyShips.Add(ship);
            }
            EnemyFighter ship2;

            for (int i = 0; i < 2; i++)
            {
                ship2 = new EnemyFighter();
                ship2.setPosition(new Vector2(0, 0));
                enemyShips.Add(ship2);
            }
            ship = new EnemySkirmisher();
            ship2 = new EnemyFighter();

            spawners.Add(new Spawner(ship, 3000, 200, 15, 2));
            spawners.Add(new Spawner(ship, 3000, 300, 15, 2));

            spawners.Add(new Spawner(ship2, 3000, 100, 4, 2));

            spawners.Add(new Spawner(ship, 3000, 400, 15, 2));

            ui = u;
        }

        public void assignBackground(Texture2D b, float s)
        {
            Random r = new Random();
            background = b;
            imageScale = s;
            size = new Vector2(b.Bounds.Width * imageScale, b.Bounds.Height * imageScale);

            foreach (Enemy e in enemyShips)
            {
                int x = (int)size.X;
                int y = (int)size.Y;
                int x2 = r.Next(x/2, x);
                int y2 = r.Next(y);
                e.setPosition(new Vector2(x2, y2));
            }

            playerStart = new Vector2(50, size.Y / 2);

            camera = new Camera();
        }

        protected override int result()
        {
            if (enemyShips.Count == 0 && spawners.Count == 0)
            {
                if (!bossSpawned)
                {
                    spawners.Add(new Spawner(new EnemyMiniBoss(), 1, 1, 1, 2));
                    spawners.Add(new Spawner(new EnemyFighter(), 1, 1, 4, 2));
                    bossSpawned = true;
                }
                else
                {
                    return Defines.WIN;
                }
            }
            else if (player.isDead())
            {
                return Defines.LOSS;
            }
            return Defines.IN_PROGRESS;
        }
    }
}
