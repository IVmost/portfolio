﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CosmicGuardian
{
    abstract class Gameworld
    {
        protected Texture2D background;
        protected Vector2 size, playerStart;
        protected float imageScale;

        protected PlayerShip player;

        protected List<Enemy> enemyShips;

        protected List<Projectile> playerProjectiles;
        protected List<Projectile> enemyProjectiles;

        protected List<GravityObject> gravityObjects;

        protected List<Spawner> spawners;

        Projectile tempProjectile;

        protected Camera camera;

        protected int maxEnemies, maxGravs;

        protected UI ui;

        protected int playerScore = 0;
        protected int enemiesKilled = 0;

        protected bool levelEnd = false;
        protected bool levelFailed = false;

        public virtual Vector2 getSize()
        {
            return size;
        }

        public virtual int runSimulation(GameTime gameTime)
        {
            updateObjects(gameTime);
            checkInteractions(gameTime);
            removeDeadObjects();
            ui.setScore(ui.getScore() + playerScore);
            playerScore = 0;
            return result();
        }

        protected virtual void updateObjects(GameTime gameTime)
        {
            //Here the status of the player is updated
            inputStruct i = InputDriver.PollGamePadInput(PlayerIndex.One);
            player.Update(gameTime);
            
            tempProjectile = player.getProjectile();
            if (tempProjectile != null)
            {
                playerProjectiles.Add(tempProjectile);
                player.resetProjectile();
            }

            //Here the status of the enemy ships are updated
            foreach (Enemy s in enemyShips)
            {
                s.AI(gameTime, player.getPosition(), player.getVelocity());
                tempProjectile = s.getProjectile();
                if (tempProjectile != null)
                {
                    enemyProjectiles.Add(tempProjectile);
                    s.resetProjectile();
                }
            }

            foreach (Projectile p in playerProjectiles)
            {
                if (p.inHomingMode())
                {
                    p.homing(gameTime, player.getPosition());
                }
                p.Update(gameTime);
            }

            foreach (Projectile p in enemyProjectiles)
            {
                if (p.inHomingMode())
                {
                    p.homing(gameTime, player.getPosition());
                }
                p.Update(gameTime);
            }

            foreach (GravityObject g in gravityObjects)
            {
                g.Update(gameTime);
            }

            foreach (Spawner s in spawners)
            {
                s.update(gameTime);
                handleSpawner(s);
            }
        }

        protected virtual void checkInteractions(GameTime gameTime)
        {
            foreach (GravityObject g in gravityObjects)
            {
                Physics.checkIfInBounds(g, this);
            }

            foreach (GravityObject g in gravityObjects)
            {
                Physics.checkCollision(player, g);

                foreach (Enemy s in enemyShips)
                {
                    Physics.checkCollision(s, g);
                }

                foreach (Projectile p in playerProjectiles)
                {
                    Physics.checkCollision(p, g);
                }

                foreach (Projectile p in enemyProjectiles)
                {
                    Physics.checkCollision(p, g);
                }
            }

            for (int i = 0; i < gravityObjects.Count - 1; i++)
            {
                for (int j = i + 1; j < gravityObjects.Count; j++)
                {
                    Physics.checkCollision(gravityObjects[i], gravityObjects[j]);
                }
            }

            //Code ensures that the player stays within the map and makes them bounce off the edge if they try to go off the map
            Physics.checkIfInBounds(player, this);

            //Ensures that enemies stay within the level
            foreach (Enemy s in enemyShips)
            {
                Physics.checkIfInBounds(s, this);
                Physics.checkCollision(player, s);
                foreach (Projectile p in playerProjectiles)
                {
                    Physics.checkCollision(p, s);
                }
            }

            foreach (Projectile p in enemyProjectiles)
            {
                Physics.checkCollision(p, player);
            }

            foreach (GravityObject g in gravityObjects)
            {
                Physics.applyGravity(g, player, gameTime);

                foreach (Projectile p in playerProjectiles)
                {
                    Physics.applyGravity(g, p, gameTime);
                }
            }
        }

        public virtual void drawLevel(SpriteBatch spriteBatch, Vector2 screenSize)
        {
            //Decides which part of the map background to draw. As the player is always at the center of the screen the portion of the
            //map which is draw is based on the current location of the player
            camera.setPosition(player.getPosition());
            Vector2 newCameraPos = camera.getPosition();

            newCameraPos.X = Math.Max(newCameraPos.X, screenSize.X / 2);
            newCameraPos.X = Math.Min(newCameraPos.X, size.X - (screenSize.X / 2));

            newCameraPos.Y = Math.Max(newCameraPos.Y, screenSize.Y / 2);
            newCameraPos.Y = Math.Min(newCameraPos.Y, size.Y - (screenSize.Y / 2));

            camera.setPosition(newCameraPos);

            int rectX = (int)((camera.getPosition().X - screenSize.X / 2) / imageScale);
            
            if (rectX < 0)
                rectX = 0;

            int rectY = (int)((camera.getPosition().Y - screenSize.Y / 2) / imageScale);
            if (rectY < 0)
                rectY = 0;

            if ((rectY + screenSize.Y/imageScale) >= background.Bounds.Height)
                rectY = (int)((background.Bounds.Height - screenSize.Y/imageScale));
            if ((rectX + screenSize.X/imageScale) >= background.Bounds.Width)
                rectX = (int)((background.Bounds.Width - screenSize.X / imageScale));

            
            Rectangle source = new Rectangle(rectX, rectY, (int)screenSize.X, (int)screenSize.Y);
            spriteBatch.Draw(background, Vector2.Zero, source, Color.White, 0, Vector2.Zero, imageScale, SpriteEffects.None, 1);
            
            //Draws all ships on the map

            foreach (Projectile p in playerProjectiles)
            {
                p.Draw(spriteBatch, screenSize, camera.getPosition());
            }

            foreach (Projectile p in enemyProjectiles)
            {
                p.Draw(spriteBatch, screenSize, camera.getPosition());
            }

            player.draw(spriteBatch, screenSize, camera.getPosition());

            foreach (Enemy s in enemyShips)
            {
                s.Draw(spriteBatch, screenSize, camera.getPosition());
            }

            foreach (GravityObject g in gravityObjects)
            {
                g.Draw(spriteBatch, screenSize, camera.getPosition());
            }

            ui.draw(player, enemyShips, size, screenSize, spriteBatch);
        }

        public virtual void drawEndLevel(SpriteBatch spriteBatch, Vector2 screenSize, SpriteFont font1)
        {
            spriteBatch.Draw(background, Vector2.Zero, new Rectangle(0, 0, background.Width, background.Height), Color.Black, 0, Vector2.Zero, imageScale, SpriteEffects.None, 1);
            spriteBatch.DrawString(font1, "STATS:", new Vector2(screenSize.X / 2 - 100, screenSize.Y / 2 - 30), Color.White);
            if (!levelFailed)
                spriteBatch.DrawString(font1, "Level Complete", new Vector2(screenSize.X / 2 - 100 , screenSize.Y / 2), Color.White);
            else
                spriteBatch.DrawString(font1, "You Were Destroyed", new Vector2(screenSize.X / 2 - 100, screenSize.Y / 2), Color.White);
            spriteBatch.DrawString(font1, "Enemys Killed: " + enemiesKilled, new Vector2(screenSize.X / 2 - 100, screenSize.Y / 2 + 20), Color.White);
            spriteBatch.DrawString(font1, "Current Score: " + ui.getScore(), new Vector2(screenSize.X / 2 - 100, screenSize.Y / 2 + 40), Color.White);
            spriteBatch.DrawString(font1, "PRESS B TO CONTINUE . . .", new Vector2(screenSize.X / 2 - 100, screenSize.Y / 2 + 70), Color.White);
        }

        private void removeDeadObjects()
        {
            for(int i = 0; i < enemyShips.Count; i++)
            {
                if(enemyShips[i].isDead())
                {
                    if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_SKIRMISHER_SCORE;
                    else if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_FIGHTER_SCORE;
                    else if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_MINI_BOSS_SCORE;
                    else if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_KAMIKAZE_SCORE;
                    else if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_TURRET_SCORE;
                    else if (enemyShips[i] is EnemySkirmisher)
                        playerScore += Defines.ENEMY_MOTHERSHIP_SCORE;

                    enemiesKilled++;
                    enemyShips.RemoveAt(i);
                    --i;
                }
            }

            for (int i = 0; i < playerProjectiles.Count; i++)
            {
                if (playerProjectiles[i].isDead())
                {
                    playerProjectiles.RemoveAt(i);
                    --i;
                }
            }

            for (int i = 0; i < enemyProjectiles.Count; i++)
            {
                if (enemyProjectiles[i].isDead())
                {
                    enemyProjectiles.RemoveAt(i);
                    --i;
                }
            }

            for (int i = 0; i < gravityObjects.Count; i++)
            {
                if (gravityObjects[i].isDead())
                {
                    gravityObjects.RemoveAt(i);
                    --i;
                }
            }

            for (int i = 0; i < spawners.Count; i++)
            {
                if (spawners[i].outOfStock())
                {
                    spawners.RemoveAt(i);
                    --i;
                }
            }
        }

        public void handleSpawner(Spawner s)
        {
            SpaceObject checkObj = s.getSpawnee();

            if ((checkObj is Enemy && enemyShips.Count < maxEnemies) || (checkObj is GravityObject && gravityObjects.Count < maxGravs))
            {
                SpaceObject obj = s.spawn(this);
                if (obj is Enemy)
                {
                    if(obj is EnemySkirmisher)
                        enemyShips.Add((EnemySkirmisher)obj);
                    if (obj is EnemyFighter)
                        enemyShips.Add((EnemyFighter)obj);
                    if (obj is EnemyMiniBoss)
                        enemyShips.Add((EnemyMiniBoss)obj);
                    if (obj is EnemyKamikaze)
                        enemyShips.Add((EnemyKamikaze)obj);
                    if (obj is EnemyTurret)
                        enemyShips.Add((EnemyTurret)obj);
                    if (obj is EnemyMothership)
                        enemyShips.Add((EnemyMothership)obj);
                }
                else if (obj is GravityObject)
                {
                    if (obj is BigAsteroid)
                        gravityObjects.Add((BigAsteroid)obj);
                    if (obj is MediumAsteroid)
                        gravityObjects.Add((MediumAsteroid)obj);
                    if (obj is SmallAsteroid)
                        gravityObjects.Add((SmallAsteroid)obj);
                }
            }
        }

        public virtual void addPlayer(PlayerShip p)
        {
            player = p;
            player.setPosition(playerStart);
            player.setVelocity(new Vector2(0, 0));
        }

        protected virtual int result()
        {
            if (enemyShips.Count == 0)
            {
                return Defines.WIN;
            }
            else if (player.isDead())
            {
                return Defines.LOSS;
            }
            return Defines.IN_PROGRESS;
        }

        public void setLevelEnd(bool b)
        {
            levelEnd = b;
        }

        public bool getLevelEnd()
        {
            return levelEnd;
        }

        public void setLevelFailed(bool b)
        {
            levelFailed = b;
        }

        public bool getLevelFailed()
        {
            return levelFailed;
        }
    }
}
