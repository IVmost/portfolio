﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Diagnostics;

namespace CosmicGuardian
{
    class Level2 : Gameworld
    {
        public Level2(UI u)
        {
            //The following code is just used to test a level with enemies

            enemyShips = new List<Enemy>();
            playerProjectiles = new List<Projectile>();
            enemyProjectiles = new List<Projectile>();
            gravityObjects = new List<GravityObject>();
            spawners = new List<Spawner>();

            maxEnemies = 20;
            maxGravs = 0;

            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 1000, -1, 2));
            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 2000, -1, 2));
            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 3000, -1, 2));

            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 1000, -1, 4));
            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 2000, -1, 4));
            spawners.Add(new Spawner(new EnemyKamikaze(), 2000, 3000, -1, 4));

            ui = u;
        }

        public void assignBackground(Texture2D b, float s)
        {
            Random r = new Random();
            background = b;
            imageScale = s;
            size = new Vector2(b.Bounds.Width * imageScale, b.Bounds.Height * imageScale);

            playerStart = new Vector2(size.X/2, size.Y);

            camera = new Camera();
        }

        protected override int result()
        {
            if (player.getPosition().Y <= 75)
            {
                return Defines.WIN;
            }
            else if(player.isDead())
            {
                return Defines.LOSS;
            }
            return Defines.IN_PROGRESS;
        }
    }
}
