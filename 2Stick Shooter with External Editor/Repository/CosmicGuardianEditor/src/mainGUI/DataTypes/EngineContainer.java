package mainGUI.DataTypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

import CosmicGuardian.EngineData;

public class EngineContainer {
	private HashMap<String, EngineData> engines = new HashMap<String, EngineData>();

	public EngineData getEngine(String engineID) {

		return engines.remove(engineID);
	}

	public boolean contains(String engineID) {

		return engines.containsKey(engineID);
	}

	public void add(EngineData editedEngine) {
		if (engines == null)
			engines = new HashMap<String, EngineData>();

		engines.put(editedEngine.engineID, editedEngine);

	}

	public Collection<EngineData> getEngines() {
		if (engines.isEmpty())
			return null;

		return engines.values();
	}

	public boolean isEmpty() {

		return engines.isEmpty();
	}

}
