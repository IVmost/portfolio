package mainGUI.DataTypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

import CosmicGuardian.ShieldData;

public class ShieldContainer {
	private HashMap<String, ShieldData> shields=new HashMap<String, ShieldData>();

	public ShieldData getShield(String shieldID) {
		
		return shields.remove(shieldID);
	}

	public boolean contains(String shieldID) {
		
		return shields.containsKey(shieldID);
	}

	public void add(ShieldData editedShield) {
		if (shields==null)
			shields= new HashMap<String, ShieldData>();
		
		shields.put(editedShield.shieldID, editedShield);
		
	}

	public Collection<ShieldData> getShields() {
		if (shields.isEmpty())
			return null;
		
		
		return  shields.values();
	}

	public boolean isEmpty() {
		
		return shields.isEmpty();
	}
	
}
