package mainGUI.DataTypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

import CosmicGuardian.WeaponData;

public class WeaponContainer {
	private HashMap<String, WeaponData> weapons=new HashMap<String, WeaponData>();

	public WeaponData getWeapon(String weaponID) {
		
		return weapons.remove(weaponID);
	}

	public boolean contains(String weaponID) {
		
		return weapons.containsKey(weaponID);
	}

	public void add(WeaponData editedWeapon) {
		if (weapons==null)
			weapons= new HashMap<String, WeaponData>();
		
		weapons.put(editedWeapon.weaponID, editedWeapon);
		
	}

	public Collection<WeaponData> getWeapons() {
		if (weapons.isEmpty())
			return null;
		
		
		return  weapons.values();
	}

	public boolean isEmpty() {
		
		return weapons.isEmpty();
	}
	
}
