package CosmicGuardian;

import java.io.Serializable;


public class WeaponData implements Serializable {
	public String weaponID;
	public String weaponName;
	public int weaponPower;
	public float weaponFireRate;
	public int shotVelocity;
	public int weaponRecoil;
	public int shotMass;
	public float shotGravityMultiplier;
	public int accuracy;
	public WeaponType weaponType;
	public String gunSpriteFilename;
	public String shotSpriteFilename;
	public String weaponSoundEffectFileName; 
	public int shotHealth;
}
