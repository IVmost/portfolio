package CosmicGuardian;

import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.filechooser.FileFilter;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import mainGUI.DataTypes.EngineContainer;
import mainGUI.DataTypes.ShieldContainer;
import mainGUI.DataTypes.ShipSize;
import mainGUI.DataTypes.WeaponContainer;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.UIManager;
import javax.xml.bind.Marshaller.Listener;

import wox.serial.Easy;

import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainWindow extends JFrame {

	JFileChooser chooser;

	private JPanel contentPane;
	/* objects for enemy creation */
	private JTable table;
	private JTextField enemyName;
	private JSpinner enemyAccelerationSpinner;
	private JSpinner enemySpeedSpinner;
	private JComboBox enemyShipSizeBox;
	private JSpinner enemyTurnSpeed;
	private JSpinner enemyMaxShields;
	private JSpinner enemyRechargeSpinner;
	private JSpinner enemyShieldRecharge;
	private JComboBox aiArchetypeBox;
	private JComboBox enemyWeaponBox;
	private JComboBox aiDefensiveBox;
	private JComboBox aiAgressiveBox;
	private JComboBox aiNeutralBox;
	private JComboBox aiSpecialBox;
	private JButton btnChooseSpriteSheet;
	private JButton btnAddEnemy;
	private String enemySpriteSheetName;

	private FileFilter spriteSheetFilter;

	/*
	 * interface objects for the player input;
	 */
	private JLabel enemyShipSpriteSheet;
	private JTextField weaponNameField;
	private JTable tableWeapon;
	private JSpinner weaponPowerSpinner;
	private JSpinner fireRateSpinner;
	private JSpinner fireVelocitySpinner;
	private final ButtonGroup weaponButtonGroup = new ButtonGroup();
	private JLabel gunSpriteLabel;
	private JLabel fireSpriteLabel;
	private JSpinner weaponRecoilSpinner;
	private JSpinner massSpinner;
	private JSpinner gravityScalerSpinner;
	private JSpinner accuracySpinner;
	private String weaponSpriteName;
	private String fireSpriteName;
	private JButton fireSpriteButton;
	private JButton gunSpritebutton;
	private JButton addWeaponbutton;
	private JRadioButton rdbtnEnemy;
	private JRadioButton playerRadioButton;
	private JPanel spriteSheetPanel;
	private JPanel gunSpritePanel;
	private JPanel shotSpritePanel;
	private JButton btnChooseGunSound;
	private JButton btnStop;
	private JButton btnPlay;
	private JPanel soundPanel;
	private String weaponSoundEffectName;
	private LineListener soundListener;
	private AudioInputStream audioInWeapon;

	private String weaponID;
	private Clip weaponSoundEffect;
	private WeaponContainer weapons;
	private FileFilter soundFilter;
	private JLabel lblShotHealth;
	private JSpinner shotHealthSpinner;
	private JPanel equipmentPanel;
	private JScrollPane equipmentScrollPane;
	private JLabel maxSpeedLabel;
	private JSpinner maxSpeedSpinner;
	private JLabel maxChargeLabel;
	private JSpinner maxShieldsSpinner;
	private JLabel accelerationRateLabel;
	private JSpinner accelerationRateSpinner;
	private JLabel chargeDelayLabel;
	private JSpinner chargeDelaySpinner;
	private JLabel maxBoostLabel;
	private JSpinner maxBoostSpinner;
	private JLabel chargeRateLabel;
	private JSpinner chargeRateSpinner;
	private JPanel engineSpritePanel;
	private JLabel engineSpriteLabel;
	private JButton chooseEngineSprite;
	private JButton newEquipmentButton;
	private JButton addEquipmentButton;
	private JPanel shieldPanel;
	private JRadioButton rdbtnShield;
	private JRadioButton rdbtnEngine;
	private JPanel enginePanel;
	private final ButtonGroup equipmentButtonGroup = new ButtonGroup();
	private String engineSpriteName;

	private String shieldID;
	private ShieldContainer shields;
	private JTextField shieldNameField;
	private JLabel lblEngineName;
	private JTextField engineNameField;

	private String engineID;
	private EngineContainer engines;
	private JTable equipmentTable;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {

				saveFiles();
			}

		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 633, 507);

		chooser = new JFileChooser();
		weapons = new WeaponContainer();
		shields = new ShieldContainer();
		engines = new EngineContainer();

		spriteSheetFilter = new FileFilter() {

			@Override
			public String getDescription() {

				return ".png";
			}

			@Override
			public boolean accept(File f) {
				String filename = f.getName();
				return filename.endsWith(".png") || f.isDirectory();
			}
		};

		soundFilter = new FileFilter() {

			@Override
			public String getDescription() {

				return ".wav or .mp3";
			}

			@Override
			public boolean accept(File f) {
				String filename = f.getName();
				return filename.endsWith(".wav") || filename.endsWith(".mp3")
						|| f.isDirectory();
			}
		};

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmNew = new JMenuItem("New");
		mnFile.add(mntmNew);

		JMenuItem mntmOpen = new JMenuItem("Open");
		mnFile.add(mntmOpen);

		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		sl_contentPane.putConstraint(SpringLayout.NORTH, tabbedPane, 0,
				SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, tabbedPane, 0,
				SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, tabbedPane, 0,
				SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, tabbedPane, 0,
				SpringLayout.EAST, contentPane);
		contentPane.add(tabbedPane);

		JPanel enemyPanel = new JPanel();
		tabbedPane.addTab("Enemy", null, enemyPanel, null);
		GridBagLayout gbl_enemyPanel = new GridBagLayout();
		gbl_enemyPanel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_enemyPanel.rowHeights = new int[] { 108, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_enemyPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		gbl_enemyPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 1.0, 0.0, Double.MIN_VALUE };
		enemyPanel.setLayout(gbl_enemyPanel);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 6;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		enemyPanel.add(scrollPane, gbc_scrollPane);

		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(new Object[][] { { null, null,
				null, null, null, null, null }, }, new String[] { "Enemy ID",
				"Name", "Type", "Weapon", "Max Shield", "Recharge Rate",
				"Sprite Sheet" }) {
			Class[] columnTypes = new Class[] { Object.class, Object.class,
					Object.class, Double.class, Object.class, Object.class,
					Object.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(6).setResizable(false);
		scrollPane.setRowHeaderView(table);

		JLabel lblName = new JLabel("Name:");
		lblName.setLabelFor(lblName);
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 1;
		enemyPanel.add(lblName, gbc_lblName);

		enemyName = new JTextField();
		enemyName.setEnabled(false);
		GridBagConstraints gbc_enemyName = new GridBagConstraints();
		gbc_enemyName.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyName.insets = new Insets(0, 0, 5, 5);
		gbc_enemyName.gridx = 1;
		gbc_enemyName.gridy = 1;
		enemyPanel.add(enemyName, gbc_enemyName);
		enemyName.setColumns(10);

		JLabel lblNewLabel = new JLabel("Weapon:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 2;
		gbc_lblNewLabel.gridy = 1;
		enemyPanel.add(lblNewLabel, gbc_lblNewLabel);

		enemyWeaponBox = new JComboBox();
		enemyWeaponBox.setEnabled(false);
		GridBagConstraints gbc_enemyWeaponBox = new GridBagConstraints();
		gbc_enemyWeaponBox.insets = new Insets(0, 0, 5, 5);
		gbc_enemyWeaponBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyWeaponBox.gridx = 3;
		gbc_enemyWeaponBox.gridy = 1;
		enemyPanel.add(enemyWeaponBox, gbc_enemyWeaponBox);

		JLabel lblAiArchetype = new JLabel("AI archetype:");
		GridBagConstraints gbc_lblAiArchetype = new GridBagConstraints();
		gbc_lblAiArchetype.anchor = GridBagConstraints.EAST;
		gbc_lblAiArchetype.insets = new Insets(0, 0, 5, 5);
		gbc_lblAiArchetype.gridx = 4;
		gbc_lblAiArchetype.gridy = 1;
		enemyPanel.add(lblAiArchetype, gbc_lblAiArchetype);

		aiArchetypeBox = new JComboBox();
		aiArchetypeBox.setEnabled(false);
		GridBagConstraints gbc_aiArchetypeBox = new GridBagConstraints();
		gbc_aiArchetypeBox.insets = new Insets(0, 0, 5, 0);
		gbc_aiArchetypeBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_aiArchetypeBox.gridx = 5;
		gbc_aiArchetypeBox.gridy = 1;
		enemyPanel.add(aiArchetypeBox, gbc_aiArchetypeBox);

		JLabel lblType = new JLabel("Type:");
		lblType.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.anchor = GridBagConstraints.EAST;
		gbc_lblType.insets = new Insets(0, 0, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 2;
		enemyPanel.add(lblType, gbc_lblType);
		lblType.setLabelFor(enemyShipSizeBox);

		enemyShipSizeBox = new JComboBox();
		enemyShipSizeBox.setEnabled(false);
		enemyShipSizeBox.setModel(new DefaultComboBoxModel(ShipSize.values()));
		GridBagConstraints gbc_enemyShipSizeBox = new GridBagConstraints();
		gbc_enemyShipSizeBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyShipSizeBox.insets = new Insets(0, 0, 5, 5);
		gbc_enemyShipSizeBox.gridx = 1;
		gbc_enemyShipSizeBox.gridy = 2;
		enemyPanel.add(enemyShipSizeBox, gbc_enemyShipSizeBox);

		JLabel lblMaxShields = new JLabel("Max Shields:");
		GridBagConstraints gbc_lblMaxShields = new GridBagConstraints();
		gbc_lblMaxShields.anchor = GridBagConstraints.EAST;
		gbc_lblMaxShields.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxShields.gridx = 2;
		gbc_lblMaxShields.gridy = 2;
		enemyPanel.add(lblMaxShields, gbc_lblMaxShields);

		enemyMaxShields = new JSpinner();
		enemyMaxShields.setEnabled(false);
		enemyMaxShields.setModel(new SpinnerNumberModel(0, 0, 1000000, 1));
		GridBagConstraints gbc_enemyMaxShields = new GridBagConstraints();
		gbc_enemyMaxShields.insets = new Insets(0, 0, 5, 5);
		gbc_enemyMaxShields.gridx = 3;
		gbc_enemyMaxShields.gridy = 2;
		enemyPanel.add(enemyMaxShields, gbc_enemyMaxShields);

		JLabel lblAiDefensive = new JLabel("AI Defensive:");
		GridBagConstraints gbc_lblAiDefensive = new GridBagConstraints();
		gbc_lblAiDefensive.anchor = GridBagConstraints.EAST;
		gbc_lblAiDefensive.insets = new Insets(0, 0, 5, 5);
		gbc_lblAiDefensive.gridx = 4;
		gbc_lblAiDefensive.gridy = 2;
		enemyPanel.add(lblAiDefensive, gbc_lblAiDefensive);

		aiDefensiveBox = new JComboBox();
		aiDefensiveBox.setEnabled(false);
		GridBagConstraints gbc_aiDefensiveBox = new GridBagConstraints();
		gbc_aiDefensiveBox.insets = new Insets(0, 0, 5, 0);
		gbc_aiDefensiveBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_aiDefensiveBox.gridx = 5;
		gbc_aiDefensiveBox.gridy = 2;
		enemyPanel.add(aiDefensiveBox, gbc_aiDefensiveBox);

		JLabel lblMaxSpeed = new JLabel("Max Speed:");
		lblMaxSpeed.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblMaxSpeed = new GridBagConstraints();
		gbc_lblMaxSpeed.anchor = GridBagConstraints.EAST;
		gbc_lblMaxSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxSpeed.gridx = 0;
		gbc_lblMaxSpeed.gridy = 3;
		enemyPanel.add(lblMaxSpeed, gbc_lblMaxSpeed);
		lblMaxSpeed.setLabelFor(enemySpeedSpinner);

		enemySpeedSpinner = new JSpinner();
		enemySpeedSpinner.setEnabled(false);
		enemySpeedSpinner.setModel(new SpinnerNumberModel(0, 0, 1400, 1));
		GridBagConstraints gbc_enemySpeedSpinner = new GridBagConstraints();
		gbc_enemySpeedSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemySpeedSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_enemySpeedSpinner.gridx = 1;
		gbc_enemySpeedSpinner.gridy = 3;
		enemyPanel.add(enemySpeedSpinner, gbc_enemySpeedSpinner);

		JLabel lblShieldRechargeDelay = new JLabel("Recharge Delay:");
		GridBagConstraints gbc_lblShieldRechargeDelay = new GridBagConstraints();
		gbc_lblShieldRechargeDelay.anchor = GridBagConstraints.EAST;
		gbc_lblShieldRechargeDelay.insets = new Insets(0, 0, 5, 5);
		gbc_lblShieldRechargeDelay.gridx = 2;
		gbc_lblShieldRechargeDelay.gridy = 3;
		enemyPanel.add(lblShieldRechargeDelay, gbc_lblShieldRechargeDelay);

		enemyShieldRecharge = new JSpinner();
		enemyShieldRecharge.setEnabled(false);
		enemyShieldRecharge.setModel(new SpinnerNumberModel(new Float(5.0),
				new Float(0.0), null, new Float(0.5)));
		GridBagConstraints gbc_enemyShieldRecharge = new GridBagConstraints();
		gbc_enemyShieldRecharge.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyShieldRecharge.insets = new Insets(0, 0, 5, 5);
		gbc_enemyShieldRecharge.gridx = 3;
		gbc_enemyShieldRecharge.gridy = 3;
		enemyPanel.add(enemyShieldRecharge, gbc_enemyShieldRecharge);

		JLabel lblAiAgressive = new JLabel("AI Agressive:");
		GridBagConstraints gbc_lblAiAgressive = new GridBagConstraints();
		gbc_lblAiAgressive.anchor = GridBagConstraints.EAST;
		gbc_lblAiAgressive.insets = new Insets(0, 0, 5, 5);
		gbc_lblAiAgressive.gridx = 4;
		gbc_lblAiAgressive.gridy = 3;
		enemyPanel.add(lblAiAgressive, gbc_lblAiAgressive);

		aiAgressiveBox = new JComboBox();
		aiAgressiveBox.setEnabled(false);
		GridBagConstraints gbc_aiAgressiveBox = new GridBagConstraints();
		gbc_aiAgressiveBox.insets = new Insets(0, 0, 5, 0);
		gbc_aiAgressiveBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_aiAgressiveBox.gridx = 5;
		gbc_aiAgressiveBox.gridy = 3;
		enemyPanel.add(aiAgressiveBox, gbc_aiAgressiveBox);

		JLabel lblAcceleration = new JLabel("Acceleration:");
		GridBagConstraints gbc_lblAcceleration = new GridBagConstraints();
		gbc_lblAcceleration.anchor = GridBagConstraints.EAST;
		gbc_lblAcceleration.insets = new Insets(0, 0, 5, 5);
		gbc_lblAcceleration.gridx = 0;
		gbc_lblAcceleration.gridy = 4;
		enemyPanel.add(lblAcceleration, gbc_lblAcceleration);

		enemyAccelerationSpinner = new JSpinner();
		enemyAccelerationSpinner.setEnabled(false);
		enemyAccelerationSpinner
				.setToolTipText("Percentage of max speed per second");
		enemyAccelerationSpinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		GridBagConstraints gbc_enemyAccelerationSpinner = new GridBagConstraints();
		gbc_enemyAccelerationSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyAccelerationSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_enemyAccelerationSpinner.gridx = 1;
		gbc_enemyAccelerationSpinner.gridy = 4;
		enemyPanel.add(enemyAccelerationSpinner, gbc_enemyAccelerationSpinner);

		JLabel lblShieldRechargeRate = new JLabel("Recharge Rate:");
		GridBagConstraints gbc_lblShieldRechargeRate = new GridBagConstraints();
		gbc_lblShieldRechargeRate.anchor = GridBagConstraints.EAST;
		gbc_lblShieldRechargeRate.insets = new Insets(0, 0, 5, 5);
		gbc_lblShieldRechargeRate.gridx = 2;
		gbc_lblShieldRechargeRate.gridy = 4;
		enemyPanel.add(lblShieldRechargeRate, gbc_lblShieldRechargeRate);

		enemyRechargeSpinner = new JSpinner();
		enemyRechargeSpinner.setEnabled(false);
		enemyRechargeSpinner.setToolTipText("Percentage of Shield Per Second");
		enemyRechargeSpinner.setModel(new SpinnerNumberModel(0, 0, 100, 1));
		GridBagConstraints gbc_enemyRechargeSpinner = new GridBagConstraints();
		gbc_enemyRechargeSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyRechargeSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_enemyRechargeSpinner.gridx = 3;
		gbc_enemyRechargeSpinner.gridy = 4;
		enemyPanel.add(enemyRechargeSpinner, gbc_enemyRechargeSpinner);

		JLabel lblAiNeutral = new JLabel("AI Neutral:");
		GridBagConstraints gbc_lblAiNeutral = new GridBagConstraints();
		gbc_lblAiNeutral.anchor = GridBagConstraints.EAST;
		gbc_lblAiNeutral.insets = new Insets(0, 0, 5, 5);
		gbc_lblAiNeutral.gridx = 4;
		gbc_lblAiNeutral.gridy = 4;
		enemyPanel.add(lblAiNeutral, gbc_lblAiNeutral);

		aiNeutralBox = new JComboBox();
		aiNeutralBox.setEnabled(false);
		GridBagConstraints gbc_aiNeutralBox = new GridBagConstraints();
		gbc_aiNeutralBox.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_aiNeutralBox.insets = new Insets(0, 0, 5, 0);
		gbc_aiNeutralBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_aiNeutralBox.gridx = 5;
		gbc_aiNeutralBox.gridy = 4;
		enemyPanel.add(aiNeutralBox, gbc_aiNeutralBox);

		JLabel lblTurnRate = new JLabel("Turn Rate:");
		GridBagConstraints gbc_lblTurnRate = new GridBagConstraints();
		gbc_lblTurnRate.anchor = GridBagConstraints.EAST;
		gbc_lblTurnRate.insets = new Insets(0, 0, 5, 5);
		gbc_lblTurnRate.gridx = 0;
		gbc_lblTurnRate.gridy = 5;
		enemyPanel.add(lblTurnRate, gbc_lblTurnRate);

		btnChooseSpriteSheet = new JButton("Choose Sprite Sheet");
		btnChooseSpriteSheet.setEnabled(false);
		btnChooseSpriteSheet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				chooser.setFileFilter(spriteSheetFilter);
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int returnVal = chooser.showOpenDialog(MainWindow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: "
							+ chooser.getSelectedFile().getName());
					enemySpriteSheetName = chooser.getSelectedFile().getName();
					((TitledBorder) (spriteSheetPanel.getBorder()))
							.setTitle("Sprite Sheet: " + enemySpriteSheetName);
					try {
						BufferedImage buffer = ImageIO.read(chooser
								.getSelectedFile());
						enemyShipSpriteSheet.setIcon(new ImageIcon(buffer));

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		enemyTurnSpeed = new JSpinner();
		enemyTurnSpeed.setEnabled(false);
		enemyTurnSpeed.setModel(new SpinnerNumberModel(new Float(1.0),
				new Float(0.5), new Float(30), new Float(0.1)));
		enemyTurnSpeed
				.setToolTipText("Number of Seconds to ship to rotate 180 degrees");
		GridBagConstraints gbc_enemyTurnSpeed = new GridBagConstraints();
		gbc_enemyTurnSpeed.fill = GridBagConstraints.HORIZONTAL;
		gbc_enemyTurnSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_enemyTurnSpeed.gridx = 1;
		gbc_enemyTurnSpeed.gridy = 5;
		enemyPanel.add(enemyTurnSpeed, gbc_enemyTurnSpeed);

		JLabel lblAiSpecial = new JLabel("AI Special:");
		GridBagConstraints gbc_lblAiSpecial = new GridBagConstraints();
		gbc_lblAiSpecial.anchor = GridBagConstraints.EAST;
		gbc_lblAiSpecial.insets = new Insets(0, 0, 5, 5);
		gbc_lblAiSpecial.gridx = 4;
		gbc_lblAiSpecial.gridy = 5;
		enemyPanel.add(lblAiSpecial, gbc_lblAiSpecial);

		aiSpecialBox = new JComboBox();
		aiSpecialBox.setEnabled(false);
		GridBagConstraints gbc_aiSpecialBox = new GridBagConstraints();
		gbc_aiSpecialBox.insets = new Insets(0, 0, 5, 0);
		gbc_aiSpecialBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_aiSpecialBox.gridx = 5;
		gbc_aiSpecialBox.gridy = 5;
		enemyPanel.add(aiSpecialBox, gbc_aiSpecialBox);

		spriteSheetPanel = new JPanel();
		spriteSheetPanel.setBorder(new TitledBorder(null, "Sprite Sheet",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_spriteSheetPanel = new GridBagConstraints();
		gbc_spriteSheetPanel.fill = GridBagConstraints.BOTH;
		gbc_spriteSheetPanel.gridwidth = 6;
		gbc_spriteSheetPanel.insets = new Insets(0, 0, 5, 0);
		gbc_spriteSheetPanel.gridx = 0;
		gbc_spriteSheetPanel.gridy = 6;
		enemyPanel.add(spriteSheetPanel, gbc_spriteSheetPanel);
		GridBagLayout gbl_spriteSheetPanel = new GridBagLayout();
		gbl_spriteSheetPanel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_spriteSheetPanel.rowHeights = new int[] { 0, 0, 0, 23, 0 };
		gbl_spriteSheetPanel.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0,
				0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_spriteSheetPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		spriteSheetPanel.setLayout(gbl_spriteSheetPanel);

		enemyShipSpriteSheet = new JLabel("");
		GridBagConstraints gbc_enemyShipSpriteSheet = new GridBagConstraints();
		gbc_enemyShipSpriteSheet.gridheight = 4;
		gbc_enemyShipSpriteSheet.gridwidth = 7;
		gbc_enemyShipSpriteSheet.insets = new Insets(0, 0, 5, 5);
		gbc_enemyShipSpriteSheet.gridx = 0;
		gbc_enemyShipSpriteSheet.gridy = 0;
		spriteSheetPanel.add(enemyShipSpriteSheet, gbc_enemyShipSpriteSheet);
		GridBagConstraints gbc_btnChooseSpriteSheet = new GridBagConstraints();
		gbc_btnChooseSpriteSheet.anchor = GridBagConstraints.WEST;
		gbc_btnChooseSpriteSheet.gridwidth = 2;
		gbc_btnChooseSpriteSheet.insets = new Insets(0, 0, 0, 5);
		gbc_btnChooseSpriteSheet.gridx = 0;
		gbc_btnChooseSpriteSheet.gridy = 7;
		enemyPanel.add(btnChooseSpriteSheet, gbc_btnChooseSpriteSheet);

		JButton btnNewEnemy = new JButton("New Enemy");
		btnNewEnemy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				enemyName.setEnabled(true);
				enemyName.setText("");
				enemyAccelerationSpinner.setEnabled(true);
				enemySpeedSpinner.setEnabled(true);
				enemyShipSizeBox.setEnabled(true);
				enemyShipSizeBox.setSelectedIndex(-1);
				enemyTurnSpeed.setEnabled(true);
				enemyMaxShields.setEnabled(true);
				enemyRechargeSpinner.setEnabled(true);
				enemyShieldRecharge.setEnabled(true);
				aiArchetypeBox.setEnabled(true);
				aiArchetypeBox.setSelectedIndex(-1);
				enemyWeaponBox.setEnabled(true);
				enemyWeaponBox.setSelectedIndex(-1);
				aiDefensiveBox.setEnabled(true);
				aiDefensiveBox.setSelectedIndex(-1);
				aiAgressiveBox.setEnabled(true);
				aiAgressiveBox.setSelectedIndex(-1);
				aiNeutralBox.setEnabled(true);
				aiNeutralBox.setSelectedIndex(-1);
				aiSpecialBox.setEnabled(true);
				aiSpecialBox.setSelectedIndex(-1);
				btnChooseSpriteSheet.setEnabled(true);

				((TitledBorder) (spriteSheetPanel.getBorder()))
						.setTitle("Sprite Sheet");
				enemyShipSpriteSheet.setIcon(null);
				enemySpriteSheetName = null;
			}
		});
		GridBagConstraints gbc_btnNewEnemy = new GridBagConstraints();
		gbc_btnNewEnemy.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewEnemy.gridx = 4;
		gbc_btnNewEnemy.gridy = 7;
		enemyPanel.add(btnNewEnemy, gbc_btnNewEnemy);

		btnAddEnemy = new JButton("Add Enemy");
		btnAddEnemy.setEnabled(false);
		GridBagConstraints gbc_btnAddEnemy = new GridBagConstraints();
		gbc_btnAddEnemy.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnAddEnemy.gridx = 5;
		gbc_btnAddEnemy.gridy = 7;
		enemyPanel.add(btnAddEnemy, gbc_btnAddEnemy);

		JPanel weaponPane = new JPanel();
		tabbedPane.addTab("Weapons", null, weaponPane, null);
		GridBagLayout gbl_weaponPane = new GridBagLayout();
		gbl_weaponPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_weaponPane.rowHeights = new int[] { 108, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_weaponPane.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		gbl_weaponPane.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 1.0, 0.0, Double.MIN_VALUE };
		weaponPane.setLayout(gbl_weaponPane);

		JScrollPane weaponTablePane = new JScrollPane();
		GridBagConstraints gbc_weaponTablePane = new GridBagConstraints();
		gbc_weaponTablePane.fill = GridBagConstraints.BOTH;
		gbc_weaponTablePane.gridwidth = 6;
		gbc_weaponTablePane.insets = new Insets(0, 0, 5, 0);
		gbc_weaponTablePane.gridx = 0;
		gbc_weaponTablePane.gridy = 0;
		weaponPane.add(weaponTablePane, gbc_weaponTablePane);

		tableWeapon = new JTable();
		tableWeapon.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableWeapon.setModel(new DefaultTableModel(new Object[][] { { null,
				null, null, null, null, null, null }, }, new String[] { "ID",
				"Name", "Power", "Fire Rate", "Weapon Image", "Shot Image",
				"Sound Effect" }) {
			Class[] columnTypes = new Class[] { String.class, String.class,
					Integer.class, Float.class, String.class, String.class,
					String.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false,
					false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableWeapon.getColumnModel().getColumn(4).setPreferredWidth(100);
		tableWeapon.getColumnModel().getColumn(5).setPreferredWidth(100);
		tableWeapon.getColumnModel().getColumn(6).setPreferredWidth(100);
		weaponTablePane.setViewportView(tableWeapon);

		JLabel weaponNameLabel = new JLabel("Name:");
		weaponNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_weaponNameLabel = new GridBagConstraints();
		gbc_weaponNameLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_weaponNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_weaponNameLabel.gridx = 0;
		gbc_weaponNameLabel.gridy = 1;
		weaponPane.add(weaponNameLabel, gbc_weaponNameLabel);

		weaponNameField = new JTextField();
		weaponNameField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				checkWeaponInput();
			}
		});

		weaponNameField.setEnabled(false);
		weaponNameField.setColumns(10);
		GridBagConstraints gbc_weaponNameField = new GridBagConstraints();
		gbc_weaponNameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_weaponNameField.insets = new Insets(0, 0, 5, 5);
		gbc_weaponNameField.gridx = 1;
		gbc_weaponNameField.gridy = 1;
		weaponPane.add(weaponNameField, gbc_weaponNameField);

		JLabel massLabel = new JLabel("Mass:");
		massLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_massLabel = new GridBagConstraints();
		gbc_massLabel.anchor = GridBagConstraints.EAST;
		gbc_massLabel.insets = new Insets(0, 0, 5, 5);
		gbc_massLabel.gridx = 2;
		gbc_massLabel.gridy = 1;
		weaponPane.add(massLabel, gbc_massLabel);

		massSpinner = new JSpinner();
		massSpinner.setEnabled(false);
		massSpinner.setModel(new SpinnerNumberModel(0, 0, 10000, 1));
		GridBagConstraints gbc_massSpinner = new GridBagConstraints();
		gbc_massSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_massSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_massSpinner.gridx = 3;
		gbc_massSpinner.gridy = 1;
		weaponPane.add(massSpinner, gbc_massSpinner);

		JLabel powerLabel = new JLabel("Power:");
		powerLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_powerLabel = new GridBagConstraints();
		gbc_powerLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_powerLabel.insets = new Insets(0, 0, 5, 5);
		gbc_powerLabel.gridx = 0;
		gbc_powerLabel.gridy = 2;
		weaponPane.add(powerLabel, gbc_powerLabel);

		weaponPowerSpinner = new JSpinner();
		weaponPowerSpinner.setEnabled(false);
		weaponPowerSpinner
				.setModel(new SpinnerNumberModel(100, 0, 10000000, 1));
		GridBagConstraints gbc_weaponPowerSpinner = new GridBagConstraints();
		gbc_weaponPowerSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_weaponPowerSpinner.gridx = 1;
		gbc_weaponPowerSpinner.gridy = 2;
		weaponPane.add(weaponPowerSpinner, gbc_weaponPowerSpinner);

		JLabel gravityScalerLabel = new JLabel("Gravity Scaler:");
		GridBagConstraints gbc_gravityScalerLabel = new GridBagConstraints();
		gbc_gravityScalerLabel.anchor = GridBagConstraints.EAST;
		gbc_gravityScalerLabel.insets = new Insets(0, 0, 5, 5);
		gbc_gravityScalerLabel.gridx = 2;
		gbc_gravityScalerLabel.gridy = 2;
		weaponPane.add(gravityScalerLabel, gbc_gravityScalerLabel);

		gravityScalerSpinner = new JSpinner();
		gravityScalerSpinner.setModel(new SpinnerNumberModel(new Float(0),
				new Float(0), new Float(10), new Float(1)));
		gravityScalerSpinner.setEnabled(false);
		GridBagConstraints gbc_gravityScalerSpinner = new GridBagConstraints();
		gbc_gravityScalerSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_gravityScalerSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_gravityScalerSpinner.gridx = 3;
		gbc_gravityScalerSpinner.gridy = 2;
		weaponPane.add(gravityScalerSpinner, gbc_gravityScalerSpinner);

		soundPanel = new JPanel();
		soundPanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Sound Effect",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_soundPanel = new GridBagConstraints();
		gbc_soundPanel.fill = GridBagConstraints.BOTH;
		gbc_soundPanel.gridheight = 3;
		gbc_soundPanel.gridwidth = 2;
		gbc_soundPanel.insets = new Insets(0, 0, 5, 0);
		gbc_soundPanel.gridx = 4;
		gbc_soundPanel.gridy = 1;
		weaponPane.add(soundPanel, gbc_soundPanel);
		GridBagLayout gbl_soundPanel = new GridBagLayout();
		gbl_soundPanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_soundPanel.rowHeights = new int[] { 0, 0, 0 };
		gbl_soundPanel.columnWeights = new double[] { 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_soundPanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		soundPanel.setLayout(gbl_soundPanel);

		btnChooseGunSound = new JButton("Choose Gun Sound");
		btnChooseGunSound.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				chooser.setFileFilter(soundFilter);
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int returnVal = chooser.showOpenDialog(MainWindow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: "
							+ chooser.getSelectedFile().getName());
					weaponSoundEffectName = chooser.getSelectedFile().getName();

					((TitledBorder) (soundPanel.getBorder()))
							.setTitle("Sprite Sheet: " + weaponSoundEffectName);
					try {
						audioInWeapon = AudioSystem.getAudioInputStream(chooser
								.getSelectedFile());
						weaponSoundEffect = AudioSystem.getClip();
						weaponSoundEffect.open(audioInWeapon);
						weaponSoundEffect.addLineListener(soundListener);

						btnPlay.setEnabled(true);
						checkWeaponInput();
					} catch (UnsupportedAudioFileException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (LineUnavailableException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		GridBagConstraints gbc_btnChooseGunSound = new GridBagConstraints();
		gbc_btnChooseGunSound.gridwidth = 2;
		gbc_btnChooseGunSound.insets = new Insets(0, 0, 5, 0);
		gbc_btnChooseGunSound.gridx = 0;
		gbc_btnChooseGunSound.gridy = 0;
		soundPanel.add(btnChooseGunSound, gbc_btnChooseGunSound);
		btnChooseGunSound.setEnabled(false);

		soundListener = new LineListener() {

			@Override
			public void update(LineEvent event) {
				if (event.getType() == LineEvent.Type.STOP) {
					btnPlay.setEnabled(true);
					btnStop.setEnabled(false);
				}

			}
		};

		btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				weaponSoundEffect.stop();
				btnPlay.setEnabled(true);
				btnStop.setEnabled(false);
			}
		});
		btnStop.setEnabled(false);
		GridBagConstraints gbc_btnStop = new GridBagConstraints();
		gbc_btnStop.anchor = GridBagConstraints.EAST;
		gbc_btnStop.insets = new Insets(0, 0, 0, 5);
		gbc_btnStop.gridx = 0;
		gbc_btnStop.gridy = 1;
		soundPanel.add(btnStop, gbc_btnStop);

		btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				weaponSoundEffect.setFramePosition(0);
				weaponSoundEffect.start();
				btnStop.setEnabled(true);
				btnPlay.setEnabled(false);
			}
		});
		btnPlay.setEnabled(false);
		GridBagConstraints gbc_btnPlay = new GridBagConstraints();
		gbc_btnPlay.anchor = GridBagConstraints.WEST;
		gbc_btnPlay.gridx = 1;
		gbc_btnPlay.gridy = 1;
		soundPanel.add(btnPlay, gbc_btnPlay);

		JLabel fireRateLabel = new JLabel("Fire Rate:");
		fireRateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_fireRateLabel = new GridBagConstraints();
		gbc_fireRateLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_fireRateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_fireRateLabel.gridx = 0;
		gbc_fireRateLabel.gridy = 3;
		weaponPane.add(fireRateLabel, gbc_fireRateLabel);

		fireRateSpinner = new JSpinner();
		fireRateSpinner.setToolTipText("Bullets Shot per second");
		fireRateSpinner.setModel(new SpinnerNumberModel(new Float(1.0),
				new Float(0.5), new Float(100), new Float(0.1)));
		fireRateSpinner.setEnabled(false);
		GridBagConstraints gbc_fireRateSpinner = new GridBagConstraints();
		gbc_fireRateSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_fireRateSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_fireRateSpinner.gridx = 1;
		gbc_fireRateSpinner.gridy = 3;
		weaponPane.add(fireRateSpinner, gbc_fireRateSpinner);

		JLabel accuracyLabel = new JLabel("Accuracy:");
		GridBagConstraints gbc_accuracyLabel = new GridBagConstraints();
		gbc_accuracyLabel.anchor = GridBagConstraints.EAST;
		gbc_accuracyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_accuracyLabel.gridx = 2;
		gbc_accuracyLabel.gridy = 3;
		weaponPane.add(accuracyLabel, gbc_accuracyLabel);

		accuracySpinner = new JSpinner();
		accuracySpinner.setToolTipText("accuracy in a degree arc");
		accuracySpinner.setModel(new SpinnerNumberModel(0, 0, 80, 1));
		accuracySpinner.setEnabled(false);
		GridBagConstraints gbc_accuracySpinner = new GridBagConstraints();
		gbc_accuracySpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_accuracySpinner.insets = new Insets(0, 0, 5, 5);
		gbc_accuracySpinner.gridx = 3;
		gbc_accuracySpinner.gridy = 3;
		weaponPane.add(accuracySpinner, gbc_accuracySpinner);

		JLabel velocityLabel = new JLabel("Velocity:");
		GridBagConstraints gbc_velocityLabel = new GridBagConstraints();
		gbc_velocityLabel.anchor = GridBagConstraints.EAST;
		gbc_velocityLabel.insets = new Insets(0, 0, 5, 5);
		gbc_velocityLabel.gridx = 0;
		gbc_velocityLabel.gridy = 4;
		weaponPane.add(velocityLabel, gbc_velocityLabel);

		fireVelocitySpinner = new JSpinner();
		fireVelocitySpinner
				.setModel(new SpinnerNumberModel(100, 0, 1000000, 1));
		fireVelocitySpinner
				.setToolTipText("Percentage of max speed per second");
		fireVelocitySpinner.setEnabled(false);
		GridBagConstraints gbc_fireVelocitySpinner = new GridBagConstraints();
		gbc_fireVelocitySpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_fireVelocitySpinner.insets = new Insets(0, 0, 5, 5);
		gbc_fireVelocitySpinner.gridx = 1;
		gbc_fireVelocitySpinner.gridy = 4;
		weaponPane.add(fireVelocitySpinner, gbc_fireVelocitySpinner);

		lblShotHealth = new JLabel("Shot Health:");
		GridBagConstraints gbc_lblShotHealth = new GridBagConstraints();
		gbc_lblShotHealth.anchor = GridBagConstraints.EAST;
		gbc_lblShotHealth.insets = new Insets(0, 0, 5, 5);
		gbc_lblShotHealth.gridx = 2;
		gbc_lblShotHealth.gridy = 4;
		weaponPane.add(lblShotHealth, gbc_lblShotHealth);

		shotHealthSpinner = new JSpinner();
		shotHealthSpinner.setModel(new SpinnerNumberModel(1, 1, 1000000, 1));
		shotHealthSpinner.setEnabled(false);
		GridBagConstraints gbc_shotHealthSpinner = new GridBagConstraints();
		gbc_shotHealthSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_shotHealthSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_shotHealthSpinner.gridx = 3;
		gbc_shotHealthSpinner.gridy = 4;
		weaponPane.add(shotHealthSpinner, gbc_shotHealthSpinner);

		JPanel weaponType = new JPanel();
		weaponType.setBorder(new TitledBorder(null, "Weapon Type:",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_weaponType = new GridBagConstraints();
		gbc_weaponType.gridheight = 2;
		gbc_weaponType.fill = GridBagConstraints.BOTH;
		gbc_weaponType.gridwidth = 2;
		gbc_weaponType.insets = new Insets(0, 0, 5, 0);
		gbc_weaponType.gridx = 4;
		gbc_weaponType.gridy = 4;
		weaponPane.add(weaponType, gbc_weaponType);
		GridBagLayout gbl_weaponType = new GridBagLayout();
		gbl_weaponType.columnWidths = new int[] { 0, 0, 0 };
		gbl_weaponType.rowHeights = new int[] { 0, 0 };
		gbl_weaponType.columnWeights = new double[] { 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_weaponType.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		weaponType.setLayout(gbl_weaponType);

		rdbtnEnemy = new JRadioButton("Enemy");
		rdbtnEnemy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gunSpritebutton.setEnabled(false);
				weaponSpriteName = null;
				gunSpriteLabel.setIcon(null);
				((TitledBorder) (gunSpritePanel.getBorder()))
						.setTitle("Weapon Sprite");
				gunSpritePanel.setEnabled(false);
				checkWeaponInput();

			}
		});
		rdbtnEnemy.setEnabled(false);
		weaponButtonGroup.add(rdbtnEnemy);
		GridBagConstraints gbc_rdbtnEnemy = new GridBagConstraints();
		gbc_rdbtnEnemy.anchor = GridBagConstraints.WEST;
		gbc_rdbtnEnemy.insets = new Insets(0, 0, 0, 5);
		gbc_rdbtnEnemy.gridx = 0;
		gbc_rdbtnEnemy.gridy = 0;
		weaponType.add(rdbtnEnemy, gbc_rdbtnEnemy);

		playerRadioButton = new JRadioButton("Player");
		playerRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gunSpritebutton.setEnabled(true);
				gunSpritePanel.setEnabled(true);
				checkWeaponInput();
			}
		});
		playerRadioButton.setEnabled(false);
		playerRadioButton.setSelected(true);
		weaponButtonGroup.add(playerRadioButton);
		GridBagConstraints gbc_playerRadioButton = new GridBagConstraints();
		gbc_playerRadioButton.anchor = GridBagConstraints.WEST;
		gbc_playerRadioButton.gridx = 1;
		gbc_playerRadioButton.gridy = 0;
		weaponType.add(playerRadioButton, gbc_playerRadioButton);

		JLabel recoilLabel = new JLabel("Recoil:");
		GridBagConstraints gbc_recoilLabel = new GridBagConstraints();
		gbc_recoilLabel.anchor = GridBagConstraints.EAST;
		gbc_recoilLabel.insets = new Insets(0, 0, 5, 5);
		gbc_recoilLabel.gridx = 0;
		gbc_recoilLabel.gridy = 5;
		weaponPane.add(recoilLabel, gbc_recoilLabel);

		weaponRecoilSpinner = new JSpinner();
		weaponRecoilSpinner.setModel(new SpinnerNumberModel(0, 0, 1000000, 1));
		weaponRecoilSpinner.setToolTipText("recoil velocity");
		weaponRecoilSpinner.setEnabled(false);
		GridBagConstraints gbc_recoilSpinner = new GridBagConstraints();
		gbc_recoilSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_recoilSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_recoilSpinner.gridx = 1;
		gbc_recoilSpinner.gridy = 5;
		weaponPane.add(weaponRecoilSpinner, gbc_recoilSpinner);

		gunSpritePanel = new JPanel();
		gunSpritePanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Gun Sprite",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_gunSpritePanel = new GridBagConstraints();
		gbc_gunSpritePanel.fill = GridBagConstraints.BOTH;
		gbc_gunSpritePanel.gridwidth = 2;
		gbc_gunSpritePanel.insets = new Insets(0, 0, 5, 5);
		gbc_gunSpritePanel.gridx = 0;
		gbc_gunSpritePanel.gridy = 6;
		weaponPane.add(gunSpritePanel, gbc_gunSpritePanel);
		GridBagLayout gbl_gunSpritePanel = new GridBagLayout();
		gbl_gunSpritePanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_gunSpritePanel.rowHeights = new int[] { 0, 0 };
		gbl_gunSpritePanel.columnWeights = new double[] { 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_gunSpritePanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		gunSpritePanel.setLayout(gbl_gunSpritePanel);

		gunSpriteLabel = new JLabel("");
		GridBagConstraints gbc_gunSpriteLabel = new GridBagConstraints();
		gbc_gunSpriteLabel.gridwidth = 2;
		gbc_gunSpriteLabel.insets = new Insets(0, 0, 0, 5);
		gbc_gunSpriteLabel.gridx = 0;
		gbc_gunSpriteLabel.gridy = 0;
		gunSpritePanel.add(gunSpriteLabel, gbc_gunSpriteLabel);

		shotSpritePanel = new JPanel();
		shotSpritePanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Shot Sprite",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_shotSpritePanel = new GridBagConstraints();
		gbc_shotSpritePanel.fill = GridBagConstraints.BOTH;
		gbc_shotSpritePanel.gridwidth = 2;
		gbc_shotSpritePanel.insets = new Insets(0, 0, 5, 5);
		gbc_shotSpritePanel.gridx = 2;
		gbc_shotSpritePanel.gridy = 6;
		weaponPane.add(shotSpritePanel, gbc_shotSpritePanel);
		GridBagLayout gbl_shotSpritePanel = new GridBagLayout();
		gbl_shotSpritePanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_shotSpritePanel.rowHeights = new int[] { 0, 0 };
		gbl_shotSpritePanel.columnWeights = new double[] { 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_shotSpritePanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		shotSpritePanel.setLayout(gbl_shotSpritePanel);

		fireSpriteLabel = new JLabel("");
		GridBagConstraints gbc_fireSpriteLabel = new GridBagConstraints();
		gbc_fireSpriteLabel.gridwidth = 2;
		gbc_fireSpriteLabel.insets = new Insets(0, 0, 0, 5);
		gbc_fireSpriteLabel.gridx = 0;
		gbc_fireSpriteLabel.gridy = 0;
		shotSpritePanel.add(fireSpriteLabel, gbc_fireSpriteLabel);

		gunSpritebutton = new JButton("Choose Gun Sprite");
		gunSpritebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				chooser.setFileFilter(spriteSheetFilter);
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int returnVal = chooser.showOpenDialog(MainWindow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: "
							+ chooser.getSelectedFile().getName());
					weaponSpriteName = chooser.getSelectedFile().getName();
					((TitledBorder) (gunSpritePanel.getBorder()))
							.setTitle("Weapon Sprite: " + weaponSpriteName);
					try {
						BufferedImage buffer = ImageIO.read(chooser
								.getSelectedFile());
						gunSpriteLabel.setIcon(new ImageIcon(buffer));
						checkWeaponInput();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		gunSpritebutton.setEnabled(false);
		GridBagConstraints gbc_gunSpritebutton = new GridBagConstraints();
		gbc_gunSpritebutton.gridwidth = 2;
		gbc_gunSpritebutton.anchor = GridBagConstraints.WEST;
		gbc_gunSpritebutton.insets = new Insets(0, 0, 0, 5);
		gbc_gunSpritebutton.gridx = 0;
		gbc_gunSpritebutton.gridy = 7;
		weaponPane.add(gunSpritebutton, gbc_gunSpritebutton);

		fireSpriteButton = new JButton("Choose Fire Sprite");
		fireSpriteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				chooser.setFileFilter(spriteSheetFilter);
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int returnVal = chooser.showOpenDialog(MainWindow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: "
							+ chooser.getSelectedFile().getName());
					fireSpriteName = chooser.getSelectedFile().getName();
					((TitledBorder) (shotSpritePanel.getBorder()))
							.setTitle("Shot Sprite: " + fireSpriteName);
					try {
						BufferedImage buffer = ImageIO.read(chooser
								.getSelectedFile());
						fireSpriteLabel.setIcon(new ImageIcon(buffer));
						checkWeaponInput();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		fireSpriteButton.setEnabled(false);
		GridBagConstraints gbc_fireSpriteButton = new GridBagConstraints();
		gbc_fireSpriteButton.anchor = GridBagConstraints.WEST;
		gbc_fireSpriteButton.gridwidth = 2;
		gbc_fireSpriteButton.insets = new Insets(0, 0, 0, 5);
		gbc_fireSpriteButton.gridx = 2;
		gbc_fireSpriteButton.gridy = 7;
		weaponPane.add(fireSpriteButton, gbc_fireSpriteButton);

		JButton newWeaponButton = new JButton("New Weapon");
		newWeaponButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetWeapon();
			}
		});
		GridBagConstraints gbc_newWeaponButton = new GridBagConstraints();
		gbc_newWeaponButton.insets = new Insets(0, 0, 0, 5);
		gbc_newWeaponButton.gridx = 4;
		gbc_newWeaponButton.gridy = 7;
		weaponPane.add(newWeaponButton, gbc_newWeaponButton);

		addWeaponbutton = new JButton("Add Weapon");
		addWeaponbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addWeapon();

			}
		});
		addWeaponbutton.setEnabled(false);
		GridBagConstraints gbc_addWeaponbutton = new GridBagConstraints();
		gbc_addWeaponbutton.anchor = GridBagConstraints.NORTHEAST;
		gbc_addWeaponbutton.gridx = 5;
		gbc_addWeaponbutton.gridy = 7;
		weaponPane.add(addWeaponbutton, gbc_addWeaponbutton);

		equipmentPanel = new JPanel();
		tabbedPane.addTab("Equipment", null, equipmentPanel, null);
		GridBagLayout gbl_equipmentPanel = new GridBagLayout();
		gbl_equipmentPanel.columnWidths = new int[] { 200, 0, 0, 0, 0 };
		gbl_equipmentPanel.rowHeights = new int[] { 108, 0, 0, 0, 0 };
		gbl_equipmentPanel.columnWeights = new double[] { 1.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_equipmentPanel.rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		equipmentPanel.setLayout(gbl_equipmentPanel);

		equipmentScrollPane = new JScrollPane();
		GridBagConstraints gbc_equipmentScrollPane = new GridBagConstraints();
		gbc_equipmentScrollPane.fill = GridBagConstraints.BOTH;
		gbc_equipmentScrollPane.gridwidth = 4;
		gbc_equipmentScrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_equipmentScrollPane.gridx = 0;
		gbc_equipmentScrollPane.gridy = 0;
		equipmentPanel.add(equipmentScrollPane, gbc_equipmentScrollPane);

		equipmentTable = new JTable();
		equipmentTable.setModel(new DefaultTableModel(new Object[][] { { null,
				null, null, null, null, null }, }, new String[] { "ID", "Type",
				"Name", "Max", "Rate", "Time" }) {
			Class[] columnTypes = new Class[] { String.class, String.class,
					String.class, Integer.class, Object.class, Object.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		equipmentScrollPane.setViewportView(equipmentTable);

		rdbtnShield = new JRadioButton("Shield");
		rdbtnShield.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				maxShieldsSpinner.setEnabled(true);
				chargeDelaySpinner.setEnabled(true);
				chargeRateSpinner.setEnabled(true);
				addEquipmentButton.setText("Add Shield");
				addEquipmentButton.setEnabled(true);
				newEquipmentButton.setText("New Shield");
				maxSpeedSpinner.setEnabled(true);
				accelerationRateSpinner.setEnabled(false);
				maxBoostSpinner.setEnabled(false);
				chooseEngineSprite.setEnabled(false);
				shieldNameField.setEnabled(true);
				engineNameField.setEnabled(false);

				if (shieldNameField.getText().trim().length() == 0) {
					addEquipmentButton.setEnabled(false);
				} else {
					addEquipmentButton.setEnabled(true);
				}

			}
		});
		rdbtnShield.setSelected(true);
		equipmentButtonGroup.add(rdbtnShield);
		GridBagConstraints gbc_rdbtnShield = new GridBagConstraints();
		gbc_rdbtnShield.anchor = GridBagConstraints.EAST;
		gbc_rdbtnShield.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnShield.gridx = 0;
		gbc_rdbtnShield.gridy = 1;
		equipmentPanel.add(rdbtnShield, gbc_rdbtnShield);

		rdbtnEngine = new JRadioButton("Engine");
		rdbtnEngine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				maxShieldsSpinner.setEnabled(false);
				chargeDelaySpinner.setEnabled(false);
				chargeRateSpinner.setEnabled(false);
				addEquipmentButton.setText("Add Engine");
				addEquipmentButton.setEnabled(false);
				newEquipmentButton.setText("New Engine");
				maxSpeedSpinner.setEnabled(true);
				accelerationRateSpinner.setEnabled(true);
				maxBoostSpinner.setEnabled(true);
				chooseEngineSprite.setEnabled(true);
				shieldNameField.setEnabled(false);
				engineNameField.setEnabled(true);
				checkEngineInput();

			}
		});
		equipmentButtonGroup.add(rdbtnEngine);
		GridBagConstraints gbc_rdbtnEngine = new GridBagConstraints();
		gbc_rdbtnEngine.anchor = GridBagConstraints.WEST;
		gbc_rdbtnEngine.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnEngine.gridx = 1;
		gbc_rdbtnEngine.gridy = 1;
		equipmentPanel.add(rdbtnEngine, gbc_rdbtnEngine);

		shieldPanel = new JPanel();
		shieldPanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Shields",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_shieldPanel = new GridBagConstraints();
		gbc_shieldPanel.anchor = GridBagConstraints.NORTH;
		gbc_shieldPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc_shieldPanel.insets = new Insets(0, 0, 5, 5);
		gbc_shieldPanel.gridx = 0;
		gbc_shieldPanel.gridy = 2;
		equipmentPanel.add(shieldPanel, gbc_shieldPanel);
		GridBagLayout gbl_shieldPanel = new GridBagLayout();
		gbl_shieldPanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_shieldPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_shieldPanel.columnWeights = new double[] { 0.0, 1.0,
				Double.MIN_VALUE };
		gbl_shieldPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		shieldPanel.setLayout(gbl_shieldPanel);

		JLabel lblShieldName = new JLabel("Shield Name:");
		GridBagConstraints gbc_lblShieldName = new GridBagConstraints();
		gbc_lblShieldName.anchor = GridBagConstraints.EAST;
		gbc_lblShieldName.insets = new Insets(0, 0, 5, 5);
		gbc_lblShieldName.gridx = 0;
		gbc_lblShieldName.gridy = 0;
		shieldPanel.add(lblShieldName, gbc_lblShieldName);

		shieldNameField = new JTextField();
		shieldNameField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				if (shieldNameField.getText().trim().length() == 0) {
					addEquipmentButton.setEnabled(false);
				} else {
					addEquipmentButton.setEnabled(true);
				}

			}
		});
		GridBagConstraints gbc_shieldNameField = new GridBagConstraints();
		gbc_shieldNameField.insets = new Insets(0, 0, 5, 0);
		gbc_shieldNameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_shieldNameField.gridx = 1;
		gbc_shieldNameField.gridy = 0;
		shieldPanel.add(shieldNameField, gbc_shieldNameField);
		shieldNameField.setColumns(10);

		maxChargeLabel = new JLabel("Max Charge:");
		GridBagConstraints gbc_maxChargeLabel = new GridBagConstraints();
		gbc_maxChargeLabel.anchor = GridBagConstraints.EAST;
		gbc_maxChargeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_maxChargeLabel.gridx = 0;
		gbc_maxChargeLabel.gridy = 1;
		shieldPanel.add(maxChargeLabel, gbc_maxChargeLabel);
		maxChargeLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		maxShieldsSpinner = new JSpinner();
		GridBagConstraints gbc_maxShieldsSpinner = new GridBagConstraints();
		gbc_maxShieldsSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxShieldsSpinner.insets = new Insets(0, 0, 5, 0);
		gbc_maxShieldsSpinner.gridx = 1;
		gbc_maxShieldsSpinner.gridy = 1;
		shieldPanel.add(maxShieldsSpinner, gbc_maxShieldsSpinner);
		maxShieldsSpinner.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		maxShieldsSpinner.setToolTipText("Shield max");

		chargeDelayLabel = new JLabel("Charge Delay:");
		GridBagConstraints gbc_chargeDelayLabel = new GridBagConstraints();
		gbc_chargeDelayLabel.anchor = GridBagConstraints.EAST;
		gbc_chargeDelayLabel.insets = new Insets(0, 0, 5, 5);
		gbc_chargeDelayLabel.gridx = 0;
		gbc_chargeDelayLabel.gridy = 2;
		shieldPanel.add(chargeDelayLabel, gbc_chargeDelayLabel);

		chargeDelaySpinner = new JSpinner();
		GridBagConstraints gbc_chargeDelaySpinner = new GridBagConstraints();
		gbc_chargeDelaySpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_chargeDelaySpinner.insets = new Insets(0, 0, 5, 0);
		gbc_chargeDelaySpinner.gridx = 1;
		gbc_chargeDelaySpinner.gridy = 2;
		shieldPanel.add(chargeDelaySpinner, gbc_chargeDelaySpinner);
		chargeDelaySpinner.setModel(new SpinnerNumberModel(new Float(0),
				new Float(0), null, new Float(0)));
		chargeDelaySpinner.setToolTipText("Seconds to recharge shields");

		chargeRateLabel = new JLabel("Charge Rate:");
		GridBagConstraints gbc_chargeRateLabel = new GridBagConstraints();
		gbc_chargeRateLabel.anchor = GridBagConstraints.EAST;
		gbc_chargeRateLabel.insets = new Insets(0, 0, 0, 5);
		gbc_chargeRateLabel.gridx = 0;
		gbc_chargeRateLabel.gridy = 3;
		shieldPanel.add(chargeRateLabel, gbc_chargeRateLabel);

		chargeRateSpinner = new JSpinner();
		GridBagConstraints gbc_chargeRateSpinner = new GridBagConstraints();
		gbc_chargeRateSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_chargeRateSpinner.gridx = 1;
		gbc_chargeRateSpinner.gridy = 3;
		shieldPanel.add(chargeRateSpinner, gbc_chargeRateSpinner);
		chargeRateSpinner.setModel(new SpinnerNumberModel(new Float(0),
				new Float(0), null, new Float(0)));
		chargeRateSpinner.setToolTipText("Rate that which charge Rate");

		enginePanel = new JPanel();
		enginePanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Engine",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_enginePanel = new GridBagConstraints();
		gbc_enginePanel.fill = GridBagConstraints.BOTH;
		gbc_enginePanel.gridwidth = 3;
		gbc_enginePanel.insets = new Insets(0, 0, 5, 0);
		gbc_enginePanel.gridx = 1;
		gbc_enginePanel.gridy = 2;
		equipmentPanel.add(enginePanel, gbc_enginePanel);
		GridBagLayout gbl_enginePanel = new GridBagLayout();
		gbl_enginePanel.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_enginePanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_enginePanel.columnWeights = new double[] { 1.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_enginePanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		enginePanel.setLayout(gbl_enginePanel);

		lblEngineName = new JLabel("Engine Name:");
		GridBagConstraints gbc_lblEngineName = new GridBagConstraints();
		gbc_lblEngineName.anchor = GridBagConstraints.EAST;
		gbc_lblEngineName.insets = new Insets(0, 0, 5, 5);
		gbc_lblEngineName.gridx = 0;
		gbc_lblEngineName.gridy = 0;
		enginePanel.add(lblEngineName, gbc_lblEngineName);

		engineNameField = new JTextField();
		engineNameField.setEnabled(false);
		GridBagConstraints gbc_engineNameField = new GridBagConstraints();
		gbc_engineNameField.insets = new Insets(0, 0, 5, 5);
		gbc_engineNameField.fill = GridBagConstraints.HORIZONTAL;
		gbc_engineNameField.gridx = 1;
		gbc_engineNameField.gridy = 0;
		enginePanel.add(engineNameField, gbc_engineNameField);
		engineNameField.setColumns(10);

		maxSpeedLabel = new JLabel("Max Speed:");
		GridBagConstraints gbc_maxSpeedLabel = new GridBagConstraints();
		gbc_maxSpeedLabel.anchor = GridBagConstraints.EAST;
		gbc_maxSpeedLabel.insets = new Insets(0, 0, 5, 5);
		gbc_maxSpeedLabel.gridx = 0;
		gbc_maxSpeedLabel.gridy = 1;
		enginePanel.add(maxSpeedLabel, gbc_maxSpeedLabel);

		maxSpeedSpinner = new JSpinner();
		maxSpeedSpinner.setEnabled(false);
		GridBagConstraints gbc_maxSpeedSpinner = new GridBagConstraints();
		gbc_maxSpeedSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxSpeedSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_maxSpeedSpinner.gridx = 1;
		gbc_maxSpeedSpinner.gridy = 1;
		enginePanel.add(maxSpeedSpinner, gbc_maxSpeedSpinner);
		maxSpeedSpinner.setModel(new SpinnerNumberModel(0, 0, 10000000, 1));

		engineSpritePanel = new JPanel();
		engineSpritePanel.setBorder(new TitledBorder(null, "",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_engineSpritePanel = new GridBagConstraints();
		gbc_engineSpritePanel.fill = GridBagConstraints.BOTH;
		gbc_engineSpritePanel.gridheight = 4;
		gbc_engineSpritePanel.gridwidth = 2;
		gbc_engineSpritePanel.insets = new Insets(0, 0, 5, 0);
		gbc_engineSpritePanel.gridx = 2;
		gbc_engineSpritePanel.gridy = 0;
		enginePanel.add(engineSpritePanel, gbc_engineSpritePanel);
		GridBagLayout gbl_engineSpritePanel = new GridBagLayout();
		gbl_engineSpritePanel.columnWidths = new int[] { 0, 0, 0 };
		gbl_engineSpritePanel.rowHeights = new int[] { 0, 0 };
		gbl_engineSpritePanel.columnWeights = new double[] { 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_engineSpritePanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		engineSpritePanel.setLayout(gbl_engineSpritePanel);

		engineSpriteLabel = new JLabel("");
		GridBagConstraints gbc_engineSpriteLabel = new GridBagConstraints();
		gbc_engineSpriteLabel.insets = new Insets(0, 0, 0, 5);
		gbc_engineSpriteLabel.gridwidth = 2;
		gbc_engineSpriteLabel.gridx = 0;
		gbc_engineSpriteLabel.gridy = 0;
		engineSpritePanel.add(engineSpriteLabel, gbc_engineSpriteLabel);

		accelerationRateLabel = new JLabel("Acceleration:");
		GridBagConstraints gbc_accelerationRateLabel = new GridBagConstraints();
		gbc_accelerationRateLabel.anchor = GridBagConstraints.EAST;
		gbc_accelerationRateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_accelerationRateLabel.gridx = 0;
		gbc_accelerationRateLabel.gridy = 2;
		enginePanel.add(accelerationRateLabel, gbc_accelerationRateLabel);

		accelerationRateSpinner = new JSpinner();
		accelerationRateSpinner.setModel(new SpinnerNumberModel(0, 0, 1000000,
				1));
		accelerationRateSpinner.setEnabled(false);
		GridBagConstraints gbc_accelerationRateSpinner = new GridBagConstraints();
		gbc_accelerationRateSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_accelerationRateSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_accelerationRateSpinner.gridx = 1;
		gbc_accelerationRateSpinner.gridy = 2;
		enginePanel.add(accelerationRateSpinner, gbc_accelerationRateSpinner);
		accelerationRateSpinner.setToolTipText("");

		maxBoostLabel = new JLabel("Max Boost:");
		GridBagConstraints gbc_maxBoostLabel = new GridBagConstraints();
		gbc_maxBoostLabel.anchor = GridBagConstraints.NORTHEAST;
		gbc_maxBoostLabel.insets = new Insets(0, 0, 5, 5);
		gbc_maxBoostLabel.gridx = 0;
		gbc_maxBoostLabel.gridy = 3;
		enginePanel.add(maxBoostLabel, gbc_maxBoostLabel);

		maxBoostSpinner = new JSpinner();
		maxBoostSpinner.setModel(new SpinnerNumberModel(new Float(0),
				new Float(0), new Float(1000000), new Float(1)));
		maxBoostSpinner.setEnabled(false);
		GridBagConstraints gbc_maxBoostSpinner = new GridBagConstraints();
		gbc_maxBoostSpinner.anchor = GridBagConstraints.NORTH;
		gbc_maxBoostSpinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_maxBoostSpinner.insets = new Insets(0, 0, 5, 5);
		gbc_maxBoostSpinner.gridx = 1;
		gbc_maxBoostSpinner.gridy = 3;
		enginePanel.add(maxBoostSpinner, gbc_maxBoostSpinner);

		chooseEngineSprite = new JButton("Choose Engine Sprite");
		chooseEngineSprite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chooser.setFileFilter(spriteSheetFilter);
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int returnVal = chooser.showOpenDialog(MainWindow.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: "
							+ chooser.getSelectedFile().getName());
					engineSpriteName = chooser.getSelectedFile().getName();
					((TitledBorder) (engineSpritePanel.getBorder()))
							.setTitle("Engine Sprite: " + engineSpriteName);
					try {
						BufferedImage buffer = ImageIO.read(chooser
								.getSelectedFile());
						engineSpriteLabel.setIcon(new ImageIcon(buffer));
						checkWeaponInput();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				checkEngineInput();
			}
		});
		chooseEngineSprite.setEnabled(false);
		GridBagConstraints gbc_chooseEngineSprite = new GridBagConstraints();
		gbc_chooseEngineSprite.anchor = GridBagConstraints.NORTHWEST;
		gbc_chooseEngineSprite.gridwidth = 2;
		gbc_chooseEngineSprite.gridx = 2;
		gbc_chooseEngineSprite.gridy = 4;
		enginePanel.add(chooseEngineSprite, gbc_chooseEngineSprite);

		newEquipmentButton = new JButton("New Shield");
		GridBagConstraints gbc_newEquipmentButton = new GridBagConstraints();
		gbc_newEquipmentButton.insets = new Insets(0, 0, 0, 5);
		gbc_newEquipmentButton.gridx = 2;
		gbc_newEquipmentButton.gridy = 3;
		equipmentPanel.add(newEquipmentButton, gbc_newEquipmentButton);

		addEquipmentButton = new JButton("Add Shield");
		addEquipmentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnShield.isSelected()) {
					addShield();
				} else {
					addEngine();
				}
			}
		});
		GridBagConstraints gbc_addEquipmentButton = new GridBagConstraints();
		gbc_addEquipmentButton.anchor = GridBagConstraints.NORTHEAST;
		gbc_addEquipmentButton.gridx = 3;
		gbc_addEquipmentButton.gridy = 3;
		equipmentPanel.add(addEquipmentButton, gbc_addEquipmentButton);
		loadFiles();

		updateWeaponsTable();
		updateEquipmentTable();
	}

	protected void checkEngineInput() {
		if (engineNameField.getText().trim().length() == 0) {
			addEquipmentButton.setEnabled(false);
			return;
		}

		if (engineSpriteName == null) {
			addEquipmentButton.setEnabled(false);
			return;
		}
		addEquipmentButton.setEnabled(true);

	}

	protected void addEngine() {
		EngineData editedEngine = new EngineData();
		if (engineID != null) {
			editedEngine = engines.getEngine(engineID);
		}
		editedEngine.maxSpeed = (Integer) maxSpeedSpinner.getValue();
		editedEngine.acceleration = (Integer) accelerationRateSpinner
				.getValue();
		editedEngine.maxBoost = (Float) maxBoostSpinner.getValue();
		editedEngine.engineName = engineNameField.getText();
		editedEngine.engineSpriteName = (engineSpriteName.split("[_]"))[0];
		if (engineID == null) {
			engineID = "E";

			engineID += editedEngine.engineName.charAt(0);
			engineID += editedEngine.engineName.charAt(editedEngine.engineName
					.length() / 2);
			engineID += editedEngine.engineName.charAt(editedEngine.engineName
					.length() - 1);
			Random rand = new Random();
			engineID += "" + rand.nextInt(editedEngine.maxSpeed);
			editedEngine.engineID = engineID;
		}
		if (!engines.contains(engineID)) {
			engines.add(editedEngine);
			updateEquipmentTable();
		}
		engineID = null;

	}

	protected void addShield() {
		ShieldData editedShield = new ShieldData();
		if (shieldID != null) {
			editedShield = shields.getShield(shieldID);
		}
		editedShield.maxShield = (Integer) maxShieldsSpinner.getValue();
		editedShield.rechargeDelay = (Float) chargeDelaySpinner.getValue();
		editedShield.rechargeRate = (Float) chargeRateSpinner.getValue();
		editedShield.shieldName = shieldNameField.getText();
		if (shieldID == null) {
			shieldID = "S";

			shieldID += editedShield.shieldName.charAt(0);
			shieldID += editedShield.shieldName.charAt(editedShield.shieldName
					.length() / 2);
			shieldID += editedShield.shieldName.charAt(editedShield.shieldName
					.length() - 1);
			Random rand = new Random();
			shieldID += "" + rand.nextInt(editedShield.maxShield);
			editedShield.shieldID = shieldID;
		}
		if (!shields.contains(shieldID)) {
			shields.add(editedShield);
			updateEquipmentTable();
		}
		shieldID = null;
	}

	private void updateEquipmentTable() {

		Collection<ShieldData> shieldArray = null;
		int numberOfItems = 0;
		if (!shields.isEmpty()) {
			shieldArray = shields.getShields();
			numberOfItems += shieldArray.size();
		}
		Collection<EngineData> engineArray = null;

		if (!engines.isEmpty()) {
			engineArray = engines.getEngines();
			numberOfItems += engineArray.size();
		}

		DefaultTableModel dm = (DefaultTableModel) equipmentTable.getModel();
		dm.getDataVector().removeAllElements();
		int x = 0;
		if (numberOfItems > 0)
			dm.setRowCount(numberOfItems);

		if (shieldArray != null) {

			equipmentTable.clearSelection();
			for (ShieldData tempShield : shieldArray) {
				equipmentTable.getModel().setValueAt(tempShield.shieldID, x, 0);
				equipmentTable.getModel().setValueAt("Shield", x, 1);
				equipmentTable.getModel().setValueAt(tempShield.shieldName, x,
						2);
				equipmentTable.getModel()
						.setValueAt(tempShield.maxShield, x, 3);
				equipmentTable.getModel().setValueAt(tempShield.rechargeRate,
						x, 4);
				equipmentTable.getModel().setValueAt(tempShield.rechargeDelay,
						x, 5);
				x++;
			}

		}

		if (engineArray != null) {

			equipmentTable.clearSelection();
			for (EngineData tempEngine : engineArray) {
				equipmentTable.getModel().setValueAt(tempEngine.engineID, x, 0);
				equipmentTable.getModel().setValueAt("Engine", x, 1);
				equipmentTable.getModel().setValueAt(tempEngine.engineName, x,
						2);
				equipmentTable.getModel().setValueAt(tempEngine.maxSpeed, x, 3);
				equipmentTable.getModel().setValueAt(tempEngine.acceleration,
						x, 4);
				equipmentTable.getModel().setValueAt(tempEngine.maxBoost, x, 5);
				x++;
			}

		}

		equipmentTable.repaint();

	}

	private void loadFiles() {
		if ((new File("Weapon.xml")).exists()) {
			WeaponData[] weaponInput;
			weaponInput = (WeaponData[]) Easy.load("Weapon.xml");

			for (int x = 0; x < weaponInput.length; x++)
				weapons.add(weaponInput[x]);
		}
		if ((new File("Engine.xml")).exists()) {
			EngineData[] engineInput = (EngineData[]) Easy.load("Engine.xml");

			for (int x = 0; x < engineInput.length; x++)
				engines.add(engineInput[x]);
		}
		if ((new File("Shield.xml")).exists()) {
			ShieldData[] shieldInput = (ShieldData[]) Easy.load("Shield.xml");

			for (int x = 0; x < shieldInput.length; x++)
				shields.add(shieldInput[x]);
		}

	}

	protected void saveFiles() {
		// TODO Auto-generated method stub
		if (!weapons.isEmpty()) {
			Collection<WeaponData> weaponArray = weapons.getWeapons();
			WeaponData[] weaponOutput = new WeaponData[weaponArray.size()];
			ArrayList<WeaponData> orderedList = new ArrayList<WeaponData>();

			int x = 0;
			for (WeaponData tempWeapon : weaponArray) {
				if (orderedList.isEmpty())
					orderedList.add(tempWeapon);
				else {
					int gunLevel = (int) (Integer
							.parseInt(tempWeapon.weaponName.split(" ")[tempWeapon.weaponName
									.split(" ").length - 1]));
					int tempGoal = orderedList.size();
					for (int y = 0; y < orderedList.size(); y++) {
						if (gunLevel < Integer
								.parseInt(orderedList.get(y).weaponName
										.split(" ")[orderedList.get(y).weaponName.split(
										" ").length - 1])) {
							tempGoal = y;
							break;
						}
					}
					if (tempGoal==orderedList.size())
						orderedList.add(tempWeapon);
					else
						orderedList.add(tempGoal, tempWeapon);
				}
			}
			for (WeaponData tempWeapon:orderedList){
				weaponOutput[x++]=tempWeapon;
			}
			Easy.save(weaponOutput, "Weapon.xml");
		}
		if (!engines.isEmpty()) {
			Collection<EngineData> engineArray = engines.getEngines();
			EngineData[] engineOutput = new EngineData[engineArray.size()];
			ArrayList<EngineData> orderedList = new ArrayList<EngineData>();

			int x = 0;
			for (EngineData tempEngine : engineArray) {
				if (orderedList.isEmpty())
					orderedList.add(tempEngine);
				else {
					int gunLevel = (int) (Integer
							.parseInt(tempEngine.engineName.split(" ")[tempEngine.engineName
									.split(" ").length - 1]));
					int tempGoal = orderedList.size();
					for (int y = 0; y < orderedList.size(); y++) {
						if (gunLevel < Integer
								.parseInt(orderedList.get(y).engineName
										.split(" ")[orderedList.get(y).engineName.split(
										" ").length - 1])) {
							tempGoal = y;
							break;
						}
					}
					if (tempGoal==orderedList.size())
						orderedList.add(tempEngine);
					else
						orderedList.add(tempGoal, tempEngine);
				}
			}
			for (EngineData tempEngine:orderedList){
				engineOutput[x++]=tempEngine;
			}
			Easy.save(engineOutput, "Engine.xml");
		}
		if (!shields.isEmpty()) {
			Collection<ShieldData> shieldArray = shields.getShields();
			ShieldData[] shieldOutput = new ShieldData[shieldArray.size()];
			ArrayList<ShieldData> orderedList = new ArrayList<ShieldData>();

			int x = 0;
			for (ShieldData tempShield : shieldArray) {
				if (orderedList.isEmpty())
					orderedList.add(tempShield);
				else {
					int gunLevel = (int) (Integer
							.parseInt(tempShield.shieldName.split(" ")[tempShield.shieldName
									.split(" ").length - 1]));
					int tempGoal = orderedList.size();
					for (int y = 0; y < orderedList.size(); y++) {
						if (gunLevel < Integer
								.parseInt(orderedList.get(y).shieldName
										.split(" ")[orderedList.get(y).shieldName.split(
										" ").length - 1])) {
							tempGoal = y;
							break;
						}
					}
					if (tempGoal==orderedList.size())
						orderedList.add(tempShield);
					else
						orderedList.add(tempGoal, tempShield);
				}
			}
			for (ShieldData tempShield:orderedList){
				shieldOutput[x++]=tempShield;
			}
			Easy.save(shieldOutput, "Shield.xml");
		}
	}

	protected void addWeapon() {
		WeaponData editedWeapon = new WeaponData();
		if (weaponID != null) {
			editedWeapon = weapons.getWeapon(weaponID);
		}
		editedWeapon.accuracy = (Integer) accuracySpinner.getValue();
		WeaponType weaponType;
		if (rdbtnEnemy.isSelected())
			weaponType = WeaponType.Enemy;
		else
			weaponType = WeaponType.Player;
		editedWeapon.weaponType = weaponType;
		if (weaponType == WeaponType.Enemy)
			editedWeapon.gunSpriteFilename = "N/A";
		else
			editedWeapon.gunSpriteFilename = (weaponSpriteName.split("[.]"))[0];

		editedWeapon.shotGravityMultiplier = (Float) gravityScalerSpinner
				.getValue();
		editedWeapon.shotMass = (Integer) massSpinner.getValue();
		editedWeapon.shotSpriteFilename = (fireSpriteName.split("[.]"))[0];
		editedWeapon.shotVelocity = (Integer) fireVelocitySpinner.getValue();
		editedWeapon.weaponFireRate = (Float) fireRateSpinner.getValue();
		editedWeapon.weaponName = weaponNameField.getText();
		editedWeapon.weaponPower = (Integer) weaponPowerSpinner.getValue();
		editedWeapon.weaponRecoil = (Integer) weaponRecoilSpinner.getValue();
		editedWeapon.weaponSoundEffectFileName = (weaponSoundEffectName
				.split("[.]"))[0];
		editedWeapon.shotHealth = (Integer) shotHealthSpinner.getValue();
		if (weaponID == null) {
			weaponID = "W";
			if (weaponType == WeaponType.Enemy)
				weaponID += "E";
			else
				weaponID += "P";
			weaponID += editedWeapon.weaponName.charAt(0);
			weaponID += editedWeapon.weaponName.charAt(editedWeapon.weaponName
					.length() / 2);
			weaponID += editedWeapon.weaponName.charAt(editedWeapon.weaponName
					.length() - 1) + editedWeapon.weaponPower;
			editedWeapon.weaponID = weaponID;
		}
		if (!weapons.contains(weaponID)) {
			weapons.add(editedWeapon);
			resetWeapon();
			updateWeaponsTable();
		}

	}

	private void updateWeaponsTable() {
		Collection<WeaponData> weaponArray = weapons.getWeapons();
		DefaultTableModel dm = (DefaultTableModel) tableWeapon.getModel();
		dm.getDataVector().removeAllElements();
		if (weaponArray != null) {

			tableWeapon.clearSelection();
			int x = 0;
			dm.setRowCount(weaponArray.size());
			for (WeaponData tempWeapon : weaponArray) {
				tableWeapon.getModel().setValueAt(tempWeapon.weaponID, x, 0);
				tableWeapon.getModel().setValueAt(tempWeapon.weaponName, x, 1);
				tableWeapon.getModel().setValueAt(tempWeapon.weaponPower, x, 2);
				tableWeapon.getModel().setValueAt(tempWeapon.weaponFireRate, x,
						3);
				tableWeapon.getModel().setValueAt(tempWeapon.gunSpriteFilename,
						x, 4);
				tableWeapon.getModel().setValueAt(
						tempWeapon.shotSpriteFilename, x, 5);
				tableWeapon.getModel().setValueAt(
						tempWeapon.weaponSoundEffectFileName, x, 6);
				x++;
			}

		}
		tableWeapon.repaint();

	}

	private void resetWeapon() {
		enemyShipSpriteSheet.setEnabled(true);
		weaponNameField.setEnabled(true);
		weaponPowerSpinner.setEnabled(true);
		fireRateSpinner.setEnabled(true);
		fireVelocitySpinner.setEnabled(true);

		gunSpriteLabel.setEnabled(true);
		fireSpriteLabel.setEnabled(true);

		gunSpriteLabel.setIcon(null);
		fireSpriteLabel.setIcon(null);

		weaponRecoilSpinner.setEnabled(true);
		massSpinner.setEnabled(true);
		gravityScalerSpinner.setEnabled(true);
		accuracySpinner.setEnabled(true);
		weaponSpriteName = null;
		fireSpriteName = null;
		fireSpriteButton.setEnabled(true);
		gunSpritebutton.setEnabled(true);

		((TitledBorder) (shotSpritePanel.getBorder())).setTitle("Shot Sprite");
		((TitledBorder) (gunSpritePanel.getBorder())).setTitle("Weapon Sprite");
		((TitledBorder) (soundPanel.getBorder())).setTitle("Sound Effect");
		rdbtnEnemy.setEnabled(true);
		playerRadioButton.setEnabled(true);
		playerRadioButton.setSelected(true);
		shotHealthSpinner.setEnabled(true);

		if (weaponSoundEffect != null)
			weaponSoundEffect.close();
		weaponSoundEffect = null;
		btnChooseGunSound.setEnabled(true);
		btnPlay.setEnabled(false);
		btnStop.setEnabled(false);

		weaponNameField.setText("");
		weaponID = null;
		checkWeaponInput();

	}

	protected void checkWeaponInput() {
		if (weaponNameField.getText().trim().length() == 0) {
			addWeaponbutton.setEnabled(false);
			return;
		}
		if (weaponSoundEffectName == null) {
			addWeaponbutton.setEnabled(false);
			return;
		}

		if (weaponSpriteName == null && !rdbtnEnemy.isSelected()) {
			addWeaponbutton.setEnabled(false);
			return;
		}
		if (fireSpriteName == null) {
			addWeaponbutton.setEnabled(false);
			return;
		}
		addWeaponbutton.setEnabled(true);
	}
}
