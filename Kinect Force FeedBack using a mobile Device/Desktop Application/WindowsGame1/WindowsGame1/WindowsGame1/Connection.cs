﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.IO;
using System.Collections.Concurrent;


namespace WindowsGame1
{
    abstract class Connection
    {
        protected NetworkStream mobileStream;
        protected StreamReader inputMobileStream;
        protected StreamWriter outputMobileStream;
        private Thread sendThread;

        public abstract void Acceptor();

        public abstract void Disconnector();
        public abstract bool isDisconnected();
        public BlockingCollection<String> communicationChannel;
        public Connection()
        {
            communicationChannel = new BlockingCollection<string>();
            this.sendThread = null;
        }

        internal void connect()
        {
            inputMobileStream = new StreamReader(mobileStream);
            outputMobileStream = new StreamWriter(mobileStream);
            Thread.Sleep(1000);
            Console.WriteLine("Sending connection Message to mobile Device");
            outputMobileStream.WriteLine("<Connect>:</Connect>");
            outputMobileStream.Flush();
            String confirmMessage = inputMobileStream.ReadLine();
            Console.WriteLine("Recieved message from mobile: {0}", confirmMessage);
        }

        internal void sendMessage()
        {
            String message;
            while (outputMobileStream != null && communicationChannel.TryTake(out message, 1000))
            {
                Console.WriteLine("Sending {0} to mobile", message);
                try
                {
                    outputMobileStream.WriteLine(message);
                    outputMobileStream.Flush();
                }
                catch
                {
                    Console.WriteLine("Output stream has been closed");
                }
            }

        }

        public bool Communicator(string message)
        {
            if (!isDisconnected())
            {
                communicationChannel.Add(message);
                if (sendThread == null || !sendThread.IsAlive)
                {
                    this.sendThread = new Thread(new ThreadStart(sendMessage));
                    this.sendThread.Start();
                }
                return true;
            }
            outputMobileStream.Close();
            outputMobileStream = null;
            inputMobileStream.Close();
            inputMobileStream = null;
            communicationChannel = new BlockingCollection<string>();
            return false;
        }
    }

    class WiFiConnection : Connection
    {
        private MobileHaptic mobileHaptic;
        private int Port;
        private TcpClient mobileConnection;
        private TcpListener listener;

        private bool running;
        private long ping;


        public WiFiConnection(MobileHaptic mobileHaptic, int Port)
        {

            this.mobileHaptic = mobileHaptic;
            this.Port = Port;
            this.running = true;
        }

        public override bool isDisconnected()
        {
            return !this.mobileConnection.Connected;
        }

        public override void Acceptor()
        {
            listener = new TcpListener(IPAddress.Any, Port);

            listener.Start();
            while (running)
            {
                try
                {
                    mobileConnection = listener.AcceptTcpClient();
                }
                catch
                {
                    Console.WriteLine("Listener being shut down by program");
                }
                if (mobileConnection != null)
                {
                    Ping clientPing = new Ping();
                    long averageTime = 0;
                    for (int x = 0; x < 4; x++)
                    {
                        PingReply result = clientPing.Send(((IPEndPoint)mobileConnection.Client.RemoteEndPoint).Address);
                        Console.WriteLine("Ping of Address: {0}, of {1}", result.Address.ToString(), result.RoundtripTime);
                        averageTime += result.RoundtripTime;
                    }
                    averageTime = averageTime / 4;
                    Console.WriteLine("Average RTT is: {0}", averageTime);
                    ping = averageTime / 2;
                    Console.WriteLine("Time to mobile Device {0}", ping);

                    mobileStream = mobileConnection.GetStream();
                    mobileHaptic.wifiConnection();
                    running = false;
                }

            }
            listener.Stop();

        }


        public override void Disconnector()
        {
            running = false;
            if (listener != null)
                listener.Stop();
            if (mobileConnection != null)
                mobileConnection.Close();
        }


    }

    class BlueToothConnection : Connection
    {
        public override void Acceptor()
        {
        }
        public override void Disconnector()
        {
        }

        public override bool isDisconnected()
        {
            return false;
        }
    }
}
