﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
//namespace of the kinect example
using Microsoft.Samples.Kinect.XnaBasics;



namespace WindowsGame1
{
    class KinectInformation
    {
        private GraphicsDeviceManager graphics;
        private KinectChooser kinectChooser;
        private DepthStreamRenderer depthStream;
        private Game game;
        private int handX;
        private int handY;

        public KinectInformation(GraphicsDeviceManager graphics,Game game)
        {
            this.kinectChooser = new KinectChooser(game, ColorImageFormat.RgbResolution640x480Fps30, DepthImageFormat.Resolution640x480Fps30);
            this.graphics = graphics;
            this.game = game;
            game.Services.AddService(typeof(KinectChooser), this.kinectChooser);

            this.depthStream = new DepthStreamRenderer(game);
            this.depthStream.Size = new Vector2((int)(graphics.PreferredBackBufferWidth * 0.15), (int)(graphics.PreferredBackBufferHeight * 0.15));
            this.depthStream.Position = new Vector2((int)(graphics.PreferredBackBufferWidth * 0.825), (int)(graphics.PreferredBackBufferHeight * 0.825));
            this.depthStream.DrawOrder = 1;

            game.Components.Add(kinectChooser);
            handX = -1;
            handY = -1;
        }

       

        public void Initialize()
        {
            game.Components.Add(depthStream);
        }

        public void setPosition(float handX, float handY)
        {
            this.handX = (int)handX;
            this.handY = (int)handY;
        }

        public int getHandY()
        {
            return depthStream.getHandY();
        }

        public int getHandX()
        {
            return depthStream.getHandX();
        }
    }
}
