﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections;



namespace WindowsGame1
{
    class Control
    {
        private Model model;
        private ArrayList targetList;
        private MobileHaptic mobileHaptic;
        private KinectInformation kinectInformation;

        public Control(Model model, GraphicsDeviceManager graphics,Game game)
        {
            this.model = model;
            this.targetList=new ArrayList();
            this.mobileHaptic = new MobileHaptic();
            this.kinectInformation = new KinectInformation(graphics,game);

        }

        

        internal void update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            // Allows the game to exit

            model.setExit(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed
                || Keyboard.GetState().IsKeyDown(Keys.Escape));
            
            bool selected = false;
            int handX = kinectInformation.getHandX();
            int handY = kinectInformation.getHandY();
            if (handX != -1 && handY != -1)
            {
                Mouse.SetPosition(handX, handY);
            }
            Point mouseLocation = new Point(Mouse.GetState().X, Mouse.GetState().Y);
            foreach (Rectangle target in targetList)
            {
                if (target.Contains(mouseLocation))
                {
                    selected = true;
                    model.setStatus(target, true);
                }
                else
                {
                    model.setStatus(target, false);
                }
            }
            if (selected){
                mobileHaptic.Vibrate();
                 GamePad.SetVibration(PlayerIndex.One, 0.5f, 1.0f);
            }else{
                mobileHaptic.StopVibrate();
                GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
            }



        }

        internal void addTarget(Rectangle target)
        {
            targetList.Add(target);
        }
    

        internal void Initialize()
        {
            mobileHaptic.Start();
            kinectInformation.Initialize();
        }

        internal void UnloadContent()
        {
            mobileHaptic.exit();
        }
    }
}
