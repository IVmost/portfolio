﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading;

namespace WindowsGame1
{
    class MobileHaptic
    {
        private Thread wifiThread;
        private Thread blueToothThread;

        private WiFiConnection wiFiConnection;
        private BlueToothConnection blueToothAcceptor;

        private Connection mobileConnection;

        private int vibrateMessageCounter;
        private int stopMessageCounter;
        

        public MobileHaptic()
        {
            
            this.vibrateMessageCounter = 0;
            this.stopMessageCounter = 0;
           
        }
        public void Start()
        {
            this.mobileConnection = null;
            this.wiFiConnection = new WiFiConnection(this, 10080);
            this.wifiThread = new Thread(new ThreadStart(wiFiConnection.Acceptor));
            this.wifiThread.Start();
        }


        internal void wifiConnection()
        {
            this.mobileConnection = this.wiFiConnection;
            //would stop listening on the bluetooth listener to end the thread;
            mobileConnection.connect();
        }
        /**
         *Method for just doing a flat trigger of mobile 
         */
        public void Vibrate()
        {
            //only sends a message to the phone once every 0.5 seconds to cut down on transmisson time
            if (mobileConnection != null)
            {

                stopMessageCounter = 0;
                if (vibrateMessageCounter == 0)
                {
                    string Message = "<Vibrate>:Vibrate:</Vibrate>";
                    if (!mobileConnection.Communicator(Message))
                    {
                        restartConnector();
                    }
                }
                vibrateMessageCounter = (vibrateMessageCounter + 1) % 10;
            }
        }
        public void StopVibrate()
        {
            if (mobileConnection != null)
            {
                vibrateMessageCounter = 0;
                if (stopMessageCounter == 0)
                {

                    string Message = "<Stop>:Stop:</Stop>";
                    if (!mobileConnection.Communicator(Message))
                    {
                        restartConnector();
                    }
                }
                stopMessageCounter = (stopMessageCounter + 1) % 10;
            }
        }

        private void restartConnector()
        {
            Start();
        }

        public void Pattern(string pattern)
        {
            if (mobileConnection != null)
            {

            }
        }

        internal void exit()
        {
            if (wiFiConnection != null)
                wiFiConnection.Disconnector();

            if (blueToothAcceptor != null)
                blueToothAcceptor.Disconnector();
        }
    }

    
}
