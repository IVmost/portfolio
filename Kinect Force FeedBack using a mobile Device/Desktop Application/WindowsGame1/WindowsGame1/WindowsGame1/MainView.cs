using System;
using System.Collections;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    /// 


    public class MainView : Microsoft.Xna.Framework.Game
    { 
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Model model;
        Control control;
        ArrayList targetsList;
        int width;
        int height;

        public MainView()
        {
            graphics = new GraphicsDeviceManager(this);

            //object used to get the native resolution of a monitor
            Resolution resolution= new Resolution();
            width = resolution.getResolutionWidth();
            height = resolution.getResolutionHeight();
            this.graphics.PreferredBackBufferWidth = width;
            this.graphics.PreferredBackBufferHeight = height;
            //adds a event listener to prevent collisions on the back buffer which would result in data loss
            this.graphics.PreparingDeviceSettings += this.GraphicsDevicePreparingDeviceSettings;
            
            graphics.ToggleFullScreen();
            Content.RootDirectory = "Content";
            this.model = new Model();
            this.control = new Control(model,graphics,this);
            this.IsMouseVisible=true;
            targetsList = new ArrayList();
           
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            
            control.Initialize();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.Services.AddService(typeof(SpriteBatch), this.spriteBatch);
            targetsList.Add(new Rectangle((int)(0.025*width), (int)(0.025*height),(int) (0.15*width), (int)(0.15*height)));
            targetsList.Add(new Rectangle((int)(0.225 * width), (int)(0.025 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.425 * width), (int)(0.025 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.625 * width), (int)(0.025 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.825 * width), (int)(0.025 * height), (int)(0.15 * width), (int)(0.15 * height)));

            targetsList.Add(new Rectangle((int)(0.825 * width), (int)(0.225 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.825 * width), (int)(0.425 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.825 * width), (int)(0.625 * height), (int)(0.15 * width), (int)(0.15 * height)));

            targetsList.Add(new Rectangle((int)(0.025 * width), (int)(0.225 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.025 * width), (int)(0.425 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.025 * width), (int)(0.625 * height), (int)(0.15 * width), (int)(0.15 * height)));
            targetsList.Add(new Rectangle((int)(0.025 * width), (int)(0.825 * height), (int)(0.15 * width), (int)(0.15 * height)));
            foreach (Rectangle target in targetsList)
            {
                control.addTarget(target);
                model.addTarget(target);
            }
            Texture2D tempTexture = new Texture2D(GraphicsDevice, 1, 1);
            tempTexture.SetData(new Color[] { Color.White });
            model.setSelectedTexture(tempTexture);
            
            tempTexture = new Texture2D(GraphicsDevice, 1, 1);
            tempTexture.SetData(new Color[] { Color.Red });
            model.setUnselectedTexture(tempTexture);


            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            control.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
          
            // TODO: Add your update logic here
            control.update(gameTime);

            if (model.exit())
                this.Exit();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            foreach (Rectangle target in targetsList)
            {
                spriteBatch.Draw(model.getTexture(target), target, Color.White);
            }
            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        /// <summary>
        /// Made by Microsoft Kinect XNA Example
        /// This method ensures that we can render to the back buffer without
        /// losing the data we already had in our previous back buffer.  This
        /// is necessary for the SkeletonStreamRenderer.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">The event args.</param>
        private void GraphicsDevicePreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            // This is necessary because we are rendering to back buffer/render targets and we need to preserve the data
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }

    }
}
