﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;



namespace WindowsGame1
{
    class Model
    {
        Boolean endGame;
        Texture2D untargedColor;
        Texture2D targetedColor;
        Hashtable targetStatus;

        public Model()
        {
            endGame=false;
            targetStatus = new Hashtable();
        }

        internal void setExit(bool p)
        {
            endGame = p;
        }

        internal bool exit()
        {
            return endGame;
        }

        internal void addTarget(Rectangle target)
        {
            if (!targetStatus.ContainsKey(target))
                targetStatus.Add(target, false);
        }

        
        internal void setSelectedTexture(Texture2D tempTexture)
        {
            targetedColor = tempTexture;
        }

        internal void setUnselectedTexture(Texture2D tempTexture)
        {
            untargedColor = tempTexture;
        }

        internal Texture2D getTexture(Rectangle target)
        {
            Boolean status=(Boolean)targetStatus[target];
            if (status)
                return targetedColor;
            else
                return untargedColor;
            
            
        }

        internal void setStatus(Rectangle target, bool status)
        {
            targetStatus[target] = status;
        }
    }
}
