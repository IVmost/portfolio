﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WindowsGame1
{
     
    class Resolution
    {
        Rectangle resolution;
        public Resolution()
        {
            resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;

        }
        public int getResolutionWidth(){
            return resolution.Width;
            
        }
        public int getResolutionHeight()
        {
            return resolution.Height;
        }
    }
}
