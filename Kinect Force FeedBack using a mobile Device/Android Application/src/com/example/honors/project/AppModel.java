package com.example.honors.project;

import java.util.Hashtable;

import android.net.MailTo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.app.Activity;
import android.content.res.Resources;

/**
 * Class which stores most of the models
 * 
 * @author Andrew Bown
 * 
 */
public class AppModel {

	private String ipAddress = "192.168.0.15";
	private String currentPort = "10080";

	// tables for recalling data
	private Hashtable<String, Button> buttonTable;
	private Hashtable<String, TextView> displayTable;
	private Hashtable<String, RadioButton> radioButtonTable;
	private Hashtable<String, EditText> fieldTable;
	private Hashtable<String, Boolean> deactivationTable;
	private RadioGroup radioButtons;

	private ConnectionView mainView;

	public AppModel(ConnectionView mainView) {
		new AppControl(mainView, this);
		this.mainView = mainView;
		Resources resources = mainView.getResources();

		// table creation
		buttonTable = new Hashtable<String, Button>();
		displayTable = new Hashtable<String, TextView>();
		fieldTable = new Hashtable<String, EditText>();
		deactivationTable = new Hashtable<String, Boolean>();

		// load all the ui elements
		TextView ipAddressDisplay = (TextView) mainView
				.findViewById(R.id.ipAddressDisplay);
		String ipAddressID = resources.getString(R.string.ipAddressID);

		TextView portDisplay = (TextView) mainView
				.findViewById(R.id.portDisplay);
		String portAddressID = resources.getString(R.string.portID);

		ipAddressDisplay.setText(ipAddress);
		portDisplay.setText(currentPort);

		// place all element in their correct tables
		displayTable.put(ipAddressID, ipAddressDisplay);
		buttonTable.put(ipAddressID,
				(Button) mainView.findViewById(R.id.saveIpButton));
		fieldTable.put(ipAddressID,
				(EditText) mainView.findViewById(R.id.ipAddressEntry));

		displayTable.put(portAddressID, portDisplay);
		buttonTable.put(portAddressID,
				(Button) mainView.findViewById(R.id.portSaveButton));
		fieldTable.put(portAddressID,
				(EditText) mainView.findViewById(R.id.portEntryField));

		String connectID = resources.getString(R.string.ConnectID);

		buttonTable.put(connectID,
				(Button) mainView.findViewById(R.id.ConnectButton));
		radioButtons = (RadioGroup) mainView
				.findViewById(R.id.connectionTypeGroup);
	}

	/**
	 * method for enabling buttons by a string id
	 * 
	 * @param ID
	 *            id of the button
	 */
	public void enableButton(String ID) {
		if (buttonTable.containsKey(ID)) {
			mainView.enableButton(buttonTable.get(ID));
		}

	}

	/**
	 * method for disabling buttons by a string id
	 * 
	 * @param ID
	 *            id of the button
	 */
	public void disableButton(String ID) {
		if (buttonTable.containsKey(ID)) {
			buttonTable.get(ID).setEnabled(false);
		}

	}

	/**
	 * method for setting display data by id
	 * 
	 * @param ID
	 *            id of the element
	 * @param string
	 *            data to be stored
	 */
	public void setDisplayData(String ID, String string) {
		if (displayTable.containsKey(ID)) {
			displayTable.get(ID).setText(string);
		}

	}

	/**
	 * method for disabling all interactive elements for a display
	 * 
	 * @param ID
	 *            id of the elements
	 */
	public void deactivateID(String ID) {

		if (fieldTable.containsKey(ID)) {
			fieldTable.get(ID).setEnabled(false);
		}
		// checks if a save button is enabled so that it can be reenabled later
		if (buttonTable.containsKey(ID) && buttonTable.get(ID).isEnabled()) {
			deactivationTable.put(ID, true);
			buttonTable.get(ID).setEnabled(false);
		}
	}

	/**
	 * method for reactivate all the elements of the id with thier proper table
	 * 
	 * @param ID
	 *            id of the elments
	 */
	public void reactivateID(String ID) {
		if (fieldTable.containsKey(ID)) {
			fieldTable.get(ID).setEnabled(true);
		}
		// check to see if a button needs to be
		if (buttonTable.containsKey(ID) && deactivationTable.containsKey(ID)) {
			deactivationTable.remove(ID);
			buttonTable.get(ID).setEnabled(true);
		}
	}

	/**
	 * disables the radio buttons
	 */
	public void disableRadioButtons() {
		setRadioButtonState(false);

	}

	/**
	 * method for disabling or enabling radio button
	 * 
	 * @param state
	 */
	private void setRadioButtonState(boolean state) {
		for (int count = 0; count < radioButtons.getChildCount(); count++) {
			((RadioButton) radioButtons.getChildAt(count)).setEnabled(state);
		}
		radioButtons.setEnabled(state);

	}

	/**
	 * enables the radio button
	 */
	public void enableRadioButtons() {
		setRadioButtonState(true);

	}

	/**
	 * Method for getting data from a display element
	 * 
	 * @param ID
	 *            the id of the element
	 * @return the data from the element
	 */
	public String getData(String ID) {
		if (displayTable.containsKey(ID))
			return displayTable.get(ID).getText().toString();
		return null;
	}

	/**
	 * method for toggling a button
	 * 
	 * @param iD
	 *            id of the button
	 */
	public void toggleButton(String iD) {
		if (buttonTable.containsKey(iD))
			mainView.clickButton(buttonTable.get(iD));

	}

}
