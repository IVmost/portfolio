package com.example.honors.project;

import java.lang.reflect.Method;

import android.os.Bundle;
import android.os.Vibrator;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

/**
 * Main activity mostly generated or calls that need to run on the ui thread
 * 
 * @author Andrew Bown
 * 
 */
public class ConnectionView extends Activity {

	private AppControl controller;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connection);

		AppModel model = new AppModel(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_connection, menu);
		return true;
	}

	/**
	 * Method used to notify the user using a toast
	 * @param string message to be displayed
	 */
	public void toastFromControl(final String string) {
		//A toast must be run on a ui thread
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), string,
						Toast.LENGTH_LONG).show();

			}
		});

	}

	/**
	 * method for invoking a button with it's related behavior
	 * @param button button to be clicked
	 */
	public void clickButton(final Button button) {
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				button.performClick();

			}
		});

	}

	/**
	 * used to enable button from outside of a ui thread
	 * @param button
	 */
	public void enableButton(final Button button) {
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				button.setEnabled(true);

			}
		});

	}

	/**
	 * get the haptic feedback service
	 * @return
	 */
	public Vibrator getVibrator() {

		return (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	}

}
