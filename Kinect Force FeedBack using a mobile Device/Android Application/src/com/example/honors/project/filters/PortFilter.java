package com.example.honors.project.filters;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import com.example.honors.project.AppModel;
import com.example.honors.project.connection.MobileHaptic;

public class PortFilter implements InputFilter{
	private AppModel appModel;
	private String buttonID;
	
	
	public PortFilter(AppModel appModel,String buttonID) {
		this.appModel=appModel;
		this.buttonID=buttonID;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		if (end>start){
			char[] newinput = new char[end - start];
			TextUtils.getChars(source, start, end, newinput, 0);
			String fixed="";
			for (int x = 0; x < newinput.length; x++) {
				if (Character.isDigit(newinput[x]))
					fixed+=newinput[x];
			}
			
			String newString=dest.toString()+fixed;
			if (newString.length()>0)
				appModel.enableButton(buttonID);
			if (Integer.parseInt(newString)>65535)
				fixed="";
			return fixed;
			
		}else{
			if (dest.toString().length()==1)
				appModel.disableButton(buttonID);
		}
		return null;
	}

}
