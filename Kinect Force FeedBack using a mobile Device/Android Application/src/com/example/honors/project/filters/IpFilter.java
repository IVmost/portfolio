package com.example.honors.project.filters;

import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;

import com.example.honors.project.AppModel;

/**
 * Filter to Allow for entry of only a desired Ip Address in the proper format
 * 
 * @author Andrew Bown
 * 
 */
public class IpFilter implements InputFilter {

	private AppModel appModel;
	private String button;

	public IpFilter(AppModel appModel, String button) {
		this.appModel = appModel;
		this.button = button;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {
		if (end > start && !source.toString().matches(".*[^0-9^\\.]+.*")) {

			char[] newinput = new char[end - start];
			TextUtils.getChars(source, start, end, newinput, 0);
			String[] splitIp = dest.toString().split("[\\.]");
			String fixed = "";
			if (splitIp.length <= 4) {
				if (splitIp[splitIp.length - 1].length() == 3
						&& newinput[newinput.length - 1] != '.'
						&& dest.toString().lastIndexOf('.') != dest.toString()
								.length() - 1)
					fixed = ".";
				for (int x = 0; x < newinput.length; x++) {
					if (Character.isDigit(newinput[x])
							|| (newinput[x] == '.' && dest.toString()
									.lastIndexOf('.') != dest.toString()
									.length() - 1) && splitIp.length != 4)
						fixed += newinput[x];
				}
				if (splitIp.length == 4
						&& splitIp[splitIp.length - 1].length() == 3)
					fixed = "";
			}
			if (fixed.indexOf('.') == -1) {
				try {
					if (Integer.parseInt((splitIp[splitIp.length - 1] + fixed)) > 255
							&& dest.toString().lastIndexOf('.') != dest
									.toString().length() - 1) {
						if (splitIp.length == 4)
							fixed = "";
						else
							fixed = "." + fixed;
					}
				} catch (Exception e) {
					return null;
				}
			}

			if (splitIp.length == 4
					&& (splitIp[splitIp.length - 1].length() + fixed.length()) > 0)
				appModel.enableButton(button);
			else {
				String[] tempSplit = (dest.toString() + fixed).split("[\\.]");
				if (tempSplit.length == 4
						&& tempSplit[tempSplit.length - 1].length() > 0)
					appModel.enableButton(button);
				else
					appModel.disableButton(button);
			}
			return fixed;
		} else if (dest.toString().lastIndexOf('.') == dest.toString().length() - 2)
			appModel.disableButton(button);
		return null;
	}

}
