package com.example.honors.project.connection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import android.util.Log;

import com.example.honors.project.AppControl;

public class WifiConnection extends ConnectionBase {

	private Socket game;
	public WifiConnection(Socket game, AppControl appControl) {
		super(appControl);
		this.game=game;
	}

	@Override
	public void disconnect() {
		try {
			game.close();
		} catch (IOException e) {
			Log.d("WiFi Connection", "Could not close Socket");
			
		}

	}

	@Override
	public InputStream getInputStream() {

		try {
			return game.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			super.appControl.toast("Input Stream Error!");
		}
		return null;
	}

	@Override
	public OutputStream getOutputStream() {

		try {
			return game.getOutputStream();
		} catch (IOException e) {
			
			super.appControl.toast("Output Stream Error!");
		}
		return null;
	}

	@Override
	public boolean connected() {
		
		return game.isConnected();
	}

}
