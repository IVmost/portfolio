package com.example.honors.project.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ThreadPoolExecutor;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

import com.example.honors.project.AppControl;


public class Connector {
	public static ConnectionBase connect(String host, int port, AppControl appControl) throws IOException {
		Socket game = new Socket();
		game.connect(new InetSocketAddress(host, port),1000);
		game.setKeepAlive(true);
		ConnectionBase newConnection = null;
		
		newConnection= new WifiConnection(game,appControl);
		return newConnection;
	}
	
	public static ConnectionBase connect(AppControl appControl) throws IOException {
		BluetoothAdapter mobileBluetoothAdapter= BluetoothAdapter.getDefaultAdapter();
		if (mobileBluetoothAdapter==null){
			return null;
		}
		if (!mobileBluetoothAdapter.isEnabled()){
			Intent enableBlueTooth= new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			
		}
			
		return null;
	}
}
