package com.example.honors.project.connection;

import java.io.IOException;

import com.example.honors.project.AppControl;
/**
 * Class for communication with the game
 * @author Andrew Bown
 *
 */
public class MobileHaptic {

	private AppControl appControl;
	private ConnectionBase connection;

	public MobileHaptic(AppControl appControl) {
		this.appControl = appControl;
	}
/**
 * Method for Bluetooth connection
 * not implemented
 */
	
	public void connect() {
		// TODO Auto-generated method stub

	}

	/**
	 * Method for wifi connection
	 * @param ipAddress 
	 * @param portAddress
	 */
	public void connect(String ipAddress, String portAddress) {

		new Thread(new WifiThread(ipAddress, portAddress)).start();

	}

	/**
	 * Thread for wifi connector
	 * @author Andrew Bown
	 *
	 */
	private class WifiThread implements Runnable {
		private String ipAddress;
		private String portAddress;

		WifiThread(String ipAddress, String portAddress) {
			this.ipAddress = ipAddress;
			this.portAddress = portAddress;
		}

		@Override
		public void run() {
			try {
				connection = Connector.connect(ipAddress,
						Integer.parseInt(portAddress), appControl);
				appControl.connected();
				connection.createCommunication();
			} catch (NumberFormatException e) {
				appControl
						.toast("Post Address Number Error for:" + portAddress);
			} catch (IOException e) {

				appControl.toast("Could not connect to Server IO Error");
			}
			if (connection==null)
				appControl.toggelConnect();
		}

	}

	/** 
	 * method for closing connection
	 */
	public void disconnect() {
		if (connection != null){
			connection.endSession();
			connection=null;
		}
		
	}

}
