package com.example.honors.project.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.widget.AnalogClock;

import com.example.honors.project.AppControl;

/**
 * 
 * Abstract class that of connection are based on
 */
public abstract class ConnectionBase {

	protected AppControl appControl;
	private BufferedReader input;
	private PrintWriter output;
	private Boolean running;
	private Thread recievingThread;

	/**
	 * Constructor of abstarct class
	 * 
	 */
	public ConnectionBase(AppControl appControl) {
		this.appControl = appControl;
		this.running = false;
	}

	/**
	 * used for ending a conection
	 */
	public abstract void disconnect();

	/**
	 * Abstract method for getting an inputstream from a concrete connection
	 * 
	 * @return the input stream
	 */
	public abstract InputStream getInputStream();

	/**
	 * Abstract method for getting the outputstream from a concrete connection
	 * @return
	 */
	public abstract OutputStream getOutputStream();

	/**
	 * Abstract method for getting the status of the connection
	 * @return
	 */
	public abstract boolean connected();

	/**
	 * method that creates the object used for communication
	 */
	public void createCommunication() {
		input = new BufferedReader(new InputStreamReader(getInputStream()));
		output = new PrintWriter(getOutputStream(), true);
		running = true;
		recievingThread = new Thread(new inputThread());
		recievingThread.start();
	}

	/**
	 * Thread for receive input from the source
	 * @author Andrew Bown
	 *
	 */
	private class inputThread implements Runnable {

		@Override
		public void run() {
			
			//sets the cpu lock to keep it awake
			PowerManager powerManager = (PowerManager) appControl
					.getSystemService(Context.POWER_SERVICE);
			WakeLock cpuLock = powerManager.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK, "Haptic FeedBack");
			cpuLock.acquire();
			//input loop
			while (running) {
				try {
					//listens for input
					String inMessage = input.readLine();
					System.out.println("Input:" + inMessage);
					if (inMessage.contains("Connect")) {
						output.println("<Connected>:</Connected>");

					} else {
						if (inMessage.contains("Vibrate")) {
							appControl.Vibrate();
						} else if (inMessage.contains("Stop")) {
							appControl.Stop();
						}
					}

				} catch (IOException e) {
					appControl.toast("Input error");
					if (running)
						appControl.disconnect();
				} catch (Exception e) {
					//disconnect if the connection is close on the other end
					Log.d("Socket Closed",
							"Socket was closed at the source or by the program");
					if (running)
						appControl.disconnect();
				}

			}
			//unlocks the cpu lock
			cpuLock.release();
		}

	}

	/**
	 * method for closing the streams
	 */
	public void endSession() {
		running = false;
		if (connected()) {
			try {
				input.close();
				output.close();
			} catch (IOException e) {
				appControl.toast("Closing stream errors");
			}
			disconnect();
		}

	}
}
