package com.example.honors.project;

import java.util.ArrayList;
import android.app.Activity;
import android.content.res.Resources;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.honors.project.connection.MobileHaptic;
import com.example.honors.project.filters.*;

/**
 * Control Class for the MVC
 * 
 * @author Andrew Bown
 * 
 */
public class AppControl {

	private ConnectionView mainView;
	private AppModel appModel;

	private ArrayList<String> idList;
	private MobileHaptic appMobileHaptic;
	private String portID;
	private String ipAddressID;
	private String connectID;
	private RadioGroup connectionTypeGroup;
	private Thread VibrateThread;
	private VibrateClass vibrateClass;

	/**
	 * The constructor for the controller
	 * 
	 * @param mainView
	 *            the view
	 * @param appModel
	 *            the model
	 */
	public AppControl(ConnectionView mainView, AppModel appModel) {
		this.mainView = mainView;
		this.appModel = appModel;

		Resources resources = mainView.getResources();
		idList = new ArrayList<String>();

		appMobileHaptic = new MobileHaptic(this);

		// loads all the interactive elements and add requisit listener
		ipAddressID = resources.getString(R.string.ipAddressID);
		idList.add(ipAddressID);

		EditText ipAddressInput = (EditText) mainView
				.findViewById(R.id.ipAddressEntry);
		ipAddressInput.setFilters(new InputFilter[] { new IpFilter(appModel,
				ipAddressID) });

		Button ipSaveButton = (Button) mainView.findViewById(R.id.saveIpButton);
		ipSaveButton.setOnClickListener(new SaveListener(ipAddressID,
				ipAddressInput));

		portID = resources.getString(R.string.portID);
		idList.add(portID);

		EditText portEntryField = (EditText) mainView
				.findViewById(R.id.portEntryField);
		portEntryField.setFilters(new InputFilter[] { new PortFilter(appModel,
				portID) });

		Button portSaveButton = (Button) mainView
				.findViewById(R.id.portSaveButton);
		portSaveButton.setOnClickListener(new SaveListener(portID,
				portEntryField));

		connectID = resources.getString(R.string.ConnectID);
		idList.add(connectID);

		ToggleButton connectButton = (ToggleButton) mainView
				.findViewById(R.id.ConnectButton);
		connectButton.setOnClickListener(new ConnectListener());

		connectionTypeGroup = (RadioGroup) mainView
				.findViewById(R.id.connectionTypeGroup);
		connectionTypeGroup
				.setOnCheckedChangeListener(new ConnectionTypeListener());

	}

	/**
	 * Listener for the save buttons
	 * 
	 * @author Andrew Bown
	 * 
	 */
	private class SaveListener implements OnClickListener {

		private String buttonID;
		private EditText inputBox;

		public SaveListener(String buttonID, EditText inputBox) {
			this.buttonID = buttonID;
			this.inputBox = inputBox;
		}

		@Override
		public void onClick(View v) {
			appModel.setDisplayData(buttonID, inputBox.getText().toString());
			inputBox.setText("");
			appModel.disableButton(buttonID);

		}

	}

	/**
	 * Thread Class to allow vibration to run in the background
	 * 
	 * @author Andrew Bown
	 * 
	 */
	private class VibrateClass implements Runnable {
		private boolean continueVibrating;
		private Vibrator vibrate;

		public VibrateClass() {
			this.continueVibrating = true;
			vibrate = mainView.getVibrator();
		}

		public void run() {
			// loop which is used to constantly vibrate
			while (continueVibrating) {
				vibrate.vibrate(500);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					toast("Sleep Error");
				}
			}
		}

		/**
		 * method that stops vibration and ends the thread
		 */
		public void stop() {
			continueVibrating = false;
			vibrate.cancel();
		}
	}

	/**
	 * Listener for the connect button
	 * 
	 * @author Andrew Bown
	 * 
	 */
	private class ConnectListener implements OnClickListener {
		private boolean connected;

		public ConnectListener() {
			connected = false;
		}

		@Override
		public void onClick(View v) {
			// swaps from connecting and disconnecting depending on the state
			if (!connected) {

				// on connection disables all interactive elements
				for (String id : idList) {
					appModel.deactivateID(id);
				}
				appModel.disableRadioButtons();
				if (connectionTypeGroup.getCheckedRadioButtonId() == R.id.BluetoothButton) {
					appMobileHaptic.connect();
				} else {
					String ipAddress = appModel.getData(ipAddressID);
					String portAddress = appModel.getData(portID);
					if (ipAddress != null && portAddress != null)
						appMobileHaptic.connect(ipAddress, portAddress);
				}

			} else {
				// reactivates all interactive element
				for (String id : idList) {
					appModel.reactivateID(id);
				}
				appMobileHaptic.disconnect();
				appModel.enableRadioButtons();
				setConnectTypeState(connectionTypeGroup
						.getCheckedRadioButtonId());

			}
			connected = !connected;

		}

	}

	/**
	 * Radio button listener
	 * 
	 * @author Andrew Bown
	 * 
	 */
	private class ConnectionTypeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			setConnectTypeState(checkedId);

		}

	}

	/**
	 * Activates elements based on which radio button is clicked
	 * 
	 * @param checkedId
	 *            id of the radio button clicked
	 */
	public void setConnectTypeState(int checkedId) {
		switch (checkedId) {
		case R.id.WifiButton:
			appModel.reactivateID(ipAddressID);
			appModel.reactivateID(portID);

			break;
		case R.id.BluetoothButton:
			appModel.deactivateID(ipAddressID);
			appModel.deactivateID(portID);
			break;
		}

	}

	/**
	 * method for activating toast
	 * 
	 * @param string
	 *            message to be printed
	 */
	public void toast(String string) {
		mainView.toastFromControl(string);

	}

	/**
	 * method for successful connections
	 */
	public void connected() {
		appModel.enableButton(connectID);

	}

	/**
	 * method for toggling the connect button
	 */
	public void toggelConnect() {
		appModel.toggleButton(connectID);

	}

	/**
	 * Activate the vibrate thread
	 */
	public void Vibrate() {
		if (VibrateThread == null) {
			vibrateClass = new VibrateClass();
			VibrateThread = new Thread(vibrateClass);
			VibrateThread.start();
		}

	}

	/**
	 * ends the vibrate thread
	 */
	public void Stop() {
		if (VibrateThread != null) {
			vibrateClass.stop();
			vibrateClass = null;
			VibrateThread = null;
		}

	}

	/**
	 * get a service from the main view
	 * 
	 * @param Service
	 *            the service needed
	 * @return the service
	 */
	public Object getSystemService(String Service) {

		return mainView.getSystemService(Service);
	}

	/**
	 * Method for dealing with a host disconnect
	 */
	public void disconnect() {
		appMobileHaptic.disconnect();
		toggelConnect();

	}

}
