#pragma once
#include "Wall.h"

#define DOOR_TEXTURE_FILE "Door_Keyhole.jpg"

class Door :
	public Wall
{
public:
	Door(D3DXVECTOR3 pos);
	Door(float x, float y, float z);
	~Door(void);
	virtual int initGeom(void);
	virtual int render(int time);
	int standardRender(int time);
	int shaderRender(int time);

private:
	static LPDIRECT3DTEXTURE9 doorTexture;
};

