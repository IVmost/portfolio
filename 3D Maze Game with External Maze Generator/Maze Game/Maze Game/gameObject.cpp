#include "GameObject.h"

IDirect3DDevice9 *GameObject::md3dDev = NULL;

D3DXMATRIX GameObject::viewMat;
D3DXMATRIX GameObject::projMat;
D3DXVECTOR3 GameObject::vLookAt;

GameObject::GameObject(void):mRoll(0.0),mPitch(0.0),mYaw(0.0),
mPosition(0.0,0.0,0.0),mDir(0.0,0.0,0.0), mIndBuf(NULL), mVtxBuf(NULL),ambientPower(0.5)
{
}

GameObject::GameObject(D3DXVECTOR3 pos):mRoll(0.0),mPitch(0.0),mYaw(0.0),
mPosition(pos.x, pos.y, pos.z),mDir(0.0,0.0,0.0), mIndBuf(NULL), mVtxBuf(NULL),ambientPower(0.5)
{
}

GameObject::GameObject(float x, float y, float z):mRoll(0.0),mPitch(0.0),mYaw(0.0),
mPosition(x, y, z),mDir(0.0,0.0,0.0), mIndBuf(NULL), mVtxBuf(NULL),ambientPower(0.5)
{
}

GameObject::~GameObject(void)
{
}

void GameObject::setAmbientPower(float ambientPower){
	this->ambientPower=ambientPower;

}
int GameObject::initGeom(void)
{
	return 0;
}

int GameObject::render(int time)
{

	return 0;
}

int GameObject::setd3dDev(IDirect3DDevice9 *d3dDev)
{
	if (md3dDev == NULL) {
		md3dDev = d3dDev;
	} else return (1);			//Device already set
	return 0;
}

D3DXVECTOR3 const GameObject::getPosition(void){
	return mPosition;
}

D3DXVECTOR3 const GameObject::getDirection(void){
	return mDir;
}

void GameObject::setPosition(float newX, float newY, float newZ){
	mPosition.x = newX;	
	mPosition.y = newY;
	mPosition.z = newZ;
}

/**
 *  This function returns the x(i.e. i) index and z(i.e. j) index of the grid that the centre
 * of the GameObject is in.
 */
void GameObject::getGridIndices(int* pX, int* pZ){
	//Counting on these being truncated
	*pX = (int)(mPosition.x / GRID_UNIT_LENGTH);
	*pZ = (int)(mPosition.z / GRID_UNIT_WIDTH);
	return;
}

int GameObject::setViewMat(D3DXMATRIX * viewMat)
{
	GameObject::viewMat = *viewMat;
	return 0;
}


int GameObject::setProjMat(D3DXMATRIX * projMat)
{
	GameObject::projMat = *projMat;
	return 0;
}

int GameObject::setLookAtVec(D3DXVECTOR3 * vLookAt)
{
	GameObject::vLookAt = *vLookAt;
	return 0;
}
