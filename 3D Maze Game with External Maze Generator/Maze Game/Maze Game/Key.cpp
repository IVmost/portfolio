#include "Key.h"

IDirect3DVertexDeclaration9* Key::keyDeclaration = NULL;

ID3DXEffect* Key::pKeyEffect=NULL;
D3DXHANDLE Key::hWorldViewProjMat=NULL;
D3DXHANDLE Key::hHighWaves=NULL;
D3DXHANDLE Key::hTechnique=NULL;
D3DXHANDLE Key::hWorldMat=NULL;
D3DXHANDLE Key::hViewMat=NULL;
D3DXHANDLE Key::hProjMat=NULL;
D3DXHANDLE Key::hAmbientPower=NULL;

Key::Key(D3DXVECTOR3 pos): GameObject(pos)
{
	mScaleX = DEFAULT_SCALE;
	mScaleY = DEFAULT_SCALE;
	mScaleZ = DEFAULT_SCALE;
	initGeom();
	if (SHADERS_ON)
		if (pKeyEffect == NULL){
			initEffect();
		}
}

Key::Key(float x, float y, float z): GameObject(x, y, z)
{
	mScaleX = DEFAULT_SCALE;
	mScaleY = DEFAULT_SCALE;
	mScaleZ = DEFAULT_SCALE;
	initGeom();
	if (SHADERS_ON)
		if (pKeyEffect == NULL){
			initEffect();
		}
}


Key::~Key(void)
{
	if(mIndBuf != NULL){
		mIndBuf->Release();
	}
	if(mVtxBuf != NULL){
		mVtxBuf->Release();
	}
	COM_RELEASE(this->pKeyEffect);
	COM_RELEASE(this->keyDeclaration);
}

int Key::initGeom(void){

	geomPoints[0].pos.x = 0;
	geomPoints[0].pos.y = 0;
	geomPoints[0].pos.z = 0;

	geomPoints[1].pos.x = 1;
	geomPoints[1].pos.y = 0;
	geomPoints[1].pos.z = 0;

	geomPoints[2].pos.x = 3;
	geomPoints[2].pos.y = 1;
	geomPoints[2].pos.z = 0;

	geomPoints[3].pos.x = 3;
	geomPoints[3].pos.y = 2;
	geomPoints[3].pos.z = 0;

	geomPoints[4].pos.x = 6;
	geomPoints[4].pos.y = 2;
	geomPoints[4].pos.z = 0;

	geomPoints[5].pos.x = 6;
	geomPoints[5].pos.y = 1;
	geomPoints[5].pos.z = 0;

	geomPoints[6].pos.x = 7;
	geomPoints[6].pos.y = 1;
	geomPoints[6].pos.z = 0;

	geomPoints[7].pos.x = 7;
	geomPoints[7].pos.y = 2;
	geomPoints[7].pos.z = 0;

	geomPoints[8].pos.x = 9;
	geomPoints[8].pos.y = 2;
	geomPoints[8].pos.z = 0;

	geomPoints[9].pos.x = 9;
	geomPoints[9].pos.y = 1;
	geomPoints[9].pos.z = 0;

	geomPoints[10].pos.x = 8;
	geomPoints[10].pos.y = 0;
	geomPoints[10].pos.z = -1;

	geomPoints[11].pos.x = 11;
	geomPoints[11].pos.y = 0;
	geomPoints[11].pos.z = -1;

	geomPoints[12].pos.x = 10;
	geomPoints[12].pos.y = 1;
	geomPoints[12].pos.z = 0;

	geomPoints[13].pos.x = 10;
	geomPoints[13].pos.y = 3;
	geomPoints[13].pos.z = 0;

	geomPoints[14].pos.x = 11;
	geomPoints[14].pos.y = 4;
	geomPoints[14].pos.z = -1;

	geomPoints[15].pos.x = 8;
	geomPoints[15].pos.y = 4;
	geomPoints[15].pos.z = -1;

	geomPoints[16].pos.x = 9;
	geomPoints[16].pos.y = 3;
	geomPoints[16].pos.z = 0;

	geomPoints[17].pos.x = 3;
	geomPoints[17].pos.y = 3;
	geomPoints[17].pos.z = 0;

	geomPoints[18].pos.x = 3;
	geomPoints[18].pos.y = 4;
	geomPoints[18].pos.z = 0;

	geomPoints[19].pos.x = 1;
	geomPoints[19].pos.y = 5;
	geomPoints[19].pos.z = 0;

	geomPoints[20].pos.x = 0;
	geomPoints[20].pos.y = 5;
	geomPoints[20].pos.z = 0;

	geomPoints[21].pos.x = 0;
	geomPoints[21].pos.y = 0;
	geomPoints[21].pos.z = 1;

	geomPoints[22].pos.x = 1;
	geomPoints[22].pos.y = 0;
	geomPoints[22].pos.z = 1;

	geomPoints[23].pos.x = 3;
	geomPoints[23].pos.y = 1;
	geomPoints[23].pos.z = 1;

	geomPoints[24].pos.x = 3;
	geomPoints[24].pos.y = 2;
	geomPoints[24].pos.z = 1;

	geomPoints[25].pos.x = 6;
	geomPoints[25].pos.y = 2;
	geomPoints[25].pos.z = 1;

	geomPoints[26].pos.x = 6;
	geomPoints[26].pos.y = 1;
	geomPoints[26].pos.z = 1;

	geomPoints[27].pos.x = 7;
	geomPoints[27].pos.y = 1;
	geomPoints[27].pos.z = 1;

	geomPoints[28].pos.x = 7;
	geomPoints[28].pos.y = 2;
	geomPoints[28].pos.z = 1;

	geomPoints[29].pos.x = 9;
	geomPoints[29].pos.y = 2;
	geomPoints[29].pos.z = 1;

	geomPoints[30].pos.x = 9;
	geomPoints[30].pos.y = 1;
	geomPoints[30].pos.z = 1;

	geomPoints[31].pos.x = 8;
	geomPoints[31].pos.y = 0;
	geomPoints[31].pos.z = 2;

	geomPoints[32].pos.x = 11;
	geomPoints[32].pos.y = 0;
	geomPoints[32].pos.z = 2;

	geomPoints[33].pos.x = 10;
	geomPoints[33].pos.y = 1;
	geomPoints[33].pos.z = 1;

	geomPoints[34].pos.x = 10;
	geomPoints[34].pos.y = 3;
	geomPoints[34].pos.z = 1;

	geomPoints[35].pos.x = 11;
	geomPoints[35].pos.y = 4;
	geomPoints[35].pos.z = 2;

	geomPoints[36].pos.x = 8;
	geomPoints[36].pos.y = 4;
	geomPoints[36].pos.z = 2;

	geomPoints[37].pos.x = 9;
	geomPoints[37].pos.y = 3;
	geomPoints[37].pos.z = 1;

	geomPoints[38].pos.x = 3;
	geomPoints[38].pos.y = 3;
	geomPoints[38].pos.z = 1;

	geomPoints[39].pos.x = 3;
	geomPoints[39].pos.y = 4;
	geomPoints[39].pos.z = 1;

	geomPoints[40].pos.x = 1;
	geomPoints[40].pos.y = 5;
	geomPoints[40].pos.z = 1;

	geomPoints[41].pos.x = 0;
	geomPoints[41].pos.y = 5;
	geomPoints[41].pos.z = 1;


	for(int i = 0; i < NUM_VERTICES; ++i){
		geomPoints[i].color = D3DCOLOR_XRGB(255, 201, 14);
	}

	indexGeom[0] = 0;
	indexGeom[1] = 20;
	indexGeom[2] = 19;
	indexGeom[3] = 0;
	indexGeom[4] = 1;
	indexGeom[5] = 19;

	indexGeom[6] = 19;
	indexGeom[7] = 18;
	indexGeom[8] = 1;
	indexGeom[9] = 1;
	indexGeom[10] = 2;
	indexGeom[11] = 18; //We kind of cheat here. inbetween 2 and 18 there's a gap.

	indexGeom[12] = 3;
	indexGeom[13] = 17;
	indexGeom[14] = 16;
	indexGeom[15] = 3;
	indexGeom[16] = 8;
	indexGeom[17] = 16; //we cheat again.

	indexGeom[18] = 4;
	indexGeom[19] = 5;
	indexGeom[20] = 7;
	indexGeom[21] = 7;
	indexGeom[22] = 6;
	indexGeom[23] = 5;

	indexGeom[24] = 8;
	indexGeom[25] = 9;
	indexGeom[26] = 12;
	indexGeom[27] = 12;
	indexGeom[28] = 11;
	indexGeom[29] = 9;

	indexGeom[30] = 9;
	indexGeom[31] = 10;
	indexGeom[32] = 11;
///////////////////////////////
	indexGeom[33] = 12;
	indexGeom[34] = 13;
	indexGeom[35] = 8;

	indexGeom[36] = 8;
	indexGeom[37] = 16;
	indexGeom[38] = 13;
	indexGeom[39] = 13;
	indexGeom[40] = 14;
	indexGeom[41] = 15;

	indexGeom[42] = 15;
	indexGeom[43] = 13;
	indexGeom[44] = 16;
//////////////////////////////////One side of the key done
	indexGeom[45] = 21;
	indexGeom[46] = 41;
	indexGeom[47] = 40;
	indexGeom[48] = 21;
	indexGeom[49] = 22;
	indexGeom[50] = 40;

	indexGeom[51] = 40;
	indexGeom[52] = 39;
	indexGeom[53] = 22;
	indexGeom[54] = 22;
	indexGeom[55] = 23;
	indexGeom[56] = 39;

	indexGeom[57] = 24;
	indexGeom[58] = 38;
	indexGeom[59] = 37;
	indexGeom[60] = 24;
	indexGeom[61] = 29;
	indexGeom[62] = 37;

	indexGeom[63] = 25;
	indexGeom[64] = 26;
	indexGeom[65] = 28;
	indexGeom[66] = 28;
	indexGeom[67] = 27;
	indexGeom[68] = 26;

	indexGeom[69] = 29;
	indexGeom[70] = 30;
	indexGeom[71] = 33;
	indexGeom[72] = 33;
	indexGeom[73] = 32;
	indexGeom[74] = 30;

	indexGeom[75] = 30;
	indexGeom[76] = 31;
	indexGeom[77] = 32;
////////////////////////////////////////
	indexGeom[78] = 33;
	indexGeom[79] = 34;
	indexGeom[80] = 29;
	indexGeom[81] = 29;
	indexGeom[82] = 37;
	indexGeom[83] = 34;

	indexGeom[84] = 34;
	indexGeom[85] = 35;
	indexGeom[86] = 36;
	indexGeom[87] = 36;
	indexGeom[88] = 34;
	indexGeom[89] = 37;
//////////////////////////////////////Second side of key done
	indexGeom[90] = 0;
	indexGeom[91] = 21;
	indexGeom[92] = 20;
	indexGeom[93] = 20;
	indexGeom[94] = 41;
	indexGeom[95] = 21;

	indexGeom[96] = 0;
	indexGeom[97] = 21;
	indexGeom[98] = 1;
	indexGeom[99] = 1;
	indexGeom[100] = 22;
	indexGeom[101] = 21;

	indexGeom[102] = 1;
	indexGeom[103] = 22;
	indexGeom[104] = 2;
	indexGeom[105] = 2;
	indexGeom[106] = 23;
	indexGeom[107] = 22;

	indexGeom[108] = 2;
	indexGeom[109] = 3;
	indexGeom[110] = 23;
	indexGeom[111] = 23;
	indexGeom[112] = 24;
	indexGeom[113] = 3;

	indexGeom[114] = 3;
	indexGeom[115] = 24;
	indexGeom[116] = 4;
	indexGeom[117] = 4;
	indexGeom[118] = 25;
	indexGeom[119] = 24;

	indexGeom[120] = 4;
	indexGeom[121] = 5;
	indexGeom[122] = 25;
	indexGeom[123] = 25;
	indexGeom[124] = 26;
	indexGeom[125] = 5;

	indexGeom[126] = 5;
	indexGeom[127] = 6;
	indexGeom[128] = 26;
	indexGeom[129] = 26;
	indexGeom[130] = 27;
	indexGeom[131] = 6;

	indexGeom[132] = 6;
	indexGeom[133] = 7;
	indexGeom[134] = 27;
	indexGeom[135] = 27;
	indexGeom[136] = 28;
	indexGeom[137] = 7;

	indexGeom[138] = 7;
	indexGeom[139] = 8;
	indexGeom[140] = 29;
	indexGeom[141] = 29;
	indexGeom[142] = 28;
	indexGeom[143] = 7;

	indexGeom[144] = 8;
	indexGeom[145] = 9;
	indexGeom[146] = 29;
	indexGeom[147] = 29;
	indexGeom[148] = 30;
	indexGeom[149] = 9;

	indexGeom[150] = 9;
	indexGeom[151] = 10;
	indexGeom[152] = 30;
	indexGeom[153] = 30;
	indexGeom[154] = 31;
	indexGeom[155] = 10;

	indexGeom[156] = 10;
	indexGeom[157] = 11;
	indexGeom[158] = 32;
	indexGeom[159] = 32;
	indexGeom[160] = 31;
	indexGeom[161] = 10;

	indexGeom[162] = 11;
	indexGeom[163] = 12;
	indexGeom[164] = 33;
	indexGeom[165] = 33;
	indexGeom[166] = 32;
	indexGeom[167] = 11;

	indexGeom[168] = 12;
	indexGeom[169] = 13;
	indexGeom[170] = 34;
	indexGeom[171] = 34;
	indexGeom[172] = 33;
	indexGeom[173] = 12;

	indexGeom[174] = 13;
	indexGeom[175] = 14;
	indexGeom[176] = 35;
	indexGeom[177] = 35;
	indexGeom[178] = 34;
	indexGeom[179] = 13;

	indexGeom[180] = 14;
	indexGeom[181] = 15;
	indexGeom[182] = 36;
	indexGeom[183] = 36;
	indexGeom[184] = 35;
	indexGeom[185] = 14;

	indexGeom[186] = 15;
	indexGeom[187] = 16;
	indexGeom[188] = 36;
	indexGeom[189] = 36;
	indexGeom[190] = 37;
	indexGeom[191] = 16;

	indexGeom[192] = 16;
	indexGeom[193] = 17;
	indexGeom[194] = 38;
	indexGeom[195] = 38;
	indexGeom[196] = 37;
	indexGeom[197] = 16;

	indexGeom[198] = 17;
	indexGeom[199] = 18;
	indexGeom[200] = 39;
	indexGeom[201] = 39;
	indexGeom[202] = 38;
	indexGeom[203] = 17;

	indexGeom[204] = 18;
	indexGeom[205] = 19;
	indexGeom[206] = 40;
	indexGeom[207] = 40;
	indexGeom[208] = 39;
	indexGeom[209] = 18;
	
	indexGeom[210] = 19;
	indexGeom[211] = 20;
	indexGeom[212] = 41;
	indexGeom[213] = 41;
	indexGeom[214] = 40;
	indexGeom[215] = 19;

	int rc=0;
	if (this->keyDeclaration== NULL){
		struct KEY_VTX v;
		D3DVERTEXELEMENT9 Decl[] =
	{
		{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()
	};
		Decl[0].Offset = (char *) &v.pos- (char *) &v;
		Decl[1].Offset = (char *) &v.color - (char *) &v;
		rc= md3dDev->CreateVertexDeclaration(Decl, &keyDeclaration);
	}
	return 0;
}


Key::KEY_VTX* const Key::getPoints(){
	return geomPoints;
}

void Key::setupIndVtxBuffers(){
	struct KEY_VTX *v = NULL;
	long *ind = NULL;
	
	if (SHADERS_ON){
		if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( NUM_VERTICES*sizeof(struct KEY_VTX), D3DUSAGE_WRITEONLY, NULL, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	
		}}else{
	if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( NUM_VERTICES*sizeof(struct KEY_VTX), D3DUSAGE_WRITEONLY, KEY_CUSTOMFVF, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	
	}}

	if(mIndBuf == NULL){
		md3dDev->CreateIndexBuffer( NUM_VTX_INDICES*sizeof(long), D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &this->mIndBuf, 0);
	}

    mVtxBuf->Lock(0, 0, (void**)&v, 0);    // lock the vertex buffer
    memcpy(v, geomPoints, NUM_VERTICES*sizeof(struct KEY_VTX));    // copy the vertices to the locked buffer
    mVtxBuf->Unlock();    // unlock the vertex buffer

	mIndBuf->Lock(0, 0, (void**)&ind, 0);
	memcpy(ind, indexGeom, NUM_VTX_INDICES*sizeof(long));
	mIndBuf->Unlock();
}

int Key::render(int time){

	if(SHADERS_ON){
		return shaderRender(time);
	}
	else{
		return standardRender(time);
	}
}

int Key::shaderRender(int time){

	int rc;
    static int angle = 0;

	static float step = 1;
	float rad = 0;

	unsigned int i=0, j=0, k=0;
	unsigned int numPasses=0;
	
	D3DXMATRIX worldMat, matTransform, matProjection, matScale, matTranslate,  matZRotation, matYRotation;

	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;
	
	D3DXMatrixRotationZ(&matZRotation, Z_ROTATION);
	worldMat *= matZRotation;
	D3DXMatrixRotationY(&matYRotation, Y_ROTATION * angle);
	worldMat *= matYRotation;
	angle = (int)(step + 1) % 361; //Allow to go from 0 - 360
	++step;
	
	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;

	rc= md3dDev->SetStreamSource(0,mVtxBuf,0, sizeof(KEY_VTX));
	rc = md3dDev->SetVertexDeclaration(keyDeclaration);

	md3dDev->SetIndices(mIndBuf);


	// set the technique
	rc = pKeyEffect->SetTechnique(hTechnique);

	// set the global matrix
	pKeyEffect->SetMatrix(hWorldViewProjMat, &(worldMat*viewMat*projMat));
	pKeyEffect->SetMatrix(hWorldMat, &worldMat);
	pKeyEffect->SetMatrix(hViewMat, &viewMat);
	pKeyEffect->SetMatrix(hProjMat, &projMat);
	pKeyEffect->SetFloat(hAmbientPower, 1.0f);
	
	
	

	// start the effect
	pKeyEffect->Begin(&numPasses, 0);		// 0 means save and restore the device states

	// start the technique passess
	for (i = 0; i < numPasses; i++) {
		pKeyEffect->BeginPass(i);

		// draw the mesh
		rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, NUM_VERTICES, 0, NUM_VTX_INDICES/3); 

		pKeyEffect->EndPass();
	}

	pKeyEffect->End();

   
	return 0;
}


int Key::standardRender(int time){
	int rc;
    static int angle = 0;

	static float step = 1;
	float rad = 0;

	static int i=0, j=0, k=0;

	D3DXMATRIX worldMat, viewMat, matTransform, matProjection, matScale, matTranslate,  matZRotation, matYRotation;
	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;

	D3DXMatrixRotationZ(&matZRotation, Z_ROTATION);
	worldMat *= matZRotation;
	D3DXMatrixRotationY(&matYRotation, Y_ROTATION * angle);
	worldMat *= matYRotation;
	angle = (int)(step + 1) % 361; //Allow to go from 0 - 360
	++step;

	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;


	//D3DXMatrixIdentity(&worldMat);
	md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

	// set the source
	rc = md3dDev->SetFVF(KEY_CUSTOMFVF);

	// set the index buffer
	rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(Key::KEY_VTX));
	rc = md3dDev->SetIndices(mIndBuf);

	rc = md3dDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
	//rc = md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	rc = md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	rc = md3dDev->SetTexture(0, NULL);
	// draw the cube
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, NUM_VERTICES, 0, NUM_VTX_INDICES/3); 

   
	return rc;
}

int Key::initEffect(void)
{
	int rc;
	ID3DXBuffer *errors = NULL;		// used to get the errors
	char *shaderFile[1] = {"ShaderFX.fx"};
	char *techniqueName[2] = {"KeyEffect", "TechniqueSobel"};



	D3DCAPS9 caps;

	md3dDev->GetDeviceCaps(&caps);

	// check for shader support of version 3

	if (caps.VertexShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}

	// check for pixel Shader Support
	if (caps.PixelShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}


	// load the effect from the file
	rc = D3DXCreateEffectFromFile(md3dDev, shaderFile[0], 0,0,D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION, 
		0, &pKeyEffect, &errors);

	if (rc) {
				switch(rc) {
			case D3DXERR_INVALIDDATA:
				rc+=0;
				break;
			case D3DERR_INVALIDCALL:
				rc+=0;
				break;
			case E_OUTOFMEMORY :
				rc+=0;
				break;
			default:
				rc+=0;
				break;
		}
	}
	if (errors != NULL) {
		// displays the errors when loading
		MessageBox(0, (char*) errors->GetBufferPointer(),0,0);
	}

	hWorldMat = hViewMat = hProjMat = NULL;
	// associate the external variable in the effect file with variable in the C++ program
	hWorldViewProjMat = pKeyEffect->GetParameterByName(NULL, "gWorldViewProjMat");

	// associate the external variable in the effect file with variable in the C++ program
	hWorldMat = pKeyEffect->GetParameterByName(NULL, "gWorldMat");

	// associate the external variable in the effect file with variable in the C++ program
	hViewMat = pKeyEffect->GetParameterByName(NULL, "gViewMat");

	// associate the external variable in the effect file with variable in the C++ program
	hProjMat = pKeyEffect->GetParameterByName(NULL, "gProjMat");

	

		// associate the technique in the effect file with the C++ program
	hTechnique = pKeyEffect->GetTechniqueByName(techniqueName[0]);
	hAmbientPower= pKeyEffect->GetParameterByName(NULL, "gAmbientPower");
	
	



	// first associate it by name and store it in a handle.  The handles can be class variables (static)


	if (errors != NULL) {
		errors->Release();
	}
	return 0;
}
