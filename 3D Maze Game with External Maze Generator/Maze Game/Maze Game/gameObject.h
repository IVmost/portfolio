#pragma once
#include "CommonHeaders.h"

//These values are half of the box they represent because they are used as offsets to the centre position
#define MOVING_OBJ_BOUND_LENGTH	 GRID_UNIT_LENGTH/4.0f
#define MOVING_OBJ_BOUND_WIDTH	 GRID_UNIT_WIDTH/4.0f
//Walls and path tiles start their geometry at 0,0,0 locally and their position is mapped to it so we don't divide by 2
#define BOUND_LENGTH	GRID_UNIT_LENGTH
#define BOUND_WIDTH		GRID_UNIT_WIDTH

class GameObject
{
	// functions
public:
	GameObject(void);
	GameObject(D3DXVECTOR3 pos);
	GameObject(float x, float y, float z);
	virtual ~GameObject(void);
	virtual int initGeom(void);
	virtual int render(int time) = 0;
	static int setd3dDev(IDirect3DDevice9 *d3dDev);  // setting the d3dDev 'global' variable
	virtual D3DXVECTOR3 const getPosition(void);
	virtual void setPosition(float newX, float newY, float newZ);
	virtual D3DXVECTOR3 const getDirection(void);
	virtual void setupIndVtxBuffers() = 0;
	virtual void getGridIndices(int* pX, int* pZ);
	void setAmbientPower(float);
	static int setViewMat(D3DXMATRIX * viewMat);
	static int setProjMat(D3DXMATRIX * projMat);
	static int setLookAtVec(D3DXVECTOR3 * vLookAt);
	float ambientPower;
	//members
protected:	
	D3DXVECTOR3 mPosition;	// position of object
	D3DXVECTOR3 mDir;	// direction of object
	float mRoll;			// angle of rotation around z-axis
	float mPitch;		// angle of rotation around x-axis
	float mYaw;			// angle of rotation around y-axis
	float mScaleX;		// scale in x
	float mScaleY;		// scale in y
	float mScaleZ;		// scale in z

	static IDirect3DDevice9 *md3dDev;


	IDirect3DIndexBuffer9 *mIndBuf;
	IDirect3DVertexBuffer9 *mVtxBuf;

	static D3DXMATRIX viewMat;
	static D3DXMATRIX projMat;
	static D3DXVECTOR3 vLookAt;
};

