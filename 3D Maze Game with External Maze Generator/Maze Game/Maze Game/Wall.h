#pragma once
#include "GameObject.h"
#include "CommonHeaders.h"
/*
 * Note: thanks to directxtutorial.com for the examples.
 * I got the brick textures from http://www.bricksntiles.com/textures/.
 */

#define BRICK_TEXTURE_FILE   "brickwork-texture.jpg"
#define BRICK_BUMP_FILE      "brickwork-bump-map.jpg"
#define BRICK_NORMAL_FILE    "brickwork_normal-map.jpg"

#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define WALL_NUM_VERTICES 40
#define WALL_NUM_INDICES 36



class Wall: public GameObject
{
public:
	struct WALL_VTX{
		D3DXVECTOR3 pos;
		DWORD color;
		D3DXVECTOR2 tex0;
	};
	Wall(D3DXVECTOR3 pos);
	Wall(float x, float y, float z);
	~Wall(void);
	virtual int initGeom(void);
	WALL_VTX* const getPoints();
	void setupIndVtxBuffers();
	virtual int render(int time);
	int initEffect(void);
	int standardRender(int time);
	int shaderRender(int time);
	int createVertexDecl();
	static int preRender();
	


	D3DXMATRIX const getTransformationMatrix();
	static ID3DXEffect *pWallEffect;

	static D3DXHANDLE hWorldViewProjMat;
	static D3DXHANDLE hHighWaves;
	static D3DXHANDLE hTechnique;
	static D3DXHANDLE hWorldMat;
	static D3DXHANDLE hViewMat;
	static D3DXHANDLE hProjMat;
	static D3DXHANDLE hTexture;
	static D3DXHANDLE hBumpMap;
	static D3DXHANDLE hLookAt;
	static D3DXHANDLE hAmbientPower;
	static IDirect3DVertexDeclaration9* wallDeclaration;
protected:

	static LPDIRECT3DTEXTURE9 brickTexture;
	static LPDIRECT3DTEXTURE9 bumpTexture;
	static LPDIRECT3DTEXTURE9 normalTexture;

	long indexGeom[WALL_NUM_INDICES];
	WALL_VTX geomPoints[WALL_NUM_VERTICES];
};
