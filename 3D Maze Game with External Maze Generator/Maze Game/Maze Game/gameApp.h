#pragma once
#include "GameIO.h"
#include "GameObject.h"
#include "SkyBox.h"
#include <time.h>
/* 
	Project: Comp 3501 project
	Filename: GameApp.h
	Purpose: Stores the Variables which are used for creating the window and starting the game.
	Owners: Andrew Bown (100326878)
			Anthony D�Angelo
			
*/

#define DEFAULT_TIME_LIMIT     5.0*60.0//time limit in seconds. default is 5 mins.
#define RESTART_RETURN_CODE    25
#define BASE_SCORE_INCREASE    10.0

class GameApp
{
public:
	GameApp(void);
	~GameApp(void);
	GameApp(HINSTANCE hInstance, char* gameName);
	
	virtual int updateGameState(long time);  // update the game state
	virtual int renderFrame(int time);		// render the frame
	virtual int gameLoop(void);					// start the game loop
	virtual int initGame(void) = 0;				// initialize the game
	D3DMATRIX * getProjMat(D3DXMATRIX * matProj);
	void setProj(float nearPlane, float farPlane, float fieldOfView, float aspectRatio);
	
	static const int GAMETIMELIMIT=45; 
	INT64 totalTime;
	INT64 LastLoop;


private:
	
	int initWindow(void); // initialize the game window (assuming only one window)
	int initD3D(void);
	int init(void);


//MEMBERS
protected:
	virtual int resetGame(void) = 0;

	HINSTANCE mhinstance; // application instance
	HWND mhwnd;			 //window handle
	
	long mFramesPerSecond;	// maximum numbef of frames per second
	int mFullScreen;  // determines whether the game is played in full screen
	
	int mWndWidth;   // window width and 
	int mWndHeight;  // window height

	char mGameName[TITLE_SIZE+1];  // title of game (to be used in the window)

	//thanks to http://www.cplusplus.com/reference/clibrary/ctime/ for help with the time functions
	time_t zeroHourTime;
	time_t currentRoundOldTime;
	time_t currentRoundTimeElapsed;
	time_t timeLimit;

	long score;

	// direct3D members
	IDirect3D9 *md3d;		// com object of directx 9;
	IDirect3DDevice9 *md3dDev;		//direct3D device;
	D3DPRESENT_PARAMETERS md3dpp;  // presentation parameters
	
	// projection
	D3DXMATRIX matProj;
	float nearPlane;
	float farPlane;
	float fieldOfView;
	float aspectRatio;

	GameIO *mInput;
	int initGameInput(void);

	int getWndWidth(void);
public:
	int getWndHeight(void);
};

