#pragma once
#include "GameObject.h"
#include "Wall.h"
#include "PathTile.h"
#include "Door.h"
#include "Key.h"
#include "CommonHeaders.h"
#include "LabyrinthParser.h"
#include <list>

struct floorVertex {
	D3DXVECTOR3 pos;
    DWORD color;        // The vertex color

};

struct surfaceInfo{
	D3DXVECTOR3 plane;
	D3DXVECTOR3 surface;

};

struct grid_Coordinate{
	int indexX;
	int indexZ;
};

class Labyrinth :
	public GameObject
{
public:
	Labyrinth(void);
	~Labyrinth(void);
	Labyrinth(float dx, float dz);
	virtual int render(int time);
	int createSurface(int numRows, int numCols, float dx, float dz);
	int createVtxDescription(void);
	int setCentrePosition(D3DXVECTOR3 centrePos);
	virtual void setupIndVtxBuffers();
	Player* const getPlayer();
	Key* const getKey();
#if !SUPERMAN_MODE
	bool movePlayerForward();
	bool movePlayerBackward();
#endif
	time_t const getTimeLimitInMins();
	float getXSpan();
	float getZSpan();
	D3DXVECTOR3 getCentreXZ();
	

public:
	float dx;	// cell dimension along x axis
	float dz;	// cell dimension along z axis
	

	static IDirect3DVertexDeclaration9* floorDecl;
	int setScale(float scaleX, float scaleY, float scaleZ);

private:
	void checkPlayerWallCollisions(list<grid_Coordinate>* collisionCoords, D3DXVECTOR3 nextPlayerPos);
	void checkPlayerKeyCollision();
	bool checkPlayerObsAreaCollision();
	int checkDeterminantWithPlayer(D3DXVECTOR3 testPoint, D3DXVECTOR3 pointA, D3DXVECTOR3 pointB);
	INT64 startTime;
	INT64 ticksPerSecond;

	void setGoalIllumination();
	double timePassed;
	LabyrinthParser labParser;
	GameObject*** mazeObjectsArray;
	int numRows;
	int numCols;
	Key* pMyKey;
	Player* pMyPlayer;
	time_t levelTimeLimit;
};

