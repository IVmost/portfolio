#pragma once
#include "Wall.h"

/*
 * Note: thanks to directxtutorial.com for the examples.
 */

class PathTile :
	public Wall
{
public:
	//Allow passing of y value in case we ever want to add height...
	PathTile(D3DXVECTOR3 pos);
	PathTile(float x, float y, float z);
	~PathTile(void);
	virtual int initGeom(void);
	virtual int render(int time);
	bool isObservationArea(void);
	void setObservationArea(bool observable);

protected:
	bool observationArea;

};

