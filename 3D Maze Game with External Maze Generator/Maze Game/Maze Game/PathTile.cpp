#include "PathTile.h"


PathTile::PathTile(D3DXVECTOR3 pos): Wall(pos), observationArea(false)
{
	mScaleY = 0;
	initGeom();
}

PathTile::PathTile(float x, float y, float z): Wall(x, y, z), observationArea(false)
{
	mScaleY = 0;
	initGeom();
}


PathTile::~PathTile(void)
{

}

int PathTile::initGeom(void){
	Wall::initGeom();

	if(observationArea){
		for(int i = 0; i < 8; ++i){
			geomPoints[i].color = D3DCOLOR_XRGB(128, 0, 255);
		}
	} else {
		for(int i = 0; i < 8; ++i){
			geomPoints[i].color = D3DCOLOR_XRGB(133, 250, 27);
		}
	}

	return 0;
}

bool PathTile::isObservationArea(void){
	return observationArea;
}

void PathTile::setObservationArea(bool observable){
	observationArea = observable;
	initGeom();
	return;
}

int PathTile::render(int time){
	if (SHADERS_ON)
		return shaderRender(time);
	else{
		int rc;
		D3DXMATRIX worldMat = getTransformationMatrix();
	
		md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

		// set the source
		rc = md3dDev->SetFVF(CUSTOMFVF);

		// set the index buffer
		rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(Wall::WALL_VTX));
		rc = md3dDev->SetIndices(mIndBuf);

		rc = md3dDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
		rc = md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
		// draw the cube
		rc =md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, WALL_NUM_VERTICES, 0, WALL_NUM_INDICES/3); 
		return rc;
	}

	
}
