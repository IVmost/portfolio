#pragma once
#include "gameobject.h"
#include "camera.h"

class Player :
	public GameObject
{
public:
	Player(D3DXVECTOR3 pos);
	Player(float x, float y, float z);
	~Player(void);
	virtual int render(int time);
	Camera* const getCamera();
	virtual void setupIndVtxBuffers();
	boolean foundKey();
	void setFoundKey(boolean bFound);
#if SUPERMAN_MODE
	int setRoll(float angleDeg);
	float getRoll();
	int updateSpeed(float newSpeed);
	float getSpeed(void);
	D3DXVECTOR3 moveForward(float numUnits);
#else
	D3DXVECTOR3 testMoveForward();
	D3DXVECTOR3 testMoveBackward();
	void moveForward(bool canMoveX = true, bool canMoveZ = true);
	void moveBackward(bool canMoveX = true, bool canMoveZ = true);
#endif
	int setPitch(float angleDeg);
	float getPitch();
	int setYaw(float angleDeg);
	float getYaw();
	int updateOrientation(D3DXVECTOR3 rotVector, float angleRad);
	D3DXVECTOR3 getUpVector(void);
	bool isUpsideDown();
	void setOnObsTile(bool onObsTile);
	bool isOnObsTile();
	int setYawSpecial(float angleDeg);
private:
	Camera gameCamera;
	boolean bFoundKey;
	//inherited mDir is like the lookAhead
	D3DXVECTOR3 upVector;
	D3DXMATRIX rotMat;
	bool onObservationTile;
#if SUPERMAN_MODE
	float speed;
#endif
};

