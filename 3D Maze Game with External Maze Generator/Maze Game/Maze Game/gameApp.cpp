
/* 
	Project: Comp 3501 project
	Filename: GameApp.cpp
	Purpose: Creates Windows and Starts the game
	Owners: Andrew Bown (100326878)
			Anthony D�Angelo
			
*/
#include "GameApp.h"
#include "CommonHeaders.h"

// DEFINE

/*
Call name globalWindProc
Purpose: the window procedure to be used when creating a window

Descripton: 
This is a single global function.  It should be modified so that message will be processed by the app
by the application.  

Return:
1 - if failed
0 - if successful

*/


LRESULT CALLBACK globalWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int rc = 1;
	switch (msg){
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			// Handle any messages the switch statement didn't
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	// Handle any messages the switch statement didn't
	return DefWindowProc(hwnd, msg, wParam, lParam);
}


/*

Function Name: ~GameApp
Purpose: destructor of the game application


Descripton: Releases any assest which have already been assigned.

*/

GameApp::~GameApp(void)
{
	if (md3dDev != NULL) {
		md3dDev->Release();
		md3dDev = NULL;
	}
	if (md3d != NULL) {
		md3d->Release();
		md3d = NULL;
	}
	if (mInput != NULL) {
		delete(mInput);
		mInput = NULL;
	}
}


/*

Function Name: GameApp()
Purpose: constructor of the game application


Descripton: constructs the GameApp and assigns the default settings

Return:


*/


GameApp::GameApp(HINSTANCE hInstance, char * gameName) : 
  mFullScreen(0), 
  mWndWidth(0), 
  mWndHeight(0)
  ,nearPlane(0), farPlane(0),md3d(NULL), timeLimit(0), score(0)
{
	this->mhinstance = hInstance;
	mFullScreen = false;
	mWndWidth = WND_WIDTH;
	mWndHeight = WND_HEIGHT;
	mFramesPerSecond = FRAMES_PER_SECOND;
	mhwnd = NULL;
	totalTime=0;
	mGameName[TITLE_SIZE] = '\0';
	if (mGameName == NULL) this->mGameName[0] = '\0';
	else strncpy_s(this->mGameName, gameName,TITLE_SIZE);
	QueryPerformanceCounter((LARGE_INTEGER *) &LastLoop);
	init();

  }

/*

Funtion Name: init

Purpose: initialize the game application devices - window, graphics and input


Descripton: Create and initializes the window direct3d and input devices

Return:
1 - if failed
0 - if successful

*/
int GameApp::init(void)
{
	int rc = 0;
	// initialize the window
	rc = initWindow();
	if (rc != 0) {
		MessageBox(0, "Failed to initalize the game window", 0, 0);
		PostQuitMessage(0);
	}


	// initialize the direct3D
	rc = initD3D();
	if (rc != 0) {
		MessageBox(0, "Failed to initalize the graphics device", 0, 0);
		PostQuitMessage(0);
	}

	// initalize the input and output
	rc = initGameInput(); 
	if (rc != 0) {
		MessageBox(0, "Failed to initalize the input devices", 0, 0);
		PostQuitMessage(0);
	}
	return 0;
}



/*

Function Name initWindow
Purpose: initialize the game window (assuming only one window)


Descripton: 

Return:
1 - if failed
0 - if successful

*/

int GameApp::initWindow(void)
{	
	int rc = 0;
	char *winClass = "GameAppClass";
	WNDCLASS wc;


	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.style			= CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc		= globalWndProc;
	wc.hInstance		= mhinstance;
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName	= winClass;
	wc.hIcon			= LoadIcon(0, IDI_APPLICATION);
	wc.lpszMenuName		= NULL;
	wc.lpszClassName	= winClass;


	rc = RegisterClass(&wc);
	if (rc == 0) {
		// could not register the window
		return(1);
	}

	rc = 0;  
	mhwnd = CreateWindow(winClass, mGameName, WS_OVERLAPPEDWINDOW, 10, 10, this->mWndWidth, this->mWndHeight, NULL, NULL, mhinstance, NULL);

   if (mhwnd == NULL) {
	   rc = GetLastError();
	   rc = 1;
   }else {
	   ShowWindow(mhwnd, SW_SHOW);
	   UpdateWindow(mhwnd);
	}

	return(rc);
}

/******************************************************************/
/*
Purpose: initializes the graphics device

Descripton: 

Return:
1 - if failed
0 - if successful

*/



int GameApp::initD3D(void)
{
	int rc = 0;;

	// get the com object if it does not exist 
	if (md3d == NULL) {// check if the com object was already initialized
		// initialize the com object
		md3d = Direct3DCreate9(D3D_SDK_VERSION);
		if (md3d == NULL) {
			rc = 1;
			return(rc);
		}
	}

	// initial the direct3D9 parameters
	ZeroMemory(&md3dpp, sizeof(md3dpp));
	md3dpp.BackBufferWidth = mWndWidth;			// the width of the back buffer
	md3dpp.BackBufferHeight = mWndHeight;		// the height of the back buffer
	md3dpp.Windowed = !mFullScreen;				// using windowed version
	md3dpp.hDeviceWindow = mhwnd;				// pass the handle to the window
	md3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;    // discard old frames
	md3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	md3dpp.EnableAutoDepthStencil = TRUE;		// allow depth buffer
	md3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;  // stencil buffer format
	



	// create a directx device which will enable drawing on the screen.
	rc = md3d->CreateDevice(D3DADAPTER_DEFAULT,		// this is the main adapter
							 D3DDEVTYPE_HAL,			// use hardware abstraction layer
							 mhwnd,					// the window to be rendered
							 D3DCREATE_SOFTWARE_VERTEXPROCESSING,
							 //D3DCREATE_HARDWARE_VERTEXPROCESSING,
							 &md3dpp,				// address of the d3d parameters structures 
							 &md3dDev);				// address of the allocated device

    // Turn off culling, so we see the front and back of the triangle
    md3dDev->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
    // Turn off D3D lighting, since we are providing our own vertex colors
    md3dDev->SetRenderState( D3DRS_LIGHTING, FALSE );
	md3dDev->SetRenderState(D3DRS_ZENABLE, TRUE);    // turn on the z-buffer

	md3dDev->SetSamplerState( 0, D3DSAMP_ADDRESSU,  D3DTADDRESS_CLAMP );
	md3dDev->SetSamplerState( 0, D3DSAMP_ADDRESSV,  D3DTADDRESS_CLAMP );

	md3dDev->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC );
	md3dDev->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC );
	md3dDev->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC );

	if (rc != S_OK) {
		// release d3d (the com object)
		md3d->Release();
		md3d = NULL;
		rc = 1;
	}
	GameObject::setd3dDev(md3dDev);
	SkyBox::setd3dDev(md3dDev);
	return(0);
}
/******************************************************************/
/*
Purpose: initializes the game input devices

Descripton: 

Return:
1 - if failed
0 - if successful

*/


int GameApp::initGameInput(void)
{
	mInput = new GameIO(mhinstance, mhwnd);
	if (mInput == NULL) return(1);
	else return 0;
}


/******************************************************************/
/*
Purpose: executes the message loop

Descripton: 
This function is a virtual function and can be replaced by a similar function in the derived class.
Thanks to http://www.cplusplus.com/forum/windows/47270/ for help with message responses.


Return:
1 - if failed
0 - if successful

*/


int GameApp::gameLoop(void)
{
	int rc = 0;
    // this struct holds Windows event messages
    MSG msg;    // this struct holds Windows event messages
	// variables for measuring the time
	

	INT64 tickPerSecond;
	INT64 curTime;
	QueryPerformanceFrequency((LARGE_INTEGER *) &tickPerSecond);
	
	static DWORD LastRenderTime = GetTickCount();
	DWORD ElapsedTime = 0;
	DWORD TimePerFrame = 1000 / mFramesPerSecond;
	DWORD CurrentTime = 0;
	static long gameTime = 0;		// "time" of the game - number of frames since the begining of the game

	
	//Start timing
	currentRoundOldTime = currentRoundTimeElapsed = zeroHourTime = time(NULL);
	if(zeroHourTime == NULL){
		return -1;
	}
	if(zeroHourTime < 0){
		return -1;
	}

	//If not set to a value higher than zero in the game's initGame() function, it is defaulted here.
	if(timeLimit < 1){
		timeLimit = (time_t)(DEFAULT_TIME_LIMIT);
	}

    // Enter the infinite message loop
    while(TRUE)
    {
		//Before we do anything, let's check if the time is up. We wait until less than zero so the display shows zero
		if (difftime(timeLimit, (time_t)difftime(currentRoundTimeElapsed, zeroHourTime)) < 0){
			//Game Over, time's up
			char buffer[256];
			sprintf_s(buffer,"Too bad! \nYou ran out of time! = (\nYour current score is %ld. Would you like to keep playing?",
				score);
			int response = MessageBox(NULL, buffer , "Game Over", MB_YESNO|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
			switch(response){
				case IDYES:
					MessageBox(NULL, "Please choose a level." , "Play Again", MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
					return RESTART_RETURN_CODE;
				case IDNO:
					// quit
					MessageBox(NULL, "Thanks for playing! Have a nice day! = )" , "Game Over", MB_OK|MB_SETFOREGROUND);
					PostQuitMessage(0);
					break;
				default:
					// quit
					MessageBox(NULL, "Thanks for playing! Have a nice day! = )" , "Game Over", MB_OK|MB_SETFOREGROUND);
					PostQuitMessage(0);
					break;
			}
		}
		
        // Check to see if any messages are waiting in the queue
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            // If the message is WM_QUIT, exit the while loop
            if (msg.message == WM_QUIT)
                break;

            // translate keystroke messages into the right format
            TranslateMessage(&msg);

            // send the message to the WindowProc function
            DispatchMessage(&msg);
        }

		// update the game state
		// note that this is done via a virtual function
		rc = updateGameState(gameTime);
		if(rc == 1){
			score += (long)(BASE_SCORE_INCREASE + (difftime(timeLimit, (time_t)difftime(currentRoundTimeElapsed, zeroHourTime)) * BASE_SCORE_INCREASE)); 
			char buffer[256];
			sprintf_s(buffer,"Congratulations! \nYou Win!\nIt took %.0f seconds. Your current score is %ld. Would you like to keep playing?",
				difftime(currentRoundTimeElapsed, zeroHourTime),
				score);
			int response = MessageBox(NULL, buffer , "Game Over", MB_YESNO|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
			switch(response){
				case IDYES:
					MessageBox(NULL, "Please choose a level." , "Play Again", MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
					return RESTART_RETURN_CODE;
				case IDNO:
					// quit
					MessageBox(NULL, "Have a nice day! = )" , "Game Over", MB_OK|MB_SETFOREGROUND);
					PostQuitMessage(0);
					break;
				default:
					// quit
					MessageBox(NULL, "Have a nice day! = )" , "Game Over", MB_OK|MB_SETFOREGROUND);
					PostQuitMessage(0);
					break;
			}
		}
		/*
		QueryPerformanceCounter((LARGE_INTEGER *) &curTime);
		totalTime+= (curTime-LastLoop)/tickPerSecond;
		LastLoop=curTime;
		if (totalTime > GAMETIMELIMIT){
			char buffer[256];
			sprintf_s(buffer,"Too bad the time lime of %d has passed.\nYou took %d seconds.",GAMETIMELIMIT,totalTime);
			MessageBox(NULL, buffer , "Game Over", MB_OK|MB_ICONEXCLAMATION|MB_SETFOREGROUND);
			rc=1;
		}*/

		if (rc != 0) {
			// quit
			PostQuitMessage(0);
		}

		// render the next frame  and present the game to user
		// note that this is done via a virtual function
		renderFrame(gameTime);
		gameTime++;
        

		// determine whether to sleep 
		CurrentTime = GetTickCount();
		ElapsedTime = CurrentTime - LastRenderTime;
		while(ElapsedTime < TimePerFrame) {
			CurrentTime = GetTickCount();
			ElapsedTime = CurrentTime - LastRenderTime;
		}

		
		LastRenderTime = GetTickCount();
		//not sure we need this old time...
		currentRoundOldTime = currentRoundTimeElapsed;
		currentRoundTimeElapsed = time(NULL);
    }

    // return this part of the WM_QUIT message to Windows
    return msg.wParam;	
	return 0;
}


/******************************************************************/
/*
Purpose: updates the game state

Descripton: 

Return:
1 - if failed
0 - if successful

*/



int GameApp::updateGameState(long time)
{
	return 0;
}


/******************************************************************/
/*
Purpose: renders one frame

Descripton: 

Return:
1 - if failed
0 - if successful

*/



int GameApp::renderFrame(int time)
{
	return 0;
}

/******************************************************************/
/*
Purpose: window width

Descripton: 

Return:
the window width

*/

int GameApp::getWndWidth(void)
{
	return (this->mWndWidth);
}

/******************************************************************/
/*
Purpose: window width

Descripton: 

Return:
the window height

*/
int GameApp::getWndHeight(void)
{
	return (this->mWndHeight);
}




D3DMATRIX * GameApp::getProjMat(D3DXMATRIX * matProj)
{
	return(D3DXMatrixPerspectiveFovLH(matProj,fieldOfView, aspectRatio, nearPlane, farPlane));
}




void GameApp::setProj(float nearPlane, float farPlane, float fieldOfView, float aspectRatio)

{
	this->nearPlane = nearPlane;
	this->farPlane = farPlane;
	this->fieldOfView = fieldOfView;
	this->aspectRatio = aspectRatio;
}
