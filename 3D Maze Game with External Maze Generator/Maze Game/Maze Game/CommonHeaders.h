#pragma once

//definitions
//thanks to http://msdn.microsoft.com/en-us/library/x98tx3cf%28v=vs.80%29.aspx for the memory leak code.
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "Defines.h"

// directX header Files:
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>
#include <d3dx9math.h>

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <WinUser.h>
#include <WinNT.h>

#define SUPERMAN_MODE 0

// C RunTime Header Files
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <time.h>
#include <string>
#include <iostream>
#include <fstream>
