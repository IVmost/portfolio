#pragma once
#include "Labyrinth.h"
#include "Camera.h"
#include "GameApp.h"
#include "SkyBox.h"
#include "CommonHeaders.h"

class MyGame : public GameApp
{
public:
	MyGame(HINSTANCE hInstance, char* gameName);
	~MyGame(void);
	int updateGameState(long time);
	int renderFrame(int time);
	int initGame(void);
	virtual int resetGame(void);
	// sets the matrices of the view points and the projection
	virtual int setMatrices(void);

private:	
	Player* pPlayer;
	Key* pKey;
	Labyrinth* ground;
	SkyBox mySkyBox;
	D3DXVECTOR3 mazeCamLookatPoint, mazeCamPos, mazeCamUp;

	void resumeFromPause();

	// variables used to position the text on the screen
	int x;	// x location of the string to be drawn
	int y;	// y location of the string to be drawn
	int dx;		// change in x at each frame
	int dy;		// change in y at each frame
	bool playerObserving;
	bool gamePaused;

	float rotSpeed;     //used to change rotation speed
#if SUPERMAN_MODE
	float acceleration; //used to change plane acceleration
#endif
	 
	LPD3DXFONT fontCourier; // font to be used 
	LPD3DXFONT fontObsInstrCourier; // font to be used 
	
	char s[1024]; // string to be drawn
	char obsInstr[1024]; // string to be drawn
	RECT textBox;	// rectangle for the text
	RECT obsInstrTextBox;	// rectangle for the text
};

