#include "SkyBox.h"

IDirect3DDevice9 *SkyBox::md3dDev = NULL;

ID3DXEffect* SkyBox::pSkyEffect=NULL;
D3DXHANDLE SkyBox::hWorldViewProjMat=NULL;
D3DXHANDLE SkyBox::hHighWaves=NULL;
D3DXHANDLE SkyBox::hTechnique=NULL;
D3DXHANDLE SkyBox::hWorldMat=NULL;
D3DXHANDLE SkyBox::hViewMat=NULL;
D3DXHANDLE SkyBox::hProjMat=NULL;
D3DXHANDLE SkyBox::hTexture=NULL;

IDirect3DVertexDeclaration9* SkyBox::SkyDeclaration=NULL;

D3DXMATRIX SkyBox::viewMat;
D3DXMATRIX SkyBox::projMat;

SkyBox::SkyBox(void): mScaleX(SKYBOX_DEPTH_SCALE), mScaleY(SKYBOX_HEIGHT_SCALE), mScaleZ(SKYBOX_DEPTH_SCALE),
	wallTexture(NULL), skyTexture(NULL), floorTexture(NULL), cameraLifted(false),
	obsScaleX(SKYBOX_DEPTH_SCALE), obsScaleZ(SKYBOX_DEPTH_SCALE),
	mIndBuf(NULL), mVtxBuf(NULL), skyBoxTexture(NULL)
{

	if(md3dDev == NULL){
		std::cerr<<"Didn't have a d3device handle for the skybox so we're going to quit..."<<std::endl;
		exit(-1);
	}

	if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, WALL_FILE, &wallTexture)){
		std::cerr<<"Couldn't open skybox texture so we're going to quit..."<<std::endl;
		exit(-1);
	}

	if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, SKY_FILE, &skyTexture)){
		std::cerr<<"Couldn't open skybox texture so we're going to quit..."<<std::endl;
		exit(-1);
	}

	if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, FLOOR_FILE, &floorTexture)){
		std::cerr<<"Couldn't open skybox texture so we're going to quit..."<<std::endl;
		exit(-1);
	}
	if (!SHADERS_ON)
		initGeom();
	else
		initShaderGeom();
	if (SHADERS_ON)
	{
		if (SkyDeclaration==NULL){
			createVertexDecl();
			initEffect();
		}
	}
	setupIndVtxBuffers();
}

int SkyBox::createVertexDecl()
{
	int rc=0;
	if (this->SkyDeclaration== NULL){
		struct SKYBOX_VTX v;
		D3DVERTEXELEMENT9 Decl[] =
	{
		{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		D3DDECL_END()
	};
		Decl[0].Offset = (char *) &v.pos- (char *) &v;
		rc= md3dDev->CreateVertexDeclaration(Decl, &SkyDeclaration);
	}
	return rc;
}

SkyBox::~SkyBox(void)
{

	if(wallTexture != NULL){
		wallTexture->Release();
		wallTexture = NULL;
	}

	if(skyTexture != NULL){
		skyTexture->Release();
		skyTexture = NULL;
	}

	if(floorTexture != NULL){
		floorTexture->Release();
		floorTexture = NULL;
	}

	if(mIndBuf != NULL){
		mIndBuf->Release();
	}

	if(mVtxBuf != NULL){
		mVtxBuf->Release();
	}
	COM_RELEASE(SkyDeclaration);
	COM_RELEASE(pSkyEffect);
	COM_RELEASE(skyBoxTexture);
}

int SkyBox::setd3dDev(IDirect3DDevice9 *d3dDev)
{
	if (md3dDev == NULL) {
		md3dDev = d3dDev;
	} else return (1);			//Device already set
	return 0;
}

int SkyBox::initEffect(void)
{
	int rc;
	ID3DXBuffer *errors = NULL;		// used to get the errors
	char *shaderFile[2] = {"ShaderFX.fx"};
	char *techniqueName[2] = {"SkyEffect"};



	D3DCAPS9 caps;

	md3dDev->GetDeviceCaps(&caps);

	// check for shader support of version 3

	if (caps.VertexShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}

	// check for pixel Shader Support
	if (caps.PixelShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}


	// load the effect from the file
	rc = D3DXCreateEffectFromFile(md3dDev, shaderFile[0], 0,0,D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION, 
		0, &pSkyEffect, &errors);

	if (rc) {
				switch(rc) {
			case D3DXERR_INVALIDDATA:
				rc+=0;
				break;
			case D3DERR_INVALIDCALL:
				rc+=0;
				break;
			case E_OUTOFMEMORY :
				rc+=0;
				break;
			default:
				rc+=0;
				break;
		}
	}
	if (errors != NULL) {
		// displays the errors when loading
		MessageBox(0, (char*) errors->GetBufferPointer(),0,0);
	}

	hWorldMat = hViewMat = hProjMat = NULL;
	// associate the external variable in the effect file with variable in the C++ program
	hWorldViewProjMat = pSkyEffect->GetParameterByName(NULL, "gWorldViewProjMat");

	// associate the external variable in the effect file with variable in the C++ program
	hWorldMat = pSkyEffect->GetParameterByName(NULL, "gWorldMat");

	// associate the external variable in the effect file with variable in the C++ program
	hViewMat = pSkyEffect->GetParameterByName(NULL, "gViewMat");

	// associate the external variable in the effect file with variable in the C++ program
	hProjMat = pSkyEffect->GetParameterByName(NULL, "gProjMat");

	
		// associate the technique in the effect file with the C++ program
	hTechnique = pSkyEffect->GetTechniqueByName(techniqueName[0]);

	hTexture = pSkyEffect->GetParameterByName(NULL, "skyBox");
	
	D3DXCreateCubeTextureFromFile( md3dDev, "SkyBoxTexture.dds" ,&skyBoxTexture);
	
	pSkyEffect-> SetTexture(hTexture, skyBoxTexture);


	// first associate it by name and store it in a handle.  The handles can be class variables (static)


	if (errors != NULL) {
		errors->Release();
	}
	return 0;
}

int SkyBox::initShaderGeom(void){

	geomPoints[0].pos.x = 1.0;
	geomPoints[0].pos.y = 1.0f;
	geomPoints[0].pos.z = -1.0f;
	geomPoints[0].tex0.x = 1.0f;
	geomPoints[0].tex0.y = 1.0f;

	geomPoints[1].pos.x = -1.0f;
	geomPoints[1].pos.y = 1.0f;
	geomPoints[1].pos.z = -1.0f;
	geomPoints[1].tex0.x = 0.0f;
	geomPoints[1].tex0.y = 1.0f;

	geomPoints[2].pos.x = -1.0f;
	geomPoints[2].pos.y = -1.0f;
	geomPoints[2].pos.z = -1.0f;
	geomPoints[2].tex0.x = 0.0f;
	geomPoints[2].tex0.y = 0.0f;

	geomPoints[3].pos.x = 1.0f;
	geomPoints[3].pos.y = -1.0f;
	geomPoints[3].pos.z = -1.0f;
	geomPoints[3].tex0.x = 1.0f;
	geomPoints[3].tex0.y = 0.0f;		//back face

	geomPoints[4].pos.x = 1.0f;
	geomPoints[4].pos.y = 1.0f;
	geomPoints[4].pos.z = 1.0f;
	geomPoints[4].tex0.x = 0.0f;
	geomPoints[4].tex0.y = 1.0f;

	geomPoints[5].pos.x = -1.0f;
	geomPoints[5].pos.y = 1.0f;
	geomPoints[5].pos.z = 1.0f;
	geomPoints[5].tex0.x = 1.0f;
	geomPoints[5].tex0.y = 1.0f;

	geomPoints[6].pos.x = -1.0f;
	geomPoints[6].pos.y = -1.0f;
	geomPoints[6].pos.z = 1.0f;
	geomPoints[6].tex0.x = 0.0f;
	geomPoints[6].tex0.y = 0.0f;

	geomPoints[7].pos.x = 1.0f;
	geomPoints[7].pos.y = -1.0f;
	geomPoints[7].pos.z = 1.0f;
	geomPoints[7].tex0.x = 1.0f;
	geomPoints[7].tex0.y = 0.0f;		//front face

	num_of_skybox_vtx=8;
	for(int i = 0; i < num_of_skybox_vtx; ++i){
		geomPoints[i].color = D3DCOLOR_XRGB(255, 255, 255);
	}

	indexGeom[0] = 0;
	indexGeom[1] = 1;
	indexGeom[2] = 3;
	indexGeom[3] = 1;
	indexGeom[4] = 2;
	indexGeom[5] = 3; //face 1: bottom

	indexGeom[6] = 0;
	indexGeom[7] = 4;
	indexGeom[8] = 1;
	indexGeom[9] = 1;
	indexGeom[10] = 4;
	indexGeom[11] = 5; //face 2: back

	indexGeom[12] = 5;
	indexGeom[13] = 4;
	indexGeom[14] = 7;
	indexGeom[15] = 5;
	indexGeom[16] = 7;
	indexGeom[17] = 6; //face 3: right

	indexGeom[18] = 1;
	indexGeom[19] = 6;
	indexGeom[20] = 5;
	indexGeom[21] = 1;
	indexGeom[22] = 2;
	indexGeom[23] = 6; //face 4: left

	indexGeom[24] = 0;
	indexGeom[25] = 3;
	indexGeom[26] = 7;
	indexGeom[27] = 0;
	indexGeom[28] = 7;
	indexGeom[29] = 4; //face 5: front

	indexGeom[30] = 2;
	indexGeom[31] = 6;
	indexGeom[32] = 7;
	indexGeom[33] = 2;
	indexGeom[34] = 7;
	indexGeom[35] = 3; //face 6: top	
	
	return 0;
}

int SkyBox::initGeom(void){

	geomPoints[0].pos.x = -0.5f;
	geomPoints[0].pos.y = 0.0f;
	geomPoints[0].pos.z = -0.5f;
	geomPoints[0].tex0.x = 1.0f;
	geomPoints[0].tex0.y = 1.0f;

	geomPoints[1].pos.x = 0.5f;
	geomPoints[1].pos.y = 0.0f;
	geomPoints[1].pos.z = -0.5f;
	geomPoints[1].tex0.x = 0.0f;
	geomPoints[1].tex0.y = 1.0f;

	geomPoints[2].pos.x = 0.5f;
	geomPoints[2].pos.y = 1.0f;
	geomPoints[2].pos.z = -0.5f;
	geomPoints[2].tex0.x = 0.0f;
	geomPoints[2].tex0.y = 0.0f;

	geomPoints[3].pos.x = -0.5f;
	geomPoints[3].pos.y = 1.0f;
	geomPoints[3].pos.z = -0.5f;
	geomPoints[3].tex0.x = 1.0f;
	geomPoints[3].tex0.y = 0.0f;		//back face

	geomPoints[4].pos.x = -0.5f;
	geomPoints[4].pos.y = 0.0f;
	geomPoints[4].pos.z = 0.5f;
	geomPoints[4].tex0.x = 0.0f;
	geomPoints[4].tex0.y = 1.0f;

	geomPoints[5].pos.x = 0.5f;
	geomPoints[5].pos.y = 0.0f;
	geomPoints[5].pos.z = 0.5f;
	geomPoints[5].tex0.x = 1.0f;
	geomPoints[5].tex0.y = 1.0f;

	geomPoints[6].pos.x = -0.5f;
	geomPoints[6].pos.y = 1.0f;
	geomPoints[6].pos.z = 0.5f;
	geomPoints[6].tex0.x = 0.0f;
	geomPoints[6].tex0.y = 0.0f;

	geomPoints[7].pos.x = 0.5f;
	geomPoints[7].pos.y = 1.0f;
	geomPoints[7].pos.z = 0.5f;
	geomPoints[7].tex0.x = 1.0f;
	geomPoints[7].tex0.y = 0.0f;		//front face

	geomPoints[8].pos.x = -0.5f;
	geomPoints[8].pos.y = 1.0f;
	geomPoints[8].pos.z = -0.5f;
	geomPoints[8].tex0.x = 0.0f;
	geomPoints[8].tex0.y = 0.0f;

	geomPoints[9].pos.x = -0.5f;
	geomPoints[9].pos.y = 1.0f;
	geomPoints[9].pos.z = 0.5f;
	geomPoints[9].tex0.x = 1.0f;
	geomPoints[9].tex0.y = 0.0f;		

	geomPoints[10].pos.x = -0.5f;
	geomPoints[10].pos.y = 0.0f;
	geomPoints[10].pos.z = -0.5f;
	geomPoints[10].tex0.x = 0.0f;
	geomPoints[10].tex0.y = 1.0f;

	geomPoints[11].pos.x = -0.5f;
	geomPoints[11].pos.y = 0.0f;
	geomPoints[11].pos.z = 0.5f;
	geomPoints[11].tex0.x = 1.0f;
	geomPoints[11].tex0.y = 1.0f;	//left face

	geomPoints[12].pos.x = 0.5f;
	geomPoints[12].pos.y = 1.0f;
	geomPoints[12].pos.z = 0.5f;
	geomPoints[12].tex0.x = 0.0f;
	geomPoints[12].tex0.y = 0.0f;

	geomPoints[13].pos.x = 0.5f;
	geomPoints[13].pos.y = 1.0f;
	geomPoints[13].pos.z = -0.5f;
	geomPoints[13].tex0.x = 1.0f;
	geomPoints[13].tex0.y = 0.0f;		

	geomPoints[14].pos.x = 0.5f;
	geomPoints[14].pos.y = 0.0f;
	geomPoints[14].pos.z = 0.5f;
	geomPoints[14].tex0.x = 0.0f;
	geomPoints[14].tex0.y = 1.0f;

	geomPoints[15].pos.x = 0.5f;
	geomPoints[15].pos.y = 0.0f;
	geomPoints[15].pos.z = -0.5f;
	geomPoints[15].tex0.x = 1.0f;
	geomPoints[15].tex0.y = 1.0f;	//right face

	geomPoints[16].pos.x = -0.5f;
	geomPoints[16].pos.y = 1.0f;
	geomPoints[16].pos.z = -0.5f;
	geomPoints[16].tex0.x = 0.0f;
	geomPoints[16].tex0.y = 0.0f;

	geomPoints[17].pos.x = -0.5f;
	geomPoints[17].pos.y = 1.0f;
	geomPoints[17].pos.z = 0.5f;
	geomPoints[17].tex0.x = 0.0f;
	geomPoints[17].tex0.y = 1.0f;		

	geomPoints[18].pos.x = 0.5f;
	geomPoints[18].pos.y = 1.0f;
	geomPoints[18].pos.z = -0.5f;
	geomPoints[18].tex0.x = 1.0f;
	geomPoints[18].tex0.y = 0.0f;

	geomPoints[19].pos.x = 0.5f;
	geomPoints[19].pos.y = 1.0f;
	geomPoints[19].pos.z = 0.5f;
	geomPoints[19].tex0.x = 1.0f;
	geomPoints[19].tex0.y = 1.0f;	//top face

	geomPoints[20].pos.x = -0.5f;
	geomPoints[20].pos.y = 0.0f;
	geomPoints[20].pos.z = -0.5f;
	geomPoints[20].tex0.x = 0.0f;
	geomPoints[20].tex0.y = 1.0f;

	geomPoints[21].pos.x = -0.5f;
	geomPoints[21].pos.y = 0.0f;
	geomPoints[21].pos.z = 0.5f;
	geomPoints[21].tex0.x = 0.0f;
	geomPoints[21].tex0.y = 0.0f;		

	geomPoints[22].pos.x = 0.5f;
	geomPoints[22].pos.y = 0.0f;
	geomPoints[22].pos.z = -0.5f;
	geomPoints[22].tex0.x = 1.0f;
	geomPoints[22].tex0.y = 1.0f;

	geomPoints[23].pos.x = 0.5f;
	geomPoints[23].pos.y = 0.0f;
	geomPoints[23].pos.z = 0.5f;
	geomPoints[23].tex0.x = 1.0f;
	geomPoints[23].tex0.y = 0.0f;	//bottom face
	num_of_skybox_vtx=SKYBOX_NUM_VERTICES;
	for(int i = 0; i < num_of_skybox_vtx; ++i){
		geomPoints[i].color = D3DCOLOR_XRGB(255, 255, 255);
	}

	indexGeom[0] = 1;
	indexGeom[1] = 0;
	indexGeom[2] = 5;
	indexGeom[3] = 0;
	indexGeom[4] = 5;
	indexGeom[5] = 4; //face 1: bottom

	indexGeom[6] = 1;
	indexGeom[7] = 0;
	indexGeom[8] = 3;
	indexGeom[9] = 1;
	indexGeom[10] = 3;
	indexGeom[11] = 2; //face 2: back

	indexGeom[12] = 5;
	indexGeom[13] = 7;
	indexGeom[14] = 1;
	indexGeom[15] = 7;
	indexGeom[16] = 1;
	indexGeom[17] = 2; //face 3: right

	indexGeom[18] = 4;
	indexGeom[19] = 6;
	indexGeom[20] = 0;
	indexGeom[21] = 6;
	indexGeom[22] = 0;
	indexGeom[23] = 3; //face 4: left

	indexGeom[24] = 5;
	indexGeom[25] = 4;
	indexGeom[26] = 7;
	indexGeom[27] = 4;
	indexGeom[28] = 7;
	indexGeom[29] = 6; //face 5: front

	indexGeom[30] = 7;
	indexGeom[31] = 6;
	indexGeom[32] = 3;
	indexGeom[33] = 7;
	indexGeom[34] = 3;
	indexGeom[35] = 2; //face 6: top	

	return 0;
}

SkyBox::SKYBOX_VTX* const SkyBox::getPoints(){
	return geomPoints;
}

void SkyBox::setupIndVtxBuffers(){
	struct SKYBOX_VTX *v = NULL;
	long *ind = NULL;
	if( SHADERS_ON){
	if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( num_of_skybox_vtx*sizeof(struct SKYBOX_VTX), D3DUSAGE_WRITEONLY, NULL, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	
	}}else{
	if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( num_of_skybox_vtx*sizeof(struct SKYBOX_VTX), D3DUSAGE_WRITEONLY, SKYBOX_CUSTOMFVF, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	}
	}
	if(mIndBuf == NULL){
		md3dDev->CreateIndexBuffer( SKYBOX_NUM_INDICES*sizeof(long), D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &this->mIndBuf, 0);
	}

    mVtxBuf->Lock(0, 0, (void**)&v, 0);    // lock the vertex buffer
	memcpy(v, geomPoints, sizeof(SKYBOX_VTX)*num_of_skybox_vtx);    // copy the vertices to the locked buffer
    mVtxBuf->Unlock();    // unlock the vertex buffer

	mIndBuf->Lock(0, 0, (void**)&ind, 0);
	memcpy(ind, indexGeom, sizeof(indexGeom));
	mIndBuf->Unlock();
	}

void SkyBox::setCameraLifted(bool cameraIsLifted){
	cameraLifted = cameraIsLifted;
	return;
}

/*
 * Thanks to http://www.toymaker.info/Games/html/skybox.html for help with this.
 */
int SkyBox::render(int time){
	if (SHADERS_ON)
		return shaderRender(time);
	else
		return standardRender(time);


}
int SkyBox::standardRender(int time){
	int rc;
	D3DXMATRIX worldMat, worldMatSave, matScale, matView, matViewSave, matTranslate, matRot;
	
	D3DXMatrixIdentity(&worldMat);

	if(!cameraLifted){
		D3DXMatrixScaling(&matScale, mScaleX, mScaleY, mScaleZ);
	} else {
		D3DXMatrixScaling(&matScale, obsScaleX, (obsScaleX + obsScaleZ)/4.0f, obsScaleZ);
	}
	worldMat *= matScale;

	md3dDev->GetTransform(D3DTS_WORLD, &worldMatSave);

	md3dDev->GetTransform(D3DTS_VIEW, &matViewSave);
	matView = matViewSave;
	matView._41 = 0.0f; 
	matView._43 = 0.0f;

	if(!cameraLifted){
		matView._42 = 0.0f; 
	} else {
		D3DXMatrixTranslation(&matTranslate, 0, -OBS_CAMERA_HEIGHT/2, 0);
		worldMat *= matTranslate;
		matView._42 = 0.0f; 
	}

	md3dDev->SetTransform(D3DTS_VIEW, &matView);
//	worldMat *= matView;

	md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

	// set the source
	rc = md3dDev->SetFVF(SKYBOX_CUSTOMFVF);

	// set the index buffer
	rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(SkyBox::SKYBOX_VTX));
	rc = md3dDev->SetIndices(mIndBuf);

	rc = md3dDev->SetTexture(0, floorTexture);
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_of_skybox_vtx, 0, 2); 

	rc = md3dDev->SetTexture(0, wallTexture);
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_of_skybox_vtx, 6, 8); 

	rc = md3dDev->SetTexture(0, skyTexture);
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_of_skybox_vtx, 30, 2); 

	md3dDev->SetTransform(D3DTS_WORLD, &worldMatSave);
	md3dDev->SetTransform(D3DTS_VIEW, &matViewSave);

	return rc;
}

int SkyBox::shaderRender(int time){
	int rc;

	float fTime;
	int counter = 0;

	unsigned int i=0, j=0, k=0;
	unsigned int numPasses = 0;

	D3DXMATRIX worldMat, worldMatSave, matScale, matView, matViewSave, matTranslate, matRot;
	
	D3DXMatrixIdentity(&worldMat);
	if(!cameraLifted){
		D3DXMatrixScaling(&matScale, mScaleX, mScaleY, mScaleZ);
	} else {
		D3DXMatrixScaling(&matScale, obsScaleX, (obsScaleX + obsScaleZ)/4.0f, obsScaleZ);
	}	worldMat *= matScale;

	matView = viewMat;
	matView._41 = 0.0f; 
	matView._43 = 0.0f;

	if(!cameraLifted){
		matView._42 = 0.0f; 
	} else {
		D3DXMatrixTranslation(&matTranslate, 0, -OBS_CAMERA_HEIGHT/2, 0);
		worldMat *= matTranslate;
		matView._42 = 0.0f; 
	}

	
	rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(SkyBox::SKYBOX_VTX));
	rc = md3dDev->SetVertexDeclaration(SkyDeclaration);
	rc = md3dDev->SetIndices(mIndBuf);

	pSkyEffect->SetTechnique(hTechnique);

	pSkyEffect->SetMatrix(hWorldViewProjMat, &(matView*projMat));
	pSkyEffect->SetMatrix(hWorldMat, &worldMat);
	pSkyEffect->SetMatrix(hViewMat, &matView);
	pSkyEffect->SetMatrix(hProjMat, &projMat);
	







	pSkyEffect->Begin(&numPasses, 0);		// 0 means save and restore the device states

	// start the technique passess
	
	for (i = 0; i < numPasses; i++) {
		pSkyEffect->BeginPass(i);

		
		rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_of_skybox_vtx, 0, SKYBOX_NUM_INDICES/3); 

		pSkyEffect->EndPass();
	}
	pSkyEffect->End();


	return rc;
}

void SkyBox::setMazeBounds(float xSpan, float zSpan){
	obsScaleX = xSpan;
	obsScaleZ = zSpan;
	return;
}

int SkyBox::setViewMat(D3DXMATRIX * viewMat)
{
	SkyBox::viewMat = *viewMat;
	return 0;
}


int SkyBox::setProjMat(D3DXMATRIX * projMat)
{
	SkyBox::projMat = *projMat;
	return 0;
}
