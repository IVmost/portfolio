#include "Door.h"

LPDIRECT3DTEXTURE9 Door::doorTexture = NULL;

Door::Door(D3DXVECTOR3 pos): Wall(pos)
{
	if(doorTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, DOOR_TEXTURE_FILE, &doorTexture)){
			std::cerr<<"Couldn't open door texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}
	initGeom();
}

Door::Door(float x, float y, float z): Wall(x, y, z)
{
	if(doorTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, DOOR_TEXTURE_FILE, &doorTexture)){
			std::cerr<<"Couldn't open door texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}
	initGeom();
}


Door::~Door(void)
{
	if(doorTexture != NULL){
		doorTexture->Release();
		doorTexture = NULL;
	}
}

int Door::initGeom(void){
	Wall::initGeom();
	for(int i = 0; i < 8; ++i){
		geomPoints[i].color = D3DCOLOR_XRGB(172, 107, 72);
	}

	return 0;
}

int Door::render(int time){

	if (SHADERS_ON)
		return shaderRender(time);
	else
		return standardRender(time);

}

int Door::standardRender(int time){
	int rc;
	D3DXMATRIX worldMat = getTransformationMatrix();
	
	md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

	// set the source
	rc = md3dDev->SetFVF(CUSTOMFVF);

	// set the index buffer
	rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(Wall::WALL_VTX));
	rc = md3dDev->SetIndices(mIndBuf);


	rc = md3dDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
	rc = md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	IDirect3DBaseTexture9 *tempTexture = NULL;
	//Grab the texture to restore.
	rc = md3dDev->GetTexture(0, &tempTexture);
	rc = md3dDev->SetTexture(0, doorTexture);
	// draw the cube
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, WALL_NUM_VERTICES, 0, WALL_NUM_INDICES/3); 
	if(tempTexture != NULL){
		rc = md3dDev->SetTexture(0, tempTexture);
	}

	return rc;
}

int Door::shaderRender(int time){

	int rc;
  

	int counter = 0;

	unsigned int i=0, j=0, k=0;
	unsigned int numPasses = 0;		// containes the number of passes of the technique

	D3DXMATRIX worldMat, matTransform, matProjection, matScale, matTranslate,  matRotation;


	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;

	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;


	// set the source
	rc = md3dDev->SetStreamSource( 0, mVtxBuf, 0, sizeof(WALL_VTX) );
	rc = md3dDev->SetVertexDeclaration(wallDeclaration);
	// set the index buffer
	md3dDev->SetIndices(mIndBuf);


	// set the technique
	rc = pWallEffect->SetTechnique(hTechnique);

	// set the global matrix
	pWallEffect->SetMatrix(hWorldViewProjMat, &(worldMat*viewMat*projMat));
	pWallEffect->SetMatrix(hWorldMat, &worldMat);
	pWallEffect->SetMatrix(hViewMat, &viewMat);
	pWallEffect->SetMatrix(hProjMat, &projMat);
	pWallEffect->SetFloat(hAmbientPower, ambientPower);
	pWallEffect-> SetTexture(hTexture, doorTexture);

	
	

	// start the effect
	pWallEffect->Begin(&numPasses, 0);		// 0 means save and restore the device states

	// start the technique passess
	for (i = 0; i < numPasses; i++) {
		pWallEffect->BeginPass(i);

		// draw the mesh
		md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, WALL_NUM_VERTICES, 0, WALL_NUM_INDICES/3);

		pWallEffect->EndPass();
	}

	pWallEffect->End();

   
	return 0;
}