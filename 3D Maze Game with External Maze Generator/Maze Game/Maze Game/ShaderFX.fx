
// transformation matrix from the program
uniform shared extern float4x4 gWorldMat;
uniform shared extern float4x4 gViewMat;
uniform shared extern float4x4 gProjMat;
uniform extern float4x4 gWorldViewProjMat;
uniform extern texture gWorldTex;
uniform extern texture gBumpTex;
uniform shared extern float3 gLookAt;
uniform extern float gAmbientPower;

sampler BumpWorldS = sampler_state
{
	Texture = <gBumpTex>;		// the texture
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    AddressU = WRAP;
    AddressV = MIRROR;
};

sampler TexWorldS = sampler_state
{
	Texture = <gWorldTex>;		// the texture
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
    AddressU = WRAP;
    AddressV = MIRROR;
};

struct inputVS {
    float3 pos : POSITION0;
    float4 col : COLOR0;
    float2 tex0: TEXCOORD0;
};



struct OutputVS {
    float4 pos : POSITION0;
    float4 col : COLOR0;
	float2 tex0: TEXCOORD0;
};

//Skybox variables used to build ther skybox in shaders
//Thanks to http://knol.google.com/k/3d-skybox# for the excellent tutorial
uniform extern texture skyBox;

samplerCUBE skySampler = sampler_state
{
	texture = <skyBox>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipFilter = LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;
	AddressW = MIRROR;
};

struct VS_INPUT_SKY{
	float3 pos : POSITION0;
};

struct VS_OUTPUT_SKY{
	float4 pos: POSITION0;
	float3 tex0: TEXCOORD0;
};
/*************************************************************************************/




/*************************************************************************************/


// vertex shader
struct OutputVS TransformationEffect(inputVS vin)
{	
	float4 ambientLight = {1.0,1.0,1.0,1.0};
	// initalize the output
	struct OutputVS vout = {{0.0,0.0,0.0,0.0},{0.0,0.0,0.0,0.0},{0.0,0.0}};
	
	
	// transform the vertex
	vout.pos = mul(float4(vin.pos,1.0f),gWorldMat);
	vout.pos = mul(vout.pos,gViewMat);
	vout.pos = mul(vout.pos,gProjMat);
	
	vout.tex0=vin.tex0;	
		
		
	vout.col = vin.col *gAmbientPower*ambientLight;
	
	
	return(vout);
	
}
float2 BumpMap(float2 texCoord){

	struct OutputVS vout = {{0.0,0.0,0.0,0.0},{0.0,0.0,0.0,0.0},{0.0,0.0}};

	float3 vEye= -gLookAt;
	float fBumpScale = 0.025f;
	float2 vCoord = texCoord;

	float fDepth = tex2D(BumpWorldS, vCoord).w;
	float2 vHalfOffset = normalize(vEye).xy* (fDepth)* fBumpScale;
		
	fDepth = (fDepth + tex2D(BumpWorldS, vCoord + vHalfOffset).x)*0.5;
	vHalfOffset = normalize(vEye).xy * (fDepth) * fBumpScale;

	fDepth = (fDepth + tex2D(BumpWorldS, vCoord + vHalfOffset).x) * 0.5;
	vHalfOffset = normalize(vEye).xy * (fDepth) * fBumpScale;
	
	return vCoord+vHalfOffset;
	
}

struct VS_OUTPUT_SKY SkyBoxVS(VS_INPUT_SKY vin){

	float ambientPower = 0.5;
	
	VS_OUTPUT_SKY vout= (VS_OUTPUT_SKY) 0;
	float4 hPos = float4(vin.pos, 0);

	vout.pos = mul (hPos, gWorldMat);
	vout.pos = mul(vout.pos, gWorldViewProjMat).xyww;
	
	vout.tex0= vin.pos.xzy;
	return vout;
}


/**************************************************************************************/

// Pixel Shader
//float4 TransformFunPS(float4 col : COLOR0) : COLOR


float4 TransformFunPS(struct OutputVS v) : COLOR
{
	v.tex0= BumpMap(v.tex0);
	v.tex0= BumpMap(v.tex0);
	v.tex0= BumpMap(v.tex0);

	// add code to access the texture
	float4 col = {0.0,0.0,0.4,1.0};	
	col.a = v.col.a;
	col= tex2D(TexWorldS,v.tex0);
	col.rgb= col.rgb*v.col.rgb;
	// add code to access the texture
	return(col);
}

float4 KeyPS(struct OutputVS v) : COLOR
{
	

	// add code to access the texture
	float4 col = {1.0,0.87,0.0,1.0};	
	col.a = v.col.a;
	col.rgb=v.col.rgb;
	// add code to access the texture
	return(col);
}



float4 SkyBoxPS(VS_OUTPUT_SKY vin) : COLOR
{
	float ambientPower = 0.5;
	float4 ambientLight = {1.0,1.0,1.0,1.0};
	
	return texCUBE(skySampler, vin.tex0)*ambientPower*ambientLight;
}


technique BumpMapEffect
{

	pass first {
		ZENABLE = TRUE;
        ZWRITEENABLE = TRUE;
        //CULLMODE = CCW;
		FillMode=solid;
		vertexshader = compile vs_3_0 TransformationEffect();

		pixelshader = compile ps_3_0 TransformFunPS();

		
	}
}

technique KeyEffect
{

	pass first {
		vertexshader = compile vs_3_0 TransformationEffect();

		pixelshader = compile ps_3_0 KeyPS();

		FillMode=WireFrame;
		FillMode = solid;
		ZENABLE = TRUE;
        ZWRITEENABLE = TRUE;
        //CULLMODE = CCW;

	}
}


technique SkyEffect
{

	pass first {
		vertexshader = compile vs_3_0 SkyBoxVS();

		pixelshader = compile ps_3_0 SkyBoxPS();

		FillMode=WireFrame;
		FillMode = solid;
		ZENABLE = FALSE;
        ZWRITEENABLE = FALSE;
		//CULLMODE =CW;
	}
}
