#pragma once
#include "CommonHeaders.h"


class Camera
{
public:
	Camera(void);
	~Camera(void);
	int roll(float angleDeg);
	int pitch(float angleDeg);
	int yaw(float angleDeg);
	D3DXVECTOR3 getPosition(void);
	D3DXVECTOR3 getLookAtPoint(void);
	D3DXVECTOR3 getUpVector(void);
	float getRoll();
	float getPitch();
	float getYaw();
	int changePoitionDelta(float dx, float dy, float dz);	// change position by relative amount
	int changePositionDelta(D3DXVECTOR3 *dv);				// change position by relative amount
	int changeAbsPoition(float x, float y, float z);		// change to a new position in space
	int changeAbsPosition(D3DXVECTOR3 *v);					// change to a new position in space
	D3DXVECTOR3 moveForward(float numUnits);  // moves the Camera forward by the numUnits units along the lookAtVector
	void setCamera(D3DXVECTOR3 position, D3DXVECTOR3 lookAtPoint, D3DXVECTOR3 upVector);
	int updateSpeed(float speed);
	float getSpeed(void);
	//new for getting variables
	D3DXVECTOR3 getSpeedVector();
	D3DXVECTOR3 getLookAtVector(); 

private:
	int updateOrientation(D3DXVECTOR3 rotVector, float angleRad); // update the Camera's orientation in space

public:
	D3DXMATRIX rotMat;
	D3DXVECTOR3 position;
	D3DXVECTOR3 upVector;
	D3DXVECTOR3 lookAtVector;
	D3DMATRIX * getViewMatrix(D3DXMATRIX * viewMatrix);
	float speed;
	// change the speed of the Camera motion
};
