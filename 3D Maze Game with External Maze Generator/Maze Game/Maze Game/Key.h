#pragma once
#include "gameobject.h"
/*
 * Note: thanks to directxtutorial.com for the examples.
 */
//TODO we probably want to change this later to use textures which will probably require some changes
#define KEY_CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE)
#define NUM_VTX_INDICES 216
#define NUM_VERTICES 42
                   //60 degrees
#define Z_ROTATION D3DX_PI/3
					//3 degrees
#define Y_ROTATION D3DX_PI/30
#define DEFAULT_SCALE 0.75f

class Key :
	public GameObject
{
public:
	struct KEY_VTX{
		D3DXVECTOR3 pos;
		DWORD color;
	};

	Key(D3DXVECTOR3 pos);
	Key(float x, float y, float z);
	~Key(void);
	virtual int initGeom(void);
	KEY_VTX* const getPoints();
	void setupIndVtxBuffers();
	virtual int render(int time);


	int initEffect(void);
	int standardRender(int time);
	int shaderRender(int time);

	static ID3DXEffect *pKeyEffect;

	static D3DXHANDLE hWorldViewProjMat;
	static D3DXHANDLE hHighWaves;
	static D3DXHANDLE hTechnique;
	static D3DXHANDLE hWorldMat;
	static D3DXHANDLE hViewMat;
	static D3DXHANDLE hProjMat;
	static D3DXHANDLE hAmbientPower;

	static IDirect3DVertexDeclaration9* keyDeclaration;

private:
	long indexGeom[NUM_VTX_INDICES];
	KEY_VTX geomPoints[NUM_VERTICES];
};

