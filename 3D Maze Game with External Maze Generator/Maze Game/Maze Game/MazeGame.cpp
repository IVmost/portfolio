#include "CommonHeaders.h"
#include "MyGame.h"

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   PSTR		 pCmdLine,
                   int		 nCmdShow)
{
	//thanks to http://msdn.microsoft.com/en-us/library/x98tx3cf%28v=vs.80%29.aspx for the memory leak code.
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR , _CRTDBG_MODE_WNDW );
	_CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_WNDW );
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_WNDW );

	// create the game
	MyGame game(hInstance, "aMAZEing Game");

	// initialize the game
	// note that this is done via a virtual function.  Thus there is no need to update this function
	do{
		game.initGame();
		if(game.gameLoop() != RESTART_RETURN_CODE){
			//I don't think we should ever get here...
			break;
		}
	}while(game.resetGame() == 1);

	return 0;
}



