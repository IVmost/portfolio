#include "Labyrinth.h"

IDirect3DVertexDeclaration9* Labyrinth::floorDecl = NULL;

/******************************************************************/
/*
Purpose: floor constructor


Descripton: 

Return:

*/

Labyrinth::Labyrinth(void)
: numRows(0),
  numCols(0),
  pMyKey(NULL),
  pMyPlayer(NULL),
  mazeObjectsArray(NULL),
  levelTimeLimit(0)
{
	labParser.parseFile(&mazeObjectsArray, &pMyPlayer, &pMyKey, &numRows, &numCols, &levelTimeLimit);
	if(pMyPlayer == NULL){
		std::cerr<<"We couldn't find the player after parsing. Oops! Exiting..."<<std::endl;
		exit(-1);
	}
	if(pMyKey == NULL){
		std::cerr<<"We couldn't find the key after parsing. Oops! Exiting..."<<std::endl;
		exit(-1);
	}
	QueryPerformanceCounter( (LARGE_INTEGER *)&startTime );
	timePassed = 0;
	//We'll probably need something like this if we move to shaders, so I'll leave it in but not called
	createVtxDescription();
}

/******************************************************************/
/*
Purpose: floor constructor


Descripton: 
Note that the y axis represents the height and the floor spans the x-z axis.

Return:

*/
Labyrinth::Labyrinth(float dx, float dz) 
: numRows(0),
  numCols(0),
  pMyKey(NULL),
  pMyPlayer(NULL),
  mazeObjectsArray(NULL),
  levelTimeLimit(0)
{
	labParser.parseFile(&mazeObjectsArray, &pMyPlayer, &pMyKey, &numRows, &numCols, &levelTimeLimit);
	if(pMyPlayer == NULL){
		std::cerr<<"We couldn't find the player after parsing. Oops! Exiting..."<<std::endl;
		exit(-1);
	}
	if(pMyKey == NULL){
		std::cerr<<"We couldn't find the key after parsing. Oops! Exiting..."<<std::endl;
		exit(-1);
	}
	setScale(1.0, 1.0, 1.0);
	//We'll probably need something like this if we move to shaders, so I'll leave it in but not called
	createVtxDescription();
	timePassed = 0;
	QueryPerformanceCounter( (LARGE_INTEGER *)&startTime );
	QueryPerformanceFrequency( (LARGE_INTEGER *)&ticksPerSecond );
	createSurface(numRows, numCols, dx, dz);
}


/******************************************************************/
/*
Purpose: floor destructor


Descripton: releases all allocated memory and com objects

Return:

*/

Labyrinth::~Labyrinth(void)
{
	// release the com objects
	COM_RELEASE(this->floorDecl);
	COM_RELEASE(this->mVtxBuf);
	COM_RELEASE(this->mIndBuf);

	if(NULL != mazeObjectsArray){
		for(int i = 0; i < numRows; ++i){
			for(int j = 0; j < numCols; ++j){
				if(NULL != mazeObjectsArray[i][j]){
					delete[](mazeObjectsArray[i][j]);
				}
			}
			delete[]mazeObjectsArray[i];
		}
		delete[](mazeObjectsArray);
	}
	delete(pMyPlayer);
	delete(pMyKey);
}

void Labyrinth::setupIndVtxBuffers(){
	createSurface(numRows, numCols, dx, dz);
	return;
}


/******************************************************************/
/*
Purpose: generates the surface - the vertices and the quads

Descripton: Calls setupIndVtxBuffers() on our maze objects

Return: Meant to be a return code but at the moment there's nothing necessitating it, so it's 0.

*/


int Labyrinth::createSurface(int numRows, int numCols, float dx, float dz)
{	
	int rc = 0;
	for(int g = 0; g < numRows; ++g){
		for(int h = 0; h < numCols; ++h){
			mazeObjectsArray[g][h]->setupIndVtxBuffers();
		}
	}
	
	if(NULL != pMyKey){
		pMyKey->setupIndVtxBuffers();
	}
	int keyX;
	int keyZ;
	pMyKey->getGridIndices(&keyX,&keyZ);
	int gLow=0;
	int gHigh=numRows;
	int hLow=0;
	int hHigh=numCols;
	if (keyX-3 > 0)
		gLow=keyX-3;
	if (keyX+3 < numRows)
		gHigh=keyX+3;
	if (keyZ-3 > 0)
		hLow=keyZ-3;
	if (keyZ+3 < numCols)
		hHigh=keyZ+3;
	for (int g = gLow; g < gHigh; ++g)
		for (int h=hLow; h < hHigh; ++h){
			float newPower= (1.0f-((abs(g-keyX)*abs(h-keyZ)/9.0f)))/2.0f+0.5f;
			mazeObjectsArray[g][h]->setAmbientPower(newPower);
		}

	return(rc);
}

/******************************************************************/
/*
Purpose: creates the vertex description interface


Descripton: 

Return:
0 if success
*/
int Labyrinth::createVtxDescription(void)
{
	int rc = 0;
	// create the vertex declaration
	if (this->floorDecl == NULL) {
		struct floorVertex v;
		D3DVERTEXELEMENT9 decl[] = 
		{{0,0,D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		 {0,0,D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		D3DDECL_END()};
		decl[0].Offset = (char *) &v.pos - (char *) &v;
		decl[1].Offset = (char *) &v.color - (char *) &v;
		rc = md3dDev->CreateVertexDeclaration(decl, &floorDecl);
	}
	return 0;
}

/******************************************************************/
/*
Purpose: change the position of the surface.


Descripton: set the position of the surfce. The surface should be 
translated so the the vertex at (0,?,0) is moved to 
Return:

*/
int Labyrinth::setCentrePosition(D3DXVECTOR3 centrePos)
{
	// to be udpated 
	this->mPosition = centrePos;
	return 0;
}


/******************************************************************/
/*
Purpose: renders the surface


Descripton: 


Return:

*/
int Labyrinth::render(int time)
{

	INT64 tempTime;
	QueryPerformanceCounter( (LARGE_INTEGER *)&tempTime );
	QueryPerformanceFrequency( (LARGE_INTEGER *)&ticksPerSecond );
	timePassed+=double(tempTime-startTime)/(double(ticksPerSecond));
	startTime=tempTime;
	int rc;
    static int angle = 0;

	static float step = 1;
	float rad = 0;

	static int i=0, j=0, k=0;

	D3DXMATRIX worldMat, viewMat, matTransform, matProjection, matScale, matTranslate,  matRotation;

	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;

	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;


	//D3DXMatrixIdentity(&worldMat);
	md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

	// set the source
	rc = md3dDev->SetStreamSource( 0, mVtxBuf, 0, sizeof(floorVertex) );
	if (SHADERS_ON){
		rc = md3dDev->SetVertexDeclaration(floorDecl);
	}else{
		rc = md3dDev->SetFVF(CUSTOMFVF);
	}

	// set the index buffer
	if (!SHADERS_ON){
		md3dDev->SetIndices(mIndBuf);
	
		md3dDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
		md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	}
	if (SHADERS_ON)
		Wall::preRender();
	int playerX;
	int playerZ;
	pMyPlayer->getGridIndices(&playerX,&playerZ);
	D3DXVECTOR3 temp= pMyPlayer->getCamera()->getPosition();
	float xdirection=temp.x;
	float zdirection=temp.z;
	int gLow=0;
	int gHigh=numRows;
	int hLow=0;
	int hHigh=numCols;
	if (temp.y < OBS_CAMERA_HEIGHT){
		if (playerX-15 > 0)
			gLow=playerX-15;
		if (playerX+15 < numRows)
			gHigh=playerX+15;
		if (playerZ-15 > 0)
			hLow=playerZ-15;
		if (playerZ+15 < numCols)
			hHigh=playerZ+15;
	}
	for(int g = gLow; g < gHigh; ++g){
		for(int h = hLow; h < hHigh; ++h){
			mazeObjectsArray[g][h]->render(time);
		}
	}

	if(NULL != pMyPlayer){
		if(!pMyPlayer->foundKey()){
			if(NULL != pMyKey){
				pMyKey->render(time);
			}
		}
	}
   
	return 0;
}



/******************************************************************/
/*
Purpose: sets the surface scaling factor


Descripton: set the position of hte surface.  The surface should be 
translated so the the vertex at (0,?,0) is moved to 
Return:

*/
int Labyrinth::setScale(float scaleX, float scaleY, float scaleZ)
{
	this->mScaleX = scaleX;
	this->mScaleY = scaleY;
	this->mScaleZ = scaleZ;
	return 0;
}

Player* const Labyrinth::getPlayer(){
	return pMyPlayer;
}

Key* const Labyrinth::getKey(){
	return pMyKey;
}

#if !SUPERMAN_MODE
//Thanks to this site for the help with lists http://www.cplusplus.com/reference/stl/list
bool Labyrinth::movePlayerBackward(){
	//First we check for wall collisions, then key, door and "observation area" collisions
	list<grid_Coordinate> collisions;
	D3DXVECTOR3 nextPlayerPos = pMyPlayer->testMoveBackward();
	checkPlayerWallCollisions(&collisions, nextPlayerPos);
	bool sawDoor = false;

	if(collisions.empty()){
		pMyPlayer->moveBackward();
	} else {
		//Figure out where he's looking and figure out if he can move this way in either x or z
		D3DXVECTOR3 playerDir = pMyPlayer->getDirection();
		int playerGridX, playerGridZ;
		pMyPlayer->getGridIndices(&playerGridX, &playerGridZ);
		bool moveX = true, moveZ = true;
		Door* dynamicDoor = NULL;

		//The collision list tells me which cells I CAN'T move to. So remove that direction from his movement. It could end up making him immobile in that direction.
		if(pMyPlayer->isUpsideDown()){
			//If he's upside down and trying to move backwards, collisions will happen in the direction he's looking
			if(playerDir.x < 0){
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} /* if(playerDir.z < 0) */
			} else {
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				}/* if(playerDir.z < 0) */
			} /* if(playerDir.x < 0) */
		} else {
			//If he's rightside up and trying to move backwards, collisions will happen in the negative direction of where he's looking
			if(playerDir.x < 0){
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} /* if(playerDir.z < 0) */
			} else {
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				}/* if(playerDir.z < 0) */
			} /* if(playerDir.x < 0) */
		}
		pMyPlayer->moveBackward(moveX, moveZ);
	} /* if(collisions.empty()) */

	
	if(!pMyPlayer->foundKey()){
		checkPlayerKeyCollision();
	}

	if(sawDoor){
		if(pMyPlayer->foundKey()){
			//Game over, player wins
			return true;
		}
	}

	pMyPlayer->setOnObsTile(checkPlayerObsAreaCollision());
	
	return false;
}

bool Labyrinth::movePlayerForward(){
	//First we check for wall collisions, then key, door and "observation area" collisions
	list<grid_Coordinate> collisions;
	D3DXVECTOR3 nextPlayerPos = pMyPlayer->testMoveForward();
	checkPlayerWallCollisions(&collisions, nextPlayerPos);
	bool sawDoor = false;

	if(collisions.empty()){
		pMyPlayer->moveForward();
	} else {
		//Figure out where he's looking and figure out if he can move this way in either x or z
		D3DXVECTOR3 playerDir = pMyPlayer->getDirection();
		int playerGridX, playerGridZ;
		pMyPlayer->getGridIndices(&playerGridX, &playerGridZ);
		bool moveX = true, moveZ = true;
		Door* dynamicDoor = NULL;		

		//The collision list tells me which cells I CAN'T move to. So remove that direction from his movement. It could end up making him immobile in that direction.
		if(pMyPlayer->isUpsideDown()){
			//If he's upside down and trying to move forwards, collisions will happen in the negative direction of where he's looking
			if(playerDir.x < 0){
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} /* if(playerDir.z < 0) */
			} else {
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				}/* if(playerDir.z < 0) */
			} /* if(playerDir.x < 0) */
		} else {
			//If he's rightside up and trying to move forwards, collisions will happen in the direction he's looking
			if(playerDir.x < 0){
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the -x direction and we had a collision, so disable movement there
						if(it->indexX < playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} /* if(playerDir.z < 0) */
			} else {
				if(playerDir.z < 0){
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the -z direction and we had a collision, so disable movement there
						if(it->indexZ < playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				} else {
					for(list<grid_Coordinate>::iterator it = collisions.begin(); it != collisions.end(); ++it){
						//We were trying to move in the +x direction and we had a collision, so disable movement there
						if(it->indexX > playerGridX){
							moveX = false;
						}
						//We were trying to move in the +z direction and we had a collision, so disable movement there
						if(it->indexZ > playerGridZ){
							moveZ = false;
						}
						if(!sawDoor){
							dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[it->indexX][it->indexZ]);
							if(NULL != dynamicDoor){
								sawDoor = true;
							}
						}
					}
				}/* if(playerDir.z < 0) */
			} /* if(playerDir.x < 0) */
		} /* if(pMyPlayer->isUpsideDown()) */
		pMyPlayer->moveForward(moveX, moveZ);
	} /* if(collisions.empty()) */

	
	if(!pMyPlayer->foundKey()){
		checkPlayerKeyCollision();
	}

	if(sawDoor){
		if(pMyPlayer->foundKey()){
			//Game Over, player wins
			return true;
		}
	}

	pMyPlayer->setOnObsTile(checkPlayerObsAreaCollision());


	return false;
}
#endif

//We check the 8 positions forming a square around the player's next move, if they exist in the grid.
//Since we are only moving in 2 dimensions, we only have to check flat shapes.
//Thanks to http://www.programmingforums.org/post168124.html, http://people.richland.edu/james/lecture/m116/matrices/area.html and http://en.wikipedia.org/wiki/Determinant#Applications.
//Thanks to thise site for help with dynamic casting http://www.cplusplus.com/doc/tutorial/typecasting/.
void Labyrinth::checkPlayerWallCollisions(list<grid_Coordinate>* collisionCoords, D3DXVECTOR3 nextPlayerPos){
	grid_Coordinate playerCoords = {(int)(nextPlayerPos.x / GRID_UNIT_LENGTH), (int)(nextPlayerPos.z / GRID_UNIT_WIDTH)};
	if(!collisionCoords->empty()){
		collisionCoords->clear();
	}
	/**
	 * The coordinates around the player's next move are as follows:
	 *
	 * [playerCoords.indexX -1, playerCoords.indexZ - 1]  [playerCoords.indexX -1, playerCoords.indexZ]  [playerCoords.indexX -1, playerCoords.indexZ + 1]
	 * [playerCoords.indexX, playerCoords.indexZ - 1]     [						Player				  ]	 [playerCoords.indexX, playerCoords.indexZ + 1]
	 * [playerCoords.indexX +1, playerCoords.indexZ - 1]  [playerCoords.indexX +1, playerCoords.indexZ]  [playerCoords.indexX +1, playerCoords.indexZ + 1]
	 */
	D3DXVECTOR3 lbPoint((nextPlayerPos.x + MOVING_OBJ_BOUND_LENGTH), (nextPlayerPos.y), (nextPlayerPos.z - MOVING_OBJ_BOUND_WIDTH)), 
				rbPoint((nextPlayerPos.x + MOVING_OBJ_BOUND_LENGTH), (nextPlayerPos.y), (nextPlayerPos.z + MOVING_OBJ_BOUND_WIDTH)), 
				rtPoint((nextPlayerPos.x - MOVING_OBJ_BOUND_LENGTH), (nextPlayerPos.y), (nextPlayerPos.z + MOVING_OBJ_BOUND_WIDTH)), 
				ltPoint((nextPlayerPos.x - MOVING_OBJ_BOUND_LENGTH), (nextPlayerPos.y), (nextPlayerPos.z - MOVING_OBJ_BOUND_WIDTH));
	for(int i = -1; i < 2; ++i){
		for(int j = -1; j < 2; ++j){
			if(    !((0 == i) && (0 == j))
				&& !((playerCoords.indexX + i) < 0) 
				&& !((playerCoords.indexX + i) >= numRows)
				&& !((playerCoords.indexZ + j) < 0) 
				&& !((playerCoords.indexZ + j) >= numCols)){

				//Only path tiles and walls are stored in the mazeObjectsArray. We'll have to check for the key after.
				D3DXVECTOR3 objPosition = mazeObjectsArray[playerCoords.indexX + i][playerCoords.indexZ + j]->getPosition();
				PathTile* dynamicPath = NULL;
				//Door*	  dynamicDoor = NULL;
				Wall*     dynamicWall = NULL;
				//We only want to check collisions with walls.
				dynamicPath = dynamic_cast<PathTile*>(mazeObjectsArray[playerCoords.indexX + i][playerCoords.indexZ + j]);
				//dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[playerCoords.indexX + i][playerCoords.indexZ + j]);
				dynamicWall = dynamic_cast<Wall*>(mazeObjectsArray[playerCoords.indexX + i][playerCoords.indexZ + j]);

				if((NULL == dynamicPath) 
					//&& (NULL == dynamicDoor)	//we may be able to check the door collision in here. Anyways, with this uncommented, we can move through doors = P
					&& (NULL != dynamicWall)){
					Wall::WALL_VTX* wallPoints = dynamicWall->getPoints();
					D3DXMATRIX wallTransform = dynamicWall->getTransformationMatrix();
					D3DXVECTOR3 tempTransformed[WALL_NUM_VERTICES];

					for(int vtxCounter = 0; vtxCounter < WALL_NUM_VERTICES; ++vtxCounter){
						bool sawPositiveSign = false;
						bool sawNegativeSign = false;
						D3DXVECTOR3 temp(wallPoints[vtxCounter].pos.x, wallPoints[vtxCounter].pos.y, wallPoints[vtxCounter].pos.z);
						D3DXVECTOR4 tempTransform;
						D3DXVec3Transform(&tempTransform, &temp, &wallTransform);
						temp.x = tempTransform.x;
						temp.y = tempTransform.y;
						temp.z = tempTransform.z;
						tempTransformed[vtxCounter] = temp;

						if(0 > checkDeterminantWithPlayer(temp, (lbPoint), (rbPoint))){
							sawNegativeSign = true;
						} else {
							sawPositiveSign = true;
						}
						if(0 > checkDeterminantWithPlayer(temp, (rbPoint), (rtPoint))){
							sawNegativeSign = true;
						} else {
							sawPositiveSign = true;
						}
						if(0 > checkDeterminantWithPlayer(temp, (rtPoint), (ltPoint))){
							sawNegativeSign = true;
						} else {
							sawPositiveSign = true;
						}
						if(0 > checkDeterminantWithPlayer(temp, (ltPoint), (lbPoint))){
							sawNegativeSign = true;
						} else {
							sawPositiveSign = true;
						}

						if(!sawPositiveSign || !sawNegativeSign){  // == if(! (sawPositiveSign && sawNegativeSign) ) 
							//We had a collision according to the aforementioned forum if they all have the same sign
							grid_Coordinate tempToAdd = {playerCoords.indexX + i, playerCoords.indexZ + j};
							collisionCoords->push_back(tempToAdd);
						}

					} /* for(int vtxCounter = 0; vtxCounter < WALL_NUM_VERTICES; ++vtxCounter) */
				} /* if((NULL == dynamicPath) && (NULL == dynamicDoor) && (NULL != dynamicWall)) */
			} /* endif */
		} /* for(int j = -1; j < 2; ++j) */
	} /* for(int i = -1; i < 2; ++i) */

	return;
}

int Labyrinth::checkDeterminantWithPlayer(D3DXVECTOR3 testPoint, D3DXVECTOR3 pointA, D3DXVECTOR3 pointB){
	int result;
	//(x3y2 - x2y3) - (x3y1 - x1y3) + (x2y1 - x1y2) 
	result = (int)(((testPoint.x * pointB.z) - (pointB.x * testPoint.z)) - ((testPoint.x * pointA.z) - (pointA.x * testPoint.z)) + ((pointB.x * pointA.z) - (pointA.x * pointB.z)));
	return result;
}

void Labyrinth::checkPlayerKeyCollision(){
	//  Check if the player's in the key's box. We kind of cheat here in that we don't do very in-depth collision detection.
	//If they find themselves in the same cell, they've collided.
	if(pMyKey != NULL){
		int keyGridX, keyGridZ, playerGridX, playerGridZ;
		pMyKey->getGridIndices(&keyGridX, &keyGridZ);
		pMyPlayer->getGridIndices(&playerGridX, &playerGridZ);

		if(!pMyPlayer->foundKey()){
			//If they are at the same cell, they've found each other
			if((keyGridX == playerGridX) && (keyGridZ == playerGridZ)){
				pMyPlayer->setFoundKey(true);
				setGoalIllumination();
			} 
		} /* if(!pMyPlayer->foundKey()) */
	} /* if(pMyKey != NULL) */
	return;
}

void Labyrinth::setGoalIllumination(){
	int doorX=0;
	int doorZ=0;
	Door* dynamicDoor = NULL;
		for(int g = 0; g < numRows; g++){
			for(int h = 0; h < numCols; h++){
				mazeObjectsArray[g][h]->setAmbientPower(0.5f);
				dynamicDoor = dynamic_cast<Door*>(mazeObjectsArray[g][h]);
				if (dynamicDoor != NULL){
					doorX=g;
					doorZ=h;
				}
			}}
			int gLow=0;
	int gHigh=numRows;
	int hLow=0;
	int hHigh=numCols;
	if (doorX-3 > 0)
		gLow=doorX-3;
	if (doorX+3 < numRows)
		gHigh=doorX+3;
	if (doorZ-3 > 0)
		hLow=doorZ-3;
	if (doorZ+3 < numCols)
		hHigh=doorZ+3;
	for (int g = gLow; g < gHigh; g++)
		for (int h=hLow; h < hHigh; h++){
			float newPower= (1.0f-((abs(g-doorX)*abs(h-doorZ)/9.0f)))/2.0f+0.5f;
			mazeObjectsArray[g][h]->setAmbientPower(newPower);
		}
	
	
}

bool Labyrinth::checkPlayerObsAreaCollision(){
	//Similar to the key collision detection, we're going to cheat here and say they've collided if they're in the same cell
	bool result = false;
	grid_Coordinate playerCoords;
	pMyPlayer->getGridIndices(&(playerCoords.indexX), &(playerCoords.indexZ));

	if(    (playerCoords.indexX >= 0) && (playerCoords.indexX < numRows)
		&& (playerCoords.indexZ >= 0) && (playerCoords.indexZ < numCols)){
		//Note we don't have to check if we're standing on a path tile. As long as he's on the grid he's occupying a cell which has to be a pathTile.
			if(((PathTile*)mazeObjectsArray[playerCoords.indexX][playerCoords.indexZ])->isObservationArea()){
				result = true;
			}
	}

	return result;
}

time_t const Labyrinth::getTimeLimitInMins(){
	return levelTimeLimit;
}

float Labyrinth::getXSpan(){
	return numRows * GRID_UNIT_LENGTH;
}
float Labyrinth::getZSpan(){
	return numCols * GRID_UNIT_WIDTH;
}

/*
 * Figuring out the centre shouldn't be as bad as it sounds.
 * Get the midpoint between [0][0] and [numRows - 1][numCols - 1].
 * The difference should always be positive with the way the parser is setup.
 */
D3DXVECTOR3 Labyrinth::getCentreXZ(){
	D3DXVECTOR3 result(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 firstPos  = mazeObjectsArray[0][0]->getPosition();
	D3DXVECTOR3 secondPos = mazeObjectsArray[numRows - 1][numCols - 1]->getPosition();

	result.x = ((secondPos.x - firstPos.x) / 2.0f) + firstPos.x;
	result.z = ((secondPos.z - firstPos.z) / 2.0f) + firstPos.z;

	return result;
}