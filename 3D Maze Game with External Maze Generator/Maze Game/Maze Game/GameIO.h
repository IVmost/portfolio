#pragma once
#include "CommonHeaders.h"

class GameIO
{

	// FUNCTIONS
public:
	GameIO(HINSTANCE hinstance, HWND hwnd);
	~GameIO(void);
	int poll(void);
	int mouseDX(void);
	int mouseDY(void);
	int mouseDZ(void);
	D3DXVECTOR3 mouseDelta(void);
	int mouseButtonPressed(int button);
	int keyboardPressed(unsigned char key);

private:
	int initKeyboard(HWND hwnd, long CooperativeFlags);
	int initMouse(HWND hwnd, long CooperativeFlags);


	//MEMBERS
private:

	// keyboard members
	IDirectInputDevice8* mKeyboardDev;  // keyboardDev
	char mKeyboardState[256];			// keyboard state

	// mouse members
	IDirectInputDevice8* mMouseDev;  // mouse device
	DIMOUSESTATE mMouseState;		// mouse state

	// direct input interface
	static IDirectInput8* diCOM;
};
