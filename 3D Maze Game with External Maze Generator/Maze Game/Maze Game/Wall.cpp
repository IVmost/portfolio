#include "Wall.h"

IDirect3DVertexDeclaration9* Wall::wallDeclaration = NULL;
LPDIRECT3DTEXTURE9 Wall::brickTexture = NULL;
LPDIRECT3DTEXTURE9 Wall::bumpTexture = NULL;
LPDIRECT3DTEXTURE9 Wall::normalTexture = NULL;

ID3DXEffect* Wall::pWallEffect=NULL;
D3DXHANDLE Wall::hWorldViewProjMat=NULL;
D3DXHANDLE Wall::hHighWaves=NULL;
D3DXHANDLE Wall::hTechnique=NULL;
D3DXHANDLE Wall::hWorldMat=NULL;
D3DXHANDLE Wall::hViewMat=NULL;
D3DXHANDLE Wall::hProjMat=NULL;
D3DXHANDLE Wall::hTexture=NULL;
D3DXHANDLE Wall::hBumpMap=NULL;
D3DXHANDLE Wall::hLookAt=NULL;
D3DXHANDLE Wall::hAmbientPower=NULL;

Wall::Wall(D3DXVECTOR3 pos): GameObject(pos){

	if(md3dDev == NULL){
		std::cerr<<"Didn't have a d3device handle for the walls so we're going to quit..."<<std::endl;
		exit(-1);
	}

	if(brickTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_TEXTURE_FILE, &brickTexture)){
			std::cerr<<"Couldn't open brick texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}

	if(bumpTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_BUMP_FILE, &bumpTexture)){
			std::cerr<<"Couldn't open brick bump map texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}

	if(normalTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_NORMAL_FILE, &normalTexture)){
			std::cerr<<"Couldn't open brick normal map texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}
	
	mScaleX = GRID_UNIT_WIDTH;
	mScaleY = GRID_UNIT_HEIGHT;
	mScaleZ = GRID_UNIT_LENGTH;
	initGeom();
	if (SHADERS_ON)
	if (pWallEffect == NULL){
		initEffect();
	}
}

Wall::Wall(float x, float y, float z): GameObject(x, y, z)
{
	if(md3dDev == NULL){
		std::cerr<<"Didn't have a d3device handle for the walls so we're going to quit..."<<std::endl;
		exit(-1);
	}

	if(brickTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_TEXTURE_FILE, &brickTexture)){
			std::cerr<<"Couldn't open brick texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}

	if(bumpTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_BUMP_FILE, &bumpTexture)){
			std::cerr<<"Couldn't open brick bump map texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}

	if(normalTexture == NULL){
		if(D3D_OK != D3DXCreateTextureFromFile(md3dDev, BRICK_NORMAL_FILE, &normalTexture)){
			std::cerr<<"Couldn't open brick normal map texture so we're going to quit..."<<std::endl;
			exit(-1);
		}
	}

	mScaleX = GRID_UNIT_WIDTH;
	mScaleY = GRID_UNIT_HEIGHT;
	mScaleZ = GRID_UNIT_LENGTH;
	initGeom();
	if (SHADERS_ON)
		if (pWallEffect == NULL){
			initEffect();
		}
}

Wall::~Wall(){

	if(brickTexture != NULL){
		brickTexture->Release();
		brickTexture = NULL;
	}

	if(bumpTexture != NULL){
		bumpTexture->Release();
		bumpTexture = NULL;
	}

	if(normalTexture != NULL){
		normalTexture->Release();
		normalTexture = NULL;
	}

	if(mIndBuf != NULL){
		mIndBuf->Release();
	}
	if(mVtxBuf != NULL){
		mVtxBuf->Release();
	}
	COM_RELEASE(pWallEffect);

}



int Wall::initGeom(void){

	geomPoints[0].pos.x = 0.0f;
	geomPoints[0].pos.y = 0.0f;
	geomPoints[0].pos.z = 0.0f;
	geomPoints[0].tex0.x = 0.0f;
	geomPoints[0].tex0.y = 1.0f;

	geomPoints[1].pos.x = 1.0f;
	geomPoints[1].pos.y = 0.0f;
	geomPoints[1].pos.z = 0.0f;
	geomPoints[1].tex0.x = 1.0f;
	geomPoints[1].tex0.y = 1.0f;

	geomPoints[2].pos.x = 1.0f;
	geomPoints[2].pos.y = 1.0f;
	geomPoints[2].pos.z = 0.0f;
	geomPoints[2].tex0.x = 1.0f;
	geomPoints[2].tex0.y = 0.0f;

	geomPoints[3].pos.x = 0.0f;
	geomPoints[3].pos.y = 1.0f;
	geomPoints[3].pos.z = 0.0f;
	geomPoints[3].tex0.x = 0.0f;
	geomPoints[3].tex0.y = 0.0f;    //front face


	geomPoints[4].pos.x = 0.0f;
	geomPoints[4].pos.y = 0.0f;
	geomPoints[4].pos.z = 1.0f;
	geomPoints[4].tex0.x = 1.0f;
	geomPoints[4].tex0.y = 1.0f;


	geomPoints[5].pos.x = 1.0f;
	geomPoints[5].pos.y = 0.0f;
	geomPoints[5].pos.z = 1.0f;
	geomPoints[5].tex0.x = 0.0f;
	geomPoints[5].tex0.y = 1.0f;


	geomPoints[6].pos.x = 0.0f;
	geomPoints[6].pos.y = 1.0f;
	geomPoints[6].pos.z = 1.0f;
	geomPoints[6].tex0.x = 1.0f;
	geomPoints[6].tex0.y = 0.0f;


	geomPoints[7].pos.x = 1.0f;
	geomPoints[7].pos.y = 1.0f;
	geomPoints[7].pos.z = 1.0f;

	//Add in the extra, bounds helping vertices, halfway between bottom and top corners
	geomPoints[8].pos.x = 0.33f;
	geomPoints[8].pos.y = 0.0f;
	geomPoints[8].pos.z = 0.0f;

	geomPoints[9].pos.x = 0.0f;
	geomPoints[9].pos.y = 0.0f;
	geomPoints[9].pos.z = 0.33f;

	geomPoints[10].pos.x = 0.33f;
	geomPoints[10].pos.y = 0.0f;
	geomPoints[10].pos.z = 1.0f;

	geomPoints[11].pos.x = 61.0f;
	geomPoints[11].pos.y = 0.0f;
	geomPoints[11].pos.z = 0.33f;

	geomPoints[12].pos.x = 0.33f;
	geomPoints[12].pos.y = 1.0f;
	geomPoints[12].pos.z = 0.0f;

	geomPoints[13].pos.x = 0.0f;
	geomPoints[13].pos.y = 1.0f;
	geomPoints[13].pos.z = 0.33f;

	geomPoints[14].pos.x = 0.33f;
	geomPoints[14].pos.y = 1.0f;
	geomPoints[14].pos.z = 1.0f;

	geomPoints[15].pos.x = 1.0f;
	geomPoints[15].pos.y = 1.0f;
	geomPoints[15].pos.z = 0.33f;

	geomPoints[16].pos.x = 0.66f;
	geomPoints[16].pos.y = 0.0f;
	geomPoints[16].pos.z = 0.0f;

	geomPoints[17].pos.x = 0.0f;
	geomPoints[17].pos.y = 0.0f;
	geomPoints[17].pos.z = 0.66f;

	geomPoints[18].pos.x = 0.66f;
	geomPoints[18].pos.y = 0.0f;
	geomPoints[18].pos.z = 1.0f;

	geomPoints[19].pos.x = 1.0f;
	geomPoints[19].pos.y = 0.0f;
	geomPoints[19].pos.z = 0.66f;

	geomPoints[20].pos.x = 0.66f;
	geomPoints[20].pos.y = 1.0f;
	geomPoints[20].pos.z = 0.0f;

	geomPoints[21].pos.x = 0.0f;
	geomPoints[21].pos.y = 1.0f;
	geomPoints[21].pos.z = 0.66f;

	geomPoints[22].pos.x = 0.66f;
	geomPoints[22].pos.y = 1.0f;
	geomPoints[22].pos.z = 1.0f;

	geomPoints[23].pos.x = 1.0f;
	geomPoints[23].pos.y = 1.0f;
	geomPoints[23].pos.z = 0.66f;

	geomPoints[24].pos.x = 0.0f;
 	geomPoints[24].pos.y = 0.0f;
 	geomPoints[24].pos.z = 0.0f;
 
 	geomPoints[25].pos.x = 0.0f;
 	geomPoints[25].pos.y = 0.0f;
 	geomPoints[25].pos.z = 1.0f;
 
 	geomPoints[26].pos.x = 0.0f;
 	geomPoints[26].pos.y = 1.0f;
 	geomPoints[26].pos.z = 1.0f;
 
 	geomPoints[27].pos.x = 0.0f;
 	geomPoints[27].pos.y = 1.0f;
 	geomPoints[27].pos.z = 0.0f;
 
 	geomPoints[28].pos.x = 1.0f;
 	geomPoints[28].pos.y = 0.0f;
 	geomPoints[28].pos.z = 0.0f;
 
 	geomPoints[29].pos.x = 1.0f;
 	geomPoints[29].pos.y = 0.0f;
 	geomPoints[29].pos.z = 1.0f;
 
 	geomPoints[30].pos.x = 1.0f;
 	geomPoints[30].pos.y = 1.0f;
 	geomPoints[30].pos.z = 1.0f;
 
 	geomPoints[31].pos.x = 1.0f;
 	geomPoints[31].pos.y = 1.0f;
 	geomPoints[31].pos.z = 0.0f;
 
 	geomPoints[32].pos.x = 0.0f;
 	geomPoints[32].pos.y = 1.0f;
 	geomPoints[32].pos.z = 0.0f;
 
 	geomPoints[33].pos.x = 1.0f;
 	geomPoints[33].pos.y = 1.0f;
 	geomPoints[33].pos.z = 0.0f;
 
 	geomPoints[34].pos.x = 1.0f;
 	geomPoints[34].pos.y = 1.0f;
 	geomPoints[34].pos.z = 1.0f;
 
 	geomPoints[35].pos.x = 0.0f;
 	geomPoints[35].pos.y = 1.0f;
 	geomPoints[35].pos.z = 1.0f;
 
 	geomPoints[36].pos.x = 0.0f;
 	geomPoints[36].pos.y = 0.0f;
 	geomPoints[36].pos.z = 0.0f;
 
 	geomPoints[37].pos.x = 1.0f;
 	geomPoints[37].pos.y = 0.0f;
 	geomPoints[37].pos.z = 0.0f;
 
 	geomPoints[38].pos.x = 1.0f;
 	geomPoints[38].pos.y = 0.0f;
 	geomPoints[38].pos.z = 1.0f;
 
 	geomPoints[39].pos.x = 0.0f;
 	geomPoints[39].pos.y = 0.0f;
 	geomPoints[39].pos.z = 1.0f;
 	


	geomPoints[7].tex0.x = 0.0f;
	geomPoints[7].tex0.y = 0.0f;    //back face

	//Add in the extra, bounds-helping vertices, halfway between bottom and top corners
	//Hopefully I won't need to make extras of these as well to satisfy the texture rendering (to make it look good)
	geomPoints[8].tex0.x = 0.33f;
	geomPoints[8].tex0.y = 1.0f;	//part of front face

	geomPoints[9].tex0.x = 0.66f;
	geomPoints[9].tex0.y = 1.0f; //part of left face

	geomPoints[10].tex0.x = 0.66f;
	geomPoints[10].tex0.y = 1.0f; //part of back face

	geomPoints[11].tex0.x = 0.33f;
	geomPoints[11].tex0.y = 1.0f; //part of right face

	geomPoints[12].tex0.x = 0.33f;
	geomPoints[12].tex0.y = 0.0f; //part of front face

	geomPoints[13].tex0.x = 0.66f;
	geomPoints[13].tex0.y = 0.0f; //part of left face

	geomPoints[14].tex0.x = 0.66f;
	geomPoints[14].tex0.y = 0.0f; //part of back face

	geomPoints[15].tex0.x = 0.33f;
	geomPoints[15].tex0.y = 0.0f; //part of right face

	geomPoints[16].tex0.x = 0.66f;
	geomPoints[16].tex0.y = 1.0f; //part of front face

	geomPoints[17].tex0.x = 0.33f;
	geomPoints[17].tex0.y = 1.0f; //part of left face

	geomPoints[18].tex0.x = 0.33f;
	geomPoints[18].tex0.y = 1.0f; //part of back face

	geomPoints[19].tex0.x = 0.66f;
	geomPoints[19].tex0.y = 1.0f; //part of right face

	geomPoints[20].tex0.x = 0.66f;
	geomPoints[20].tex0.y = 0.0f; //part of front face

	geomPoints[21].tex0.x = 0.33f;
	geomPoints[21].tex0.y = 0.0f; //part of left face

	geomPoints[22].tex0.x = 0.33f;
	geomPoints[22].tex0.y = 0.0f; //part of back face

	geomPoints[23].tex0.x = 0.66f;
	geomPoints[23].tex0.y = 0.0f; //part of right face

	//Continue with the faces (not the band = P)
	geomPoints[24].tex0.x = 1.0f;
	geomPoints[24].tex0.y = 1.0f;

	geomPoints[25].tex0.x = 0.0f;
	geomPoints[25].tex0.y = 1.0f;

	geomPoints[26].tex0.x = 0.0f;
	geomPoints[26].tex0.y = 0.0f;

	geomPoints[27].tex0.x = 1.0f;
	geomPoints[27].tex0.y = 0.0f;    //left face

	geomPoints[28].tex0.x = 0.0f;
	geomPoints[28].tex0.y = 1.0f;

	geomPoints[29].tex0.x = 1.0f;
	geomPoints[29].tex0.y = 1.0f;

	geomPoints[30].tex0.x = 1.0f;
	geomPoints[30].tex0.y = 0.0f;

	geomPoints[31].tex0.x = 0.0f;
	geomPoints[31].tex0.y = 0.0f;    //right face

	geomPoints[32].tex0.x = 0.0f;
	geomPoints[32].tex0.y = 1.0f;

	geomPoints[33].tex0.x = 1.0f;
	geomPoints[33].tex0.y = 1.0f;

	geomPoints[34].tex0.x = 1.0f;
	geomPoints[34].tex0.y = 0.0f;

	geomPoints[35].tex0.x = 0.0f;
	geomPoints[35].tex0.y = 0.0f;    //top face

	geomPoints[36].tex0.x = 0.0f;
	geomPoints[36].tex0.y = 0.0f;

	geomPoints[37].tex0.x = 1.0f;
	geomPoints[37].tex0.y = 0.0f;

	geomPoints[38].tex0.x = 1.0f;
	geomPoints[38].tex0.y = 1.0f;

	
	geomPoints[39].tex0.x = 0.0f;
	geomPoints[39].tex0.y = 1.0f;    //bottom face

	for(int i = 0; i < WALL_NUM_VERTICES; ++i){
		geomPoints[i].color = D3DCOLOR_XRGB(255, 255, 255);
	}

	indexGeom[0] = 0;
	indexGeom[1] = 1;
	indexGeom[2] = 2;
	indexGeom[3] = 0;
	indexGeom[4] = 2;
	indexGeom[5] = 3; //face 1: front

	indexGeom[6] = 0;
	indexGeom[7] = 1;
	indexGeom[8] = 4;
	indexGeom[9] = 1;
	indexGeom[10] = 4;
	indexGeom[11] = 5; //face 2: bottom

	indexGeom[12] = 1;
	indexGeom[13] = 2;
	indexGeom[14] = 5;
	indexGeom[15] = 2;
	indexGeom[16] = 5;
	indexGeom[17] = 7; //face 3: right

	indexGeom[18] = 0;
	indexGeom[19] = 3;
	indexGeom[20] = 4;
	indexGeom[21] = 3;
	indexGeom[22] = 4;
	indexGeom[23] = 6; //face 4: left

	indexGeom[24] = 4;
	indexGeom[25] = 5;
	indexGeom[26] = 6;
	indexGeom[27] = 5;
	indexGeom[28] = 6;
	indexGeom[29] = 7; //face 5: back

	indexGeom[30] = 2;
	indexGeom[31] = 3;
	indexGeom[32] = 6;
	indexGeom[33] = 2;
	indexGeom[34] = 6;
	indexGeom[35] = 7; //face 6: top	

	if (SHADERS_ON)
		createVertexDecl();
	
	return 0;
}

int Wall::createVertexDecl()
{
	int rc=0;
	if (this->wallDeclaration== NULL){
		struct WALL_VTX v;
		D3DVERTEXELEMENT9 Decl[] =
	{
		{0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		{0, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
		{0,0, D3DDECLTYPE_FLOAT2,D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,0},
		D3DDECL_END()
	};
		Decl[0].Offset = (char *) &v.pos- (char *) &v;
		Decl[1].Offset = (char *) &v.color - (char *) &v;
		Decl[2].Offset= (char *) &v.tex0 - (char *) &v;
		rc= md3dDev->CreateVertexDeclaration(Decl, &wallDeclaration);
	}
	return rc;
}

Wall::WALL_VTX* const Wall::getPoints(){
	return geomPoints;
}

void Wall::setupIndVtxBuffers(){
	struct WALL_VTX *v = NULL;
	long *ind = NULL;

if( SHADERS_ON){
	if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( WALL_NUM_VERTICES*sizeof(struct WALL_VTX), D3DUSAGE_WRITEONLY, NULL, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	
	}}else{

	if(mVtxBuf == NULL){
		md3dDev->CreateVertexBuffer( WALL_NUM_VERTICES*sizeof(struct WALL_VTX), D3DUSAGE_WRITEONLY, CUSTOMFVF, D3DPOOL_MANAGED, &this->mVtxBuf, NULL);
	
	}

	}

	if(mIndBuf == NULL){
		md3dDev->CreateIndexBuffer( WALL_NUM_INDICES*sizeof(long), D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &this->mIndBuf, 0);
	}

    mVtxBuf->Lock(0, 0, (void**)&v, 0);    // lock the vertex buffer
 	memcpy(v, geomPoints, sizeof(geomPoints));    // copy the vertices to the locked buffer
    mVtxBuf->Unlock();    // unlock the vertex buffer

	mIndBuf->Lock(0, 0, (void**)&ind, 0);
	memcpy(ind, indexGeom, sizeof(indexGeom));
	mIndBuf->Unlock();
}

int Wall::render(int time){
	
if( SHADERS_ON){
	shaderRender(time);
}
else{
	standardRender(time);
}
	return 0;
}

int Wall::standardRender(int time){
	int rc;
	D3DXMATRIX worldMat = getTransformationMatrix();
	
	//D3DXMatrixIdentity(&worldMat);
	md3dDev->SetTransform(D3DTS_WORLD, &worldMat);

	// set the source
	rc = md3dDev->SetFVF(CUSTOMFVF);

	// set the index buffer
	rc = md3dDev->SetStreamSource(0, mVtxBuf, 0, sizeof(Wall::WALL_VTX));
	rc = md3dDev->SetIndices(mIndBuf);

	rc = md3dDev->SetRenderState(D3DRS_COLORVERTEX, FALSE);
	rc = md3dDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	rc = md3dDev->SetTexture(0, brickTexture);
	// draw the cube
	rc = md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, WALL_NUM_VERTICES, 0, WALL_NUM_INDICES/3); 

	return rc;

}
int Wall::preRender(){
	pWallEffect->SetMatrix(hViewMat, &viewMat);
	pWallEffect->SetMatrix(hProjMat, &projMat);
	pWallEffect->SetFloatArray(hLookAt,vLookAt,3);

	
	pWallEffect->SetTexture(hBumpMap, normalTexture);
	return 0;

}

int Wall::shaderRender(int time){

	int rc;
  

	int counter = 0;

	unsigned int i=0, j=0, k=0;
	unsigned int numPasses = 0;		// containes the number of passes of the technique

	D3DXMATRIX worldMat, matTransform, matProjection, matScale, matTranslate,  matRotation;


	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;

	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;


	// set the source
	rc = md3dDev->SetStreamSource( 0, mVtxBuf, 0, sizeof(WALL_VTX) );
	rc = md3dDev->SetVertexDeclaration(wallDeclaration);
	// set the index buffer
	md3dDev->SetIndices(mIndBuf);


	// set the technique
	rc = pWallEffect->SetTechnique(hTechnique);

	// set the global matrix
	pWallEffect->SetMatrix(hWorldViewProjMat, &(worldMat*viewMat*projMat));
	pWallEffect->SetMatrix(hWorldMat, &worldMat);
	pWallEffect-> SetTexture(hTexture, brickTexture);
	pWallEffect->SetFloat(hAmbientPower, ambientPower);
	

	
	

	// start the effect
	pWallEffect->Begin(&numPasses, 0);		// 0 means save and restore the device states

	// start the technique passess
	for (i = 0; i < numPasses; i++) {
		pWallEffect->BeginPass(i);

		// draw the mesh
		md3dDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, WALL_NUM_VERTICES, 0, WALL_NUM_INDICES/3);

		pWallEffect->EndPass();
	}

	pWallEffect->End();

   
	return 0;
}
D3DXMATRIX const Wall::getTransformationMatrix(){
	D3DXMATRIX worldMat, matTransform, matScale, matTranslate;

	D3DXMatrixScaling(&matScale,mScaleX, mScaleY, mScaleZ);
	worldMat = matScale;

	D3DXMatrixTranslation(&matTranslate, mPosition.x, mPosition.y, mPosition.z);
	worldMat *= matTranslate;
	return worldMat;
}

int Wall::initEffect(void)
{
	int rc;
	ID3DXBuffer *errors = NULL;		// used to get the errors
	char *shaderFile[2] = {"ShaderFX.fx"};
	char *techniqueName[2] = {"BumpMapEffect", "TechniqueSobel"};



	D3DCAPS9 caps;

	md3dDev->GetDeviceCaps(&caps);

	// check for shader support of version 3

	if (caps.VertexShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}

	// check for pixel Shader Support
	if (caps.PixelShaderVersion < D3DVS_VERSION(3,0)) {
		// no support for Vertex Shader version 3
		// do something � use fixed pipline, try version 2.0 etc.
	}


	// load the effect from the file
	rc = D3DXCreateEffectFromFile(md3dDev, shaderFile[0], 0,0,D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION, 
		0, &pWallEffect, &errors);

	if (rc) {
				switch(rc) {
			case D3DXERR_INVALIDDATA:
				rc+=0;
				break;
			case D3DERR_INVALIDCALL:
				rc+=0;
				break;
			case E_OUTOFMEMORY :
				rc+=0;
				break;
			default:
				rc+=0;
				break;
		}
	}
	if (errors != NULL) {
		// displays the errors when loading
		MessageBox(0, (char*) errors->GetBufferPointer(),0,0);
	}

	hWorldMat = hViewMat = hProjMat = NULL;
	// associate the external variable in the effect file with variable in the C++ program
	hWorldViewProjMat = pWallEffect->GetParameterByName(NULL, "gWorldViewProjMat");

	// associate the external variable in the effect file with variable in the C++ program
	hWorldMat = pWallEffect->GetParameterByName(NULL, "gWorldMat");

	// associate the external variable in the effect file with variable in the C++ program
	hViewMat = pWallEffect->GetParameterByName(NULL, "gViewMat");

	// associate the external variable in the effect file with variable in the C++ program
	hProjMat = pWallEffect->GetParameterByName(NULL, "gProjMat");


	hLookAt = pWallEffect ->GetParameterByName(NULL, "gLookAt");
	hAmbientPower = pWallEffect->GetParameterByName(NULL, "gAmbientPower");
		// associate the technique in the effect file with the C++ program
	hTechnique = pWallEffect->GetTechniqueByName(techniqueName[0]);

	hTexture = pWallEffect->GetParameterByName(NULL, "gWorldTex");
	hBumpMap = pWallEffect->GetParameterByName(NULL, "gBumpTex");
	
	
	



	// first associate it by name and store it in a handle.  The handles can be class variables (static)


	if (errors != NULL) {
		errors->Release();
	}
	return 0;
}
