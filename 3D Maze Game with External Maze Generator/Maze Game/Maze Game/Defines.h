
//size of the title of the window
#define TITLE_SIZE 128
#define MAX_LOADSTRING 100
//if shaders are being used
#define SHADERS_ON TRUE

//window width and height definitions
#define WND_WIDTH 800
#define WND_HEIGHT 600

//definition for the game frame rate
#define FRAMES_PER_SECOND 30
#define DIRECTINPUT_VERSION 0x0800

//DirectX definitions
#define DIRECTINPUT_VERSION 0x0800
#define NUSS_SHADERS 1

#ifndef NUSS_SHADERS
#define floor_VERTEX_FVF (D3DFVF_XYZ | D3DFVF_DIFFUSE)
#endif

// macro for release the com objects (move to a utility file)
#define COM_RELEASE(X) {if ((X) != NULL) {(X)->Release(); (X) = NULL;}}

// macro for release allocated memory
#define FREE_MEMORY(X) {if ((X) != NULL) {free(X); (X) = NULL;}}

//Auto generated diffines for the mains
#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_FLIGHT_SIM_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_FLIGHT_SIM			107
#define IDI_SMALL				108
#define IDC_FLIGHT_SIM			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif

//global user defs
#define GRID_UNIT_LENGTH 20.0f
#define GRID_UNIT_WIDTH 20.0f
#define GRID_UNIT_HEIGHT 20.0f

#define UP					0.0f, 1.0f, 0.0f
#define OBS_CAMERA_HEIGHT	GRID_UNIT_HEIGHT * 5.0f
