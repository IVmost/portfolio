#include "LabyrinthParser.h"
#include "atlbase.h"
#include "atlconv.h"

//Thanks to http://www.codeproject.com/KB/vista/VGFileDialogs.aspx#basicopenexample for the help and the dialog box code
LabyrinthParser::LabyrinthParser(void) : filePath("")
{
	getFilePathFromUser();
}


LabyrinthParser::~LabyrinthParser(void)
{

}

void LabyrinthParser::getFilePathFromUser(void){
	HRESULT hr;
	FILEOPENDIALOGOPTIONS dwOptions;
	CComPtr<IFileOpenDialog> pDlg;
	COMDLG_FILTERSPEC aFileTypes[] = {
		{ L"Labyrinth files", L"*.labyrinth" }
		};
	hr = pDlg.CoCreateInstance ( __uuidof(FileOpenDialog) );
 
	if ( FAILED(hr) ){
		MessageBox(NULL, "Failed to open a file selector dialog box. Exiting application.", "Dialog Box Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);
	}

	pDlg->SetFileTypes ( _countof(aFileTypes), aFileTypes );
	pDlg->SetTitle ( L"Choose A Labyrinth To Load" );
	hr = pDlg->GetOptions(&dwOptions);

	if ( FAILED(hr) ){
		MessageBox(NULL, "Failed to open a file selector dialog box. Exiting application.", "Dialog Box Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);
	}

	hr = pDlg->SetOptions(dwOptions|FOS_FORCEFILESYSTEM);

	if ( FAILED(hr) ){
		MessageBox(NULL, "Failed to open a file selector dialog box. Exiting application.", "Dialog Box Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);
	}

	hr = pDlg->Show ( NULL );

	if(hr == HRESULT_FROM_WIN32(ERROR_CANCELLED)){
		MessageBox(NULL, "You've decided not to open a labyrinth file. A labyrinth file is required. Exiting application.", "File Selection Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);
	} else if ( SUCCEEDED(hr) ){
		CComPtr<IShellItem> pItem;
 
		hr = pDlg->GetResult ( &pItem );
 
		if ( SUCCEEDED(hr) ){
			LPOLESTR pwsz = NULL;
 
			hr = pItem->GetDisplayName ( SIGDN_FILESYSPATH, &pwsz);
 
			if ( SUCCEEDED(hr) ){
				USES_CONVERSION;
				std::string s(OLE2A(pwsz));
				CoTaskMemFree ( pwsz );
//Might be useful to display the chosen path i.e. debugging				MessageBox (NULL, s.c_str(), "You chose...", MB_OK);
				filePath = s;
			} else {
				MessageBox(NULL, "There was a problem trying to open the labyrinth file. A labyrinth file is required. Exiting application.", "File Selection Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
				exit(-1);
			}
		}
	}	
}

/*
 *LabyrinthParser::parseFile
 *Directly Returns: Void
 *Indirectly Returns: The 2D array of GameObject pointers that represent our maze, a player, a key, the number of rows and columns and the time limit if explicitly stated
 *Parameters: [In]- None
 *			  [Out]- GameObject[][]* myMaze, Player* pMyPlayer, Key* pMyKey, int* numRows, int* numCols, time_t* timeLimit
 *Comments: Meant to be called by whomever stores the maze representation.
 *			Thanks to Nick Porter for help converting the 'readings' to c++ from c.
 *
 */
void LabyrinthParser::parseFile(GameObject**** myMaze, Player** ppMyPlayer, Key** ppMyKey, int* numRows, int* numCols, time_t* timeLimit){
	ifstream inFile;
	char testChar = '\0';
	bool bSeenKey = false;
	bool bSeenDoor = false;
	bool bSeenStart = false;
	string timeString;

	if(filePath.empty()){
		MessageBox(NULL, "There was an internal error trying to open the labyrinth file (null path). A labyrinth file is required. Exiting application.", "File Read Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);
	}

	inFile.open(filePath, ifstream::in);
	if(false != inFile.fail()){
	   MessageBox(NULL, "There was a problem trying to open the labyrinth file.", "File Open Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
    }

	inFile >> *numRows >> testChar >> *numCols >> testChar;

	if(false != inFile.fail()){
		MessageBox(NULL, "There was a problem with the format of the labyrinth file."
			"Needed the first line to be two numbers indicating number of rows, a colon followed by number of columns, and a semicolon."
			"Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		inFile.close();
		if(false != inFile.fail())
		{
			MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		}	
		exit(-1);
	}

	getline(inFile, timeString, ';');
	size_t timeInput = timeString.find(':');

	if ((false != inFile.fail()) || (timeString.find("time") == string::npos) || (timeInput == string::npos) || (timeString.length() < (timeInput + 2))){
		MessageBox(NULL, "There was a problem with the format of the labyrinth file. "
			"Needed the second line to be the word \'time\' followed by a colon followed by the level's time limit in minutes and a semicolon. "
			"Inputting anything less than 1 defaults the limit to 5 minutes. The number must be a whole number. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		inFile.close();
		if(false != inFile.fail())
		{
			MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		}	
		exit(-1);
	}

	istringstream timeValue (timeString.substr(timeInput + 1), istringstream::in);
	long extractor;
	timeValue >> extractor;

	if (false != timeValue.fail()){
		MessageBox(NULL, "There was a problem with the format of the labyrinth file. "
			"Needed the second line to be the word \'time\' followed by a colon followed by the level's time limit in minutes and a semicolon. "
			"Inputting anything less than 1 defaults the limit to 5 minutes. The number must be a whole number. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		inFile.close();
		if(false != inFile.fail())
		{
			MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		}	
		exit(-1);
	}

	*timeLimit = (time_t) extractor;

	//We don't need to skip to the next line manually. >> automatically eats up all whitespace characters that come before the character of interest.

	(*myMaze) = (GameObject***) calloc(*numRows, sizeof(GameObject**));

	if(NULL == (*myMaze)){
		MessageBox(NULL, "There was a problem allocating memory for the maze objects."
					" Exiting application.", "Memory Allocation Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		inFile.close();
		if(false != inFile.fail())
		{
			MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		}
		exit(-1);	
	}

	for(int i = 0; i < *numRows; ++i){
		(*myMaze)[i] = (GameObject**) calloc(*numCols, sizeof(GameObject*));
		if(NULL == ((*myMaze)[i])){
			MessageBox(NULL, "There was a problem allocating memory for the maze objects."
						" Exiting application.", "Memory Allocation Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
			inFile.close();
			if(false != inFile.fail())
			{
				MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
			}
			exit(-1);	
		}
	}

	 for(int i = 0; i < *numRows; ++i){
		 //To accomodate the semicolons at the ends of the lines we use cols + 1
		 for(int j = 0; j < (*numCols + 1); j++){
			inFile>>testChar;
			if(false != inFile.fail())
			{
				MessageBox(NULL, "There was a problem with the format of the labyrinth file. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
				inFile.close();
				if(false != inFile.fail())
				{
					MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
				}
				exit(-1);
			}			
			if((*numCols == j) && (testChar != ';')){
				MessageBox(NULL, "There was a problem with the format of the labyrinth file."
					"Didn't see a semicolon where there should have been one. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
				inFile.close();
				if(false != inFile.fail())
				{
					MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
				}
				exit(-1);		
			}
			 switch(testChar){
				case('-'):
				 //For now we only have one type of wall. If that ever changes, come uncomment this break
//					break;
				case('|'):
					//Wall will have 'height' (i.e. 'y') of 0, i as 'z' and j as 'x'
					(*myMaze)[i][j] = new Wall((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
					break;

				case('p'):
				case('P'):
					(*myMaze)[i][j] = new PathTile((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
					break;

				case('k'):
				case('K'):
					if(!bSeenKey){
						(*myMaze)[i][j] = new PathTile((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
						(*ppMyKey) = new Key((float)((i * GRID_UNIT_LENGTH) + (GRID_UNIT_LENGTH/2)), (float)0, (float)((j * GRID_UNIT_WIDTH) + (GRID_UNIT_WIDTH/2)));
						bSeenKey = true;
					} else {
//TODO should the second one just be ignored?
						MessageBox(NULL, "There was a problem with the format of the labyrinth file."
							"Saw two keys. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						inFile.close();
						if(false != inFile.fail())
						{
							MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						}
						exit(-1);
					}
					break;

				case('d'):
				case('D'):
					if(!bSeenDoor){
						(*myMaze)[i][j] = new Door((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
						bSeenDoor = true;
					} else {
//TODO should the second one just be ignored?
						MessageBox(NULL, "There was a problem with the format of the labyrinth file."
							"Saw two doors. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						inFile.close();
						if(false != inFile.fail())
						{
							MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						}						
						exit(-1);
					}
					break;

				case('o'):
				case('O'):
					(*myMaze)[i][j] = new PathTile((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
					((PathTile*)(*myMaze)[i][j])->setObservationArea(true);
					break;

				case('s'):
				case('S'):
					if(!bSeenStart){
						(*myMaze)[i][j] = new PathTile((float)(i * GRID_UNIT_LENGTH), (float)0, (float)(j * GRID_UNIT_WIDTH));
						(*ppMyPlayer) = new Player((float)((i * GRID_UNIT_LENGTH) + (GRID_UNIT_LENGTH/2)), (float)(3*GRID_UNIT_HEIGHT/4), (float)((j * GRID_UNIT_WIDTH) + (GRID_UNIT_WIDTH/2)));
						bSeenStart = true;
					} else {
//TODO should the second one just be ignored?
						MessageBox(NULL, "There was a problem with the format of the labyrinth file."
							"Saw two Starts. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						inFile.close();
						if(false != inFile.fail())
						{
							MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						}						
						exit(-1);
					}
					break;

				case(';'):
					if(*numCols != j){
						MessageBox(NULL, "There was a problem with the format of the labyrinth file."
							"Saw a semicolon where there shouldn't have been one. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						inFile.close();
						if(false != inFile.fail())
						{
							MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
						}
						exit(-1);		
					}
					break;

				default:
					MessageBox(NULL, "There was a problem with the format of the labyrinth file."
						"Unrecognized character. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
					inFile.close();
					if(false != inFile.fail())
					{
						MessageBox(NULL, "Warning: the labyrinth file didn't close properly.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
					}					
					exit(-1);	
					break;
			 }
			//We don't need to skip to the next line manually. >> automatically eats up all whitespace characters that come before the character of interest.
		 }
	 }

	inFile.close();
	if(false != inFile.fail())
	{
		MessageBox(NULL, "Warning: the labyrinth file didn't close properly. We'll continue but it may be unusable"
			" in the future.", "File Close Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
	}
	 if(!bSeenDoor || !bSeenKey || !bSeenStart){
		MessageBox(NULL, "There was a problem with the format of the labyrinth file."
			"Either a door, a start or a key was missing. Exiting application.", "File Format Error", MB_OK|MB_ICONERROR|MB_SETFOREGROUND);
		exit(-1);	
	 }
}