#pragma once
#include "CommonHeaders.h"
#include "gameObject.h"

#define SKYBOX_DEPTH_SCALE   200.0f
#define SKYBOX_HEIGHT_SCALE  100.0f

/*
 * The pictures for the walls of the skybox originally came from
 * http://www.mayang.com/textures/Buildings/images/Old%20Residential%20Buildings/castle_wall_2701.JPG
 * which I then modified. The floor is just black and the sky is just a sampled colour of the sky from the walls picture.
 */

#define WALL_FILE   "1024sky box walls.jpg"
#define SKY_FILE    "1024almost same blue.jpg"
#define FLOOR_FILE  "black.jpg"

#define SKYBOX_CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define SKYBOX_NUM_VERTICES 24
#define SKYBOX_NUM_INDICES 36

class SkyBox
{
public:

	struct SKYBOX_VTX{
		D3DXVECTOR3 pos;
		DWORD color;
		D3DXVECTOR2 tex0;
	};

	SkyBox(void);
	~SkyBox(void);
	int initGeom(void);
	int initShaderGeom(void);

	int num_of_skybox_vtx;

	int render(int time);
	void setCameraLifted(bool cameraIsLifted);
	static int setd3dDev(IDirect3DDevice9 *d3dDev);  // setting the d3dDev 'global' variable
	void setupIndVtxBuffers();
	SKYBOX_VTX* const getPoints();
	void setMazeBounds(float xSpan, float zSpan);


	int initEffect(void);
	int standardRender(int time);
	int shaderRender(int time);
	int createVertexDecl();

	static ID3DXEffect *pSkyEffect;

	static D3DXHANDLE hWorldViewProjMat;
	static D3DXHANDLE hHighWaves;
	static D3DXHANDLE hTechnique;
	static D3DXHANDLE hWorldMat;
	static D3DXHANDLE hViewMat;
	static D3DXHANDLE hProjMat;
	static D3DXHANDLE hTexture;

	static IDirect3DVertexDeclaration9* SkyDeclaration;
	static IDirect3DTexture9 *mSkyTexture;
	static IDirect3DTexture9 *mWallTexture;
	static IDirect3DTexture9 *mFloorTexture;

	IDirect3DCubeTexture9* skyBoxTexture;
	static int setViewMat(D3DXMATRIX * viewMat);
	static int setProjMat(D3DXMATRIX * projMat);
	static D3DXMATRIX viewMat;
	static D3DXMATRIX projMat;

private:
	float mScaleX;		// scale in x
	float mScaleY;		// scale in y
	float mScaleZ;		// scale in z

	float obsScaleX;
	float obsScaleZ;

	bool cameraLifted;

	LPDIRECT3DTEXTURE9 wallTexture;
	LPDIRECT3DTEXTURE9 skyTexture;
	LPDIRECT3DTEXTURE9 floorTexture;

	static IDirect3DDevice9 *md3dDev;
	
	IDirect3DIndexBuffer9 *mIndBuf;
	IDirect3DVertexBuffer9 *mVtxBuf;

	long indexGeom[SKYBOX_NUM_INDICES];
	SKYBOX_VTX geomPoints[SKYBOX_NUM_VERTICES];
};

