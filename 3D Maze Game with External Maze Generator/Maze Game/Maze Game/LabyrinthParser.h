#pragma once
#include "Shobjidl.h"
#include "gameObject.h"
#include "Wall.h"
#include "PathTile.h"
#include "Door.h"
#include "Player.h"
#include "Key.h"
#include "CommonHeaders.h"
#include <sstream>
using namespace std;

class LabyrinthParser
{
public:
	LabyrinthParser(void);
	virtual ~LabyrinthParser(void);

	//void parseFile([out] GameObject*** myMaze, [out] Player* pMyPlayer, [out] Key* pMyKey, [out] int* numRows, [out] int* numCols, time_t* timeLimit);
	//                The 2D array of GameObject pointers that represent our maze, a player, a key, the number of rows/cols and the time limit in the level.
	void parseFile(GameObject**** myMaze, Player** ppMyPlayer, Key** ppMyKey, int* numRows, int* numCols, time_t* timeLimit);

private:
	void getFilePathFromUser(void);
	string filePath;
	
};

