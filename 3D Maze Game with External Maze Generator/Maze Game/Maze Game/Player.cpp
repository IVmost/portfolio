#include "Player.h"


Player::Player(D3DXVECTOR3 pos) : upVector(0.0,1.0,0.0)
#if SUPERMAN_MODE
								,speed(0)
#endif
{
	mPosition.x = pos.x;
	mPosition.y = pos.y;
	mPosition.z = pos.z;
	mDir.x = mPosition.x + 1.0f;
	mDir.y = mPosition.y;
	mDir.z = mPosition.z + 1.0f;
	D3DXVec3Normalize(&mDir, &mDir);
	gameCamera.setCamera(mPosition, mDir + mPosition, upVector);
	bFoundKey = false;
	onObservationTile = false;
}

Player::Player(float x, float y, float z) : upVector(0.0,1.0,0.0)
#if SUPERMAN_MODE
											,speed(0)
#endif
{
	mPosition.x = x;
	mPosition.y = y;
	mPosition.z = z;
	mDir.x = mPosition.x + 1.0f;
	mDir.y = mPosition.y;
	mDir.z = mPosition.z + 1.0f;	
	D3DXVec3Normalize(&mDir, &mDir);
	gameCamera.setCamera(mPosition, mDir + mPosition, upVector);
	bFoundKey = false;
	onObservationTile = false;
}

Player::~Player(void)
{
}

int Player::render(int time){
	return 0;
}


Camera* const Player::getCamera(){
	return &gameCamera;
}

void Player::setupIndVtxBuffers(){
	//Right now this does nothing. If we want to draw a person later we'll need this.
	return;
}

boolean Player::foundKey(){
	return bFoundKey;
}

void Player::setFoundKey(boolean bFound){
	bFoundKey = bFound;
}

int Player::setPitch(float angleDeg){
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,0.0,0.0);
	D3DXVec3Cross(&rotVector,&upVector,&mDir);

	// get rotation axis
	updateOrientation(rotVector, angleRad);

	gameCamera.pitch(angleDeg);

	return 0;
}

//Note: Thanks to Nick Porter for pointing out the solution to the 'roll' problem.
int Player::setYaw(float angleDeg){
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,1.0,0.0);

	updateOrientation(rotVector, angleRad);

	gameCamera.yaw(angleDeg);

	return 0;
}

int Player::setYawSpecial(float angleDeg){
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,0.0,0.0);
	rotVector=upVector;
	updateOrientation(rotVector, angleRad);

	gameCamera.yaw(angleDeg);

	return 0;
}
int Player::updateOrientation(D3DXVECTOR3 rotVector, float angleRad)
{
	D3DXVECTOR3 xaxis(0.0,0.0,0.0);

	// create rotation matrix
	D3DXMatrixRotationAxis(&rotMat, &rotVector, angleRad);

	// rotate the Camera (up vector and/or mDir)
	D3DXVec3TransformCoord(&upVector, &upVector, &rotMat);
	D3DXVec3TransformCoord(&mDir, &mDir, &rotMat);

	// update the upVector
	D3DXVec3Cross(&xaxis, &upVector, &mDir);
	D3DXVec3Cross(&upVector,  &mDir, &xaxis);
	D3DXVec3Normalize(&upVector, &upVector);
	D3DXVec3Normalize(&mDir, &mDir);

	return 0;
}

D3DXVECTOR3 Player::getUpVector(void)
{
	return upVector;
}

float Player::getPitch(){
	return gameCamera.getPitch();
}

float Player::getYaw(){
	return gameCamera.getYaw();
}

bool Player::isUpsideDown(){
	return (upVector.y < 0);
}

void Player::setOnObsTile(bool onObsTile){
	onObservationTile = onObsTile;
}

bool Player::isOnObsTile(){
	return onObservationTile;
}

#if SUPERMAN_MODE
	float Player::getRoll(){
		return gameCamera.getRoll();
	}

	int Player::updateSpeed(float newSpeed)
	{
		speed += newSpeed;
		gameCamera.updateSpeed(newSpeed);
		return 0;
	}

	int Player::setRoll(float angleDeg){
		float angleRad = D3DXToRadian(angleDeg);
		D3DXVECTOR3 rotVector(0.0,0.0,0.0);

		// get rotation axis
		rotVector = mDir;

		updateOrientation(rotVector, angleRad);

		gameCamera.roll(angleDeg);

		return 0;
	}

	float Player::getSpeed(void){
		return speed;
	}

	D3DXVECTOR3 Player::moveForward(float numUnits)
	{
		mPosition += mDir*numUnits;
		gameCamera.moveForward(numUnits);
		return mPosition;
	}

#else

	D3DXVECTOR3 Player::testMoveBackward(){
		D3DXVECTOR3 result;
		result.y = 0.0f;
		if(upVector.y < 0){
			//upside down, move in the direction we're looking on x-z plane
			result.x = mPosition.x + mDir.x;
			result.z = mPosition.z + mDir.z;
		} else {
			result.x = mPosition.x - mDir.x;
			result.z = mPosition.z - mDir.z;
		}		
		return result;
	}

	D3DXVECTOR3 Player::testMoveForward(){
		D3DXVECTOR3 result;
		result.y = 0.0f;
		if(upVector.y < 0){
			//upside down, move in the direction we're looking on x-z plane
			result.x = mPosition.x - mDir.x;
			result.z = mPosition.z - mDir.z;
		} else {
			result.x = mPosition.x + mDir.x;
			result.z = mPosition.z + mDir.z;
		}		
		return result;
	}

	// canMoveX and canMoveZ defaulted to true
	void Player::moveBackward(bool canMoveX, bool canMoveZ){

		if(upVector.y < 0){
			//upside down, move in the direction we're looking on x-z plane
			if(canMoveX){
				mPosition.x += mDir.x;
			}
			if(canMoveZ){
				mPosition.z += mDir.z;
			}			
		} else {
			if(canMoveX){
				mPosition.x -= mDir.x;
			}
			if(canMoveZ){
				mPosition.z -= mDir.z;
			}	
		}
		gameCamera.setCamera(mPosition, mDir + mPosition, upVector);
	}

	// canMoveX and canMoveZ defaulted to true
	void Player::moveForward(bool canMoveX, bool canMoveZ){

		if(upVector.y < 0){
			//upside down, move in the opposite direction we're looking on x-z plane
			if(canMoveX){
				mPosition.x -= mDir.x;
			}
			if(canMoveZ){
				mPosition.z -= mDir.z;
			}	
		} else {
			if(canMoveX){
				mPosition.x += mDir.x;
			}
			if(canMoveZ){
				mPosition.z += mDir.z;
			}	
		}
		gameCamera.setCamera(mPosition, mDir + mPosition, upVector);
	}
#endif