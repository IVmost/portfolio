#include "Camera.h"



/******************************************************************/
/*
Purpose: Camera constructor


Descripton: 

Return:


*/

Camera::Camera(void): position(100.0,100.0,-50.0), lookAtVector(0.0,0.0,1.0), upVector(0.0,1.0,0.0)
, speed(0)
{

}

/******************************************************************/
/*
Purpose: Camera destructor


Descripton: 

Return:


*/
Camera::~Camera(void)
{
}


/******************************************************************/
/*
Purpose: change the orientation of the Camera (roll transformation)


Descripton: 

Return:
1 - if failed
0 - if successful

*/


int Camera::roll(float angleDeg)
{
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,0.0,0.0);


	// get rotation axis
	rotVector = lookAtVector;

	updateOrientation(rotVector, angleRad);


	return 0;
}


/******************************************************************/
/*
Purpose: change the orientation of the Camera (pitch transformation)


Descripton: 

Return:
1 - if failed
0 - if successful

*/

int Camera::pitch(float angleDeg)
{
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,0.0,0.0);
	D3DXVec3Cross(&rotVector,&upVector,&lookAtVector);

		// get rotation axis
	updateOrientation(rotVector, angleRad);

	return 0;
}

/******************************************************************/
/*
Purpose: change the orientation of the Camera (yaw transformation)


Descripton: 

Note: Thanks to Nick Porter for pointing out the solution to the 'roll' problem.
Return:
1 - if failed
0 - if successful

*/

int Camera::yaw(float angleDeg)
{
	float angleRad = D3DXToRadian(angleDeg);
	D3DXVECTOR3 rotVector(0.0,1.0,0.0);

	updateOrientation(rotVector, angleRad);


	return 0;
}

/******************************************************************/
/*
Purpose: obtains the current position of the Camera


Descripton: 

Return:
D3DXVECTOR3 - Camera position

*/

D3DXVECTOR3 Camera::getPosition(void)
{
	return (position);
}


/******************************************************************/
/*
Purpose: obtains the lookAt point of the Camera


Descripton: 

Return:
D3DXVECTOR3 - Camera lookAt point

*/


D3DXVECTOR3 Camera::getLookAtPoint(void)
{
	return (position + lookAtVector);
}

/******************************************************************/
/*
Purpose: obtains the Camera's up vector


Descripton: 

Return:
D3DXVECTOR3 - Camera upVector

*/



D3DXVECTOR3 Camera::getUpVector(void)
{
	return (upVector);
}


/******************************************************************/
/*
Purpose: changes the Camera's position relative to its current position


Descripton: 

Return:
0 - success

*/


int Camera::changePoitionDelta(float dx, float dy, float dz)
{
	position.x += dx;
	position.y += dy;
	position.z += dz;

	return 0;
}


/******************************************************************/
/*
Purpose: changes the Camera's position relative to its current position


Descripton: 

Return:
0 - success

*/


int Camera::changePositionDelta(D3DXVECTOR3 *dv)
{
	position += *dv;
	return 0;
}

/******************************************************************/
/*
Purpose: changes the Camera's position to a new position


Descripton: 

Return:
0 - success

*/


int Camera::changeAbsPoition(float x, float y, float z)
{
	position.x += x;
	position.y += y;
	position.z += z;
	return 0;
}

/******************************************************************/
/*
Purpose: changes the Camera's position to a new position


Descripton: 

Return:
0 - success

*/

int Camera::changeAbsPosition(D3DXVECTOR3 *v)
{
	position=*v;
	return 0;
}

/******************************************************************/
/*
Purpose: changes the Camera's position along the LootAt vector


Descripton: move the Camera forward by the numUnits along the looAtVector

Return:
the new position

*/


D3DXVECTOR3 Camera::moveForward(float numUnits)
{
	position+=lookAtVector*numUnits;
	return (position);
}

/******************************************************************/
/*
Purpose: 


Descripton: 

Return:


*/


int Camera::updateOrientation(D3DXVECTOR3 rotVector, float angleRad)
{

	D3DXVECTOR3 xaxis(0.0,0.0,0.0);

	// create rotation matrix
	D3DXMatrixRotationAxis(&rotMat, &rotVector,angleRad);

	// rotate the Camera (up vector and/or looAtVector)
	D3DXVec3TransformCoord(&upVector, &upVector, &rotMat);
	D3DXVec3TransformCoord(&lookAtVector, &lookAtVector, &rotMat);

	// update the upVector
	D3DXVec3Cross(&xaxis, &upVector, &lookAtVector);
	D3DXVec3Cross(&upVector,  &lookAtVector, &xaxis);
	D3DXVec3Normalize(&upVector, &upVector);
	D3DXVec3Normalize(&lookAtVector, &lookAtVector);

	return 0;
}

/******************************************************************/
/*
Purpose: obtains the view transformation matrix


Descripton: 

Return:
the transformation matrix

*/



D3DMATRIX * Camera::getViewMatrix(D3DXMATRIX * viewMatrix)
{
	D3DXVECTOR3 lookAt;
	
	lookAt = position+lookAtVector;

	return(D3DXMatrixLookAtLH(viewMatrix, &position,&lookAt, &upVector));
}

/******************************************************************/
/*
Purpose: set the Camera parameters


Descripton: 

Return:


*/


void Camera::setCamera(D3DXVECTOR3 position, D3DXVECTOR3 lookAtPoint, D3DXVECTOR3 upVector)
{
	this->position = position;
	this->lookAtVector = lookAtPoint - position;
	this->upVector = upVector;
	D3DXVec3Normalize(&this->upVector, &this->upVector);
	D3DXVec3Normalize(&this->lookAtVector, &this->lookAtVector);

}
// change the speed of the Camera motion
int Camera::updateSpeed(float speed)
{
	this->speed += speed;
	return 0;
}

float Camera::getSpeed(void)
{
	return(speed);
}

/******************************************************************/
/*
Purpose: Returns the current Vector speed


Descripton: 

Return:
D3DXVECTOR3 - speed of plane

*/

D3DXVECTOR3 Camera::getSpeedVector(){
	return lookAtVector*speed;
}

/******************************************************************/
/*
Purpose: Returns the look at Vector


Descripton: 

Return:
D3DXVECTOR3 - lookAtVector

*/

D3DXVECTOR3 Camera::getLookAtVector(){
	return lookAtVector;
}


float Camera::getPitch(){
	float result = 0;
	//Make sure we're dealing with a normalized vector
	D3DXVECTOR3 temp(0.0, 0.0, 0.0);
	D3DXVec3Normalize(&temp, &this->lookAtVector);

	D3DXVECTOR3 yAxis(0.0, 1.0, 0.0);

	if(upVector.y < 0){
		yAxis *= -1;
		result = 90 + (180 - D3DXToDegree(acos(D3DXVec3Dot(&yAxis, &temp))));
		if(temp.x > 0){
			//This is meant to stop the unnecessary addition of 180 as soon as we're upside down. I think we need to start considering z-values to make it work...
			//No time for that...
//			result -= 180;
//			result *= -1;
		}
	} else {
		result = 90 - D3DXToDegree(acos(D3DXVec3Dot(&yAxis, &temp)));
	}

	return -result;
}

float Camera::getYaw(){
	float result = 0;
	//Make sure we're dealing with a normalized vector
	D3DXVECTOR3 temp(0.0, 0.0, 0.0);
	D3DXVec3Normalize(&temp, &this->lookAtVector);

	D3DXVECTOR3 xAxis(1.0, 0.0, 0.0);
	

	if(lookAtVector.z < 0){
		xAxis *= -1;
		result = 90 + (180 - D3DXToDegree(acos(D3DXVec3Dot(&xAxis, &temp))));
	} else {
		result = 90 - D3DXToDegree(acos(D3DXVec3Dot(&xAxis, &temp)));
	}

	return result;
}

float Camera::getRoll(){
	float result = 0;
	//Make sure we're dealing with a normalized vector
	D3DXVECTOR3 temp(0.0, 0.0, 0.0);

	D3DXVec3Cross(&temp, &upVector, &lookAtVector);
	D3DXVec3Normalize(&temp, &temp);

	D3DXVECTOR3 yAxis(0.0, 1.0, 0.0);

	if(upVector.y < 0){
		yAxis *= -1;
		result = 90 + (180 - D3DXToDegree(acos(D3DXVec3Dot(&yAxis, &temp))));
		if(temp.x > 0){
			//This is meant to stop the unnecessary addition of 180 as soon as we're upside down. I think we need to start considering z-values to make it work...
			//No time for that...
//			result -= 180;
//			result *= -1;
		}
	} else {
		result = 90 - D3DXToDegree(acos(D3DXVec3Dot(&yAxis, &temp)));
	}

	return result;
}