package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileFilter;
import algorithm.MazeAlgorithm;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MazeGenerator extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final ButtonGroup xOnesGroup = new ButtonGroup();
	private final ButtonGroup xTensGroup = new ButtonGroup();
	private final ButtonGroup yTensGroup = new ButtonGroup();
	private final ButtonGroup yOnesGroup = new ButtonGroup();

	private int yOnes;
	private int yTens;

	private int xOnes;
	private int xTens;
	
	private int minutesTens;
	private int minutesOnes;

	private int observationArea;

	private JMenu menuX;
	private JMenu menuY;
	private MazeDrawer mazeBoard;

	private boolean modeSelected;

	private MazeAlgorithm algorithm;

	private final ButtonGroup generationMode = new ButtonGroup();
	private JMenuItem mntmRegenerateMaze;
	private JMenuItem mntmSaveMaze;
	private final ButtonGroup ObservationGroup = new ButtonGroup();
	private JMenu ObservationArea;

	private final JFileChooser fc = new JFileChooser(
			System.getProperty("user.dir"));
	private final ButtonGroup tensMinutes = new ButtonGroup();
	private final ButtonGroup oneMinutes = new ButtonGroup();
	private JRadioButtonMenuItem minutesRadio5;
	private JRadioButtonMenuItem minutesRadio0;
	private JMenu mnTimeLimit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MazeGenerator frame = new MazeGenerator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MazeGenerator() {
		minutesOnes=5;
		minutesTens=0;
		
		FileFilter fileFilter = new FileFilter() {

			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean accept(File f) {
				if (f.isDirectory())
					return true;
				String ext = null;
				String s = f.getName();
				int i = s.lastIndexOf('.');

				if (i > 0 && i < s.length() - 1) {
					ext = s.substring(i + 1).toLowerCase();
				}
				if (ext != null)
					if (ext.equals("labyrinth"))
						return true;
				return false;
			}
		};

		fc.setFileFilter(fileFilter);
		observationArea = 3;
		setResizable(false);
		setTitle("aMAZEing GENErator");
		modeSelected = false;
		yTens = 10;
		yOnes = 0;

		xTens = 10;
		xOnes = 0;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 528, 571);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmNewMaze = new JMenuItem("New Maze");
		mntmNewMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newMazeCreator();
				mntmRegenerateMaze.setEnabled(true);
				mntmSaveMaze.setEnabled(true);

			}
		});
		mnFile.add(mntmNewMaze);

		mntmSaveMaze = new JMenuItem("Save Maze");
		mntmSaveMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int  returnVal = fc.showSaveDialog(MazeGenerator.this);
				if (returnVal == JFileChooser.APPROVE_OPTION){
					File file= fc.getSelectedFile();
					String tempString= file.getName().toLowerCase();
					String tempExt=null;
					int i = tempString.lastIndexOf('.');
					if (i > 0 && i < tempString.length() -1 )
					{
						tempExt=tempString.substring(i+1);
						tempString=tempString.substring(0,i);
						if (tempExt.equals("labyrinth"))
							tempExt="labyrinth";
					} else{
						tempExt="labyrinth";
					}
					file= new File(tempString+"."+tempExt);
					if (file.exists()){
						file.delete();
					}
					try {
						file.createNewFile();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					FileWriter fstream;
					try {
						fstream = new FileWriter(file);
						BufferedWriter out = new BufferedWriter(fstream);
						String output = algorithm.createfile(minutesOnes+minutesTens);
						out.write(output);
						out.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		mntmSaveMaze.setEnabled(false);
		mnFile.add(mntmSaveMaze);

		JSeparator separator = new JSeparator();
		mnFile.add(separator);

		JMenu mnGenerateMoves = new JMenu("Generation Mode");
		mnFile.add(mnGenerateMoves);

		JRadioButtonMenuItem rdbtnmntmDepthFirstMode = new JRadioButtonMenuItem(
				"Depth First Mode");
		rdbtnmntmDepthFirstMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modeSelected = false;
			}
		});
		rdbtnmntmDepthFirstMode.setSelected(true);
		generationMode.add(rdbtnmntmDepthFirstMode);
		mnGenerateMoves.add(rdbtnmntmDepthFirstMode);

		JRadioButtonMenuItem rdbtnmntmDivideAndConqure = new JRadioButtonMenuItem(
				"Divide and Conquer");
		rdbtnmntmDivideAndConqure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modeSelected = true;
			}
		});
		generationMode.add(rdbtnmntmDivideAndConqure);
		mnGenerateMoves.add(rdbtnmntmDivideAndConqure);

		mntmRegenerateMaze = new JMenuItem("Regenerate Maze");
		mntmRegenerateMaze.setEnabled(false);
		mntmRegenerateMaze.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				algorithm.restart();
				generateMaze();
			}
		});
		mnFile.add(mntmRegenerateMaze);

		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);

		JMenuItem mntmExitProgram = new JMenuItem("Exit Program");
		mntmExitProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmExitProgram);

		menuX = new JMenu("X:");
		menuBar.add(menuX);
		
				JMenu xOneMenu = new JMenu("Ones");
				menuX.add(xOneMenu);
				
						JRadioButtonMenuItem x0Button = new JRadioButtonMenuItem("0");
						x0Button.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								xOnes = 0;
								updateXMenu();
							}

						});
						x0Button.setSelected(true);
						xOnesGroup.add(x0Button);
						xOneMenu.add(x0Button);
						
								JRadioButtonMenuItem x1Button = new JRadioButtonMenuItem("1");
								x1Button.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										xOnes = 1;
										updateXMenu();
									}
								});
								xOnesGroup.add(x1Button);
								xOneMenu.add(x1Button);
								
										JRadioButtonMenuItem x2Button = new JRadioButtonMenuItem("2");
										x2Button.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												xOnes = 2;
												updateXMenu();
											}
										});
										xOnesGroup.add(x2Button);
										xOneMenu.add(x2Button);
										
												JRadioButtonMenuItem x3Button = new JRadioButtonMenuItem("3");
												x3Button.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														xOnes = 3;
														updateXMenu();
													}
												});
												xOnesGroup.add(x3Button);
												xOneMenu.add(x3Button);
												
														JRadioButtonMenuItem x4Button = new JRadioButtonMenuItem("4");
														x4Button.addActionListener(new ActionListener() {
															public void actionPerformed(ActionEvent e) {
																xOnes = 4;
																updateXMenu();
															}
														});
														xOnesGroup.add(x4Button);
														xOneMenu.add(x4Button);
														
																JRadioButtonMenuItem x5Button = new JRadioButtonMenuItem("5");
																x5Button.addActionListener(new ActionListener() {
																	public void actionPerformed(ActionEvent e) {
																		xOnes = 5;
																		updateXMenu();
																	}
																});
																xOnesGroup.add(x5Button);
																xOneMenu.add(x5Button);
																
																		JRadioButtonMenuItem x6Button = new JRadioButtonMenuItem("6");
																		x6Button.addActionListener(new ActionListener() {
																			public void actionPerformed(ActionEvent e) {
																				xOnes = 6;
																				updateXMenu();
																			}
																		});
																		xOnesGroup.add(x6Button);
																		xOneMenu.add(x6Button);
																		
																				JRadioButtonMenuItem x7Button = new JRadioButtonMenuItem("7");
																				x7Button.addActionListener(new ActionListener() {
																					public void actionPerformed(ActionEvent e) {
																						xOnes = 7;
																						updateXMenu();
																					}
																				});
																				xOnesGroup.add(x7Button);
																				xOneMenu.add(x7Button);
																				
																						JRadioButtonMenuItem x8Button = new JRadioButtonMenuItem("8");
																						x8Button.addActionListener(new ActionListener() {
																							public void actionPerformed(ActionEvent e) {
																								xOnes = 8;
																								updateXMenu();
																							}
																						});
																						xOnesGroup.add(x8Button);
																						xOneMenu.add(x8Button);
																						
																								JRadioButtonMenuItem x9Button = new JRadioButtonMenuItem("9");
																								x9Button.addActionListener(new ActionListener() {
																									public void actionPerformed(ActionEvent e) {
																										xOnes = 9;
																										updateXMenu();
																									}
																								});
																								xOnesGroup.add(x9Button);
																								xOneMenu.add(x9Button);

		JMenu xTensMenu = new JMenu("Tens");
		menuX.add(xTensMenu);

		JRadioButtonMenuItem x10Button = new JRadioButtonMenuItem("10");
		x10Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 10;
				updateXMenu();
			}
		});
		x10Button.setSelected(true);
		xTensGroup.add(x10Button);
		xTensMenu.add(x10Button);

		JRadioButtonMenuItem x20Button = new JRadioButtonMenuItem("20");
		x20Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 20;
				updateXMenu();
			}
		});
		xTensGroup.add(x20Button);
		xTensMenu.add(x20Button);

		JRadioButtonMenuItem x30Button = new JRadioButtonMenuItem("30");
		x30Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 30;
				updateXMenu();
			}
		});
		xTensGroup.add(x30Button);
		xTensMenu.add(x30Button);

		JRadioButtonMenuItem x40Button = new JRadioButtonMenuItem("40");
		x40Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 40;
				updateXMenu();
			}
		});
		xTensGroup.add(x40Button);
		xTensMenu.add(x40Button);

		JRadioButtonMenuItem x50Button = new JRadioButtonMenuItem("50");
		x50Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 50;
				updateXMenu();
			}
		});
		xTensGroup.add(x50Button);
		xTensMenu.add(x50Button);

		JRadioButtonMenuItem x60Button = new JRadioButtonMenuItem("60");
		x60Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 60;
				updateXMenu();
			}
		});
		xTensGroup.add(x60Button);
		xTensMenu.add(x60Button);

		JRadioButtonMenuItem x70Button = new JRadioButtonMenuItem("70");
		x70Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 70;
				updateXMenu();
			}
		});
		xTensGroup.add(x70Button);
		xTensMenu.add(x70Button);

		JRadioButtonMenuItem x80Button = new JRadioButtonMenuItem("80");
		x80Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 80;
				updateXMenu();
			}
		});
		xTensGroup.add(x80Button);
		xTensMenu.add(x80Button);

		JRadioButtonMenuItem x90Button = new JRadioButtonMenuItem("90");
		x90Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				xTens = 90;
				updateXMenu();
			}
		});
		xTensGroup.add(x90Button);
		xTensMenu.add(x90Button);

		menuY = new JMenu("Y:");
		menuBar.add(menuY);

		JMenu yOnesMenu = new JMenu("Ones");
		menuY.add(yOnesMenu);

		JRadioButtonMenuItem y0Button = new JRadioButtonMenuItem("0");
		y0Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 0;
				updateYMenu();
			}
		});
		yOnesGroup.add(y0Button);
		y0Button.setSelected(true);
		yOnesMenu.add(y0Button);

		JRadioButtonMenuItem y1Button = new JRadioButtonMenuItem("1");
		y1Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 1;
				updateYMenu();
			}
		});
		yOnesGroup.add(y1Button);
		yOnesMenu.add(y1Button);

		JRadioButtonMenuItem y2Button = new JRadioButtonMenuItem("2");
		y2Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 2;
				updateYMenu();
			}
		});
		yOnesGroup.add(y2Button);
		yOnesMenu.add(y2Button);

		JRadioButtonMenuItem y3Button = new JRadioButtonMenuItem("3");
		y3Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 3;
				updateYMenu();
			}
		});
		yOnesGroup.add(y3Button);
		yOnesMenu.add(y3Button);

		JRadioButtonMenuItem y4Button = new JRadioButtonMenuItem("4");
		y4Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 4;
				updateYMenu();
			}
		});
		yOnesGroup.add(y4Button);
		yOnesMenu.add(y4Button);

		JRadioButtonMenuItem y5Button = new JRadioButtonMenuItem("5");
		y5Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				yOnes = 5;
				updateYMenu();
			}
		});
		yOnesGroup.add(y5Button);
		yOnesMenu.add(y5Button);

		JRadioButtonMenuItem y6Button = new JRadioButtonMenuItem("6");
		y6Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				yOnes = 6;
				updateYMenu();
			}
		});
		yOnesGroup.add(y6Button);
		yOnesMenu.add(y6Button);

		JRadioButtonMenuItem y7Button = new JRadioButtonMenuItem("7");
		y7Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 7;
				updateYMenu();
			}
		});
		yOnesGroup.add(y7Button);
		yOnesMenu.add(y7Button);

		JRadioButtonMenuItem y8Button = new JRadioButtonMenuItem("8");
		y8Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 8;
				updateYMenu();
			}
		});
		yOnesGroup.add(y8Button);
		yOnesMenu.add(y8Button);

		JRadioButtonMenuItem y9Button = new JRadioButtonMenuItem("9");
		y9Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yOnes = 9;
				updateYMenu();
			}
		});
		yOnesGroup.add(y9Button);
		yOnesMenu.add(y9Button);

		JMenu yTensMenu = new JMenu("Tens");
		menuY.add(yTensMenu);

		JRadioButtonMenuItem y10Button = new JRadioButtonMenuItem("10");
		y10Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 10;
				updateYMenu();
			}
		});
		yTensGroup.add(y10Button);
		y10Button.setSelected(true);
		yTensMenu.add(y10Button);

		JRadioButtonMenuItem y20Button = new JRadioButtonMenuItem("20");
		y20Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 20;
				updateYMenu();
			}
		});
		yTensGroup.add(y20Button);
		yTensMenu.add(y20Button);

		JRadioButtonMenuItem y30Button = new JRadioButtonMenuItem("30");
		y30Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 30;
				updateYMenu();
			}
		});
		yTensGroup.add(y30Button);
		yTensMenu.add(y30Button);

		JRadioButtonMenuItem y40Button = new JRadioButtonMenuItem("40");
		y40Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 40;
				updateYMenu();
			}
		});
		yTensGroup.add(y40Button);
		yTensMenu.add(y40Button);

		JRadioButtonMenuItem y50Button = new JRadioButtonMenuItem("50");
		y50Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 50;
				updateYMenu();
			}
		});
		yTensGroup.add(y50Button);
		yTensMenu.add(y50Button);

		JRadioButtonMenuItem y60Button = new JRadioButtonMenuItem("60");
		y60Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 60;
				updateYMenu();
			}
		});
		yTensGroup.add(y60Button);
		yTensMenu.add(y60Button);

		JRadioButtonMenuItem y70Button = new JRadioButtonMenuItem("70");
		y70Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 70;
				updateYMenu();
			}
		});
		yTensGroup.add(y70Button);
		yTensMenu.add(y70Button);

		JRadioButtonMenuItem y80Button = new JRadioButtonMenuItem("80");
		y80Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 80;
				updateYMenu();
			}
		});
		yTensGroup.add(y80Button);
		yTensMenu.add(y80Button);

		JRadioButtonMenuItem y90Button = new JRadioButtonMenuItem("90");
		y90Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				yTens = 90;
				updateYMenu();
			}
		});
		yTensGroup.add(y90Button);
		yTensMenu.add(y90Button);

		ObservationArea = new JMenu("Observation Square per Area: 3x3");
		menuBar.add(ObservationArea);

		JRadioButtonMenuItem b3X3Radio = new JRadioButtonMenuItem("3x3");
		b3X3Radio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				observationArea = 3;
				updateObservationMenu();
			}
		});
		ObservationGroup.add(b3X3Radio);
		b3X3Radio.setSelected(true);
		ObservationArea.add(b3X3Radio);

		JRadioButtonMenuItem b4X4Radio = new JRadioButtonMenuItem("4x4");
		b4X4Radio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				observationArea = 4;
				updateObservationMenu();
			}
		});
		ObservationGroup.add(b4X4Radio);
		ObservationArea.add(b4X4Radio);

		JRadioButtonMenuItem b5X5Radio = new JRadioButtonMenuItem("5x5");
		b5X5Radio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				observationArea = 5;
				updateObservationMenu();
			}
		});
		ObservationGroup.add(b5X5Radio);
		ObservationArea.add(b5X5Radio);

		JRadioButtonMenuItem b6X6Radio = new JRadioButtonMenuItem("6x6");
		b6X6Radio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				observationArea = 6;
				updateObservationMenu();
			}
		});
		ObservationGroup.add(b6X6Radio);
		ObservationArea.add(b6X6Radio);
		
		mnTimeLimit = new JMenu("Time Limit: 5 mins");
		menuBar.add(mnTimeLimit);
		
		JMenu menu = new JMenu("Ones");
		mnTimeLimit.add(menu);
		
		minutesRadio0 = new JRadioButtonMenuItem("0");
		minutesRadio0.setEnabled(false);
		minutesRadio0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=0;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio0);
		menu.add(minutesRadio0);
		
		JRadioButtonMenuItem minutesRadio1 = new JRadioButtonMenuItem("1");
		minutesRadio1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=1;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio1);
		menu.add(minutesRadio1);
		
		JRadioButtonMenuItem minutesRadio2 = new JRadioButtonMenuItem("2");
		minutesRadio2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=2;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio2);
		menu.add(minutesRadio2);
		
		JRadioButtonMenuItem minutesRadio3 = new JRadioButtonMenuItem("3");
		minutesRadio3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=3;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio3);
		menu.add(minutesRadio3);
		
		JRadioButtonMenuItem minutesRadio4 = new JRadioButtonMenuItem("4");
		minutesRadio4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=4;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio4);
		menu.add(minutesRadio4);
		
		minutesRadio5 = new JRadioButtonMenuItem("5");
		minutesRadio5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				minutesOnes=5;
				updateMinuteMenu();
			}
		});
		minutesRadio5.setSelected(true);
		oneMinutes.add(minutesRadio5);
		menu.add(minutesRadio5);
		
		JRadioButtonMenuItem minutesRadio6 = new JRadioButtonMenuItem("6");
		minutesRadio6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=6;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio6);
		menu.add(minutesRadio6);
		
		JRadioButtonMenuItem minutesRadio7 = new JRadioButtonMenuItem("7");
		minutesRadio7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesOnes=7;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio7);
		menu.add(minutesRadio7);
		
		JRadioButtonMenuItem minutesRadio8 = new JRadioButtonMenuItem("8");
		minutesRadio8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				minutesOnes=8;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio8);
		menu.add(minutesRadio8);
		
		JRadioButtonMenuItem minutesRadio9 = new JRadioButtonMenuItem("9");
		minutesRadio9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				minutesOnes=9;
				updateMinuteMenu();
			}
		});
		oneMinutes.add(minutesRadio9);
		menu.add(minutesRadio9);
		
		JMenu menu_1 = new JMenu("Tens");
		mnTimeLimit.add(menu_1);
		
		JRadioButtonMenuItem minutesRadio00 = new JRadioButtonMenuItem("00");
		minutesRadio00.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=0;
				updateMinuteMenu();
			}
		});
		minutesRadio00.setSelected(true);
		tensMinutes.add(minutesRadio00);
		menu_1.add(minutesRadio00);
		
		JRadioButtonMenuItem minutesRadio10 = new JRadioButtonMenuItem("10");
		minutesRadio10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=10;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio10);
		menu_1.add(minutesRadio10);
		
		JRadioButtonMenuItem minutesRadio20 = new JRadioButtonMenuItem("20");
		minutesRadio20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=20;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio20);
		menu_1.add(minutesRadio20);
		
		JRadioButtonMenuItem minutesRadio30 = new JRadioButtonMenuItem("30");
		minutesRadio30.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=30;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio30);
		menu_1.add(minutesRadio30);
		
		JRadioButtonMenuItem minutesRadio40 = new JRadioButtonMenuItem("40");
		minutesRadio40.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=40;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio40);
		menu_1.add(minutesRadio40);
		
		JRadioButtonMenuItem minutesRadio60 = new JRadioButtonMenuItem("60");
		minutesRadio60.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=60;
				updateMinuteMenu();
			}
		});
		
		JRadioButtonMenuItem minutesRadio50 = new JRadioButtonMenuItem("50");
		minutesRadio50.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=50;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio50);
		menu_1.add(minutesRadio50);
		tensMinutes.add(minutesRadio60);
		menu_1.add(minutesRadio60);
		
		JRadioButtonMenuItem minutesRadio70 = new JRadioButtonMenuItem("70");
		minutesRadio70.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=70;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio70);
		menu_1.add(minutesRadio70);
		
		JRadioButtonMenuItem minutesRadio80 = new JRadioButtonMenuItem("80");
		minutesRadio80.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=80;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio80);
		menu_1.add(minutesRadio80);
		
		JRadioButtonMenuItem minutesRadio90 = new JRadioButtonMenuItem("90");
		minutesRadio90.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minutesTens=90;
				updateMinuteMenu();
			}
		});
		tensMinutes.add(minutesRadio90);
		menu_1.add(minutesRadio90);
		contentPane = new JPanel();
		contentPane.setForeground(Color.GRAY);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(11, 11, 500, 500);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK,
				Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY));
		panel.setBackground(Color.LIGHT_GRAY);

		contentPane.add(panel);
		panel.setLayout(null);

		mazeBoard = new MazeDrawer(xOnes + xTens, yOnes + yTens);
		mazeBoard.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				mazeBoard.highlightSpace(arg0.getPoint());

			}
		});
		mazeBoard.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent arg0) {
				mazeBoard.cursorExited();
			}
		});
		mazeBoard.setBounds(2, 2, 500, 500);
		mazeBoard.setBackground(Color.WHITE);
		panel.add(mazeBoard);
		updateXMenu();
		updateYMenu();
	}

	protected void updateMinuteMenu() {
		// TODO Auto-generated method stub
		int currentTimeSetting= minutesOnes +minutesTens;
		String tempString="Time Limit: ";
		if (currentTimeSetting ==0){
			minutesOnes=5;
			minutesRadio5.setSelected(true);
			minutesRadio0.setEnabled(false);
			currentTimeSetting=minutesOnes+minutesTens;
		}
		if (currentTimeSetting < 10){
			minutesRadio0.setEnabled(false);
		}
		if (currentTimeSetting > 10)
			minutesRadio0.setEnabled(true);
		
		tempString+=currentTimeSetting + " min";
		if (currentTimeSetting > 1)
			tempString+="s";
		mnTimeLimit.setText(tempString);
	}

	protected void updateObservationMenu() {
		ObservationArea.setText("Observation Square per Area: "
				+ observationArea + "x" + observationArea);

	}

	protected void newMazeCreator() {
		algorithm = new MazeAlgorithm(xOnes + xTens, yOnes + yTens);
		mazeBoard.updateBoard(xOnes + xTens, yOnes + yTens);

		generateMaze();
	}

	private void generateMaze() {
		algorithm.placePlayerAndDoor();
		updateMazeDrawer();
		if (!modeSelected) {
			algorithm.generateMainPath();
			algorithm.generateSidePaths();
		} else {
			algorithm.generateMaze();
		}

		algorithm.placeKeyAndObservation(observationArea);
		updateMazeDrawer();

	}

	protected void updateMazeDrawer() {
		mazeBoard.updateMaze(algorithm.getMaze());

	}

	public void updateXMenu() {

		menuX.setText("X:" + (xTens + xOnes));
		mntmRegenerateMaze.setEnabled(false);
	}

	public void updateYMenu() {
		menuY.setText("Y: " + (yTens + yOnes));
		mntmRegenerateMaze.setEnabled(false);
	}

	protected JMenu getMenuX() {
		return menuX;
	}

	protected JMenu getMenuY() {
		return menuY;
	}

	protected MazeDrawer getMazeBoard() {
		return mazeBoard;
	}

	protected JMenuItem getMntmRegenerateMaze() {
		return mntmRegenerateMaze;
	}

	protected JMenuItem getMntmSaveMaze() {
		return mntmSaveMaze;
	}

	protected JMenu getObservationArea() {
		return ObservationArea;
	}
	protected JRadioButtonMenuItem getMinutesRadio5() {
		return minutesRadio5;
	}
	protected JRadioButtonMenuItem getMinutesRadio0() {
		return minutesRadio0;
	}
	protected JMenu getMnTimeLimit() {
		return mnTimeLimit;
	}
}
