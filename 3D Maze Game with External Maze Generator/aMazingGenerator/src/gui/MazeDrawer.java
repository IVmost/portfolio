package gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import algorithm.MazeSetting;

public class MazeDrawer extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int WIDTH;
	private int HEIGHT;
	private int cursorX;
	private int cursorY;
	private MazeSetting[][] maze;

	public MazeDrawer(int width, int height) {
		this.WIDTH = width;
		this.HEIGHT = height;
		cursorX = -1;
		cursorY = -1;
		maze=null;
	}

	public void paint(Graphics g) {
		int i;
		int curWidth = getSize().height;
		int curHeight = getSize().width;

		// draw the rows
		int rowHt = curHeight / (HEIGHT);
		int colWid = curWidth / (WIDTH);
		if (maze!= null)
			for (int x = 0; x < WIDTH; x++)
				for (int y = 0; y < HEIGHT; y++) {
					switch (maze[x][y]) {
					case WALL:
						g.setColor(Color.darkGray);
						break;
					case CONCRETEWALL:
						g.setColor(Color.black);
						break;
					case OBSERVATIONPOINT:
						g.setColor(Color.pink);
						break;
					case DOOR:
						g.setColor(Color.cyan);
						break;
					case KEY:
						g.setColor(Color.lightGray);
						break;
					case PATH:
						g.setColor(Color.blue);
						break;
					case PLAYERSTART:
						g.setColor(Color.orange);
						break;
					}
					g.fillRect(x * colWid, y * rowHt, colWid, rowHt);
				}
		g.setColor(Color.red);

		g.setColor(Color.BLUE);

		g.setColor(Color.white);
		for (i = 0; i <= HEIGHT; i++) {
			g.drawLine(0, i * rowHt, colWid * WIDTH, i * rowHt);
		}
		// draw the columns

		for (i = 0; i <= WIDTH; i++) {
			g.drawLine(i * colWid, 0, i * colWid, rowHt * HEIGHT);
		}
		g.setColor(Color.green);
		if (cursorX >= 0 && cursorY >= 0 && cursorX < WIDTH && cursorY < HEIGHT)
			g.fillRect(cursorX * colWid, cursorY * rowHt, colWid, rowHt);

	}

	public void updateBoard(int width, int height) {
		this.WIDTH = width;
		this.HEIGHT = height;
	}

	public void highlightSpace(Point tempPoint) {
		int tempCursorX = tempPoint.x / (getSize().width / WIDTH);
		int tempCursorY = tempPoint.y / (getSize().height / HEIGHT);
		if (tempCursorX != cursorX || tempCursorY != cursorY) {
			this.cursorX = tempCursorX;
			this.cursorY = tempCursorY;
			repaint();
		}

	}

	public void cursorExited() {
		this.cursorX = -1;
		this.cursorY = -1;
		repaint();
	}

	public void updateMaze(MazeSetting[][] maze) {
		this.maze = maze;
		repaint();
	}

}
