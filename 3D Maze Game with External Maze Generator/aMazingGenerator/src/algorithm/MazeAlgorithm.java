package algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Random;

public class MazeAlgorithm {
	MazeSetting[][] maze;
	ArrayList<MazeSpace> winningPath = new ArrayList<MazeSpace>();
	private final int WIDTH;
	private final int HEIGHT;
	private MazeSpace playerPosition;
	private MazeSpace doorPosition;
	@SuppressWarnings("unused")
	private MazeSpace keyPosition;

	public MazeAlgorithm(int xWidth, int yHeight) {
		maze = new MazeSetting[xWidth][yHeight];
		this.WIDTH = xWidth;
		this.HEIGHT = yHeight;
		for (int x = 0; x < WIDTH; x++)
			for (int y = 0; y < HEIGHT; y++) {
				if (x == 0 || x == (WIDTH - 1) || y == 0 || y == (HEIGHT - 1)) {
					maze[x][y] = MazeSetting.CONCRETEWALL;
				} else {
					maze[x][y] = MazeSetting.WALL;
				}
			}

	}

	public MazeSetting[][] getMaze() {
		return maze;
	}

	public void placePlayerAndDoor() {
		Random random = new Random();
		int doorSide = random.nextInt(4);
		int tempYDoor = random.nextInt(HEIGHT - 2) + 1;
		int tempXDoor = random.nextInt(WIDTH - 2) + 1;
		switch (doorSide) {
		case 0:
			tempYDoor = 0;
			break;
		case 1:
			tempYDoor = HEIGHT - 1;
			break;
		case 2:
			tempXDoor = 0;
			break;
		case 3:
			tempXDoor = WIDTH - 1;
			break;
		}
		maze[tempXDoor][tempYDoor] = MazeSetting.DOOR;
		doorPosition = new MazeSpace(tempXDoor, tempYDoor);

		int tempYPlayer = random.nextInt(HEIGHT - 2) + 1;
		int tempXPlayer = random.nextInt(WIDTH - 2) + 1;
		double tempCalculation = (Math.pow(tempXPlayer - tempXDoor, 2) + Math
				.pow(tempYPlayer - tempYDoor, 2))
				- (Math.pow(HEIGHT / (random.nextInt(4) + 2), 2) + Math.pow(
						WIDTH / (random.nextInt(4) + 2), 2));
		while (tempCalculation < 0) {
			tempYPlayer = random.nextInt(HEIGHT - 2) + 1;
			tempXPlayer = random.nextInt(WIDTH - 2) + 1;
			tempCalculation = (Math.pow(tempXPlayer - tempXDoor, 2) + Math.pow(
					tempYPlayer - tempYDoor, 2))
					- (Math.pow(HEIGHT / (random.nextInt(4) + 2), 2) + Math
							.pow(WIDTH / (random.nextInt(4) + 2), 2));
		}

		maze[tempXPlayer][tempYPlayer] = MazeSetting.PLAYERSTART;
		playerPosition = new MazeSpace(tempXPlayer, tempYPlayer);
	}

	public void generateMainPath() {
		int minimumDepth = Math.abs(doorPosition.X - playerPosition.X)
				+ Math.abs(doorPosition.Y - playerPosition.Y);
		Random random = new Random();
		int randomDepth = (((HEIGHT - 2) * (WIDTH - 2)) / 2)
				- (random.nextInt((((HEIGHT - 2) * (WIDTH - 2)) / 2)
						- minimumDepth));
		winningPath = depthFirstGenerateMaze(playerPosition.X,
				playerPosition.Y, randomDepth, 1);
		for (int x = 1; x < WIDTH - 1; x++)
			for (int y = 1; y < HEIGHT - 1; y++)
				if (maze[x][y] == MazeSetting.CONCRETEWALL)
					maze[x][y] = MazeSetting.WALL;
		if (winningPath == null)
			generateMainPath();
	}

	private ArrayList<MazeSpace> depthFirstGenerateMaze(int x, int y,
			int maxDepth, int currentDepth) {
		if (currentDepth > maxDepth)
			return null;
		ArrayList<MazeSpace> tempArray = new ArrayList<MazeSpace>();
		int pathCount = 0;
		for (int xAdjust = -1; xAdjust <= 1; xAdjust++) {
			if (xAdjust == 0) {
				for (int yAdjust = -1; yAdjust <= 1; yAdjust++) {
					if (yAdjust != 0) {
						if (maze[x][y + yAdjust] == MazeSetting.DOOR) {
							ArrayList<MazeSpace> tempReturn = new ArrayList<MazeSpace>();
							maze[x][y] = MazeSetting.PATH;
							tempReturn.add((new MazeSpace(x, y)));
							return tempReturn;
						} else if (maze[x][y + yAdjust] == MazeSetting.WALL) {
							tempArray.add(new MazeSpace(x, y + yAdjust));
							maze[x][y + yAdjust] = MazeSetting.CONCRETEWALL;
						} else if (maze[x][y + yAdjust] == MazeSetting.PATH
								|| maze[x][y + yAdjust] == MazeSetting.PLAYERSTART) {
							pathCount++;
						}
					}
				}
			} else {
				if (maze[x + xAdjust][y] == MazeSetting.DOOR) {
					ArrayList<MazeSpace> tempReturn = new ArrayList<MazeSpace>();
					maze[x][y] = MazeSetting.PATH;
					tempReturn.add((new MazeSpace(x, y)));
					return tempReturn;

				} else if (maze[x + xAdjust][y] == MazeSetting.WALL) {
					tempArray.add(new MazeSpace(x + xAdjust, y));
					maze[x + xAdjust][y] = MazeSetting.CONCRETEWALL;
				} else if (maze[x + xAdjust][y] == MazeSetting.PATH
						|| maze[x + xAdjust][y] == MazeSetting.PLAYERSTART) {
					pathCount++;
				}
			}
		}
		if (pathCount > 1 || tempArray.isEmpty())
			return null;

		ArrayList<MazeSpace> randomList = new ArrayList<MazeSpace>();
		Random random = new Random();
		while (!tempArray.isEmpty()) {
			randomList.add(tempArray.remove(random.nextInt(tempArray.size())));
		}
		for (MazeSpace temp : randomList) {
			maze[temp.X][temp.Y] = MazeSetting.PATH;
			tempArray = depthFirstGenerateMaze(temp.X, temp.Y, maxDepth,
					currentDepth + 1);
			if (tempArray != null) {
				tempArray.add(new MazeSpace(x, y));
				return tempArray;

			} else {
				maze[temp.X][temp.Y] = MazeSetting.WALL;
			}
		}
		return null;
	}

	public void generateSidePaths() {

		maze[playerPosition.X][playerPosition.Y] = MazeSetting.PATH;

		for (MazeSpace temp : winningPath) {
			recurseSidePath(temp.X, temp.Y);
		}
		maze[playerPosition.X][playerPosition.Y] = MazeSetting.PLAYERSTART;

	}

	private void recurseSidePath(int x, int y) {
		ArrayList<MazeSpace> tempList = new ArrayList<MazeSpace>();
		if (maze[x - 1][y] == MazeSetting.PATH
				&& maze[x - 1][y - 1] == MazeSetting.PATH
				&& maze[x][y - 1] == MazeSetting.PATH)
			return;
		if (maze[x - 1][y] == MazeSetting.PATH
				&& maze[x - 1][y + 1] == MazeSetting.PATH
				&& maze[x][y + 1] == MazeSetting.PATH)
			return;
		if (maze[x + 1][y] == MazeSetting.PATH
				&& maze[x + 1][y - 1] == MazeSetting.PATH
				&& maze[x][y - 1] == MazeSetting.PATH)
			return;
		if (maze[x + 1][y] == MazeSetting.PATH
				&& maze[x + 1][y + 1] == MazeSetting.PATH
				&& maze[x][y + 1] == MazeSetting.PATH)
			return;

		if (maze[x][y] != MazeSetting.PLAYERSTART)
			maze[x][y] = MazeSetting.PATH;
		int pathCount = 0;
		for (int xAdjust = -1; xAdjust <= 1; xAdjust++) {
			if (xAdjust == 0)
				for (int yAdjust = -1; yAdjust <= 1; yAdjust++) {
					if (yAdjust != 0)
						if (maze[x][y + yAdjust] == MazeSetting.WALL) {
							tempList.add(new MazeSpace(x, y + yAdjust));
						}
					if (maze[x][y + yAdjust] == MazeSetting.PATH) {
						pathCount++;
					}
				}
			else {
				if (maze[x + xAdjust][y] == MazeSetting.WALL) {
					tempList.add(new MazeSpace(x + xAdjust, y));
				}
				if (maze[x + xAdjust][y] == MazeSetting.PATH)
					pathCount++;
			}

		}
		Random random = new Random();
		if (pathCount > 2)
			return;
		for (MazeSpace temp : tempList) {
			if (random.nextInt(7) > 0) {
				recurseSidePath(temp.X, temp.Y);
			}
		}

	}

	public void placeKeyAndObservation(int observationArea) {
		Random random= new Random();
		
		int tempYKey = random.nextInt(HEIGHT - 2) + 1;
		int tempXKey = random.nextInt(WIDTH - 2) + 1;
		double tempCalculation = (Math.pow(tempXKey-doorPosition.X-playerPosition.X, 2) + Math
				.pow(tempYKey - doorPosition.Y-playerPosition.Y, 2))
				- (Math.pow(HEIGHT / (random.nextInt(4) + 2), 2) + Math.pow(
						WIDTH / (random.nextInt(4) + 2), 2));
		while (tempCalculation < 0 || maze[tempXKey][tempYKey] !=MazeSetting.PATH) {
			tempYKey = random.nextInt(HEIGHT - 2) + 1;
			tempXKey = random.nextInt(WIDTH - 2) + 1;
			tempCalculation = (Math.pow(tempXKey - doorPosition.X- playerPosition.X, 2) + Math.pow(
					tempYKey-doorPosition.Y-playerPosition.Y, 2))
					- (Math.pow(HEIGHT / (random.nextInt(4) + 2), 2) + Math
							.pow(WIDTH / (random.nextInt(4) + 2), 2));
		}

		maze[tempXKey][tempYKey] = MazeSetting.KEY;
		keyPosition = new MazeSpace(tempXKey, tempYKey);
		
		Hashtable<String, ArrayList<MazeSpace>> observationTable= new Hashtable<String, ArrayList<MazeSpace>>();
		for (int x=1; x<WIDTH-1; x++){
			for (int y=1; y<HEIGHT-1;y++){
				if (!observationTable.containsKey((x-1)/observationArea+" "+(y-1)/observationArea)){
					observationTable.put((x-1)/observationArea + " " + (y-1)/observationArea, new ArrayList<MazeSpace>());
				}
				if (maze[x][y] == MazeSetting.PATH)
					observationTable.get((x-1)/observationArea + " " + (y-1)/observationArea).add(new MazeSpace(x, y));
			}
		}
	
		Collection<ArrayList<MazeSpace>> pathLists = observationTable.values();
		for (ArrayList<MazeSpace> tempList : pathLists){
			if (!tempList.isEmpty()){
				MazeSpace tempSpace= tempList.get(random.nextInt(tempList.size()));
				maze[tempSpace.X][tempSpace.Y]= MazeSetting.OBSERVATIONPOINT;
			}
		}

	}

	public void generateMaze() {
		for (int x = 1; x < WIDTH - 1; x++)
			for (int y = 1; y < HEIGHT - 1; y++)
				maze[x][y] = MazeSetting.PATH;
		divideAndConqure(0, 0, WIDTH - 1, HEIGHT - 1, new MazeSpace(-1, -1));
		maze[playerPosition.X][playerPosition.Y]=MazeSetting.PLAYERSTART;
	}

	private void divideAndConqure(int xLow, int yLow, int xHigh, int yHigh,
			MazeSpace path) {
		Random random = new Random();
		int vertWall = -1;
		int yPathLow = -1;
		int yPathHigh = -1;
		int xPathLow = -1;
		int xPathHigh = -1;
		
		for (int y = yLow; y < yHigh; y++) {
			if (maze[xLow][y] == MazeSetting.PATH || maze[xLow][y] == MazeSetting.DOOR)
				yPathLow = y;
			if (maze[xHigh][y] == MazeSetting.PATH || maze[xHigh][y] == MazeSetting.DOOR)
				yPathHigh = y;
		}
		for (int x = xLow; x < xHigh; x++) {
			if (maze[x][yLow] == MazeSetting.PATH || maze[x][yLow] == MazeSetting.DOOR)
				xPathLow = x;
			if (maze[x][yHigh] == MazeSetting.PATH || maze [x][yHigh] == MazeSetting.DOOR)
				xPathHigh = x;
		}

		if (xHigh - xLow >= 4
				&& !(xHigh - xLow == 4 && (doorPosition.X == xLow + 2
						|| xPathLow == xLow + 2 || xPathHigh == xLow + 2))) {
			vertWall = random.nextInt(xHigh - xLow - 1) + xLow + 1;
			while (vertWall <= xLow + 1 || vertWall >= xHigh - 1) {
				vertWall = random.nextInt(xHigh - xLow - 1) + xLow + 1;
			}
		}
		int horzWall = -1;
		if (yHigh - yLow >= 4
				&& !(yHigh - yLow == 4 && (doorPosition.Y == yLow + 2
						|| yPathLow == yLow + 2 || yPathHigh == yLow + 2))) {
			horzWall = random.nextInt(yHigh - yLow - 1) + yLow + 1;
			while (horzWall <= yLow + 1 || horzWall >= yHigh - 1) {
				horzWall = random.nextInt(yHigh - yLow - 1) + yLow + 1;
			}
		}

		if (vertWall != -1 && horzWall != -1) {
			int tempYHigh=yHigh;
			int tempXHigh=xHigh;
			int tempYLow=yLow;
			int tempXLow=xLow;
			if (vertWall == xPathLow )
				tempYLow++;
			if ( vertWall == xPathHigh)
				tempYHigh--;
			
			if (horzWall == yPathLow)
				tempXLow++;
			if ( horzWall == yPathHigh)
				tempXHigh--;
			
			for (int y = tempYLow + 1; y < tempYHigh; y++)
				maze[vertWall][y] = MazeSetting.WALL;
			for (int x = tempXLow + 1; x < tempXHigh; x++)
				maze[x][horzWall] = MazeSetting.WALL;

			if (random.nextBoolean()) {
				int pathY1 = random.nextInt(horzWall - yLow - 1) + yLow + 1;
				maze[vertWall][pathY1] = MazeSetting.PATH;
				int pathY2 = random.nextInt(yHigh - horzWall - 1) + horzWall
						+ 1;
				maze[vertWall][pathY2] = MazeSetting.PATH;
				int pathX = random.nextInt(xHigh - xLow - 1) + xLow + 1;
				while (pathX == vertWall) {
					pathX = random.nextInt(xHigh - xLow - 1) + xLow + 1;
				}
				maze[pathX][horzWall] = MazeSetting.PATH;
				divideAndConqure(xLow, yLow, vertWall, horzWall, new MazeSpace(
						pathX, pathY1));
				divideAndConqure(vertWall, yLow, xHigh, horzWall,
						new MazeSpace(pathX, pathY1));
				divideAndConqure(xLow, horzWall, vertWall, yHigh,
						new MazeSpace(pathX, pathY2));
				divideAndConqure(vertWall, horzWall, xHigh, yHigh,
						new MazeSpace(pathX, pathY2));
			} else {
				int pathX1 = random.nextInt(vertWall - xLow - 1) + xLow + 1;
				maze[pathX1][horzWall] = MazeSetting.PATH;
				int pathX2 = random.nextInt(xHigh - vertWall - 1) + vertWall
						+ 1;
				maze[pathX2][horzWall] = MazeSetting.PATH;
				int pathY = random.nextInt(yHigh - yLow - 1) + yLow + 1;
				while (pathY == horzWall) {
					pathY = random.nextInt(yHigh - yLow - 1) + yLow + 1;
				}
				maze[vertWall][pathY] = MazeSetting.PATH;
				divideAndConqure(xLow, yLow, vertWall, horzWall, new MazeSpace(
						pathX1, pathY));
				divideAndConqure(vertWall, yLow, xHigh, horzWall,
						new MazeSpace(pathX2, pathY));
				divideAndConqure(xLow, horzWall, vertWall, yHigh,
						new MazeSpace(pathX1, pathY));
				divideAndConqure(vertWall, horzWall, xHigh, yHigh,
						new MazeSpace(pathX2, pathY));
			}
		} else {
			if (vertWall != -1) {
				int tempYHigh=yHigh;
				int tempYLow=yLow;
				if (vertWall == xPathLow )
					tempYLow++;
				if ( vertWall == xPathHigh)
					tempYHigh--;
				
				
				for (int y = tempYLow + 1; y < tempYHigh; y++) {
					maze[vertWall][y] = MazeSetting.WALL;
				}
				int pathY = random.nextInt(yHigh - yLow - 1) + yLow + 1;
				maze[vertWall][pathY] = MazeSetting.PATH;
				divideAndConqure(xLow, yLow, vertWall, yHigh, new MazeSpace(-1,
						pathY));
				divideAndConqure(vertWall, yLow, xHigh, yHigh, new MazeSpace(
						-1, pathY));
			} else if (horzWall != -1) {
				int tempXHigh=xHigh;
				int tempXLow=xLow;
				
				if (horzWall == yPathLow)
					tempXLow++;
				if ( horzWall == yPathHigh)
					tempXHigh--;
				
				for (int x = tempXLow + 1; x < tempXHigh; x++) {
					maze[x][horzWall] = MazeSetting.WALL;
				}
				int pathX = random.nextInt(xHigh - xLow - 1) + xLow + 1;
				maze[pathX][horzWall] = MazeSetting.PATH;
				divideAndConqure(xLow, yLow, xHigh, horzWall, new MazeSpace(
						pathX, -1));
				divideAndConqure(xLow, horzWall, xHigh, yHigh, new MazeSpace(
						pathX, -1));
			}
		}

	}

	@SuppressWarnings("unused")
	private void printMaze() {
		for (int x = 0; x < WIDTH; x++) {
			System.out.print("\n");
			for (int y = 0; y < HEIGHT; y++) {
				switch (maze[x][y]) {
				case CONCRETEWALL:
				case WALL:
					System.out.print("W");
					break;
				case PATH:
					System.out.print(".");
					break;
				case DOOR:
					System.out.print("-");
					break;
				}
			}
		}
		System.out.println("\n\n");

	}

	public void restart() {

		for (int x = 0; x < WIDTH; x++)
			for (int y = 0; y < HEIGHT; y++) {
				if (x == 0 || x == (WIDTH - 1) || y == 0 || y == (HEIGHT - 1)) {
					maze[x][y] = MazeSetting.CONCRETEWALL;
				} else {
					maze[x][y] = MazeSetting.WALL;
				}
			}
		playerPosition=null;
		winningPath= new ArrayList<MazeSpace>();
		doorPosition=null;
		keyPosition=null;
		
	}

	public String createfile(int timeLimit) {
		String tempOutput = HEIGHT + ":" + WIDTH + ";\n";
		tempOutput +="time:" + timeLimit +";\n";
		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				switch (maze[x][y]) {
				case CONCRETEWALL:
				case WALL:
					tempOutput+="-";
					break;
				case PATH:
					tempOutput+="p";
					break;
				case DOOR:
					tempOutput+="D";
					break;
				case KEY:
					tempOutput+="K";
					break;
				case OBSERVATIONPOINT:
					tempOutput+="o";
					break;
				case PLAYERSTART:
					tempOutput+="S";
					break;
				}
			}
			tempOutput+=";\n";
		}
		return tempOutput;
	}
}
